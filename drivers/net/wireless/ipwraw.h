/******************************************************************************

  Copyright(c) 2003 - 2006 Intel Corporation. All rights reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along with
  this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110, USA

  The full GNU General Public License is included in this distribution in the
  file called LICENSE.

  Contact Information:
  James P. Ketrenos <ipw2100-admin@linux.intel.com>
  Intel Corporation, 5200 N.E. Elam Young Parkway, Hillsboro, OR 97124-6497

******************************************************************************/

#ifndef __ipwraw_h__
#define __ipwraw_h__

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/autoconf.h>
#include <linux/init.h>

#include <linux/version.h>
#include <linux/pci.h>
#include <linux/netdevice.h>
#include <linux/ethtool.h>
#include <linux/skbuff.h>
#include <linux/etherdevice.h>
#include <linux/delay.h>
#include <linux/random.h>

#include <linux/firmware.h>
#include <linux/wireless.h>
#include <asm/io.h>
#include <linux/workqueue.h>

#include <net/ieee80211.h>

#include "iwlwifi_hw.h"

#define DRV_NAME	"ipwraw"

#define ALM_APMG_RFKILL                                 (0x003014)
#ifndef IRQF_SHARED
#define IRQF_SHARED SA_SHIRQ
#endif

#ifndef SET_MODULE_OWNER
#define SET_MODULE_OWNER(dev) do { } while (0)
#endif
#ifndef MAC_FMT
#define MAC_FMT "%02x:%02x:%02x:%02x:%02x:%02x"
#endif
#ifndef MAC_ARG
#define MAC_ARG(x) ((u8*)(x))[0],((u8*)(x))[1],((u8*)(x))[2],((u8*)(x))[3],((u8*)(x))[4],((u8*)(x))[5]
#endif



/* Debug and printf string expansion helpers for printing bitfields */
#define BIT_FMT8 "%c%c%c%c-%c%c%c%c"
#define BIT_FMT16 BIT_FMT8 ":" BIT_FMT8
#define BIT_FMT32 BIT_FMT16 " " BIT_FMT16

#define BITC(x,y) (((x>>y)&1)?'1':'0')
#define BIT_ARG8(x) \
BITC(x,7),BITC(x,6),BITC(x,5),BITC(x,4),\
BITC(x,3),BITC(x,2),BITC(x,1),BITC(x,0)

#define BIT_ARG16(x) \
BITC(x,15),BITC(x,14),BITC(x,13),BITC(x,12),\
BITC(x,11),BITC(x,10),BITC(x,9),BITC(x,8),\
BIT_ARG8(x)

#define BIT_ARG32(x) \
BITC(x,31),BITC(x,30),BITC(x,29),BITC(x,28),\
BITC(x,27),BITC(x,26),BITC(x,25),BITC(x,24),\
BITC(x,23),BITC(x,22),BITC(x,21),BITC(x,20),\
BITC(x,19),BITC(x,18),BITC(x,17),BITC(x,16),\
BIT_ARG16(x)

/* ipwraw does not expose the build option to build w/out debugging... */
#define CONFIG_IPWRAW_DEBUG y
#ifdef CONFIG_IPWRAW_DEBUG
#define IPW_DEBUG(level, fmt, args...) \
do { if (ipw_debug_level & (level)) \
  printk(KERN_ERR DRV_NAME": %c %s " fmt, \
         in_interrupt() ? 'I' : 'U', __FUNCTION__ , ## args); } while (0)
#else
#define IPW_DEBUG(level, fmt, args...) do {} while (0)
#endif				/* CONFIG_IPWRAW_DEBUG */

#define DEFAULT_RTS_THRESHOLD     2347U
#define MIN_RTS_THRESHOLD         0U
#define MAX_RTS_THRESHOLD         2347U
#define MAX_MSDU_SIZE            2304U
#define MAX_MPDU_SIZE            2346U
#define DEFAULT_BEACON_INTERVAL   100U
#define        DEFAULT_SHORT_RETRY_LIMIT 7U
#define        DEFAULT_LONG_RETRY_LIMIT  4U
#define IEEE80211_ERP_PRESENT                  (0x01)
#define IEEE80211_ERP_USE_PROTECTION           (0x02)
#define IEEE80211_ERP_BARKER_PREAMBLE_MODE     (0x04)

/*
 * To use the debug system;
 *
 * If you are defining a new debug classification, simply add it to the #define
 * list here in the form of:
 *
 * #define IPW_DL_xxxx VALUE
 *
 * shifting value to the left one bit from the previous entry.  xxxx should be
 * the name of the classification (for example, WEP)
 *
 * You then need to either add a IPW_xxxx_DEBUG() macro definition for your
 * classification, or use IPW_DEBUG(IPW_DL_xxxx, ...) whenever you want
 * to send output to that classification.
 *
 * To add your debug level to the list of levels seen when you perform
 *
 * % cat /proc/net/ipw/debug_level
 *
 * you simply need to add your entry to the ipw_debug_levels array.
 *
 * If you do not see debug_level in /proc/net/ipw then you do not have
 * CONFIG_IPWRAW_DEBUG defined in your kernel configuration
 *
 */

#define IPW_DL_INFO          (1<<0)
#define IPW_DL_HOST_COMMAND  (1<<1)

#define IPW_DL_RADIO         (1<<4)
#define IPW_DL_POWER         (1<<5)

#define IPW_DL_NOTIF         (1<<8)
#define IPW_DL_DROP          (1<<9)

#define IPW_DL_FW            (1<<12)
#define IPW_DL_RF_KILL       (1<<13)
#define IPW_DL_FW_ERRORS     (1<<14)

#define IPW_DL_LED           (1<<16)

#define IPW_DL_TX            (1<<20)
#define IPW_DL_RX            (1<<21)
#define IPW_DL_ISR           (1<<22)
#define IPW_DL_IO            (1<<23)

#define IPW_ERROR(f, a...) printk(KERN_ERR DRV_NAME ": " f, ## a)
#define IPW_WARNING(f, a...) printk(KERN_ERR DRV_NAME ": " f, ## a)

#define IPW_DEBUG_INFO(f, a...)    IPW_DEBUG(IPW_DL_INFO, f, ## a)
#define IPW_DEBUG_HC(f, a...) IPW_DEBUG(IPW_DL_HOST_COMMAND, f, ## a)

#define IPW_DEBUG_RADIO(f, a...)  IPW_DEBUG(IPW_DL_RADIO, f, ## a)
#define IPW_DEBUG_POWER(f, a...)  IPW_DEBUG(IPW_DL_POWER, f, ## a)

#define IPW_DEBUG_NOTIF(f, a...) IPW_DEBUG(IPW_DL_NOTIF, f, ## a)
#define IPW_DEBUG_DROP(f, a...) IPW_DEBUG(IPW_DL_DROP, f, ## a)

#define IPW_DEBUG_FW(f, a...) IPW_DEBUG(IPW_DL_FW, f, ## a)
#define IPW_DEBUG_RF_KILL(f, a...) IPW_DEBUG(IPW_DL_RF_KILL, f, ## a)

#define IPW_DEBUG_LED(f, a...) IPW_DEBUG(IPW_DL_LED, f, ## a)

#define IPW_DEBUG_TX(f, a...)     IPW_DEBUG(IPW_DL_TX, f, ## a)
#define IPW_DEBUG_RX(f, a...)     IPW_DEBUG(IPW_DL_RX, f, ## a)
#define IPW_DEBUG_ISR(f, a...)    IPW_DEBUG(IPW_DL_ISR, f, ## a)
#define IPW_DEBUG_IO(f, a...) IPW_DEBUG(IPW_DL_IO, f, ## a)

/* Amount of time after sending an association request before we assume
 * the request has failed in ms (default 750ms)*/
#define ASSOCIATE_TIMEOUT 750

#define IPW_MIN_CHANNEL_24  1
#define IPW_MAX_CHANNEL_24  14
#define IPW_MIN_CHANNEL_52  34
#define IPW_MAX_CHANNEL_52  165
#define IPW_MAX_CHANNEL_MIX (IPW_MAX_CHANNEL_24 + IPW_MAX_CHANNEL_52)

/* Authentication  and Association States */
enum connection_manager_assoc_states {
	CMAS_INIT = 0,
	CMAS_TX_AUTH_SEQ_1,
	CMAS_RX_AUTH_SEQ_2,
	CMAS_AUTH_SEQ_1_PASS,
	CMAS_AUTH_SEQ_1_FAIL,
	CMAS_TX_AUTH_SEQ_3,
	CMAS_RX_AUTH_SEQ_4,
	CMAS_AUTH_SEQ_2_PASS,
	CMAS_AUTH_SEQ_2_FAIL,
	CMAS_AUTHENTICATED,
	CMAS_TX_ASSOC,
	CMAS_RX_ASSOC_RESP,
	CMAS_ASSOCIATED,
	CMAS_LAST
};

#define IPW_POWER_MODE_CAM           0x00	//(always on)
#define IPW_POWER_INDEX_3            0x03
#define IPW_POWER_INDEX_5            0x05
#define IPW_POWER_AC                 0x06
#define IPW_POWER_BATTERY            0x07
#define IPW_POWER_LIMIT              0x07
#define IPW_POWER_MASK               0x0F
#define IPW_POWER_ENABLED            0x10
#define IPW_POWER_LEVEL(x)           ((x) & IPW_POWER_MASK)

#define RFD_SIZE                              4
#define NUM_TFD_CHUNKS                        4

#define RX_QUEUE_SIZE                        64
#define RX_QUEUE_SIZE_LOG                     6

#define IPW_A_MODE                         0
#define IPW_B_MODE                         1
#define IPW_G_MODE                         2

enum {
	/* CMD_SIZE_NORMAL = 0, */
	CMD_SIZE_HUGE = (1 << 0),
	/* CMD_SYNC = 0, */
	CMD_ASYNC = (1 << 1),
	/* CMD_NO_SKB = 0, */
	CMD_WANT_SKB = (1 << 2),
	/* CMD_LOCK = 0, */
	CMD_NO_LOCK = (1 << 3),
};

struct ipw_cmd;
struct ipw_priv;

typedef int (*IPW_CALLBACK_FUNC) (struct ipw_priv * priv,
				  struct ipw_cmd * cmd, struct sk_buff * skb);

#define CMD_VAR_MAGIC 0xA987
struct ipw_cmd_meta {
	struct ipw_cmd_meta *source;
	union {
		struct sk_buff *skb;
		IPW_CALLBACK_FUNC callback;
	} __attribute__ ((packed)) u;

	u16 len;

	/* The CMD_SIZE_HUGE flag bit indicates that the command
	 * structure is stored at the end of the shared queue memory. */
	u8 flags;

	u8 token;
	u16 magic;
} __attribute__ ((packed));

struct ipw_cmd {
	struct ipw_cmd_meta meta;

	/* From here to the end of cmd is uCode interface specific */
	struct ipw_cmd_header hdr;
	union {
		struct ipw_addsta_cmd addsta;
		struct ipw_led_cmd led;
		u32 flags;
		u8 val8;
		u16 val16;
		u32 val32;
		struct ipw_bt_cmd bt;
		struct ipw_rxon_time_cmd rxon_time;
		struct ipw_powertable_cmd powertable;
		struct ipw_tx_cmd tx;
		struct ipw_key_cmd key;
		struct ipw_tx_beacon_cmd tx_beacon;
		struct ipw_rxon_assoc_cmd rxon_assoc;
		struct RateScalingCmdSpecifics rate_scale;
		u8 *indirect;
		u8 payload[360];
	} __attribute__ ((packed)) cmd;
} __attribute__ ((packed));

struct ipw_host_cmd {
	u8 id;
	u16 len;
	struct ipw_cmd_meta meta;
	void *data;
};

#define TFD_MAX_PAYLOAD_SIZE (sizeof(struct ipw_cmd) - \
                              sizeof(struct ipw_cmd_meta))

/**
 * Tx Queue for DMA. Queue consists of circular buffer of
 * BD's and required locking structures.
 */
struct ipw_tx_queue {
	struct ipw_queue q;
	struct tfd_frame *bd;
	struct ipw_cmd *cmd;
	dma_addr_t dma_addr_cmd;
	struct sk_buff **skb;
	int need_update;	/* flag to indicate we need to update read/write index */
};

struct ipw_rx_mem_buffer {
	dma_addr_t dma_addr;
	struct sk_buff *skb;
	struct list_head list;
};				/* Not transferred over network, so not  __attribute__ ((packed)) */

struct ipw_rx_queue {
	void *bd;
	dma_addr_t dma_addr;
	struct ipw_rx_mem_buffer pool[RX_QUEUE_SIZE + RX_FREE_BUFFERS];
	struct ipw_rx_mem_buffer *queue[RX_QUEUE_SIZE];
	u32 processed;		/* Internal index to last handled Rx packet */
	u32 read;		/* Shared index to newest available Rx buffer */
	u32 write;		/* Shared index to oldest written Rx packet */
	u32 free_count;		/* Number of pre-allocated buffers in rx_free */
	/* Each of these lists is used as a FIFO for ipw_rx_mem_buffers */
	struct list_head rx_free;	/* Own an SKBs */
	struct list_head rx_used;	/* No SKB allocated */
	int need_update;	/* flag to indicate we need to update read/write index */
	spinlock_t lock;
};				/* Not transferred over network, so not  __attribute__ ((packed)) */

#define SCAN_INTERVAL 100

#define MAX_A_CHANNELS  252
#define MIN_A_CHANNELS  7

#define MAX_B_CHANNELS  14
#define MIN_B_CHANNELS  1

#define STATUS_HCMD_ACTIVE      (1<<0)	/**< host command in progress */

#define STATUS_INT_ENABLED      (1<<1)
#define STATUS_RF_KILL_HW       (1<<2)
#define STATUS_RF_KILL_SW       (1<<3)
#define STATUS_RF_KILL_MASK     (STATUS_RF_KILL_HW | STATUS_RF_KILL_SW)

#define STATUS_INIT             (1<<4)
#define STATUS_ALIVE            (1<<5)
#define STATUS_READY            (1<<6)

#define STATUS_EXIT_PENDING     (1<<9)
#define STATUS_IN_SUSPEND       (1<<10)

#define STATUS_POWER_PMI        (1<<24)
#define STATUS_RESTRICTED       (1<<26)
#define STATUS_FW_ERROR         (1<<27)

#define STATUS_SECURITY_UPDATED (1<<31)	/* Security sync needed */

#define CFG_STATIC_BSSID        (1<<2)	/* Restrict assoc. to single BSSID */
#define CFG_CUSTOM_MAC          (1<<3)
#define CFG_PREAMBLE_LONG       (1<<4)
#define CFG_ADHOC_PERSIST       (1<<5)
#define CFG_NO_LED              (1<<9)
#define CFG_TXPOWER_LIMIT       (1<<11)

//todoG need to support adding adhoc station MAX_STATION should be 25
#define IPW_INVALID_STATION     (0xff)

#define MAX_TID_COUNT           6

struct ipw_tid_data {
	u16 seq_number;
};

struct ipw_station_entry {
	struct ipw_addsta_cmd sta;
	struct ipw_tid_data tid[MAX_TID_COUNT];
	u8 current_rate;
	u8 used;
};

struct ipw_rate_info {
	u8 rate_plcp;
	u8 rate_ieee;
	s32 rate_scale_index;
	u32 bps;		/* Bits per symbol, only OFDM */
	u32 dur_ack;
	u32 dur_rts;
	u32 dur_cts;
	u32 dur_back;
};

#define AVG_ENTRIES 8
struct average {
	long entries[AVG_ENTRIES];
	long sum;
	u8 pos;
	u8 init;
};

#define IPW_LED_ACTIVITY                (1<<0)
#define IPW_LED_LINK                    (1<<1)

struct ipw_led {
	u8 time_on;		/* ON time in interval units - 0 == OFF */
	u8 time_off;		/* OFF time in interval units - 0 == always ON if
				 * time_on != 0 */
};

struct ipw_led_info {
	u32 interval;		/* uSec length of "interval" */
	struct ipw_led activity;
	struct ipw_led link;
	struct ipw_led tech;
};

struct ipw_shared_t {
	volatile u32 tx_base_ptr[8];
	volatile u32 rx_read_ptr[3];
};

struct fw_image_desc {
	void *v_addr; /* access by driver */
	dma_addr_t p_addr; /* access by card's busmaster DMA */
	u32 len; /* bytes */
};

/* uCode file layout */
struct ipw_ucode {
	u32 ver;		/* major/minor/subminor */
	u32 inst_size;		/* bytes of runtime instructions */
	u32 data_size;		/* bytes of runtime data */
	u32 init_size;		/* bytes of initialization instructions */
	u32 init_data_size;	/* bytes of initialization data */
	u32 boot_size;		/* bytes of bootstrap instructions */
	u8 data[0];		/* data in same order as "size" elements */
};

struct ipw_tpt_entry {
	s32 min_rssi;
	u32 no_protection_tpt;
	u32 cts_rts_tpt;
	u32 cts_to_self_tpt;
	s32 rate_scale_index;
};

struct ipw_rate_scale_data {
	u64 data;
	s32 success_counter;
	s32 success_ratio;
	s32 counter;
	s32 average_tpt;
	unsigned long stamp;
};

struct ipw_rate_scale_mgr {
	spinlock_t lock;
	struct ipw_rate_scale_data window[IPW_MAX_RATES];
	s32 max_window_size;
	struct RateScalingCmdSpecifics scale_rate_cmd;
	s32 *expected_tpt;
	u8 *next_higher_rate;
	u8 *next_lower_rate;
	unsigned long stamp;
	unsigned long stamp_last;
	u32 flush_time;
	u32 tx_packets;
};

/* Power management (not Tx power) structures */

struct ipw_power_vec_entry {
	struct ipw_powertable_cmd cmd;
	u8 no_dtim;
};
#define IPW_POWER_RANGE_0  (0)
#define IPW_POWER_RANGE_1  (1)

struct ipw_power_mgr {
	spinlock_t lock;
	struct ipw_power_vec_entry pwr_range_0[IPW_POWER_AC];
	struct ipw_power_vec_entry pwr_range_1[IPW_POWER_AC];
	u8 active_index;
	u32 dtim_val;
};

/* The LED interval is expressed in uSec and is the time
 * unit by which all other LED command are represented
 *
 * A value of '1000' for makes each unit 1ms.
 */

#define IPW_LED_INTERVAL 1000

#define DEFAULT_POWER_SAVE_ON       LED_SOLID_ON
#define DEFAULT_POWER_SAVE_OFF      0
#define DEFAULT_POWER_SAVE_INTERVAL 1000

struct ipw_activity_blink {
	u16 throughput;		/* threshold in Mbs */
	u8 off;			/* OFF time in interval units - 0 == always ON if
				 * time_on != 0 */
	u8 on;			/* ON time in interval units - 0 == OFF */
};

enum {
	IPW_LED_LINK_UNINIT = 0,
	IPW_LED_LINK_RADIOOFF,
	IPW_LED_LINK_UNASSOCIATED,
	IPW_LED_LINK_SCANNING,
	IPW_LED_LINK_ASSOCIATED,
	IPW_LED_LINK_ROAMING,
};

struct ipw_link_blink {
	u16 interval;		/* Number of interval units per second */
	u8 off;			/* OFF time in interval units - 0 == always ON if
				 * time_on != 0 */
	u8 on;			/* ON time in interval units - 0 == OFF */
};

struct ipw_frame {
	union {
		struct ieee80211_hdr frame;
		u8 raw[IEEE80211_FRAME_LEN];
		u8 cmd[360];
	} u;
	struct list_head list;
};

enum ipw_prom_filter {
	IPW_PROM_CTL_HEADER_ONLY = (1 << 0),
	IPW_PROM_MGMT_HEADER_ONLY = (1 << 1),
	IPW_PROM_DATA_HEADER_ONLY = (1 << 2),
	IPW_PROM_ALL_HEADER_ONLY = 0xf,	/* bits 0..3 */
	IPW_PROM_NO_TX = (1 << 4),
	IPW_PROM_NO_RX = (1 << 5),
	IPW_PROM_NO_CTL = (1 << 6),
	IPW_PROM_NO_MGMT = (1 << 7),
	IPW_PROM_NO_DATA = (1 << 8),
};

struct ipw_priv;
struct ipw_prom_priv {
	struct ipw_priv *priv;
	struct net_device_stats stats;
	enum ipw_prom_filter filter;
	int tx_packets;
	int rx_packets;
};

/* Magic struct that slots into the radiotap header -- no reason
 * to build this manually element by element, we can write it much
 * more efficiently than we can parse it. ORDER MATTERS HERE
 *
 * When sent to us via the simulated Rx interface in sysfs, the entire
 * structure is provided regardless of any bits unset.
 */
struct ipw_rt_hdr {
	struct ieee80211_radiotap_header rt_hdr;
	u64 rt_tsf;		/* TSF */
	u8 rt_flags;		/* radiotap packet flags */
	u8 rt_rate;		/* rate in 500kb/s */
	u16 rt_channel;		/* channel in mhz */
	u16 rt_chbitmask;	/* channel bitfield */
	s8 rt_dbmsignal;	/* signal in dbM, kluged to signed */
	s8 rt_dbmnoise;
	u8 rt_antenna;		/* antenna number */
	u8 payload[0];		/* payload... */
} __attribute__ ((packed));

/* channel tx power info for normal tx and scan */

struct ipw_channel_tgd_info {
	u8 type;
	s8 max_power;
};

struct ipw_channel_tgh_info {
	s64 last_radar_time;
};

/* current Tx power values to use, one for each rate for each channel.
 * requested power is limited by:
 * -- regulatory EEPROM limits for this channel
 * -- hardware capabilities (clip-powers)
 * -- spectrum management
 * -- user preference (e.g. iwconfig)
 * when requested power is set, base power index must also be set. */
struct ipw_channel_power_info {
	struct ipw_tx_power tpc;	/* actual radio and DSP gain settings */
	s8 power_table_index;	/* actual (temp-comp'd) index into gain table */
	s8 base_power_index;	/* gain index for req. power at factory temp. */
	s8 requested_power;	/* power (dBm) requested for this chnl/rate */
};

/* current scan Tx power values to use, one for each scan rate for each channel.
 * "base_power_index" is a reference for adjusting scan powers when
 *    using PA feedback as thermal calibration method.  It must be reset
 *    to new power_table_index whenever we reset the cumulative
 *    PA adjustment value, which is whenever we do a "brute force"
 *    thermal calibration with reg_compensate_for_temperature_dif().  */
struct ipw_scan_power_info {
	struct ipw_tx_power tpc;	/* actual radio and DSP gain settings */
	s8 power_table_index;	/* actual (temp-comp'd) index into gain table */
	s8 base_ref_index;	/* temp compensated, reference for PA adjust */
	s8 requested_power;	/* scan pwr (dBm) requested for chnl/rate */
};

/* Channel unlock period is 15 seconds. If no beacon or probe response
 * has been received within 15 seconds on a locked channel then the channel
 * remains locked. */
#define TX_UNLOCK_PERIOD 15

/* CSA lock perios is 15 seconds.  If a CSA has been received on a channel in
 * the last 15 seconds, the channel is locked */
#define CSA_LOCK_PERIOD 15
/*
 * One for each channel, holds all channel setup data
 * Some of the fields (e.g. eeprom and flags/max_power_avg) are redundant
 *     with one another!
 */
struct ipw_channel_info {
	struct ipw_channel_tgd_info tgd;
	struct ipw_channel_tgh_info tgh;
	struct ipw_eeprom_channel eeprom;	/* EEPROM regulatory limit */

	u8 channel;		/* channel number */
	u8 flags;		/* flags copied from EEPROM */
	s8 max_power_avg;	/* (dBm) regul. eeprom, normal Tx, any rate */
	s8 curr_txpow;		/* (dBm) regulatory/spectrum/user (not h/w) */
	s8 min_power;		/* always 0 */
	s8 scan_power;		/* (dBm) regul. eeprom, direct scans, any rate */

	u8 group_index;		/* 0-4, maps channel to group1/2/3/4/5 */
	u8 band_index;		/* 0-4, maps channel to band1/2/3/4/5 */
	u8 band;		/* IEEE80211_24GHZ_BAND or IEEE80211_52GHZ_BAND */

	u8 tx_locked;		/* 0 - Tx allowed.  1 - Tx disabled.
				 * Any channel requiring RADAR DETECT or PASSIVE ONLY
				 * is Tx locked until a beacon or probe response is
				 * received (for PASSIVE ONLY) */
	unsigned long rx_unlock;	/* For channels that are not tx_unlocked,
					 * this is the time (in seconds) of the last
					 * frame that will unlock this channel.  If
					 * more than 15s have passed then the channel
					 * is not unlocked. */
	unsigned long csa_received;	/* Time that the last CSA was received on
					 * this channel, or 0 if never.  A channel
					 * can only be scanned or used if no CSA
					 * has been received in the past 15s */

	/* Radio/DSP gain settings for each "normal" data Tx rate.
	 * These include, in addition to RF and DSP gain, a few fields for
	 *   remembering/modifying gain settings (indexes). */
	struct ipw_channel_power_info power_info[IPW_MAX_RATES];

	/* Radio/DSP gain settings for each scan rate, for directed scans. */
	struct ipw_scan_power_info scan_pwr_info[IPW_NUM_SCAN_RATES];
};

struct ipw_clip_group {
	/* maximum power level to prevent clipping for each rate, derived by
	 *   us from this band's saturation power in EEPROM */
	s8 clip_powers[IPW_MAX_RATES];
};

/* The following macros are neccessary to retain compatibility
 * around the workqueue chenges happened in kernels >= 2.6.20:
 * - INIT_WORK changed to take 2 arguments and let the work function
 *   get its own data through the container_of macro
 * - delayed works have been split from normal works to save some
 *   memory usage in struct work_struct
 */
#if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 20)
#define IPW_INIT_WORK(_work, _fun)		INIT_WORK(_work, ((void (*)(void *))_fun), _work)
#define IPW_INIT_DELAYED_WORK(_work, _fun)	INIT_WORK(_work, ((void (*)(void *))_fun), _work)
#define IPW_DELAYED_DATA(_ptr, _type, _m)	container_of(_ptr, _type, _m)
typedef struct work_struct delayed_work_t;
#else
#define IPW_INIT_WORK				INIT_WORK
#define IPW_INIT_DELAYED_WORK			INIT_DELAYED_WORK
#define IPW_DELAYED_DATA(_ptr, _type, _m)	container_of(_ptr, _type, _m.work)
typedef struct delayed_work delayed_work_t;
#endif

struct ipw_priv {
	struct net_device_stats stats;

	/* spectrum measurement report caching */
	struct ipw_spectrum_notification measure_report;

	/* we allocate array of ipw_channel_info for NIC's channels.
	 * NOTE:  We store all channels here, even channels that do not
	 * allow Tx (invalid)
	 *
	 * The BG channels 1-14 are accessed by channel index 0-13
	 * The A channels are stored conse*/
	struct ipw_channel_info *channel_info;	/* channel info array */
	u8 channel_count;	/* # of valid channels */
	u8 bg_count;
	u8 a_count;

	/* each calibration channel group in the EEPROM has a derived
	 * clip setting for each rate. */
	struct ipw_clip_group clip_groups[5];

	/* thermal calibration */
	int curr_temperature;
	int last_temperature;

	/* spinlock */
	spinlock_t lock;
	struct mutex mutex;

	/* basic pci-network driver stuff */
	struct pci_dev *pci_dev;
	struct net_device *net_dev;

	/* Promiscuous mode */
	struct ipw_prom_priv *prom_priv;
	struct net_device *prom_net_dev;

	/* pci hardware address support */
	void __iomem *hw_base;

	/* uCode images, save to reload in case of failure */
	struct fw_image_desc ucode_code;	/* runtime inst */
	struct fw_image_desc ucode_data;	/* runtime data original */
	struct fw_image_desc ucode_data_backup;	/* runtime data save/restore */
	struct fw_image_desc ucode_init;	/* initialization inst */
	struct fw_image_desc ucode_init_data;	/* initialization data */
	struct fw_image_desc ucode_boot;	/* bootstrap inst */

	struct ipw_shared_t *shared_virt;
	dma_addr_t shared_phys;

	struct ipw_rxon_cmd staging_config;
	struct ipw_rxon_cmd active_config;

	// 1st responses from initialize and runtime uCode images.
	struct ipw_init_alive_resp card_alive_init;
	struct ipw_alive_resp card_alive;

	/* LED related variables */
	unsigned long led_packets;
	int led_state;

	/* Tx rate scaling and retry information */
	u8 rate_mbps;
	u8 rate_plcp;
	u16 active_rate;
	u16 active_rate_basic;
	s8 data_retry_limit;
	u8 retry_rate;

	wait_queue_head_t wait_command_queue;

	int activity_timer_active;

	/* Rx and Tx DMA processing queues */
	struct ipw_rx_queue *rxq;
	struct ipw_tx_queue txq[6];
	u32 status;
	u32 config;

	struct ipw_power_mgr power_data;

	struct ipw_notif_statistics statistics;

	u8 channel;
	u8 band;

	u32 power_mode;
	u32 antenna;

	u16 rts_threshold;

	u8 mac_addr[ETH_ALEN];

	u8 num_stations;
	struct ipw_station_entry stations[NUM_OF_STATIONS];

	u8 netdev_registered;

	int is_abg;

	/* Wireless statistics */
	unsigned long last_rx_jiffies;
	u32 last_beacon_time;
	u64 last_tsf;
	u8 last_rx_rssi;
	u16 last_noise;

	/* Statistics and counters normalized with each association */
	u32 last_missed_beacons;
	u32 last_tx_packets;
	u32 last_rx_packets;
	u32 last_tx_failures;
	u32 last_rx_err;
	u32 last_rate;

	u32 missed_beacons;
	unsigned long rx_packets;
	unsigned long tx_packets;
	unsigned long long rx_bytes;
	unsigned long long tx_bytes;
	u32 quality;

	/* Duplicate packet detection */
	u16 last_seq_num;
	u16 last_frag_num;
	unsigned long last_packet_time;

	/* eeprom */
	struct ipw_eeprom eeprom;

	/* Driver and iwconfig driven work queue */
	struct workqueue_struct *workqueue;

	delayed_work_t commit;
	delayed_work_t up;
	delayed_work_t restart;
	delayed_work_t rx_replenish;
	delayed_work_t rf_kill;
	delayed_work_t activity_timer;
	delayed_work_t init_alive_start;
	delayed_work_t alive_start;
	delayed_work_t resume_work;
	delayed_work_t send_txpower;
	delayed_work_t thermal_periodic;;

	struct tasklet_struct irq_tasklet;

#define IPW_DEFAULT_TX_POWER 0x0F
	s8 user_txpower_limit;
	s8 actual_txpower_limit;
	s8 max_txpower_limit;
	int max_power_override;
#ifdef CONFIG_PM
	u32 pm_state[16];
#endif

	/* Used to pass the current INTA value from ISR to Tasklet */
	u32 isr_inta;

};				/*ipw_priv */

/* debug macros */

#include <linux/ctype.h>

/*
* Register bit definitions
*/

/* NIC type as found in the one byte EEPROM_NIC_TYPE  offset*/

/* Defines a single bit in a by bit number (0-31) */

/* Interrupts masks */
#define IPW_RX_BUF_SIZE 3000
enum {
	IPW_FW_ERROR_OK = 0,
	IPW_FW_ERROR_FAIL,
	IPW_FW_ERROR_MEMORY_UNDERFLOW,
	IPW_FW_ERROR_MEMORY_OVERFLOW,
	IPW_FW_ERROR_BAD_PARAM,
	IPW_FW_ERROR_BAD_CHECKSUM,
	IPW_FW_ERROR_NMI_INTERRUPT,
	IPW_FW_ERROR_BAD_DATABASE,
	IPW_FW_ERROR_ALLOC_FAIL,
	IPW_FW_ERROR_DMA_UNDERRUN,
	IPW_FW_ERROR_DMA_STATUS,
	IPW_FW_ERROR_DINO_ERROR,
	IPW_FW_ERROR_EEPROM_ERROR,
	IPW_FW_ERROR_SYSASSERT,
	IPW_FW_ERROR_FATAL_ERROR
};

#define AUTH_OPEN       0
#define AUTH_SHARED_KEY 1

#define HC_ASSOCIATE      0
#define HC_REASSOCIATE    1
#define HC_DISASSOCIATE   2
#define HC_DISASSOC_QUIET 5

#define HC_QOS_SUPPORT_ASSOC  0x01

#define IPW_RATE_CAPABILITIES 1
#define IPW_RATE_CONNECT      0

/*
 * Rate values and masks
 */
#define IPW_TX_RATE_1MB  0x0A
#define IPW_TX_RATE_2MB  0x14
#define IPW_TX_RATE_5MB  0x37
#define IPW_TX_RATE_6MB  0x0D
#define IPW_TX_RATE_9MB  0x0F
#define IPW_TX_RATE_11MB 0x6E
#define IPW_TX_RATE_12MB 0x05
#define IPW_TX_RATE_18MB 0x07
#define IPW_TX_RATE_24MB 0x09
#define IPW_TX_RATE_36MB 0x0B
#define IPW_TX_RATE_48MB 0x01
#define IPW_TX_RATE_54MB 0x03

#define IPW_ORD_TABLE_0_MASK              0x0000F000
#define IPW_ORD_TABLE_1_MASK              0x0000F100
#define IPW_ORD_TABLE_2_MASK              0x0000F200
#define IPW_ORD_TABLE_3_MASK              0x0000F300
#define IPW_ORD_TABLE_4_MASK              0x0000F400
#define IPW_ORD_TABLE_5_MASK              0x0000F500
#define IPW_ORD_TABLE_6_MASK              0x0000F600
#define IPW_ORD_TABLE_7_MASK              0x0000F700

/*
 * Table 0 Entries (all entries are 32 bits)
 */
enum {
	IPW_ORD_STAT_TX_CURR_RATE = IPW_ORD_TABLE_0_MASK + 1,
	IPW_ORD_STAT_FRAG_TRESHOLD,
	IPW_ORD_STAT_RTS_THRESHOLD,
	IPW_ORD_STAT_TX_HOST_REQUESTS,
	IPW_ORD_STAT_TX_HOST_COMPLETE,
	IPW_ORD_STAT_TX_DIR_DATA,
	IPW_ORD_STAT_TX_DIR_DATA_B_1,
	IPW_ORD_STAT_TX_DIR_DATA_B_2,
	IPW_ORD_STAT_TX_DIR_DATA_B_5_5,
	IPW_ORD_STAT_TX_DIR_DATA_B_11,
	/* Hole */

	IPW_ORD_STAT_TX_DIR_DATA_G_1 = IPW_ORD_TABLE_0_MASK + 19,
	IPW_ORD_STAT_TX_DIR_DATA_G_2,
	IPW_ORD_STAT_TX_DIR_DATA_G_5_5,
	IPW_ORD_STAT_TX_DIR_DATA_G_6,
	IPW_ORD_STAT_TX_DIR_DATA_G_9,
	IPW_ORD_STAT_TX_DIR_DATA_G_11,
	IPW_ORD_STAT_TX_DIR_DATA_G_12,
	IPW_ORD_STAT_TX_DIR_DATA_G_18,
	IPW_ORD_STAT_TX_DIR_DATA_G_24,
	IPW_ORD_STAT_TX_DIR_DATA_G_36,
	IPW_ORD_STAT_TX_DIR_DATA_G_48,
	IPW_ORD_STAT_TX_DIR_DATA_G_54,
	IPW_ORD_STAT_TX_NON_DIR_DATA,
	IPW_ORD_STAT_TX_NON_DIR_DATA_B_1,
	IPW_ORD_STAT_TX_NON_DIR_DATA_B_2,
	IPW_ORD_STAT_TX_NON_DIR_DATA_B_5_5,
	IPW_ORD_STAT_TX_NON_DIR_DATA_B_11,
	/* Hole */

	IPW_ORD_STAT_TX_NON_DIR_DATA_G_1 = IPW_ORD_TABLE_0_MASK + 44,
	IPW_ORD_STAT_TX_NON_DIR_DATA_G_2,
	IPW_ORD_STAT_TX_NON_DIR_DATA_G_5_5,
	IPW_ORD_STAT_TX_NON_DIR_DATA_G_6,
	IPW_ORD_STAT_TX_NON_DIR_DATA_G_9,
	IPW_ORD_STAT_TX_NON_DIR_DATA_G_11,
	IPW_ORD_STAT_TX_NON_DIR_DATA_G_12,
	IPW_ORD_STAT_TX_NON_DIR_DATA_G_18,
	IPW_ORD_STAT_TX_NON_DIR_DATA_G_24,
	IPW_ORD_STAT_TX_NON_DIR_DATA_G_36,
	IPW_ORD_STAT_TX_NON_DIR_DATA_G_48,
	IPW_ORD_STAT_TX_NON_DIR_DATA_G_54,
	IPW_ORD_STAT_TX_RETRY,
	IPW_ORD_STAT_TX_FAILURE,
	IPW_ORD_STAT_RX_ERR_CRC,
	IPW_ORD_STAT_RX_ERR_ICV,
	IPW_ORD_STAT_RX_NO_BUFFER,
	IPW_ORD_STAT_FULL_SCANS,
	IPW_ORD_STAT_PARTIAL_SCANS,
	IPW_ORD_STAT_TGH_ABORTED_SCANS,
	IPW_ORD_STAT_TX_TOTAL_BYTES,
	IPW_ORD_STAT_CURR_RSSI_RAW,
	IPW_ORD_STAT_RX_BEACON,
	IPW_ORD_STAT_MISSED_BEACONS,
	IPW_ORD_TABLE_0_LAST
};

/* Table 1 Entries
 */
enum {
	IPW_ORD_TABLE_1_LAST = IPW_ORD_TABLE_1_MASK | 1,
};

/*
 * Table 2 Entries
 *
 * FW_VERSION:    16 byte string
 * FW_DATE:       16 byte string (only 14 bytes used)
 * UCODE_VERSION: 4 byte version code
 * UCODE_DATE:    5 bytes code code
 * ADAPTER_MAC:   6 byte MAC address
 * RTC:           4 byte clock
 */
enum {
	IPW_ORD_STAT_FW_VERSION = IPW_ORD_TABLE_2_MASK | 1,
	IPW_ORD_STAT_FW_DATE,
	IPW_ORD_STAT_UCODE_VERSION,
	IPW_ORD_STAT_UCODE_DATE,
	IPW_ORD_STAT_ADAPTER_MAC,
	IPW_ORD_STAT_RTC,
	IPW_ORD_TABLE_2_LAST
};

/* Table 3 */
enum {
	IPW_ORD_STAT_TX_PACKET = IPW_ORD_TABLE_3_MASK | 0,
	IPW_ORD_STAT_TX_PACKET_FAILURE,
	IPW_ORD_STAT_TX_PACKET_SUCCESS,
	IPW_ORD_STAT_TX_PACKET_ABORTED,
	IPW_ORD_TABLE_3_LAST
};

/* Table 4 */
enum {
	IPW_ORD_TABLE_4_LAST = IPW_ORD_TABLE_4_MASK
};

/* Table 5 */
enum {
	IPW_ORD_STAT_AVAILABLE_AP_COUNT = IPW_ORD_TABLE_5_MASK,
	IPW_ORD_STAT_AP_ASSNS,
	IPW_ORD_STAT_ROAM,
	IPW_ORD_STAT_ROAM_CAUSE_MISSED_BEACONS,
	IPW_ORD_STAT_ROAM_CAUSE_UNASSOC,
	IPW_ORD_STAT_ROAM_CAUSE_RSSI,
	IPW_ORD_STAT_ROAM_CAUSE_LINK_QUALITY,
	IPW_ORD_STAT_ROAM_CAUSE_AP_LOAD_BALANCE,
	IPW_ORD_STAT_ROAM_CAUSE_AP_NO_TX,
	IPW_ORD_STAT_LINK_UP,
	IPW_ORD_STAT_LINK_DOWN,
	IPW_ORD_ANTENNA_DIVERSITY,
	IPW_ORD_CURR_FREQ,
	IPW_ORD_TABLE_5_LAST
};

/* Table 6 */
enum {
	IPW_ORD_COUNTRY_CODE = IPW_ORD_TABLE_6_MASK,
	IPW_ORD_CURR_BSSID,
	IPW_ORD_CURR_SSID,
	IPW_ORD_TABLE_6_LAST
};

/* Table 7 */
enum {
	IPW_ORD_STAT_PERCENT_MISSED_BEACONS = IPW_ORD_TABLE_7_MASK,
	IPW_ORD_STAT_PERCENT_TX_RETRIES,
	IPW_ORD_STAT_PERCENT_LINK_QUALITY,
	IPW_ORD_STAT_CURR_RSSI_DBM,
	IPW_ORD_TABLE_7_LAST
};

struct ipw_fixed_rate {
	u16 tx_rates;
	u16 reserved;
} __attribute__ ((packed));

#define IPW_MIN_RSSI_VAL                 -100
#define IPW_MAX_RSSI_VAL                    0
#define IPW_RSSI_OFFSET                    95
#define IPW_RATE_SCALE_FLUSH          (3*HZ/10)	//300 milli
#define IPW_RATE_SCALE_WIN_FLUSH      (HZ/2)	//500 milli
#define IPW_RATE_SCALE_HIGH_TH          11520
#define IPW_RATE_SCALE_MIN_FAILURE_TH       8
#define IPW_RATE_SCALE_MIN_SUCCESS_TH       8
#define IPW_RATE_SCALE_DECREASE_TH       1920

#endif				/* __ipwraw_h__ */
