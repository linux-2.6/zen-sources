/**
 * sysfs_system_layout.c
 * zen-tune
 *
 * Copyright (C) 2008 Ryan Hope <rmh3093@gmail.com>
 *
 * Released under the GPL version 2 only.
 *
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

#include "zen-system-layout-main.h"

extern unsigned int sysctl_sched_latency;
extern unsigned int sysctl_sched_min_granularity;
extern unsigned int sysctl_sched_wakeup_granularity;
extern unsigned int sched_nr_latency;
extern int vm_dirty_ratio;
extern int vm_swappiness;
extern int system_layout;
extern struct layout available_layouts[ZEN_LAST];
extern struct kobject *zen_kobj;

static struct kobject *custom_layout_kobj;

static ssize_t cl_settings_show(struct kobject *kobj,
        struct kobj_attribute *attr, char *buf)
{
	int var;

        if (strcmp(attr->attr.name, "sched_latency") == 0) {
		var = sysctl_sched_latency;
        } else if (strcmp(attr->attr.name, "sched_min_granularity") == 0) {
		var = sysctl_sched_min_granularity;
        } else if (strcmp(attr->attr.name, "sched_wakeup_granularity") == 0) {
		var = sysctl_sched_wakeup_granularity;
        } else if (strcmp(attr->attr.name, "vm_swappiness") == 0) {
		var = vm_swappiness;
        } else if (strcmp(attr->attr.name, "vm_dirty_ratio") == 0) {
		var = vm_dirty_ratio;
	} else {
		return 0;
	}
        return sprintf(buf, "%d\n", var);
}

static ssize_t cl_settings_store(struct kobject *kobj,
        struct kobj_attribute *attr, const char *buf, size_t count)
{
        int var;

        sscanf(buf, "%du", &var);
        if (strcmp(attr->attr.name, "sched_latency") == 0) {
                sysctl_sched_latency = var;
        } else if (strcmp(attr->attr.name, "sched_min_granularity") == 0) {
		sysctl_sched_min_granularity = var;
        } else if (strcmp(attr->attr.name, "sched_wakeup_granularity") == 0) {
                sysctl_sched_wakeup_granularity = var;
        } else if (strcmp(attr->attr.name, "vm_swappiness") == 0) {
                vm_swappiness = var;
        } else if (strcmp(attr->attr.name, "vm_dirty_ratio") == 0) {
                vm_dirty_ratio = var;
        }

        return count;
}

static struct kobj_attribute cl_sched_latency =
	__ATTR(sched_latency, 0666, cl_settings_show,
	cl_settings_store);
static struct kobj_attribute cl_sched_min_granularity =
	__ATTR(sched_min_granularity, 0666, cl_settings_show,
	cl_settings_store);
static struct kobj_attribute cl_sched_wakeup_granularity =
        __ATTR(sched_wakeup_granularity, 0666, cl_settings_show,
	cl_settings_store);
static struct kobj_attribute cl_vm_swappiness =
        __ATTR(vm_swappiness, 0666, cl_settings_show,
	cl_settings_store);
static struct kobj_attribute cl_vm_dirty_ratio =
        __ATTR(vm_dirty_ratio, 0666, cl_settings_show,
	cl_settings_store);

static struct attribute *custom_layout_attrs[] = {
        &cl_sched_latency.attr,
        &cl_sched_min_granularity.attr,
        &cl_sched_wakeup_granularity.attr,
        &cl_vm_swappiness.attr,
        &cl_vm_dirty_ratio.attr,
        NULL,
};

static struct attribute_group custom_layout_group = {
        .attrs = custom_layout_attrs,
};

static ssize_t layouts_show(struct kobject *kobj, struct kobj_attribute *attr,
			char *buf)
{
	int i;

	int len = 0;
	for (i=0; (i<ZEN_LAST); i++) {
		if (i==system_layout)
			len += sprintf(buf+len, "[%s] ",
				available_layouts[i].name);
		else
			len += sprintf(buf+len, "%s ",
				available_layouts[i].name);
	}
	len += sprintf(len+buf, "\n");

	return len;
}

static ssize_t layouts_store(struct kobject *kobj, struct kobj_attribute *attr,
			const char *buf, size_t count)
{
        int i;

        for (i=0; i<(ZEN_LAST); i++) {
		if (strcmp(buf, available_layouts[i].name) == 0) {
		        system_layout = i;
			zen_set_system_layout(i);
			break;
		}
	}
	return count;
}

static struct kobj_attribute layouts_attribute =
        __ATTR(system_layout, 0666, layouts_show, layouts_store);

static struct attribute *layout_attrs[] = {
	&layouts_attribute.attr,
        NULL,
};

static struct attribute_group layout_group = {
        .attrs = layout_attrs,
};

int init_sysfs_interface_system_layout(void)
{
        int ret = 0;

	ret = sysfs_create_group(zen_kobj, &layout_group);
	if (!ret) {
		custom_layout_kobj =
			kobject_create_and_add("custom_system_layout", zen_kobj);
		if (!custom_layout_kobj) {
			ret = -ENOMEM;
		} else {
			ret = sysfs_create_group(custom_layout_kobj,
				&custom_layout_group);
			if (ret) {
				kobject_put(custom_layout_kobj);
			}
		}
	}

	if (!ret)
	        printk(KERN_INFO "Zen-Tune Sysfs [Zen System Layout Support].\n");

        return ret;
}

void cleanup_sysfs_interface_system_layout(void)
{
	kobject_put(custom_layout_kobj);
}

module_init(init_sysfs_interface_system_layout);
module_exit(cleanup_sysfs_interface_system_layout);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ryan Hope <rmh3093@gmail.com>");

