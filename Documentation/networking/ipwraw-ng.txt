
The developers at intellinuxwireless.org are working on a new driver for wireless adapters from Intel named iwlwifi. Meanwhile they also made a driver for testing purposes called ipwraw which allows raw packet Tx/Rx with the Intel PRO/Wireless 3945ABG adapter.
Handling raw packets over the wireless card is nice for programs like aircrack-ng, kismet, mdk3, ... just to name a few, but they use the wireless extensions to control the cards. Since ipwraw doesn't even have wireless extensions, I modified the driver to add those that are required for the above mentioned programs to work.
Because this can be considered an "upgraded" ipwraw, and mainly because I made the modifications to use it with aircrack-ng, I called the modified driver ipwraw-ng.

Don't bother the original developers of this driver with anything related with this modification. For now just use the aircrack-ng forum instead, in this thread http://tinyshell.be/aircrackng/forum/index.php?topic=1985.0
You can contact me at tolas_feup@hotmail.com, but I don't use very often, so you better use the forum.
The home page for this driver was gently provided by ASPj:
http://homepages.tu-darmstadt.de/~p_larbig/wlan/

This package contains ucode version 2.14.4 and the modified driver source code.
Both can be downloaded at http://intellinuxwireless.org/index.php?n=Downloads
More information about what has been added can be found on the included CHANGELOG.ipwraw-ng file.
For anything related with licenses, read the LICENSE file.

**** TIPS:
You just need to make && make install. If you want you can also make install_ucode if your firmware isn't 2.14.4.
To load the module you can use the included 'load' script, or as usual for driver modules do modprobe ipwraw.
I left the default speed at 54 MBit. You may want to lower it to 1 MBit before injection with iwconfig wifi0 rate 1M.
