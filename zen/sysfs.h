/**
 * sysfs.h
 * zen-tune
 *
 * Copyright (C) 2008 Ryan Hope <rmh3093@gmail.com>
 *
 * Released under the GPL version 2 only.
 *
 */

int init_sysfs_interface(void);
void cleanup_sysfs_interface(void);
