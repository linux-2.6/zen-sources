/******************************************************************************

  Copyright(c) 2003 - 2006 Intel Corporation. All rights reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along with
  this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110, USA

  The full GNU General Public License is included in this distribution in the
  file called LICENSE.

  Contact Information:
  James P. Ketrenos <ipw2100-admin@linux.intel.com>
  Intel Corporation, 5200 N.E. Elam Young Parkway, Hillsboro, OR 97124-6497

******************************************************************************/

#ifndef	__iwlwifi_hw_h__
#define __iwlwifi_hw_h__

/* microcode and hardware interface definitions */

/*
 *  Time constants
 */
#define SHORT_SLOT_TIME 9
#define LONG_SLOT_TIME 20
#define OFDM_SYMBOL_TIME 4

/*
 * Driver API command-id
 */
enum {
	REPLY_ALIVE = 0x1,
	REPLY_ERROR = 0x2,

/* RXON state commands */
	REPLY_RXON = 0x10,
	REPLY_RXON_ASSOC = 0x11,
	REPLY_QOS_PARAM = 0x13,
	REPLY_RXON_TIMING = 0x14,

/* Multi-Station support */
	REPLY_ADD_STA = 0x18,
	REPLY_REMOVE_STA = 0x19,
	REPLY_REMOVE_ALL_STA = 0x1a,

/* RX, TX */
	REPLY_RX = 0x1b,
	REPLY_TX = 0x1c,

/* MISC commands */
	REPLY_RATE_SCALE = 0x47,
	REPLY_LEDS_CMD = 0x48,

/* timers commands */
	REPLY_BCON = 0x27,	//  off

	/* measurements *** */
	RADAR_NOTIFICATION = 0x70,
	CHANNEL_SWITCH_NOTIFICATION = 0x73,

	SPECTRUM_MEASURE_NOTIFICATION = 0x75,

/* Power Management ****/
	POWER_TABLE_CMD = 0x77,
	PM_SLEEP_NOTIFICATION = 0x7A,
	PM_DEBUG_STATISTIC_NOTIFIC = 0x7B,

/* Scan commands and notifications ****/
	REPLY_SCAN_ABORT_CMD = 0x81,
	REPLY_SCAN_CMD = 0x80,

	SCAN_START_NOTIFICATION = 0x82,
	SCAN_RESULTS_NOTIFICATION = 0x83,
	SCAN_COMPLETE_NOTIFICATION = 0x84,

/* IBSS/AP commands ****/
	BEACON_NOTIFICATION = 0x90,
	REPLY_TX_BEACON = 0x91,
	WHO_IS_AWAKE_NOTIFICATION = 0x94,

	QUIET_NOTIFICATION = 0x96,
	REPLY_TX_PWR_TABLE_CMD = 0x97,
	MEASURE_ABORT_NOTIFICATION = 0x99,

//bt config command
	REPLY_BT_CONFIG = 0x9b,
	REPLY_STATISTICS_CMD = 0x9c,
	STATISTICS_NOTIFICATION = 0x9d,
/* RF-KILL commands and notifications ****/
	REPLY_CARD_STATE_CMD = 0xa0,
	CARD_STATE_NOTIFICATION = 0xa1,

/* Missed beacons notification ****/
	MISSED_BEACONS_NOTIFICATION = 0xa2,
	MISSED_BEACONS_NOTIFICATION_TH_CMD = 0xa3,
};

union tsf {
	u8 byte[8];		//7:0
	u16 word[4];		//7:6,5:4,3:2,1:0
	u32 dw[2];		//7:4,3:0
};

/*
 * Alive Command & Response
 */

#define UCODE_VALID_OK      (0x1)
#define INITIALIZE_SUBTYPE    (9)

struct ipw_alive_resp {
	u8 ucode_minor;
	u8 ucode_major;
	u16 reserved1;
	u8 sw_rev[8];
	u8 ver_type;
	u8 ver_subtype;
	u16 reserved2;
	u32 log_event_table_ptr;
	u32 error_event_table_ptr;
	u32 timestamp;
	u32 is_valid;
} __attribute__ ((packed));

struct ipw_init_alive_resp {
	u8 ucode_minor;
	u8 ucode_major;
	__le16 reserved1;
	u8 sw_rev[8];
	u8 ver_type;
	u8 ver_subtype;
	__le16 reserved2;
	__le32 log_event_table_ptr;
	__le32 error_event_table_ptr;
	__le32 timestamp;
	__le32 is_valid;

	//NOTE: 4965 has extra fields here
} __attribute__ ((packed));

/*
 * Error Command & Response
 */

struct ipw_error_resp {
	u32 enumErrorType;	//7:4
	u8 currentCmdID;	//8
	u8 reserved1;		//9
	u16 erroneousCmdSeqNum;	//11:10
	u16 reserved2;		//13:12
	u32 errorInfo;		//17:14
	union tsf timestamp;	//all TSF  //25:18
} __attribute__ ((packed));

/*
 * Rx config defines & structure
 */
/* rx_config device types  */
enum {
	RXON_DEV_TYPE_AP = 1,
	RXON_DEV_TYPE_ESS = 3,
	RXON_DEV_TYPE_IBSS = 4,
	RXON_DEV_TYPE_SNIFFER = 6,
};

/* rx_config flags */
enum {
	/* band & modulation selection */
	RXON_FLG_BAND_24G_MSK = (1 << 0),
	RXON_FLG_CCK_MSK = (1 << 1),
	/* auto detection enable */
	RXON_FLG_AUTO_DETECT_MSK = (1 << 2),
	/* TGg protection when tx */
	RXON_FLG_TGG_PROTECT_MSK = (1 << 3),
	/* cck short slot & preamble */
	RXON_FLG_SHORT_SLOT_MSK = (1 << 4),
	RXON_FLG_SHORT_PREAMBLE_MSK = (1 << 5),
	/* antenna selection */
	RXON_FLG_DIS_DIV_MSK = (1 << 7),
	RXON_FLG_ANT_SEL_MSK = 0x0f00,
	RXON_FLG_ANT_A_MSK = (1 << 8),
	RXON_FLG_ANT_B_MSK = (1 << 9),
	/* radar detection enable */
	RXON_FLG_RADAR_DETECT_MSK = (1 << 12),
	RXON_FLG_TGJ_NARROW_BAND_MSK = (1 << 13),
	/* rx response to host with 8-byte TSF
	 * (according to ON_AIR deassertion) */
	RXON_FLG_TSF2HOST_MSK = (1 << 15)
};

/* rx_config filter flags */
enum {
	/* accept all data frames */
	RXON_FILTER_PROMISC_MSK = (1 << 0),
	/* pass control & management to host */
	RXON_FILTER_CTL2HOST_MSK = (1 << 1),
	/* accept multi-cast */
	RXON_FILTER_ACCEPT_GRP_MSK = (1 << 2),
	/* don't decrypt uni-cast frames */
	RXON_FILTER_DIS_DECRYPT_MSK = (1 << 3),
	/* don't decrypt multi-cast frames */
	RXON_FILTER_DIS_GRP_DECRYPT_MSK = (1 << 4),
	/* STA is associated */
	RXON_FILTER_ASSOC_MSK = (1 << 5),
	/* transfer to host non bssid beacons in associated state */
	RXON_FILTER_BCON_AWARE_MSK = (1 << 6)
};


#if 0
static struct ipw_rate_info rate_table_info[] = {
/*  CCK rate info   */
	{10, 2, 8, 0, 112, 160, 112, 1216},	/*   1mbps */
	{20, 4, 9, 0, 56, 80, 56, 608},	        /*   2mbps */
	{55, 11, 10, 0, 21, 29, 21, 222},	/* 5.5mbps */
	{110, 22, 11, 0, 11, 15, 11, 111},	/*  11mbps */
/*  OFDM rate info   */
	{13, 6 * 2, 0, 24, 44, 52, 44, 228},	/*   6mbps */
	{15, 9 * 2, 1, 36, 36, 44, 36, 160},	/*   9mbps */
	{5, 12 * 2, 2, 48, 32, 36, 32, 124},	/*  12mbps */
	{7, 18 * 2, 3, 72, 28, 32, 28, 92},	/*  18mbps */
	{9, 24 * 2, 4, 96, 28, 32, 28, 72},	/*  24mbps */
	{11, 36 * 2, 5, 144, 24, 28, 24, 56},	/*  36mbps */
	{1, 48 * 2, 6, 192, 24, 24, 24, 48},	/*  48mbps */
	{3, 54 * 2, 7, 216, 24, 24, 24, 44},	/*  54mbps */
};
#endif

#define IPW_CCK_RATES  4
#define IPW_OFDM_RATES 8
#define IPW_MAX_RATES  (IPW_CCK_RATES + IPW_OFDM_RATES)

/*
 * Rate values and masks
 */
#define IPW_TX_RATE_1MB  0x0A
#define IPW_TX_RATE_2MB  0x14
#define IPW_TX_RATE_5MB  0x37
#define IPW_TX_RATE_6MB  0x0D
#define IPW_TX_RATE_9MB  0x0F
#define IPW_TX_RATE_11MB 0x6E
#define IPW_TX_RATE_12MB 0x05
#define IPW_TX_RATE_18MB 0x07
#define IPW_TX_RATE_24MB 0x09
#define IPW_TX_RATE_36MB 0x0B
#define IPW_TX_RATE_48MB 0x01
#define IPW_TX_RATE_54MB 0x03

/*
 * RXON-ASSOCIATED Command & Response
 */
struct ipw_rxon_assoc_cmd {
	u32 flags;
	u32 filter_flags;
	u8 ofdm_basic_rates;	// 12
	u8 cck_basic_rates;	// 13
	u16 reserved;		// 15:14
} __attribute__ ((packed));

struct ipw_rxon_cmd {
	u8 node_addr[6];
	u16 reserved1;
	u8 bssid_addr[6];
	u16 reserved2;
	u8 wlap_bssid_addr[6];
	u16 reserved3;
	u8 dev_type;
	u8 air_propagation;
	u16 reserved4;		// 27:26
	u8 ofdm_basic_rates;
	u8 cck_basic_rates;
	u16 assoc_id;
	u32 flags;
	u32 filter_flags;
	u16 channel;
	u16 reserved5;
} __attribute__ ((packed));

/*
 * RXON-Timings Command & Response
 */
struct ipw_rxon_time_cmd {
	union tsf timestamp;	// all TSF  //11:4
	u16 beaconInterval;	//13:12
	u16 atimWindow;		//15:14
	u32 beaconTimerInitVal;	//19:16
	u16 listenInterval;	//21:20
	u16 reserved;		//23:22
} __attribute__ ((packed));

/*
 * beacon QOS parameters Command & Response
 */
struct ipw_ac_qos {
	u16 ieee80211CWmin;		// 9+(8*(N-1)):8+(8*(N-1))
	u16 ieee80211CWmax;		// 11+(8*(N-1)):10+(8*(N-1))
	u8 ieee80211AIFSN;		// 12+(8*(N-1))
	u8 reserved1;		// 13+(8*(N-1))
	u16 edca_txop;		// 15+(8*(N-1)):14+(8*(N-1))
} __attribute__ ((packed));

/*
 *  TXFIFO Queue number defines
 */
/* number of Access catergories (AC) (EDCA), queues 0..3 */
#define AC_NUM                4
/* total number of queues */
#define QUEUE_NUM             7
/* command queue number */
#define CMD_QUEUE_NUM         4

struct ipw_qosparam_cmd {
	u32 qos_flags;		// 7:4
	struct ipw_ac_qos ac[AC_NUM];	// 39:8
} __attribute__ ((packed));

/*
 * Multi station support
 */
#define NUM_OF_STATIONS 25
#define AP_ID           0
#define MULTICAST_ID    1
#define STA_ID          2
#define BROADCAST_ID    24

#define STA_CONTROL_MODIFY_MSK             0x01

/*
 * Add/Modify Station Command & Response
 */
struct ipw_keyinfo {
	u16 key_flags;
	u8 tkip_rx_tsc_byte2;	//byte  2 TSC[2] for key mix ph1 detection
	u8 reserved1;		//byte  3
	u16 tkip_rx_ttak[5];	//byte 13:4 10-byte unicast TKIP TTAK
	u16 reserved2;		//byte 15:14
	u8 key[16];		//byte 32:16 16-byte unicast decryption key
} __attribute__ ((packed));

struct sta_id_modify {
	u8 MACAddr[ETH_ALEN];
	u16 reserved1;
	u8 staID;
	u8 modify_mask;
	u16 reserved2;
} __attribute__ ((packed));

struct ipw_addsta_cmd {
	u8 ctrlAddModify;	// byte  4
	u8 reserved[3];		// bytes 7:5
	struct sta_id_modify sta;	// bytes 19:8
	struct ipw_keyinfo key;	//byte 51:20
	u32 station_flags;	//bytes 55:52
	u32 station_flags_msk;	//bytes 59:56
	u16 tid_disable_tx;	//bytes 61:60
	u8 tx_rate;		//byte 62
	u8 reserved_1;		//byte 63
	u8 add_immediate_ba_tid;	//byte 64
	u8 remove_immediate_ba_tid;	//byte 65
	u16 add_immediate_ba_start_seq;	//byte 67:66
} __attribute__ ((packed));

struct ipw_add_sta_resp {
	u8 status;
} __attribute__ ((packed));

#define ADD_STA_SUCCESS_MSK              0x1

/*
 * WEP/CKIP group key command
 */
struct ipw_key {
	u8 index;
	u8 reserved[3];
	u32 size;
	u8 key[16];
} __attribute__ ((packed));

struct ipw_key_cmd {
	u8 count;
	u8 decrypt_type;
	u8 reserved[2];
	struct ipw_key key[4];
} __attribute__ ((packed));

struct ipw_rx_frame_stats {
	u8 mib_count;
	u8 id;
	u8 rssi;
	u8 agc;
	u16 sig_avg;
	u16 noise_diff;
	u8 payload[0];
} __attribute__ ((packed));

struct ipw_rx_frame_hdr {
	u16 channel;		//5:4
	u16 phy_flags;		//7:6
	u8 reserved1;		//macHeaderLength;          //8
	u8 rate;		//9
	u16 len;		//17:16
	u8 payload[0];
} __attribute__ ((packed));

enum {
	RX_RES_STATUS_NO_CRC32_ERROR = (1 << 0),
	RX_RES_STATUS_NO_RXE_OVERFLOW = (1 << 1),
};

enum {
	RX_RES_PHY_FLAGS_BAND_24_MSK = (1 << 0),
	RX_RES_PHY_FLAGS_MOD_CCK_MSK = (1 << 1),
	RX_RES_PHY_FLAGS_SHORT_PREAMBLE_MSK = (1 << 2),
	RX_RES_PHY_FLAGS_NARROW_BAND_MSK = (1 << 3),
	RX_RES_PHY_FLAGS_ANTENNA_MSK = 0xf0,
};

struct ipw_rx_frame_end {
	u32 status;		//3+(N+20):0+(N+20)
	u64 timestamp;		//11+(N+20):4+(N+20)
	u32 beaconTimeStamp;
} __attribute__ ((packed));

/* NOTE:  DO NOT dereference from casts to this structure
 * It is provided only for calculating minimum data set size.
 * The actual offsets of the hdr and end are dynamic based on
 * stats.mib_count */
struct ipw_rx_frame {
	struct ipw_rx_frame_stats stats;
	struct ipw_rx_frame_hdr hdr;
	struct ipw_rx_frame_end end;
} __attribute__ ((packed));

/*
 * Tx Command & Response
 */

/* Tx flags */
enum {
	TX_CMD_FLG_RTS_MSK = (1 << 1),
	TX_CMD_FLG_CTS_MSK = (1 << 2),
	TX_CMD_FLG_ACK_MSK = (1 << 3),
	TX_CMD_FLG_FULL_TXOP_PROT_MSK = (1 << 7),
	TX_CMD_FLG_ANT_SEL_MSK = 0xf00,
	TX_CMD_FLG_ANT_A_MSK = (1 << 8),
	TX_CMD_FLG_ANT_B_MSK = (1 << 9),

	/* ucode ignores BT priority for this frame */
	TX_CMD_FLG_BT_DIS_MSK = (1 << 12),

	/* ucode overides sequence control */
	TX_CMD_FLG_SEQ_CTL_MSK = (1 << 13),

	/* signal that this frame is non-last MPDU */
	TX_CMD_FLG_MORE_FRAG_MSK = (1 << 14),

	/* calculate TSF in outgoing frame */
	TX_CMD_FLG_TSF_MSK = (1 << 16),

	/* activate TX calibration. */
	TX_CMD_FLG_CALIB_MSK = (1 << 17),

	/* HCCA-AP - disable duration overwriting. */
	TX_CMD_FLG_DUR_MSK = (1 << 25),
};

/*
 * TX command security control
 */
#define TX_CMD_SEC_CCM               0x2
#define TX_CMD_SEC_TKIP              0x3

/*
 * TX command Frame life time
 */

#define MAX_REAL_TX_QUEUE_NUM 5

/* the number of bytes in tx_cmd_s should be 32-bit aligned as to allow
 * fast block transfer */

/*
 * Tx Command & Response:
 */

struct ipw_tx_cmd {
	u16 len;		//byte 5:411500
	u16 next_frame_len;	//byte 7:6
	u32 tx_flags;		//byte 11:8
	u8 rate;		//byte 12
	u8 sta_id;		//byte 13
	u8 tid_tspec;		//byte 14
	u8 sec_ctl;
	u8 key[16];
	union {
		u8 byte[8];	//7:0
		u16 word[4];	//7:6,5:4,3:2,1:0
		u32 dw[2];	//7:4,3:0
	} tkip_mic;		/* byte 39:32 */
	u32 next_frame_info;	//byte 43:40
	union {
		u32 life_time;
		u32 attemp_stop_time;
	} u;
	u8 supp_rates[2];	//byte 49:48
	u8 rts_retry_limit;	//byte 50
	u8 data_retry_limit;	//byte 51
	union {
		u16 pm_frame_timeout;
		u16 attemp_duration;
	} u2;
	u16 driver_txop;	//byte 55:54
	u8 payload[0];
	struct ieee80211_hdr hdr[0];
} __attribute__ ((packed));

/*
 * TX command response status
 */
enum {
	TX_STATUS_SUCCESS = 0x1,
	TX_STATUS_FAIL_SHORT_LIMIT = 0x82,
	TX_STATUS_FAIL_LONG_LIMIT = 0x83,
	TX_STATUS_FAIL_FIFO_UNDERRUN = 0x84,
	TX_STATUS_FAIL_MGMNT_ABORT = 0x85,
	TX_STATUS_FAIL_NEXT_FRAG = 0x86,
	TX_STATUS_FAIL_LIFE_EXPIRE = 0x87,
	TX_STATUS_FAIL_DEST_PS = 0x88,
	TX_STATUS_FAIL_ABORTED = 0x89,
	TX_STATUS_FAIL_BT_RETRY = 0x8a,
	TX_STATUS_FAIL_STA_INVALID = 0x8b,
	TX_STATUS_FAIL_FRAG_DROPPED = 0x8c,
	TX_STATUS_FAIL_TID_DISABLE = 0x8d,
	TX_STATUS_FAIL_FRAME_FLUSHED = 0x8e,
	TX_STATUS_FAIL_INSUFFICIENT_CF_POLL = 0x8f,
	TX_STATUS_FAIL_TX_LOCKED = 0x90,
	TX_STATUS_FAIL_NO_BEACON_ON_RADAR = 0x91,
};

enum {
	TX_PACKET_MODE_REGULAR = 0x0000,
	TX_PACKET_MODE_BURST_PART = 0x00100,
	TX_PACKET_MODE_BURST_FIRST = 0x0200,
};

enum {
	TX_POWER_PA_NOT_ACTIVE = 0x0,
};

enum {
	TX_STATUS_MSK = 0x000000ff,          /* bits 0:7 */
	TX_PACKET_MODE_MSK = 0x0000ff00,     /* bits 8:15 */
	TX_FIFO_NUMBER_MSK = 0x00070000,     /* bits 16:18 */
	TX_RESERVED = 0x00780000,            /* bits 19:22 */
	TX_POWER_PA_DETECT_MSK = 0x7f800000, /* bits 23:30 */
	TX_ABORT_REQUIRED_MSK = 0x80000000,  /* bits 31:31 */
};

struct ipw_tx_resp {
	u8 failure_rts;
	u8 failure_frame;
	u8 bt_kill_count;
	u8 rate;
	u32 wireless_media_time;
	u32 status;
} __attribute__ ((packed));


/* TX command response is sent after *all* transmission attempts.
 *
 * NOTES:
 *
 * TX_STATUS_FAIL_NEXT_FRAG
 *
 * If the fragment flag in the MAC header for the frame being transmitted
 * is set and there is insufficient time to transmit the next frame, the
 * TX status will be returned with 'TX_STATUS_FAIL_NEXT_FRAG'.
 *
 * TX_STATUS_FIFO_UNDERRUN
 *
 * Indicates the host did not provide bytes to the FIFO fast enough while
 * a TX was in progress.
 *
 * TX_STATUS_FAIL_MGMNT_ABORT
 *
 * This status is only possible if the ABORT ON MGMT RX parameter was
 * set to true with the TX command.
 *
 * If the MSB of the status parameter is set then an abort sequence is required.
 * This sequence consists of the host activating the TX Abort control line, and then
 * waiting for the TX Abort command response.  This indicates that a the device
 * is no longer in a transmit state, and that the command FIFO has been cleared.
 * The host must then deactivate the TX Abort control line.  Receiving is still
 * allowed in this case.
 */


/*
 * Tx Power Table Command
 */
struct ipw_tx_power {
	u8 tx_gain;		/* gain for analog radio */
	u8 dsp_atten;		/* gain for DSP */
} __attribute__ ((packed));

struct ipw_power_per_rate {
	u8 rate;
	struct ipw_tx_power tpc;
	u8 reserved;
} __attribute__ ((packed));

struct ipw_txpowertable_cmd {
	u8 band;
	u8 reserved;
	u16 channel;
	struct ipw_power_per_rate power[IPW_MAX_RATES];
} __attribute__ ((packed));
/*
 * Scan Request Commands , Responses  & Notifications
 */

/* Can abort will notify by complete notification with abort status. */
#define CAN_ABORT_STATUS        0x1

struct ipw_scan_channel {
	u8 type;
	/* type is defined as:
	 * 0:0 active (0 - passive)
	 * 1:4 SSID direct
	 *     If 1 is set then corresponding SSID IE is transmitted in probe
	 * 5:6 reserved
	 * 7:7 Narrow
	 */
	u8 channel;
	struct ipw_tx_power tpc;
	u16 active_dwell;
	u16 passive_dwell;
} __attribute__ ((packed));

struct ipw_ssid_ie {
	u8 id;
	u8 len;
	u8 ssid[32];
} __attribute__ ((packed));

#define PROBE_OPTION_MAX        0x4
#define TX_CMD_FLG_SEQ_CTL_MSK  0x2000
#define TX_CMD_LIFE_TIME_INFINITE       0xFFFFFFFF
#define IPW_GOOD_CRC_TH             (1)

#define IPW_MAX_SCAN_SIZE 1024
struct ipw_scan_cmd {
	u16 len;
	u8 reserved0;
	u8 channel_count;
	u16 quiet_time;		/* dwell only this long on quiet chnl (active scan) */
	u16 quiet_plcp_th;	/* quiet chnl is < this # pkts (typ. 1) */
	u16 good_CRC_th;	/* passive -> active promotion threshold */
	u16 reserved1;
	u32 max_out_time;	/* max msec to be out of associated (service) chnl */
	u32 suspend_time;	/* pause scan this long when returning to svc chnl */

	u32 flags;
	u32 filter_flags;

	struct ipw_tx_cmd tx_cmd;
	struct ipw_ssid_ie direct_scan[PROBE_OPTION_MAX];

	u8 data[0];
	/*
	   The channels start after the probe request payload and are of type:

	   struct ipw_scan_channel channels[0];

	   NOTE:  Only one band of channels can be scanned per pass.  You
	   can not mix 2.4Ghz channels and 5.2Ghz channels and must
	   request a scan multiple times (not concurrently)

	 */
} __attribute__ ((packed));

struct ipw_scanreq_notification {
	u32 status;
} __attribute__ ((packed));

struct ipw_scanstart_notification {
	u32 tsf_low;
	u32 tsf_high;
	u32 beacon_timer;
	u8 channel;
	u8 band;
	u8 reserved[2];
	u32 status;
} __attribute__ ((packed));

#define  SCAN_OWNER_STATUS 0x1;
#define  MEASURE_OWNER_STATUS 0x2;

#define NUMBER_OF_STATISTICS 1	// first DW is good CRC
struct ipw_scanresults_notification {
	u8 channel;
	u8 band;
	u8 reserved[2];
	u32 tsf_low;
	u32 tsf_high;
	u32 statistics[NUMBER_OF_STATISTICS];	//TBD
} __attribute__ ((packed));

struct ipw_scancomplete_notification {
	u8 scanned_channels;
	u8 status;
	u8 reserved;
	u8 last_channel;
	u32 tsf_low;
	u32 tsf_high;
} __attribute__ ((packed));

//complete notification statuses
#define ABORT_STATUS            0x2	// Abort status for scan finish notification

// **************************************************
// * Rate Scaling Command & Response
// **************************************************

// *****************************************
// * ofdm & cck rate codes
// *****************************************
#define R_6M 0xd
#define R_9M 0xf
#define R_12M 0x5
#define R_18M 0x7
#define R_24M 0x9
#define R_36M 0xb
#define R_48M 0x1
#define R_54M 0x3

#define R_1M 0xa
#define R_2M 0x14
#define R_5_5M 0x37
#define R_11M 0x6e

// OFDM rates mask values
#define RATE_SCALE_6M_INDEX  0
#define RATE_SCALE_9M_INDEX  1
#define RATE_SCALE_12M_INDEX 2
#define RATE_SCALE_18M_INDEX 3
#define RATE_SCALE_24M_INDEX 4
#define RATE_SCALE_36M_INDEX 5
#define RATE_SCALE_48M_INDEX 6
#define RATE_SCALE_54M_INDEX 7

// CCK rate mask values
#define RATE_SCALE_1M_INDEX   8
#define RATE_SCALE_2M_INDEX   9
#define RATE_SCALE_5_5M_INDEX 10
#define RATE_SCALE_11M_INDEX  11

/*  OFDM rates mask values */
enum {
	R_6M_MSK = (1 << 0),
	R_9M_MSK = (1 << 1),
	R_12M_MSK = (1 << 2),
	R_18M_MSK = (1 << 3),
	R_24M_MSK = (1 << 4),
	R_36M_MSK = (1 << 5),
	R_48M_MSK = (1 << 6),
	R_54M_MSK = (1 << 7),
};

/* CCK rate mask values */
enum {
	R_1M_MSK = (1 << 0),
	R_2M_MSK = (1 << 1),
	R_5_5M_MSK = (1 << 2),
	R_11M_MSK = (1 << 3),
};

struct ipw_rate_scaling_info {
	u8 tx_rate;
	u8 flags;
	u8 try_cnt;
	u8 next_rate_index;
} __attribute__ ((packed));

struct RateScalingCmdSpecifics {
	u8 table_id;
	u8 reserved[3];
	struct ipw_rate_scaling_info rate_scale_table[IPW_MAX_RATES];
} __attribute__ ((packed));

/*
 * LEDs Command & Response
 */
struct ipw_led_cmd {
	u32 interval;		// 4
	u8 id;			// 8
	u8 off;			// 9
	u8 on;			//10
	u8 reserved;		// 11
} __attribute__ ((packed));

/*
 * card_state Command and Notification
 */

#define CARD_STATE_CMD_DISABLE 0x00
#define CARD_STATE_CMD_ENABLE 0x01

struct ipw_card_state_notif {
	u32 flags;
} __attribute__ ((packed));

#define HW_CARD_DISABLED 0x01
#define SW_CARD_DISABLED 0x02

// **************************************************
// * TxBeacon Command & Response
// **************************************************

// Command Notification and Response Headers are Covered by the

// Beacon Notification
struct BeaconNtfSpecifics {
	struct ipw_tx_resp bconNotifHdr;	//15:4
	u32 lowTSF;		//19:16
	u32 highTSF;		//23:20
	u32 ibssMgrStatus;	//27:24
} __attribute__ ((packed));

// TxBeacon Command
struct ipw_tx_beacon_cmd {
	struct ipw_tx_cmd tx;;	//byte 55:4
	u16 tim_idx;		//byte 57:56
	u8 tim_size;		//byte 58
	u8 reserved1;		//byte 59
	struct ieee80211_hdr frame[0];
	// Beacon Frame
} __attribute__ ((packed));

// TxBeacon response

struct ipw_spectrum_notification {
	u16 reserved1;
	u8 reserved2;
	u8 state;		/* 0 - start, 1 - stop */
	u8 reserved3[96];
} __attribute__ ((packed));

struct ipw_csa_notification {
	u16 band;
	u16 channel;
	u32 status;		// 0 - OK, 1 - fail
} __attribute__ ((packed));

/*
 * Power Table Command & Response
 *
 * FLAGS
 *   PM allow:
 *   bit 0 - '0' Driver not allow power management
 *           '1' Driver allow PM (use rest of parameters)
 *   uCode send sleep notifications:
 *   bit 1 - '0' Don't send sleep notification
 *           '1' send sleep notification (SEND_PM_NOTIFICATION)
 *   Sleep over DTIM
 *   bit 2 - '0' PM have to walk up every DTIM
 *           '1' PM could sleep over DTIM till listen Interval.
 *   force sleep Modes
 *    bit 31/30- '00' use both mac/xtal sleeps
 *               '01' force Mac sleep
 *               '10' force xtal sleep
 *               '11' Illegal set
 * NOTE: if SleepInterval[SLEEP_INTRVL_TABLE_SIZE-1] > DTIM period then
 * ucode assume sleep over DTIM is allowed and we don't need to wakeup
 * for every DTIM.
 */
#define PMC_TCMD_SLEEP_INTRVL_TABLE_SIZE          5

#define PMC_TCMD_FLAG_DRIVER_ALLOW_SLEEP_MSK      0x1
#define PMC_TCMD_FLAG_SLEEP_OVER_DTIM_MSK         0x4

struct ipw_powertable_cmd {
	u32 flags;
	u32 RxDataTimeout;
	u32 TxDataTimeout;
	u32 SleepInterval[PMC_TCMD_SLEEP_INTRVL_TABLE_SIZE];
} __attribute__ ((packed));

struct ipw_sleep_notification {
	u8 pm_sleep_mode;
	u8 pm_wakeup_src;
	u16 reserved;
	u32 sleep_time;
	u32 tsf_low;
	u32 bcon_timer;
} __attribute__ ((packed));

enum {
	IPW_PM_NO_SLEEP = 0,
	IPW_PM_SLP_MAC = 1,
	IPW_PM_SLP_FULL_MAC_UNASSOCIATE = 2,
	IPW_PM_SLP_FULL_MAC_CARD_STATE = 3,
	IPW_PM_SLP_PHY = 4,
	IPW_PM_SLP_REPENT = 5,
	IPW_PM_WAKEUP_BY_TIMER = 6,
	IPW_PM_WAKEUP_BY_DRIVER = 7,
	IPW_PM_WAKEUP_BY_RFKILL = 8,
	/* 3 reserved */
	IPW_PM_NUM_OF_MODES = 12,
};

struct ipw_bt_cmd {
	u8 flags;
	u8 leadTime;
	u8 maxKill;
	u8 reserved;
	u32 killAckMask;
	u32 killCTSMask;
} __attribute__ ((packed));

struct rx_phy_statistics {
	u32 ina_cnt;		/* number of INA signal assertions (enter RX) */
	u32 fina_cnt;		/* number of FINA signal assertions
				 * (false_alarm = INA - FINA) */
	u32 plcp_err;		/* number of bad PLCP header detections
				 * (PLCP_good = FINA - PLCP_bad) */
	u32 crc32_err;		/* number of CRC32 error detections */
	u32 overrun_err;	/* number of Overrun detections (this is due
				 * to RXE sync overrun) */
	u32 early_overrun_err;	/* number of times RX is aborted at the
				 * begining because rxfifo is full behind
				 * threshold */
	u32 crc32_good;		/* number of frames with good CRC */
	u32 false_alarm_cnt;	/* number of times false alarm was
				 * detected (i.e. INA w/o FINA) */
	u32 fina_sync_err_cnt;	/* number of times sync problem between
				 * HW & SW FINA counter was found */
	u32 sfd_timeout;	/* number of times got SFD timeout
				 * (i.e. got FINA w/o rx_frame) */
	u32 fina_timeout;	/* number of times got FINA timeout (i.e. got
				 * INA w/o FINA, w/o false alarm) */
	u32 unresponded_rts;	/* un-responded RTS, due to NAV not zero */
	u32 rxe_frame_limit_overrun;	/* RXE got frame limit overrun */
	u32 sent_ack_cnt;	/* ACK TX count */
	u32 sent_cts_cnt;	/* CTS TX count */
} __attribute__ ((packed));

struct rx_non_phy_statistics {
	u32 bogus_cts;		/* CTS received when not expecting CTS */
	u32 bogus_ack;		/* ACK received when not expecting ACK */
	u32 non_bssid_frames;	/* number of frames with BSSID that doesn't
				 * belong to the STA BSSID */
	u32 filtered_frames;	/* count frames that were dumped in the
				 * filtering process */
} __attribute__ ((packed));

struct rx_statistics {
	struct rx_phy_statistics ofdm;
	struct rx_phy_statistics cck;
	struct rx_non_phy_statistics general;
} __attribute__ ((packed));

struct tx_non_phy_statistics {
	u32 preamble_cnt;	/* number of times preamble was asserted */
	u32 rx_detected_cnt;	/* number of times TX was delayed to RX
				 * detected */
	u32 bt_prio_defer_cnt;	/* number of times TX was deferred due to
				 * BT priority */
	u32 bt_prio_kill_cnt;	/* number of times TX was killed due to BT
				 * priority */
	u32 few_bytes_cnt;	/* number of times TX was delayed due to not
				 * enough bytes in TXFIFO */
	u32 cts_timeout;	/* timeout when waiting for CTS */
	u32 ack_timeout;	/* timeout when waiting for ACK */
	u32 expected_ack_cnt;	/* number of data frames that need ack or
				 * rts that need cts */
	u32 actual_ack_cnt;	/* number of expected ack or cts that were
				 * actually received */
} __attribute__ ((packed));

struct tx_statistics {
	struct tx_non_phy_statistics general;
} __attribute__ ((packed));

struct debug_statistics {
	u32 cont_burst_chk_cnt;	/* number of times continuation or
				 * fragmentation or bursting was checked */
	u32 cont_burst_cnt;	/* number of times continuation or fragmentation
				 * or bursting was successfull */
	u32 reserved[4];
} __attribute__ ((packed));

struct general_statistics {
	u32 temperature;
	struct debug_statistics debug;
	u32 usec_sleep;	  /**< usecs NIC was asleep. Running counter. */
	u32 slots_out;	   /**< slots NIC was out of serving channel */
	u32 slots_idle;	   /**< slots NIC was idle */
} __attribute__ ((packed));

// This struct is used as a reference for the driver.
// uCode is using global variables that are defined in
struct statistics {
	u32 flags;
	struct rx_statistics rx_statistics;
	struct tx_statistics tx_statistics;
	struct general_statistics general_statistics;
} __attribute__ ((packed));


//if ucode consecutively missed  beacons above CONSEQUTIVE_MISSED_BCONS_TH
//then this notification will be sent.
#define CONSEQUTIVE_MISSED_BCONS_TH 20

/* 3945ABG register and values */
/* base */
#define CSR_BASE    (0x0)
#define HBUS_BASE   (0x400)
#define FH_BASE     (0x800)

/*=== CSR (control and status registers) ===*/

#define CSR_HW_IF_CONFIG_REG    (CSR_BASE+0x000)
#define CSR_INT                 (CSR_BASE+0x008)
#define CSR_INT_MASK            (CSR_BASE+0x00c)
#define CSR_FH_INT_STATUS       (CSR_BASE+0x010)
#define CSR_GPIO_IN             (CSR_BASE+0x018)
#define CSR_RESET               (CSR_BASE+0x020)
#define CSR_GP_CNTRL            (CSR_BASE+0x024)
/* 0x028 - reserved */
#define CSR_EEPROM_REG          (CSR_BASE+0x02c)
#define CSR_EEPROM_GP           (CSR_BASE+0x030)
#define CSR_UCODE_DRV_GP1       (CSR_BASE+0x054)
#define CSR_UCODE_DRV_GP1_SET   (CSR_BASE+0x058)
#define CSR_UCODE_DRV_GP1_CLR   (CSR_BASE+0x05c)
#define CSR_UCODE_DRV_GP2       (CSR_BASE+0x060)
#define CSR_GIO_CHICKEN_BITS    (CSR_BASE+0x100)
#define CSR_ANA_PLL_CFG         (CSR_BASE+0x20c)

/* BSM (Bootstrap and SM low speed serial Bus) */
#define BSM_BASE                        (CSR_BASE + 0x3400)

#define BSM_WR_CTRL_REG                 (BSM_BASE + 0x000)
#define BSM_WR_MEM_SRC_REG              (BSM_BASE + 0x004)
#define BSM_WR_MEM_DST_REG              (BSM_BASE + 0x008)
#define BSM_WR_DWCOUNT_REG              (BSM_BASE + 0x00C)

#define BSM_DRAM_INST_PTR_REG           (BSM_BASE + 0x090)
#define BSM_DRAM_INST_BYTECOUNT_REG     (BSM_BASE + 0x094)
#define BSM_DRAM_DATA_PTR_REG           (BSM_BASE + 0x098)
#define BSM_DRAM_DATA_BYTECOUNT_REG     (BSM_BASE + 0x09C)

#define BSM_SRAM_LOWER_BOUND            (CSR_BASE + 0x3800)

/* DBG MON */

/* SCD */
#define SCD_BASE                        (CSR_BASE + 0x2E00)

#define SCD_MODE_REG                    (SCD_BASE + 0x000)
#define SCD_ARASTAT_REG                 (SCD_BASE + 0x004)
#define SCD_TXFACT_REG                  (SCD_BASE + 0x010)
#define SCD_TXF4MF_REG                  (SCD_BASE + 0x014)
#define SCD_TXF5MF_REG                  (SCD_BASE + 0x020)
#define SCD_SBYP_MODE_1_REG             (SCD_BASE + 0x02C)
#define SCD_SBYP_MODE_2_REG             (SCD_BASE + 0x030)

/*=== HBUS (Host-side bus) ===*/

#define HBUS_TARG_MEM_RADDR     (HBUS_BASE+0x00c)
#define HBUS_TARG_MEM_WADDR     (HBUS_BASE+0x010)
#define HBUS_TARG_MEM_WDAT      (HBUS_BASE+0x018)
#define HBUS_TARG_MEM_RDAT      (HBUS_BASE+0x01c)
#define HBUS_TARG_PRPH_WADDR    (HBUS_BASE+0x044)
#define HBUS_TARG_PRPH_RADDR    (HBUS_BASE+0x048)
#define HBUS_TARG_PRPH_WDAT     (HBUS_BASE+0x04c)
#define HBUS_TARG_PRPH_RDAT     (HBUS_BASE+0x050)
#define HBUS_TARG_WRPTR         (HBUS_BASE+0x060)
/*=== FH (data Flow handler) ===*/

#define FH_CBCC_TABLE           (FH_BASE+0x140)
#define FH_TFDB_TABLE           (FH_BASE+0x180)
#define FH_RCSR_TABLE           (FH_BASE+0x400)
#define FH_RSSR_TABLE           (FH_BASE+0x4c0)
#define FH_TCSR_TABLE           (FH_BASE+0x500)
#define FH_TSSR_TABLE           (FH_BASE+0x680)

/* TFDB (Transmit Frame Buffer Descriptor) */
#define FH_TFDB(_channel,buf)                    (FH_TFDB_TABLE+((_channel)*2+(buf))*0x28)
#define ALM_FH_TFDB_CHNL_BUF_CTRL_REG(_channel)  (FH_TFDB_TABLE + 0x50 * _channel)
/* CBCC _channel is [0,2] */
#define FH_CBCC(_channel)           (FH_CBCC_TABLE+(_channel)*0x8)
#define FH_CBCC_CTRL(_channel)      (FH_CBCC(_channel)+0x00)
#define FH_CBCC_BASE(_channel)      (FH_CBCC(_channel)+0x04)

/* RCSR _channel is [0,2] */
#define FH_RCSR(_channel)           (FH_RCSR_TABLE+(_channel)*0x40)
#define FH_RCSR_CONFIG(_channel)    (FH_RCSR(_channel)+0x00)
#define FH_RCSR_RBD_BASE(_channel)  (FH_RCSR(_channel)+0x04)
#define FH_RCSR_WPTR(_channel)      (FH_RCSR(_channel)+0x20)
#define FH_RCSR_RPTR_ADDR(_channel) (FH_RCSR(_channel)+0x24)
/* RSSR */
#define FH_RSSR_CTRL            (FH_RSSR_TABLE+0x000)
#define FH_RSSR_STATUS          (FH_RSSR_TABLE+0x004)
/* TCSR */
#define FH_TCSR(_channel)           (FH_TCSR_TABLE+(_channel)*0x20)
#define FH_TCSR_CONFIG(_channel)    (FH_TCSR(_channel)+0x00)
#define FH_TCSR_CREDIT(_channel)    (FH_TCSR(_channel)+0x04)
#define FH_TCSR_BUFF_STTS(_channel) (FH_TCSR(_channel)+0x08)
/* TSSR */
#define FH_TSSR_CBB_BASE        (FH_TSSR_TABLE+0x000)
#define FH_TSSR_MSG_CONFIG      (FH_TSSR_TABLE+0x008)
#define FH_TSSR_TX_STATUS       (FH_TSSR_TABLE+0x010)
/* 18 - reserved */

/* card static random access memory (SRAM) for processor data and instructs */
#define RTC_INST_LOWER_BOUND                                (0x00000)
#define ALM_RTC_INST_UPPER_BOUND                            (0x14000)

#define RTC_DATA_LOWER_BOUND                                (0x800000)
#define ALM_RTC_DATA_UPPER_BOUND                            (0x808000)

#define ALM_RTC_INST_SIZE           (ALM_RTC_INST_UPPER_BOUND - RTC_INST_LOWER_BOUND)
#define ALM_RTC_DATA_SIZE           (ALM_RTC_DATA_UPPER_BOUND - RTC_DATA_LOWER_BOUND)

#define VALID_RTC_DATA_ADDR(addr)               \
    ( ((addr) >= RTC_DATA_LOWER_BOUND) && ((addr) < ALM_RTC_DATA_UPPER_BOUND) )

/*=== Periphery ===*/

/* HW I/F configuration */
#define CSR_HW_IF_CONFIG_REG_BIT_ALMAGOR_MB         (0x00000100)
#define CSR_HW_IF_CONFIG_REG_BIT_ALMAGOR_MM         (0x00000200)
#define CSR_HW_IF_CONFIG_REG_BIT_SKU_MRC            (0x00000400)
#define CSR_HW_IF_CONFIG_REG_BIT_BOARD_TYPE         (0x00000800)
#define CSR_HW_IF_CONFIG_REG_BITS_SILICON_TYPE_A    (0x00000000)
#define CSR_HW_IF_CONFIG_REG_BITS_SILICON_TYPE_B    (0x00001000)

#define CSR_UCODE_DRV_GP1_BIT_MAC_SLEEP             (0x00000001)
#define CSR_UCODE_SW_BIT_RFKILL                     (0x00000002)
#define CSR_UCODE_DRV_GP1_BIT_CMD_BLOCKED           (0x00000004)

#define CSR_GPIO_IN_BIT_AUX_POWER                   (0x00000200)
#define CSR_GPIO_IN_VAL_VAUX_PWR_SRC                (0x00000000)
#define CSR_GIO_CHICKEN_BITS_REG_BIT_L1A_NO_L0S_RX  (0x00800000)
#define CSR_GPIO_IN_VAL_VMAIN_PWR_SRC               CSR_GPIO_IN_BIT_AUX_POWER

#define PCI_CFG_PMC_PME_FROM_D3COLD_SUPPORT         (0x80000000)
/*   interrupt flags */
#define BIT_INT_RX           (1<<31)
#define BIT_INT_SWERROR      (1<<25)
#define BIT_INT_ERR          (1<<29)
#define BIT_INT_TX           (1<<27)
#define BIT_INT_WAKEUP       (1<< 1)
#define BIT_INT_ALIVE        (1<<0)

#define CSR_INI_SET_MASK      ( BIT_INT_RX      |  \
                                BIT_INT_SWERROR |  \
                                BIT_INT_ERR     |  \
                                BIT_INT_TX      |  \
                                BIT_INT_ALIVE   |  \
                                BIT_INT_WAKEUP )

/* RESET */
#define CSR_RESET_REG_FLAG_NEVO_RESET                (0x00000001)
#define CSR_RESET_REG_FLAG_FORCE_NMI                 (0x00000002)
#define CSR_RESET_REG_FLAG_SW_RESET                  (0x00000080)
#define CSR_RESET_REG_FLAG_MASTER_DISABLED           (0x00000100)
#define CSR_RESET_REG_FLAG_STOP_MASTER               (0x00000200)

/* GP (general purpose) CONTROL */
#define CSR_GP_CNTRL_REG_FLAG_MAC_CLOCK_READY        (0x00000001)
#define CSR_GP_CNTRL_REG_FLAG_INIT_DONE              (0x00000004)
#define CSR_GP_CNTRL_REG_FLAG_MAC_ACCESS_REQ         (0x00000008)
#define CSR_GP_CNTRL_REG_FLAG_GOING_TO_SLEEP         (0x00000010)

#define CSR_GP_CNTRL_REG_VAL_MAC_ACCESS_EN          (0x00000001)

#define CSR_GP_CNTRL_REG_MSK_POWER_SAVE_TYPE        (0x07000000)
#define CSR_GP_CNTRL_REG_FLAG_MAC_POWER_SAVE         (0x04000000)

/* APMG (power management) constants */
#define APMG_CLK_CTRL_REG                        (0x003000)
#define ALM_APMG_CLK_EN                          (0x003004)
#define ALM_APMG_CLK_DIS                         (0x003008)
#define ALM_APMG_PS_CTL                          (0x00300c)
#define ALM_APMG_PCIDEV_STT                      (0x003010)
#define ALM_APMG_LARC_INT                        (0x00301c)
#define ALM_APMG_LARC_INT_MSK                    (0x003020)

#define APMG_CLK_REG_VAL_DMA_CLK_RQT                (0x00000200)
#define APMG_CLK_REG_VAL_BSM_CLK_RQT                (0x00000800)

#define APMG_PS_CTRL_REG_VAL_ALM_R_RESET_REQ        (0x04000000)

#define APMG_DEV_STATE_REG_VAL_L1_ACTIVE_DISABLE    (0x00000800)

#define APMG_PS_CTRL_REG_MSK_POWER_SRC              (0x03000000)
#define APMG_PS_CTRL_REG_VAL_POWER_SRC_VMAIN        (0x00000000)
#define APMG_PS_CTRL_REG_VAL_POWER_SRC_VAUX         (0x01000000)
/* BSM (bootstrap and SM low-speed serial bus) */
#define BSM_WR_CTRL_REG_BIT_START_EN                (0x40000000)

/* DBM */

#define ALM_FH_SRVC_CHNL                            (6)

#define ALM_FH_RCSR_RX_CONFIG_REG_POS_RBDC_SIZE     (20)
#define ALM_FH_RCSR_RX_CONFIG_REG_POS_IRQ_RBTH      (4)

#define ALM_FH_RCSR_RX_CONFIG_REG_BIT_WR_STTS_EN    (0x08000000)

#define ALM_FH_RCSR_RX_CONFIG_REG_VAL_DMA_CHNL_EN_ENABLE        (0x80000000)

#define ALM_FH_RCSR_RX_CONFIG_REG_VAL_RDRBD_EN_ENABLE           (0x20000000)

#define ALM_FH_RCSR_RX_CONFIG_REG_VAL_MAX_FRAG_SIZE_128         (0x01000000)

#define ALM_FH_RCSR_RX_CONFIG_REG_VAL_IRQ_DEST_INT_HOST         (0x00001000)

#define ALM_FH_RCSR_RX_CONFIG_REG_VAL_MSG_MODE_FH               (0x00000000)

#define ALM_FH_TCSR_TX_CONFIG_REG_VAL_MSG_MODE_TXF              (0x00000000)
#define ALM_FH_TCSR_TX_CONFIG_REG_VAL_MSG_MODE_DRIVER           (0x00000001)

#define ALM_FH_TCSR_TX_CONFIG_REG_VAL_DMA_CREDIT_DISABLE_VAL    (0x00000000)
#define ALM_FH_TCSR_TX_CONFIG_REG_VAL_DMA_CREDIT_ENABLE_VAL     (0x00000008)

#define ALM_FH_TCSR_TX_CONFIG_REG_VAL_CIRQ_HOST_IFTFD           (0x00200000)

#define ALM_FH_TCSR_TX_CONFIG_REG_VAL_CIRQ_RTC_NOINT            (0x00000000)

#define ALM_FH_TCSR_TX_CONFIG_REG_VAL_DMA_CHNL_PAUSE            (0x00000000)
#define ALM_FH_TCSR_TX_CONFIG_REG_VAL_DMA_CHNL_ENABLE           (0x80000000)

#define ALM_FH_TCSR_CHNL_TX_BUF_STS_REG_VAL_TFDB_VALID          (0x00004000)

#define ALM_FH_TCSR_CHNL_TX_BUF_STS_REG_BIT_TFDB_WPTR           (0x00000001)

#define ALM_FH_TSSR_TX_MSG_CONFIG_REG_VAL_SNOOP_RD_TXPD_ON      (0xFF000000)
#define ALM_FH_TSSR_TX_MSG_CONFIG_REG_VAL_ORDER_RD_TXPD_ON      (0x00FF0000)

#define ALM_FH_TSSR_TX_MSG_CONFIG_REG_VAL_MAX_FRAG_SIZE_128B    (0x00000400)

#define ALM_FH_TSSR_TX_MSG_CONFIG_REG_VAL_SNOOP_RD_TFD_ON       (0x00000100)
#define ALM_FH_TSSR_TX_MSG_CONFIG_REG_VAL_ORDER_RD_CBB_ON       (0x00000080)

#define ALM_FH_TSSR_TX_MSG_CONFIG_REG_VAL_ORDER_RSP_WAIT_TH     (0x00000020)
#define ALM_FH_TSSR_TX_MSG_CONFIG_REG_VAL_RSP_WAIT_TH           (0x00000005)

#define ALM_TB_MAX_BYTES_COUNT      (0xFFF0)

#define ALM_FH_TSSR_TX_STATUS_REG_BIT_BUFS_EMPTY(_channel)         ((1LU << _channel) << 24)
#define ALM_FH_TSSR_TX_STATUS_REG_BIT_NO_PEND_REQ(_channel)        ((1LU << _channel) << 16)

#define ALM_FH_TSSR_TX_STATUS_REG_MSK_CHNL_IDLE(_channel)          (ALM_FH_TSSR_TX_STATUS_REG_BIT_BUFS_EMPTY(_channel) | \
                                                                 ALM_FH_TSSR_TX_STATUS_REG_BIT_NO_PEND_REQ(_channel))
#define PCI_CFG_REV_ID_BIT_BASIC_SKU                (0x40)	/* bit 6    */
#define PCI_CFG_REV_ID_BIT_RTP                      (0x80)	/* bit 7    */

#define TFD_QUEUE_MIN           0
#define TFD_QUEUE_MAX           6
#define TFD_QUEUE_SIZE_MAX      (256)

/* spectrum and channel data structures */
#define IPW_NUM_SCAN_RATES         (2)

/* eeprom channel flags */
enum {
	IPW_CHANNEL_VALID = (1<<0),	/* legally usable for this SKU/geo */
	IPW_CHANNEL_IBSS = (1<<1),	/* usable as an IBSS channel */
	/* bit 2 reserved */
	IPW_CHANNEL_ACTIVE = (1<<3),	/* active scanning allowed */
	IPW_CHANNEL_RADAR = (1<<4),	/* radar detection required */
	IPW_CHANNEL_WIDE = (1<<5),
	IPW_CHANNEL_NARROW = (1<<6),
	IPW_CHANNEL_DFS = (1<<7),	/* dynamic freq selection candidate */
};

#define IPW_SCAN_FLAG_24GHZ  (1<<0)
#define IPW_SCAN_FLAG_52GHZ  (1<<1)
#define IPW_SCAN_FLAG_ACTIVE (1<<2)
#define IPW_SCAN_FLAG_DIRECT (1<<3)

#define IPW_MAX_CMD_SIZE 1024

/* *regulatory* channel data from eeprom, one for each channel */
struct ipw_eeprom_channel {
	u8 flags;		/* flags copied from EEPROM */
	s8 max_power_avg;	/* max power (dBm) on this chnl, limit 31 */
} __attribute__ ((packed));

/*
 * Mapping of a Tx power level, at factory calibration temperature,
 *   to a radio/DSP gain table index.
 * One for each of 5 "sample" power levels in each band.
 * v_det is measured at the factory, using the 3945's built-in power amplifier
 *   (PA) output voltage detector.  This same detector is used during Tx of long
 *   packets in normal operation to provide feedback as to proper output level.
 * Data copied from EEPROM.
 */
struct ipw_eeprom_txpower_sample {
	u8 gain_index;		/* index into power (gain) setup table ... */
	s8 power;		/* ... for this pwr level for this chnl group */
	u16 v_det;		/* PA output voltage */
} __attribute__ ((packed));

/*
 * Mappings of Tx power levels -> nominal radio/DSP gain table indexes.
 * One for each channel group (a.k.a. "band") (1 for BG, 4 for A).
 * Tx power setup code interpolates between the 5 "sample" power levels
 *    to determine the nominal setup for a requested power level.
 * Data copied from EEPROM.
 * DO NOT ALTER THIS STRUCTURE!!!
 */
struct ipw_eeprom_txpower_group {
	struct ipw_eeprom_txpower_sample samples[5];	/* 5 power levels */
	s32 a, b, c, d, e;	/* coefficients for voltage->power formula */
	s32 Fa, Fb, Fc, Fd, Fe;	/* these modify coeffs based on frequency */
	s8 saturation_power;	/* highest power possible by h/w in this band */
	u8 group_channel;	/* "representative" channel # in this band */
	s16 temperature;	/* h/w temperature at factory calib this band */
} __attribute__ ((packed));

/*
 * Temperature-based Tx-power compensation data, not band-specific.
 * These coefficients are use to modify a/b/c/d/e coeffs based on
 *   difference between current temperature and factory calib temperature.
 * Data copied from EEPROM.
 */
struct ipw_eeprom_temperature_corr {
	s32 Ta;
	s32 Tb;
	s32 Tc;
	s32 Td;
	s32 Te;
} __attribute__ ((packed));

struct ipw_eeprom {
	u8 reserved0[42];
#define EEPROM_MAC_ADDRESS                  (2*0x15)	/* 6  bytes */
	u8 mac_address[6];	/* abs.ofs: 42 */
	u8 reserved1[58];
#define EEPROM_BOARD_REVISION               (2*0x35)	/* 2  bytes */
	u16 board_revision;	/* abs.ofs: 106 */
	u8 reserved2[11];
#define EEPROM_BOARD_PBA_NUMBER             (2*0x3B+1)	/* 9  bytes */
	u8 board_pba_number[9];	/* abs.ofs: 119 */
	u8 reserved3[8];
#define EEPROM_VERSION                      (2*0x44)	/* 2  bytes */
	u16 version;		/* abs.ofs: 136 */
#define EEPROM_SKU_CAP                      (2*0x45)	/* 1  bytes */
	u8 sku_cap;		/* abs.ofs: 138 */
#define EEPROM_LEDS_MODE                    (2*0x45+1)	/* 1  bytes */
	u8 leds_mode;		/* abs.ofs: 139 */
	u8 reserved4[4];
#define EEPROM_LEDS_TIME_INTERVAL           (2*0x48)	/* 2  bytes */
	u16 leds_time_interval;	/* abs.ofs: 144 */
#define EEPROM_LEDS_OFF_TIME                (2*0x49)	/* 1  bytes */
	u8 leds_off_time;	/* abs.ofs: 146 */
#define EEPROM_LEDS_ON_TIME                 (2*0x49+1)	/* 1  bytes */
	u8 leds_on_time;	/* abs.ofs: 147 */
#define EEPROM_ALMGOR_M_VERSION             (2*0x4A)	/* 1  bytes */
	u8 almgor_m_version;	/* abs.ofs: 148 */
#define EEPROM_ANTENNA_SWITCH_TYPE          (2*0x4A+1)	/* 1  bytes */
	u8 antenna_switch_type;	/* abs.ofs: 149 */
	u8 reserved5[42];
#define EEPROM_REGULATORY_SKU_ID            (2*0x60)	/* 4  bytes */
	u8 sku_id[4];		/* abs.ofs: 192 */
#define EEPROM_REGULATORY_BAND_1            (2*0x62)	/* 2  bytes */
	u16 band_1_count;	/* abs.ofs: 196 */
#define EEPROM_REGULATORY_BAND_1_CHANNELS   (2*0x63)	/* 28 bytes */
	struct ipw_eeprom_channel band_1_channels[14];	/* abs.ofs: 196 */
#define EEPROM_REGULATORY_BAND_2            (2*0x71)	/* 2  bytes */
	u16 band_2_count;	/* abs.ofs: 226 */
#define EEPROM_REGULATORY_BAND_2_CHANNELS   (2*0x72)	/* 26 bytes */
	struct ipw_eeprom_channel band_2_channels[13];	/* abs.ofs: 228 */
#define EEPROM_REGULATORY_BAND_3            (2*0x7F)	/* 2  bytes */
	u16 band_3_count;	/* abs.ofs: 254 */
#define EEPROM_REGULATORY_BAND_3_CHANNELS   (2*0x80)	/* 24 bytes */
	struct ipw_eeprom_channel band_3_channels[12];	/* abs.ofs: 256 */
#define EEPROM_REGULATORY_BAND_4            (2*0x8C)	/* 2  bytes */
	u16 band_4_count;	/* abs.ofs: 280 */
#define EEPROM_REGULATORY_BAND_4_CHANNELS   (2*0x8D)	/* 22 bytes */
	struct ipw_eeprom_channel band_4_channels[11];	/* abs.ofs: 282 */
#define EEPROM_REGULATORY_BAND_5            (2*0x98)	/* 2  bytes */
	u16 band_5_count;	/* abs.ofs: 304 */
#define EEPROM_REGULATORY_BAND_5_CHANNELS   (2*0x99)	/* 12 bytes */
	struct ipw_eeprom_channel band_5_channels[6];	/* abs.ofs: 306 */
	u8 reserved6[194];
#define EEPROM_TXPOWER_CALIB_GROUP0 0x200
#define EEPROM_TXPOWER_CALIB_GROUP1 0x240
#define EEPROM_TXPOWER_CALIB_GROUP2 0x280
#define EEPROM_TXPOWER_CALIB_GROUP3 0x2c0
#define EEPROM_TXPOWER_CALIB_GROUP4 0x300
#define IPW_NUM_TX_CALIB_GROUPS 5
	struct ipw_eeprom_txpower_group groups[IPW_NUM_TX_CALIB_GROUPS];/* abs.ofs: 512 */
#define EEPROM_CALIB_TEMPERATURE_CORRECT 0x340
	struct ipw_eeprom_temperature_corr corrections;	/* abs.ofs: 832 */
	u8 reserved7[172];	/* fill out to full 1024 byte block */

} __attribute__ ((packed));

/* EEPROM field values */

/* EEPROM field lengths */
#define EEPROM_BOARD_PBA_NUMBER_LENTGH                  11

/* EEPROM field lengths */
#define EEPROM_BOARD_PBA_NUMBER_LENTGH                  11
#define EEPROM_REGULATORY_SKU_ID_LENGTH                 4
#define EEPROM_REGULATORY_BAND1_CHANNELS_LENGTH         14
#define EEPROM_REGULATORY_BAND2_CHANNELS_LENGTH         13
#define EEPROM_REGULATORY_BAND3_CHANNELS_LENGTH         12
#define EEPROM_REGULATORY_BAND4_CHANNELS_LENGTH         11
#define EEPROM_REGULATORY_BAND5_CHANNELS_LENGTH         6
#define EEPROM_REGULATORY_CHANNELS_LENGTH ( \
EEPROM_REGULATORY_BAND1_CHANNELS_LENGTH         +\
EEPROM_REGULATORY_BAND2_CHANNELS_LENGTH         +\
EEPROM_REGULATORY_BAND3_CHANNELS_LENGTH         +\
EEPROM_REGULATORY_BAND4_CHANNELS_LENGTH         +\
EEPROM_REGULATORY_BAND5_CHANNELS_LENGTH)

#define EEPROM_REGULATORY_NUMBER_OF_BANDS               5

/* SKU Capabilities */
#define EEPROM_SKU_CAP_SW_RF_KILL_ENABLE                (1 << 0)
#define EEPROM_SKU_CAP_HW_RF_KILL_ENABLE                (1 << 1)
#define EEPROM_SKU_CAP_OP_MODE_MRC                      (1 << 7)

/* LEDs mode */

#define IPW_DEFAULT_TX_RETRY  15
#define IPW_MAX_TX_RETRY      16

/*********************************************/

#define RFD_SIZE                              4
#define NUM_TFD_CHUNKS                        4

#define RX_QUEUE_SIZE                        64
#define RX_QUEUE_SIZE_LOG                     6

/*
 * TX Queue Flag Definitions
 */

/* abort attempt if mgmt frame is rx'd */

/* require CTS */

/* use short preamble */
#define DCT_FLAG_LONG_PREAMBLE             0x00
#define DCT_FLAG_SHORT_PREAMBLE            0x04

/* RTS/CTS first */

/* dont calculate duration field */

/* even if MAC WEP set (allows pre-encrypt) */
#define IPW_
/* overwrite TSF field */

/* ACK rx is expected to follow */
#define DCT_FLAG_ACK_REQD                  0x80

#define IPW_MB_DISASSOCIATE_THRESHOLD_DEFAULT           24
#define IPW_MB_ROAMING_THRESHOLD_DEFAULT                8
#define IPW_REAL_RATE_RX_PACKET_THRESHOLD               300

/* QoS  definitions */

#define CW_MIN_OFDM          15
#define CW_MAX_OFDM          1023
#define CW_MIN_CCK           31
#define CW_MAX_CCK           1023

#define QOS_TX0_CW_MIN_OFDM      CW_MIN_OFDM
#define QOS_TX1_CW_MIN_OFDM      CW_MIN_OFDM
#define QOS_TX2_CW_MIN_OFDM      ( (CW_MIN_OFDM + 1) / 2 - 1 )
#define QOS_TX3_CW_MIN_OFDM      ( (CW_MIN_OFDM + 1) / 4 - 1 )

#define QOS_TX0_CW_MIN_CCK       CW_MIN_CCK
#define QOS_TX1_CW_MIN_CCK       CW_MIN_CCK
#define QOS_TX2_CW_MIN_CCK       ( (CW_MIN_CCK + 1) / 2 - 1 )
#define QOS_TX3_CW_MIN_CCK       ( (CW_MIN_CCK + 1) / 4 - 1 )

#define QOS_TX0_CW_MAX_OFDM      CW_MAX_OFDM
#define QOS_TX1_CW_MAX_OFDM      CW_MAX_OFDM
#define QOS_TX2_CW_MAX_OFDM      CW_MIN_OFDM
#define QOS_TX3_CW_MAX_OFDM      ( (CW_MIN_OFDM + 1) / 2 - 1 )

#define QOS_TX0_CW_MAX_CCK       CW_MAX_CCK
#define QOS_TX1_CW_MAX_CCK       CW_MAX_CCK
#define QOS_TX2_CW_MAX_CCK       CW_MIN_CCK
#define QOS_TX3_CW_MAX_CCK       ( (CW_MIN_CCK + 1) / 2 - 1 )

#define QOS_TX0_AIFS            (3)
#define QOS_TX1_AIFS            (7)
#define QOS_TX2_AIFS            (2)
#define QOS_TX3_AIFS            (2)

#define QOS_TX0_ACM             0
#define QOS_TX1_ACM             0
#define QOS_TX2_ACM             0
#define QOS_TX3_ACM             0

#define QOS_TX0_TXOP_LIMIT_CCK          0
#define QOS_TX1_TXOP_LIMIT_CCK          0
#define QOS_TX2_TXOP_LIMIT_CCK          6016
#define QOS_TX3_TXOP_LIMIT_CCK          3264

#define QOS_TX0_TXOP_LIMIT_OFDM      0
#define QOS_TX1_TXOP_LIMIT_OFDM      0
#define QOS_TX2_TXOP_LIMIT_OFDM      3008
#define QOS_TX3_TXOP_LIMIT_OFDM      1504

#define DEF_TX0_CW_MIN_OFDM      CW_MIN_OFDM
#define DEF_TX1_CW_MIN_OFDM      CW_MIN_OFDM
#define DEF_TX2_CW_MIN_OFDM      CW_MIN_OFDM
#define DEF_TX3_CW_MIN_OFDM      CW_MIN_OFDM

#define DEF_TX0_CW_MIN_CCK       CW_MIN_CCK
#define DEF_TX1_CW_MIN_CCK       CW_MIN_CCK
#define DEF_TX2_CW_MIN_CCK       CW_MIN_CCK
#define DEF_TX3_CW_MIN_CCK       CW_MIN_CCK

#define DEF_TX0_CW_MAX_OFDM      CW_MAX_OFDM
#define DEF_TX1_CW_MAX_OFDM      CW_MAX_OFDM
#define DEF_TX2_CW_MAX_OFDM      CW_MAX_OFDM
#define DEF_TX3_CW_MAX_OFDM      CW_MAX_OFDM

#define DEF_TX0_CW_MAX_CCK       CW_MAX_CCK
#define DEF_TX1_CW_MAX_CCK       CW_MAX_CCK
#define DEF_TX2_CW_MAX_CCK       CW_MAX_CCK
#define DEF_TX3_CW_MAX_CCK       CW_MAX_CCK

#define DEF_TX0_AIFS            (2)
#define DEF_TX1_AIFS            (2)
#define DEF_TX2_AIFS            (2)
#define DEF_TX3_AIFS            (2)

#define DEF_TX0_ACM             0
#define DEF_TX1_ACM             0
#define DEF_TX2_ACM             0
#define DEF_TX3_ACM             0

#define DEF_TX0_TXOP_LIMIT_CCK        0
#define DEF_TX1_TXOP_LIMIT_CCK        0
#define DEF_TX2_TXOP_LIMIT_CCK        0
#define DEF_TX3_TXOP_LIMIT_CCK        0

#define DEF_TX0_TXOP_LIMIT_OFDM       0
#define DEF_TX1_TXOP_LIMIT_OFDM       0
#define DEF_TX2_TXOP_LIMIT_OFDM       0
#define DEF_TX3_TXOP_LIMIT_OFDM       0

#define QOS_QOS_SETS                  3
#define QOS_PARAM_SET_ACTIVE          0
#define QOS_PARAM_SET_DEF_CCK         1
#define QOS_PARAM_SET_DEF_OFDM        2

#define CTRL_QOS_NO_ACK               (0x0020)
#define DCT_FLAG_EXT_QOS_ENABLED      (0x10)

#define IPW_TX_QUEUE_1        1
#define IPW_TX_QUEUE_2        2
#define IPW_TX_QUEUE_3        3
#define IPW_TX_QUEUE_4        4

#define EEPROM_IMAGE_SIZE              (0x200 * sizeof(u16))
#define U32_PAD(n)                     ((4-(n%4))%4)

#define AC_BE_TID_MASK 0x9	//TID 0 and 3
#define AC_BK_TID_MASK 0x6	//TID 1 and 2

/* QoS sturctures */
struct ipw_qos_info {
	int qos_enable;
	struct ieee80211_qos_parameters *def_qos_parm_OFDM;
	struct ieee80211_qos_parameters *def_qos_parm_CCK;
	u32 burst_duration_CCK;
	u32 burst_duration_OFDM;
	u16 qos_no_ack_mask;
	int burst_enable;
};

/**************************************************************/
/**
 * Generic queue structure
 *
 * Contains common data for Rx and Tx queues
 */
struct ipw_queue {
	int n_bd;		       /**< number of BDs in this queue */
	int first_empty;	       /**< 1-st empty entry (index) */
	int last_used;		       /**< last used entry (index) */
	dma_addr_t dma_addr;		/**< physical addr for BD's */
	int n_window;
	u32 id;
	int low_mark;		       /**< low watermark, resume queue if free space more than this */
	int high_mark;		       /**< high watermark, stop queue if free space less than this */
} __attribute__ ((packed));

#define TFD_CTL_COUNT_SET(n)       (n<<24)
#define TFD_CTL_COUNT_GET(ctl)     ((ctl>>24) & 7)
#define TFD_CTL_PAD_SET(n)         (n<<28)
#define TFD_CTL_PAD_GET(ctl)       (ctl>>28)

struct tfd_frame_data {
	u32 addr;
	u32 len;
} __attribute__ ((packed));

struct tfd_frame {
	u32 control_flags;
	struct tfd_frame_data pa[4];
	u8 reserved[28];
} __attribute__ ((packed));

#define SEQ_TO_FIFO(x)  ((x >> 8) & 0xbf)
#define FIFO_TO_SEQ(x)  ((x & 0xbf) << 8)
#define SEQ_TO_INDEX(x) (x & 0xff)
#define INDEX_TO_SEQ(x) (x & 0xff)
#define SEQ_HUGE_FRAME  (0x4000)
#define SEQ_RX_FRAME    (0x8000)

#define TFD_TX_CMD_SLOTS 64
#define TFD_CMD_SLOTS 32



#define TFD_MAX_PAYLOAD_SIZE (sizeof(struct ipw_cmd) - \
                              sizeof(struct ipw_cmd_meta))

/*
 * RX related structures and functions
 */
#define RX_FREE_BUFFERS 64
#define RX_LOW_WATERMARK 8

#define SUP_RATE_11A_MAX_NUM_CHANNELS  8
#define SUP_RATE_11B_MAX_NUM_CHANNELS  4
#define SUP_RATE_11G_MAX_NUM_CHANNELS  12

struct ipw_cmd_header {
	u8 cmd;
	u8 flags;
	/* We have 15 LSB to use as we please (MSB indicates
	 * a frame Rx'd from the HW).  We encode the following
	 * information into the sequence field:
	 *
	 *  0:7    index in fifo
	 *  8:13   fifo selection
	 * 14:14   bit indicating if this packet references the 'extra'
	 *         storage at the end of the memory queue
	 * 15:15   (Rx indication)
	 *
	 */
	u16 sequence;

	/* command data follows immediately */
	u8 data[0];
} __attribute__ ((packed));

// Used for passing to driver number of successes and failures per rate
struct rate_histogram {
	union {
		u32 a[SUP_RATE_11A_MAX_NUM_CHANNELS];
		u32 b[SUP_RATE_11B_MAX_NUM_CHANNELS];
		u32 g[SUP_RATE_11G_MAX_NUM_CHANNELS];
	} success;
	union {
		u32 a[SUP_RATE_11A_MAX_NUM_CHANNELS];
		u32 b[SUP_RATE_11B_MAX_NUM_CHANNELS];
		u32 g[SUP_RATE_11G_MAX_NUM_CHANNELS];
	} failed;
} __attribute__ ((packed));

/* statistics command response */

struct statistics_rx_phy {
	u32 ina_cnt;
	u32 fina_cnt;
	u32 plcp_err;
	u32 crc32_err;
	u32 overrun_err;
	u32 early_overrun_err;
	u32 crc32_good;
	u32 false_alarm_cnt;
	u32 fina_sync_err_cnt;
	u32 sfd_timeout;
	u32 fina_timeout;
	u32 unresponded_rts;
	u32 rxe_frame_limit_overrun;
	u32 sent_ack_cnt;
	u32 sent_cts_cnt;
} __attribute__ ((packed));

struct statistics_rx {
	struct statistics_rx_phy ofdm;
	struct statistics_rx_phy cck;
	u32 bogus_cts;
	u32 bogus_ack;
	u32 non_bssid_frames;
	u32 filtered_frames;
	u32 non_channel_beacons;
} __attribute__ ((packed));

struct statistics_tx {
	u32 preamble_cnt;
	u32 rx_detected_cnt;
	u32 bt_prio_defer_cnt;
	u32 bt_prio_kill_cnt;
	u32 few_bytes_cnt;
	u32 cts_timeout;
	u32 ack_timeout;
	u32 expected_ack_cnt;
	u32 actual_ack_cnt;
} __attribute__ ((packed));

struct statistics_dbg {
	u32 burst_check;
	u32 burst_count;
	u32 reserved[4];
} __attribute__ ((packed));

struct statistics_div {
	u32 tx_on_a;
	u32 tx_on_b;
	u32 exec_time;
	u32 probe_time;
} __attribute__ ((packed));

struct statistics_general {
	u32 temperature;
	struct statistics_dbg dbg;
	u32 sleep_time;
	u32 slots_out;
	u32 slots_idle;
	u32 ttl_timestamp;
	struct statistics_div div;
} __attribute__ ((packed));

struct ipw_notif_statistics {
	u32 flag;
	struct statistics_rx rx;
	struct statistics_tx tx;
	struct statistics_general general;
} __attribute__ ((packed));

struct ipw_rx_packet {
	u32 len;
	struct ipw_cmd_header hdr;
	union {
		struct ipw_alive_resp alive_frame;
		struct ipw_rx_frame rx_frame;
		struct ipw_tx_resp tx_resp;
		struct ipw_spectrum_notification spectrum_notif;
		struct ipw_csa_notification csa_notif;
		struct ipw_error_resp err_resp;
		struct ipw_card_state_notif card_state_notif;
		struct ipw_notif_statistics stats;
		struct BeaconNtfSpecifics beacon_status;
		struct ipw_add_sta_resp add_sta;
		struct ipw_sleep_notification sleep_notif;
		u32 status;
		u8 raw[0];
	} u;
} __attribute__ ((packed));

#define IPW_RX_FRAME_SIZE        (4 + sizeof(struct ipw_rx_frame))

struct ipw_multicast_addr {
	u8 num_of_multicast_addresses;
	u8 reserved[3];
	u8 mac1[6];
	u8 mac2[6];
	u8 mac3[6];
	u8 mac4[6];
} __attribute__ ((packed));

struct ipw_tgi_tx_key {
	u8 key_id;
	u8 security_type;
	u8 station_index;
	u8 flags;
	u8 key[16];
	u32 tx_counter[2];
} __attribute__ ((packed));

struct ipw_associate {
	u8 channel;
	u8 auth_type:4, auth_key:4;
	u8 assoc_type;
	u8 reserved;
	u16 policy_support;
	u8 preamble_length;
	u8 ieee_mode;
	u8 bssid[ETH_ALEN];
	u32 assoc_tsf_msw;
	u32 assoc_tsf_lsw;
	u16 capability;
	u16 listen_interval;
	u16 beacon_interval;
	u8 dest[ETH_ALEN];
	u16 atim_window;
	u8 smr;
	u8 reserved1;
	u16 reserved2;
	u16 assoc_id;
	u8 erp_value;
} __attribute__ ((packed));

#define IPW_SUPPORTED_RATES_IE_LEN         8

struct ipw_supported_rates {
	u8 ieee_mode;
	u8 num_rates;
	u8 purpose;
	u8 reserved;
	u8 supported_rates[IPW_MAX_RATES];
} __attribute__ ((packed));

struct ipw_channel_tx_power {
	u8 channel_number;
	s8 tx_power;
} __attribute__ ((packed));


#define IPW_RX_BUF_SIZE 3000
#define IPW_MAX_BSM_SIZE ALM_RTC_INST_SIZE
#define IPW_MAX_INST_SIZE ALM_RTC_INST_SIZE
#define IPW_MAX_DATA_SIZE ALM_RTC_DATA_SIZE

/* BSM (Bootstrap State Machine) */
#define BSM_BASE                     (CSR_BASE + 0x3400)

#define BSM_WR_CTRL_REG              (BSM_BASE + 0x000) /* ctl and status */
#define BSM_WR_MEM_SRC_REG           (BSM_BASE + 0x004) /* source in BSM mem */
#define BSM_WR_MEM_DST_REG           (BSM_BASE + 0x008) /* dest in SRAM mem */
#define BSM_WR_DWCOUNT_REG           (BSM_BASE + 0x00C) /* bytes */
#define BSM_WR_STATUS_REG            (BSM_BASE + 0x010) /* bit 0:  1 == done */

/* pointers and size regs for bootstrap load and data SRAM save */
#define BSM_DRAM_INST_PTR_REG        (BSM_BASE + 0x090)
#define BSM_DRAM_INST_BYTECOUNT_REG  (BSM_BASE + 0x094)
#define BSM_DRAM_DATA_PTR_REG        (BSM_BASE + 0x098)
#define BSM_DRAM_DATA_BYTECOUNT_REG  (BSM_BASE + 0x09C)

/* BSM special memory, stays powered during power-save sleeps */
#define BSM_SRAM_LOWER_BOUND         (CSR_BASE + 0x3800)
#define BSM_SRAM_SIZE			(1024)
/* BSM (bootstrap state machine) */
#define BSM_WR_CTRL_REG_BIT_START     (0x80000000) /* start boot load now */
#define BSM_WR_CTRL_REG_BIT_START_EN  (0x40000000) /* enable boot after pwrup*/

#endif				/* __iwlwifi_hw_h__ */
