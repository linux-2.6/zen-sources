/******************************************************************************

  Copyright(c) 2003 - 2007 Intel Corporation. All rights reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along with
  this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110, USA

  The full GNU General Public License is included in this distribution in the
  file called LICENSE.

  Contact Information:
  James P. Ketrenos <ipw2100-admin@linux.intel.com>
  Intel Corporation, 5200 N.E. Elam Young Parkway, Hillsboro, OR 97124-6497

******************************************************************************/

/*
 *
 * About the threading and lock model of the driver...
 *
 * There are three paths of execution through the driver.
 *
 * 1.  ioctl based (wireless extensions, netdev, etc.)
 * 2.  interrupt based
 * 3.  scheduled work queue items
 *
 * As soon as an interrupt comes in, it schedules a tasklet.  That tasklet,
 * when run, does any HW checks, pulls any data from the read queue,
 * and schedules other layers to do the actual work.
 *
 *
 */
#include <net/ieee80211.h>
#include <net/ieee80211_radiotap.h>
#include <asm/div64.h>
#include <net/iw_handler.h>

#include "ipwraw.h"

static inline int frame_get_hdrlen(u16 fc)
{
	int hdrlen = IEEE80211_3ADDR_LEN;
	u16 stype = WLAN_FC_GET_STYPE(fc);

	switch (WLAN_FC_GET_TYPE(fc)) {
	case IEEE80211_FTYPE_DATA:
		if ((fc & IEEE80211_FCTL_FROMDS) && (fc & IEEE80211_FCTL_TODS))
			hdrlen = IEEE80211_4ADDR_LEN;
		if (stype & IEEE80211_STYPE_QOS_DATA)
			hdrlen += 2;
		break;
	case IEEE80211_FTYPE_CTL:
		switch (WLAN_FC_GET_STYPE(fc)) {
		case IEEE80211_STYPE_CTS:
		case IEEE80211_STYPE_ACK:
			hdrlen = IEEE80211_1ADDR_LEN;
			break;
		default:
			hdrlen = IEEE80211_2ADDR_LEN;
			break;
		}
		break;
	}

	return hdrlen;
}

#define frame_is_probe_response(fc) \
   ((fc & IEEE80211_FCTL_FTYPE) == IEEE80211_FTYPE_MGMT && \
    (fc & IEEE80211_FCTL_STYPE) == IEEE80211_STYPE_PROBE_RESP )

#define frame_is_management(fc) \
   ((fc & IEEE80211_FCTL_FTYPE) == IEEE80211_FTYPE_MGMT)

#define frame_is_control(fc) \
   ((fc & IEEE80211_FCTL_FTYPE) == IEEE80211_FTYPE_CTL)

#define frame_is_data(fc) \
   ((fc & IEEE80211_FCTL_FTYPE) == IEEE80211_FTYPE_DATA)

#define frame_is_assoc_request(fc) \
   ((fc & IEEE80211_FCTL_STYPE) == IEEE80211_STYPE_ASSOC_REQ)

#define frame_is_reassoc_request(fc) \
   ((fc & IEEE80211_FCTL_STYPE) == IEEE80211_STYPE_REASSOC_REQ)

#ifdef CONFIG_IPW3945_DEBUG
#define VD "d"
#else
#define VD
#endif

#define IPWRAW_VERSION "2.3.4" VD

#define DRV_DESCRIPTION	"Intel(R) PRO/Wireless 3945 Network Connection driver for Linux"
#define DRV_COPYRIGHT	"Copyright(c) 2003-2006 Intel Corporation"
#define DRV_VERSION     IPWRAW_VERSION

MODULE_DESCRIPTION(DRV_DESCRIPTION);
MODULE_VERSION(DRV_VERSION);
MODULE_AUTHOR(DRV_COPYRIGHT);
MODULE_LICENSE("GPL");

#ifdef CONFIG_IPWRAW_DEBUG
static int param_debug = 0;
#endif
static int param_channel = 0;
static int param_led = 1;	/* def: use LEDs */
static int param_disable = 0;	/* def: enable radio */
static int param_antenna = 0;	/* def: 0 = both antennas (use diversity) */

static int param_rtap_iface = 1;	/* def: 1 -- create rtap interface */

static u32 ipw_debug_level;
static const char ipw_modes[] = {
	'a', 'b', 'g', '?'
};

#define LD_TIME_LINK_ON 300

static int from_priority_to_tx_queue[] = {
	IPW_TX_QUEUE_1, IPW_TX_QUEUE_2, IPW_TX_QUEUE_2,
	IPW_TX_QUEUE_1,
	IPW_TX_QUEUE_3, IPW_TX_QUEUE_3, IPW_TX_QUEUE_4, IPW_TX_QUEUE_4
};

static int ipw_update_power_cmd(struct ipw_priv *priv,
				struct ipw_powertable_cmd *cmd, u32 mode);
static int ipw_power_init_handle(struct ipw_priv *priv);

static int ipw_queue_tx_hcmd(struct ipw_priv *priv, struct ipw_host_cmd *cmd);

static void ipw_clear_stations_table(struct ipw_priv *priv);

static int ipw_rx_queue_update_write_ptr(struct ipw_priv *priv,
					 struct ipw_rx_queue *q);
static int ipw_tx_queue_update_write_ptr(struct ipw_priv *priv,
					 struct ipw_tx_queue *txq, int tx_id);
#define NOSLP 0
#define SLP PMC_TCMD_FLAG_DRIVER_ALLOW_SLEEP_MSK
#define MSEC_TO_USEC 1024

/* default power management (not Tx power) table values */
/* for tim  0-10 */
static struct ipw_power_vec_entry range_0[IPW_POWER_AC] = {
	{{NOSLP, 0 * MSEC_TO_USEC, 0 * MSEC_TO_USEC, {0, 0, 0, 0, 0}}, 0},
	{{SLP, 200 * MSEC_TO_USEC, 500 * MSEC_TO_USEC, {1, 2, 3, 4, 4}}, 0},
	{{SLP, 200 * MSEC_TO_USEC, 300 * MSEC_TO_USEC, {2, 4, 6, 7, 7}}, 0},
	{{SLP, 50 * MSEC_TO_USEC, 100 * MSEC_TO_USEC, {2, 6, 9, 9, 10}}, 0},
	{{SLP, 50 * MSEC_TO_USEC, 25 * MSEC_TO_USEC, {2, 7, 9, 9, 10}}, 1},
	{{SLP, 25 * MSEC_TO_USEC, 25 * MSEC_TO_USEC, {4, 7, 10, 10, 10}}, 1}
};

/* for tim > 10 */
static struct ipw_power_vec_entry range_1[IPW_POWER_AC] = {
	{{NOSLP, 0 * MSEC_TO_USEC, 0 * MSEC_TO_USEC, {0, 0, 0, 0, 0}}, 0},
	{{SLP, 200 * MSEC_TO_USEC, 500 * MSEC_TO_USEC, {1, 2, 3, 4, 0xFF}}, 0},
	{{SLP, 200 * MSEC_TO_USEC, 300 * MSEC_TO_USEC, {2, 4, 6, 7, 0xFF}}, 0},
	{{SLP, 50 * MSEC_TO_USEC, 100 * MSEC_TO_USEC, {2, 6, 9, 9, 0xFF}}, 0},
	{{SLP, 50 * MSEC_TO_USEC, 25 * MSEC_TO_USEC, {2, 7, 9, 9, 0xFF}}, 0},
	{{SLP, 25 * MSEC_TO_USEC, 25 * MSEC_TO_USEC, {4, 7, 10, 10, 0xFF}}, 0}
};


/************************************************/
static void ipw_rx_handle(struct ipw_priv *priv);
static int ipw_queue_tx_reclaim(struct ipw_priv *priv, int qindex, int index);
static int ipw_queue_reset(struct ipw_priv *priv);

static void ipw_tx_queue_free(struct ipw_priv *);

static int ipw_stop_tx_queue(struct ipw_priv *priv);

static struct ipw_rx_queue *ipw_rx_queue_alloc(struct ipw_priv *);
static void ipw_rx_queue_free(struct ipw_priv *, struct ipw_rx_queue *);
static void ipw_rx_queue_replenish(struct ipw_priv *);

static int ipw_up(struct ipw_priv *);
static void ipw_bg_up(struct work_struct *work);
static void ipw_down(struct ipw_priv *);
static void ipw_bg_restart(struct work_struct *work);

static int ipw_card_show_info(struct ipw_priv *priv);

static void ipw_bg_alive_start(struct work_struct *work);
static void ipw_bg_init_alive_start(struct work_struct *work);
static int ipw_send_card_state(struct ipw_priv *priv, u32 flags, u8 meta_flag);

static u8 ipw_add_station(struct ipw_priv *priv, const u8 * bssid,
			  int is_ap, u8 flags);
static u8 ipw_remove_station(struct ipw_priv *priv, u8 * bssid, int is_ap);

static int snprint_line(char *buf, size_t count,
			const u8 * data, u32 len, u32 ofs)
{
	int out, i, j, l;
	char c;

	out = snprintf(buf, count, "%08X", ofs);

	for (l = 0, i = 0; i < 2; i++) {
		out += snprintf(buf + out, count - out, " ");
		for (j = 0; j < 8 && l < len; j++, l++)
			out +=
			    snprintf(buf + out, count - out, "%02X ",
				     data[(i * 8 + j)]);
		for (; j < 8; j++)
			out += snprintf(buf + out, count - out, "   ");
	}
	out += snprintf(buf + out, count - out, " ");
	for (l = 0, i = 0; i < 2; i++) {
		out += snprintf(buf + out, count - out, " ");
		for (j = 0; j < 8 && l < len; j++, l++) {
			c = data[(i * 8 + j)];
			if (!isascii(c) || !isprint(c))
				c = '.';

			out += snprintf(buf + out, count - out, "%c", c);
		}

		for (; j < 8; j++)
			out += snprintf(buf + out, count - out, " ");
	}

	return out;
}

#ifdef CONFIG_IPWRAW_DEBUG
static void printk_buf(int level, const u8 * data, u32 len)
{
	char line[81];
	u32 ofs = 0;
	if (!(ipw_debug_level & level))
		return;

	while (len) {
		snprint_line(line, sizeof(line), &data[ofs],
			     min(len, 16U), ofs);
		printk(KERN_DEBUG "%s\n", line);
		ofs += 16;
		len -= min(len, 16U);
	}
}
#else
#define printk_buf(x, y, z) do {} while (0)
#endif

#define IGNORE_RETURN(x) if (x);else;

#define IPW_RATE_SCALE_MAX_WINDOW 62
#define IPW_INVALID_VALUE  -1


/*
 * IO, register, and NIC memory access functions
 *
 * NOTE on naming convention and macro usage for these
 *
 * A single _ prefix before a an access function means that no state
 * check or debug information is printed when that function is called.
 *
 * A double __ prefix before an access function means that state is checked
 * (in the case of *restricted calls) and the current line number is printed
 * in addition to any other debug output.
 *
 * The non-prefixed name is the #define that maps the caller into a
 * #define that provides the caller's __LINE__ to the double prefix version.
 *
 * If you wish to call the function without any debug or state checking,
 * you should use the single _ prefix version (as is used by dependent IO
 * routines, for example _ipw_read_restricted calls the non-check version of
 * _ipw_read32.
 *
 */

#define _ipw_write32(ipw, ofs, val) writel((val), (ipw)->hw_base + (ofs))
#define ipw_write32(ipw, ofs, val) \
 IPW_DEBUG_IO("%s %d: write_direct32(0x%08X, 0x%08X)\n", __FILE__, __LINE__, (u32)(ofs), (u32)(val)); \
 _ipw_write32(ipw, ofs, val)

#define _ipw_read32(ipw, ofs) readl((ipw)->hw_base + (ofs))
static inline u32 __ipw_read32(char *f, u32 l, struct ipw_priv *ipw, u32 ofs)
{
	IPW_DEBUG_IO("%s %d: read_direct32(0x%08X)\n", f, l, (u32) (ofs));
	return _ipw_read32(ipw, ofs);
}

#define ipw_read32(ipw, ofs) __ipw_read32(__FILE__, __LINE__, ipw, ofs)

static inline int ipw_poll_bit(struct ipw_priv *priv, u32 addr,
			       u32 bits, u32 mask, int timeout)
{
	int i = 0;

	do {
		if ((_ipw_read32(priv, addr) & mask) == (bits & mask))
			return i;
		mdelay(10);
		i += 10;
	} while (i < timeout);

	return -ETIMEDOUT;
}

static inline void ipw_set_bit(struct ipw_priv *priv, u32 reg, u32 mask)
{
	_ipw_write32(priv, reg, ipw_read32(priv, reg) | mask);
	_ipw_read32(priv, CSR_INT_MASK);
}

static inline void ipw_clear_bit(struct ipw_priv *priv, u32 reg, u32 mask)
{
	_ipw_write32(priv, reg, ipw_read32(priv, reg) & ~mask);
	_ipw_read32(priv, CSR_INT_MASK);
}

static inline int _ipw_grab_restricted_access(struct ipw_priv *priv)
{
	int rc;
	ipw_set_bit(priv, CSR_GP_CNTRL, CSR_GP_CNTRL_REG_FLAG_MAC_ACCESS_REQ);
	rc = ipw_poll_bit(priv, CSR_GP_CNTRL,
			  CSR_GP_CNTRL_REG_VAL_MAC_ACCESS_EN,
			  (CSR_GP_CNTRL_REG_FLAG_MAC_CLOCK_READY |
			   CSR_GP_CNTRL_REG_FLAG_GOING_TO_SLEEP), 4000);
	if (rc < 0) {
		IPW_ERROR("MAC is in deep sleep!\n");
//              ipw_set_bit(priv, CSR_RESET, CSR_RESET_REG_FLAG_FORCE_NMI);
		return -EIO;
	}

	priv->status |= STATUS_RESTRICTED;

	return 0;
}

static inline int __ipw_grab_restricted_access(u32 line, struct ipw_priv *priv)
{
	if (priv->status & STATUS_RESTRICTED)
		IPW_ERROR
		    ("Grabbing access while already held at line %d.\n", line);

	return _ipw_grab_restricted_access(priv);
}

#define ipw_grab_restricted_access(priv) \
__ipw_grab_restricted_access(__LINE__, priv)

static inline void _ipw_release_restricted_access(struct ipw_priv *priv)
{
	ipw_clear_bit(priv, CSR_GP_CNTRL, CSR_GP_CNTRL_REG_FLAG_MAC_ACCESS_REQ);

	priv->status &= ~STATUS_RESTRICTED;
}

static inline void __ipw_release_restricted_access(u32 line, struct ipw_priv
						   *priv)
{
	if (!(priv->status & STATUS_RESTRICTED))
		IPW_ERROR
		    ("Release unheld restricted access at line %d.\n", line);

	_ipw_release_restricted_access(priv);
}

#define ipw_release_restricted_access(priv) \
__ipw_release_restricted_access(__LINE__, priv)

static inline u32 _ipw_read_restricted(struct ipw_priv *priv, u32 reg)
{
	u32 val;
//      _ipw_grab_restricted_access(priv);
	val = _ipw_read32(priv, reg);
//      _ipw_release_restricted_access(priv);
	return val;
}

static u32 __ipw_read_restricted(u32 line, struct ipw_priv *priv, u32 reg)
{
	u32 value;

	if (!(priv->status & STATUS_RESTRICTED))
		IPW_ERROR("Unrestricted access from line %d\n", line);

	value = _ipw_read_restricted(priv, reg);

	IPW_DEBUG_IO(" reg = 0x%4X : value = 0x%4x \n", reg, value);

	return value;
}

#define ipw_read_restricted(a, b) __ipw_read_restricted(__LINE__, a, b)

static void inline _ipw_write_restricted(struct ipw_priv *priv,
					 u32 reg, u32 value)
{
//      _ipw_grab_restricted_access(priv);
	_ipw_write32(priv, reg, value);
	_ipw_read32(priv, CSR_INT_MASK);
//      _ipw_release_restricted_access(priv);
}
static void __ipw_write_restricted(u32 line,
				   struct ipw_priv *priv, u32 reg, u32 value)
{
	if (!(priv->status & STATUS_RESTRICTED))
		IPW_ERROR("Unrestricted access from line %d\n", line);
	_ipw_write_restricted(priv, reg, value);
}

#define ipw_write_restricted(priv, reg, value) \
__ipw_write_restricted(__LINE__, priv, reg, value)

/* This function seems to be left unused with changes for new ucode
static void ipw_write_buffer_restricted(struct ipw_priv *priv,
					u32 reg, u32 len, u32 * values)
{
	u32 count = sizeof(u32);
	if ((priv != NULL) && (values != NULL)) {
		for (; 0 < len; len -= count, reg += count, values++)
			_ipw_write_restricted(priv, reg, *values);
	}
}
*/

static inline int ipw_poll_restricted_bit(struct ipw_priv *priv,
					  u32 addr, u32 mask, int timeout)
{
	int i = 0;

	do {
		if ((_ipw_read_restricted(priv, addr) & mask) == mask)
			return i;
		mdelay(10);
		i += 10;
	} while (i < timeout);

	return -ETIMEDOUT;
}

static inline u32 _ipw_read_restricted_reg(struct ipw_priv *priv, u32 reg)
{
	_ipw_write_restricted(priv, HBUS_TARG_PRPH_RADDR, reg | (3 << 24));
	return _ipw_read_restricted(priv, HBUS_TARG_PRPH_RDAT);
}
static inline u32 __ipw_read_restricted_reg(u32 line,
					    struct ipw_priv *priv, u32 reg)
{
	if (!(priv->status & STATUS_RESTRICTED))
		IPW_ERROR("Unrestricted access from line %d\n", line);
	return _ipw_read_restricted_reg(priv, reg);
}

#define ipw_read_restricted_reg(priv, reg) \
__ipw_read_restricted_reg(__LINE__, priv, reg)

static inline void _ipw_write_restricted_reg(struct ipw_priv *priv,
					     u32 addr, u32 val)
{
	_ipw_write_restricted(priv, HBUS_TARG_PRPH_WADDR,
			      ((addr & 0x0000FFFF) | (3 << 24)));
	_ipw_write_restricted(priv, HBUS_TARG_PRPH_WDAT, val);
}
static inline void __ipw_write_restricted_reg(u32 line,
					      struct ipw_priv *priv,
					      u32 addr, u32 val)
{
	if (!(priv->status & STATUS_RESTRICTED))
		IPW_ERROR("Unrestricted access from line %d\n", line);
	_ipw_write_restricted_reg(priv, addr, val);
}

#define ipw_write_restricted_reg(priv, addr, val) \
__ipw_write_restricted_reg(__LINE__, priv, addr, val)

#define _ipw_set_bits_restricted_reg(priv, reg, mask) \
	_ipw_write_restricted_reg(priv, reg, \
				  (_ipw_read_restricted_reg(priv, reg) | mask))
static void inline __ipw_set_bits_restricted_reg(u32 line, struct ipw_priv
						 *priv, u32 reg, u32 mask)
{
	if (!(priv->status & STATUS_RESTRICTED))
		IPW_ERROR("Unrestricted access from line %d\n", line);
	_ipw_set_bits_restricted_reg(priv, reg, mask);
}

#define ipw_set_bits_restricted_reg(priv, reg, mask) \
__ipw_set_bits_restricted_reg(__LINE__, priv, reg, mask)

#define _ipw_set_bits_mask_restricted_reg(priv, reg, bits, mask) \
        _ipw_write_restricted_reg( \
            priv, reg, ((_ipw_read_restricted_reg(priv, reg) & mask) | bits))
static void inline __ipw_set_bits_mask_restricted_reg(u32 line, struct ipw_priv
						      *priv, u32 reg,
						      u32 bits, u32 mask)
{
	if (!(priv->status & STATUS_RESTRICTED))
		IPW_ERROR("Unrestricted access from line %d\n", line);
	_ipw_set_bits_mask_restricted_reg(priv, reg, bits, mask);
}

#define ipw_set_bits_mask_restricted_reg(priv, reg, bits, mask) \
__ipw_set_bits_mask_restricted_reg(__LINE__, priv, reg, bits, mask)

static inline void ipw_write_restricted_reg_buffer(struct ipw_priv
						   *priv, u32 reg,
						   u32 len, u8 * values)
{
	u32 reg_offset = reg;
	u32 aligment = reg & 0x3;

	if (len < sizeof(u32)) {
		if ((aligment + len) <= sizeof(u32)) {
			u8 size;
			u32 value = 0;
			size = len - 1;
			memcpy(&value, values, len);
			reg_offset = (reg_offset & 0x0000FFFF);

			_ipw_write_restricted(priv,
					      HBUS_TARG_PRPH_WADDR,
					      (reg_offset | (size << 24)));
			_ipw_write_restricted(priv, HBUS_TARG_PRPH_WDAT, value);
		}

		return;
	}

	for (; reg_offset < (reg + len);
	     reg_offset += sizeof(u32), values += sizeof(u32))
		_ipw_write_restricted_reg(priv, reg_offset, *((u32 *) values));
}

static inline void ipw_clear_bits_restricted_reg(struct ipw_priv
						 *priv, u32 reg, u32 mask)
{
	u32 val = _ipw_read_restricted_reg(priv, reg);
	_ipw_write_restricted_reg(priv, reg, (val & ~mask));
}


static inline void ipw_enable_interrupts(struct ipw_priv *priv)
{
	if (priv->status & STATUS_INT_ENABLED)
		return;
	priv->status |= STATUS_INT_ENABLED;
	ipw_write32(priv, CSR_INT_MASK, CSR_INI_SET_MASK);
	ipw_read32(priv, CSR_INT_MASK);
}

static inline void ipw_disable_interrupts(struct ipw_priv *priv)
{
	if (!(priv->status & STATUS_INT_ENABLED))
		return;
	priv->status &= ~STATUS_INT_ENABLED;
	ipw_write32(priv, CSR_INT_MASK, 0x00000000);
	ipw_write32(priv, CSR_INT, CSR_INI_SET_MASK);
	ipw_write32(priv, CSR_FH_INT_STATUS, 0xff);
	ipw_write32(priv, CSR_FH_INT_STATUS, 0x00070000);
	ipw_read32(priv, CSR_INT_MASK);
}

static u32 ipw_read_restricted_mem(struct ipw_priv *priv, u32 addr)
{
	ipw_write_restricted(priv, HBUS_TARG_MEM_RADDR, addr);
	return ipw_read_restricted(priv, HBUS_TARG_MEM_RDAT);
}

static void ipw_write_restricted_mem(struct ipw_priv *priv, u32 addr, u32 val)
{
	ipw_write_restricted(priv, HBUS_TARG_MEM_WADDR, addr);
	ipw_write_restricted(priv, HBUS_TARG_MEM_WDAT, val);
}

/************************* END *******************************/

static const char *desc_lookup(int i)
{
	switch (i) {
	case 1:
		return "FAIL";
	case 2:
		return "BAD_PARAM";
	case 3:
		return "BAD_CHECKSUM";
	case 4:
		return "NMI_INTERRUPT";
	case 5:
		return "SYSASSERT";
	case 6:
		return "FATAL_ERROR";
	}

	return "UNKNOWN";
}

#define ERROR_START_OFFSET  (1 * sizeof(u32))
#define ERROR_ELEM_SIZE     (7 * sizeof(u32))
static void ipw_dump_nic_error_log(struct ipw_priv *priv)
{
	u32 desc, time, blnk, blink2, ilnk, ilink2, idata, i, count, base;
	int rc;

	base = priv->card_alive.error_event_table_ptr;

	if (!VALID_RTC_DATA_ADDR(base)) {
		IPW_ERROR("Not valid error log pointer 0x%08X\n", base);
		return;
	}

	rc = ipw_grab_restricted_access(priv);
	if (rc) {
		IPW_WARNING("Can not read from adapter at this time.\n");
		return;
	}

	count = ipw_read_restricted_mem(priv, base);

	if (ERROR_START_OFFSET <= count * ERROR_ELEM_SIZE) {
		IPW_ERROR("Start IPW Error Log Dump:\n");
		IPW_ERROR("Status: 0x%08X, Config: %08X count: %d\n",
			  priv->status, priv->config, count);
	}

	IPW_ERROR("Desc       Time       asrtPC     const      "
		  "ilink1     nmiPC      Line\n");
	for (i = ERROR_START_OFFSET;
	     i < (count * ERROR_ELEM_SIZE) + ERROR_START_OFFSET;
	     i += ERROR_ELEM_SIZE) {
		desc = ipw_read_restricted_mem(priv, base + i);
		time =
		    ipw_read_restricted_mem(priv, base + i + 1 * sizeof(u32));
		blnk =
		    ipw_read_restricted_mem(priv, base + i + 2 * sizeof(u32));
		blink2 =
		    ipw_read_restricted_mem(priv, base + i + 3 * sizeof(u32));
		ilnk =
		    ipw_read_restricted_mem(priv, base + i + 4 * sizeof(u32));
		ilink2 =
		    ipw_read_restricted_mem(priv, base + i + 5 * sizeof(u32));
		idata =
		    ipw_read_restricted_mem(priv, base + i + 6 * sizeof(u32));

		IPW_ERROR
		    ("%-8s (#%d) 0x%08X 0x%08X 0x%08X 0x%08X 0x%08X %u\n",
		     desc_lookup(desc), desc, time, blnk, blink2,
		     ilnk, ilink2, idata);
	}

	ipw_release_restricted_access(priv);

}

#define IPW_EVT_DISABLE (0)	/* 1 = enable the ipw_disable_events() function */
#define IPW_EVT_DISABLE_SIZE (1532/32)
/* Disable selected events in uCode event log, by writing "1"s into "disable"
 *   bitmap in SRAM.  Bit position corresponds to Event # (id/type).
 *   Default values of 0 enable uCode events to be logged.
 * Use for only special debugging.  This function is just a placeholder as-is,
 *   you'll need to provide the special bits! ...
 *   ... and set IPW_EVT_DISABLE to 1. */
static void ipw_disable_events(struct ipw_priv *priv)
{
	int rc;
	int i;
	u32 base;		/* SRAM address of event log header */
	u32 disable_ptr;	/* SRAM address of event-disable bitmap array */
	u32 array_size;		/* # of u32 entries in array */
	u32 evt_disable[IPW_EVT_DISABLE_SIZE] = {
		0x00000000,	/*   31 -    0  Event id numbers */
		0x00000000,	/*   63 -   32 */
		0x00000000,	/*   95 -   64 */
		0x00000000,	/*  127 -   96 */
		0x00000000,	/*  159 -  128 */
		0x00000000,	/*  191 -  160 */
		0x00000000,	/*  223 -  192 */
		0x00000000,	/*  255 -  224 */
		0x00000000,	/*  287 -  256 */
		0x00000000,	/*  319 -  288 */
		0x00000000,	/*  351 -  320 */
		0x00000000,	/*  383 -  352 */
		0x00000000,	/*  415 -  384 */
		0x00000000,	/*  447 -  416 */
		0x00000000,	/*  479 -  448 */
		0x00000000,	/*  511 -  480 */
		0x00000000,	/*  543 -  512 */
		0x00000000,	/*  575 -  544 */
		0x00000000,	/*  607 -  576 */
		0x00000000,	/*  639 -  608 */
		0x00000000,	/*  671 -  640 */
		0x00000000,	/*  703 -  672 */
		0x00000000,	/*  735 -  704 */
		0x00000000,	/*  767 -  736 */
		0x00000000,	/*  799 -  768 */
		0x00000000,	/*  831 -  800 */
		0x00000000,	/*  863 -  832 */
		0x00000000,	/*  895 -  864 */
		0x00000000,	/*  927 -  896 */
		0x00000000,	/*  959 -  928 */
		0x00000000,	/*  991 -  960 */
		0x00000000,	/* 1023 -  992 */
		0x00000000,	/* 1055 - 1024 */
		0x00000000,	/* 1087 - 1056 */
		0x00000000,	/* 1119 - 1088 */
		0x00000000,	/* 1151 - 1120 */
		0x00000000,	/* 1183 - 1152 */
		0x00000000,	/* 1215 - 1184 */
		0x00000000,	/* 1247 - 1216 */
		0x00000000,	/* 1279 - 1248 */
		0x00000000,	/* 1311 - 1280 */
		0x00000000,	/* 1343 - 1312 */
		0x00000000,	/* 1375 - 1344 */
		0x00000000,	/* 1407 - 1376 */
		0x00000000,	/* 1439 - 1408 */
		0x00000000,	/* 1471 - 1440 */
		0x00000000,	/* 1503 - 1472 */
	};

	base = priv->card_alive.log_event_table_ptr;
	if (!VALID_RTC_DATA_ADDR(base)) {
		IPW_ERROR("Invalid event log pointer 0x%08X\n", base);
		return;
	}

	rc = ipw_grab_restricted_access(priv);
	if (rc) {
		IPW_WARNING("Can not read from adapter at this time.\n");
		return;
	}

	disable_ptr = ipw_read_restricted_mem(priv, base + (4 * sizeof(u32)));
	array_size = ipw_read_restricted_mem(priv, base + (5 * sizeof(u32)));
	ipw_release_restricted_access(priv);

	if (IPW_EVT_DISABLE && (array_size == IPW_EVT_DISABLE_SIZE)) {
		IPW_DEBUG_INFO("Disabling selected uCode log events at 0x%x\n",
			       disable_ptr);
		rc = ipw_grab_restricted_access(priv);
		for (i = 0; i < IPW_EVT_DISABLE_SIZE; i++) {
			ipw_write_restricted_mem(priv,
						 disable_ptr +
						 (i * sizeof(u32)),
						 evt_disable[i]);
		}
		ipw_release_restricted_access(priv);
	} else {
		IPW_DEBUG_INFO("Selected uCode log events may be disabled\n");
		IPW_DEBUG_INFO("  by writing \"1\"s into disable bitmap\n");
		IPW_DEBUG_INFO("  in SRAM at 0x%x, size %d u32s\n",
			       disable_ptr, array_size);
	}

}

#define EVENT_START_OFFSET (6 * sizeof(u32))
/* Must be called with ipw_grab_restricted_access() already obtained! */
static void ipw_print_event_log(struct ipw_priv *priv, u32 start_idx,
				u32 num_events, u32 mode)
{
	u32 i;
	u32 base;		/* SRAM byte address of event log header */
	u32 event_size;		/* 2 u32s, or 3 u32s if timestamp recorded */
	u32 ptr;		/* SRAM byte address of log data */
	u32 ev, time, data;	/* event log data */

	if (num_events == 0)
		return;

	base = priv->card_alive.log_event_table_ptr;

	if (mode == 0)
		event_size = 2 * sizeof(u32);
	else
		event_size = 3 * sizeof(u32);

	ptr = base + EVENT_START_OFFSET + (start_idx * event_size);

	/* "time" is actually "data" for mode 0 (no timestamp).
	 * place event id # at far right for easier visual parsing. */
	for (i = 0; i < num_events; i++) {
		ev = ipw_read_restricted_mem(priv, ptr);
		ptr += sizeof(u32);
		time = ipw_read_restricted_mem(priv, ptr);
		ptr += sizeof(u32);
		if (mode == 0) {
			IPW_ERROR("0x%08x\t%04u\n", time, ev);	/* data, ev */
		} else {
			data = ipw_read_restricted_mem(priv, ptr);
			ptr += sizeof(u32);
			IPW_ERROR("%010u\t0x%08x\t%04u\n", time, data, ev);
		}
	}
}

static void ipw_dump_nic_event_log(struct ipw_priv *priv)
{
	int rc;
	u32 base;		/* SRAM byte address of event log header */
	u32 capacity;		/* event log capacity in # entries */
	u32 mode;		/* 0 - no timestamp, 1 - timestamp recorded */
	u32 num_wraps;		/* # times uCode wrapped to top of log */
	u32 next_entry;		/* index of next entry to be written by uCode */
	u32 size;		/* # entries that we'll print */

	base = priv->card_alive.log_event_table_ptr;
	if (!VALID_RTC_DATA_ADDR(base)) {
		IPW_ERROR("Invalid event log pointer 0x%08X\n", base);
		return;
	}

	rc = ipw_grab_restricted_access(priv);
	if (rc) {
		IPW_WARNING("Can not read from adapter at this time.\n");
		return;
	}

	/* event log header */
	capacity = ipw_read_restricted_mem(priv, base);
	mode = ipw_read_restricted_mem(priv, base + (1 * sizeof(u32)));
	num_wraps = ipw_read_restricted_mem(priv, base + (2 * sizeof(u32)));
	next_entry = ipw_read_restricted_mem(priv, base + (3 * sizeof(u32)));

	size = num_wraps ? capacity : next_entry;

	/* bail out if nothing in log */
	if (size == 0) {
		ipw_release_restricted_access(priv);
		return;
	}

	IPW_ERROR("Start IPW Event Log Dump: display count %d, wraps %d\n",
		  size, num_wraps);

	/* if uCode has wrapped back to top of log, start at the oldest entry,
	 *    i.e the next one that uCode would fill. */
	if (num_wraps) {
		ipw_print_event_log(priv, next_entry,
				    capacity - next_entry, mode);
	}

	/* (then/else) start at top of log */
	ipw_print_event_log(priv, 0, next_entry, mode);

	ipw_release_restricted_access(priv);
}

static inline int ipw_is_ready(struct ipw_priv *priv)
{
	/* The adapter is 'ready' if READY bit is
	 * set but EXIT_PENDING is not */
	return ((priv->status & (STATUS_READY | STATUS_EXIT_PENDING)) ==
		STATUS_READY) ? 1 : 0;
}

static inline int ipw_is_alive(struct ipw_priv *priv)
{
	return (priv->status & STATUS_ALIVE) ? 1 : 0;
}

static inline int ipw_is_init(struct ipw_priv *priv)
{
	return (priv->status & STATUS_INIT) ? 1 : 0;
}

#define IPW_RX_HDR(x) ((struct ipw_rx_frame_hdr *)(\
                       x->u.rx_frame.stats.payload + \
                       x->u.rx_frame.stats.mib_count))
#define IPW_RX_END(x) ((struct ipw_rx_frame_end *)(\
                       IPW_RX_HDR(x)->payload + \
                       le16_to_cpu(IPW_RX_HDR(x)->len)))
#define IPW_RX_STATS(x) (&x->u.rx_frame.stats)
#define IPW_RX_DATA(x) (IPW_RX_HDR(x)->payload)

#define IPW_CMD(x) case x : return #x
#define IPW_CMD3945(x) case REPLY_ ## x : return #x

static const char *get_cmd_string(u8 cmd)
{
	switch (cmd) {
		IPW_CMD(STATISTICS_NOTIFICATION);
		IPW_CMD3945(ALIVE);
		IPW_CMD3945(ERROR);
		IPW_CMD3945(RXON);
		IPW_CMD3945(RXON_ASSOC);
		IPW_CMD3945(ADD_STA);
		IPW_CMD3945(RX);
		IPW_CMD3945(TX);
		IPW_CMD3945(BCON);
		IPW_CMD3945(LEDS_CMD);
		IPW_CMD3945(TX_BEACON);
		IPW_CMD3945(BT_CONFIG);
		IPW_CMD3945(TX_PWR_TABLE_CMD);
		IPW_CMD3945(STATISTICS_CMD);
		IPW_CMD3945(CARD_STATE_CMD);
	case POWER_TABLE_CMD:
		return "POWER_TABLE_CMD";
	default:
		return "UNKNOWN";

	}
}

#define HOST_COMPLETE_TIMEOUT (HZ / 2)

static inline int is_cmd_sync(struct ipw_host_cmd *cmd)
{
	return !(cmd->meta.flags & CMD_ASYNC);
}

static inline int is_cmd_small(struct ipw_host_cmd *cmd)
{
	return !(cmd->meta.flags & CMD_SIZE_HUGE);
}

static inline int cmd_needs_lock(struct ipw_host_cmd *cmd)
{
	return !(cmd->meta.flags & CMD_NO_LOCK);
}

/*
  low level function to send 3945 command.

  The caller can control if the command is async/sync.
  sync    : 1 do sync call and sleep until notified the command is complese
  resp    : pointer to command response.
  do_lock : 1 use spin lock, 0 assume the user has lock.
            we can not have do_lock=0 and sync = 1.
  is_huge_cmd : used for scan since this command is very large, we use
                special buffer to copy the command.
*/
static int ipw_send_cmd(struct ipw_priv *priv, struct ipw_host_cmd *cmd)
    __attribute__ ((warn_unused_result));
static int ipw_send_cmd(struct ipw_priv *priv, struct ipw_host_cmd *cmd)
{
	int rc;
	unsigned long flags = 0;

	/* If this is an asynchronous command, and we are in a shutdown
	 * process then don't let it start */
	if (!is_cmd_sync(cmd) && (priv->status & STATUS_EXIT_PENDING))
		return -EBUSY;

	/*
	 * The following BUG_ONs are meant to catch programming API misuse
	 * and not run-time failures due to timing, resource constraint, etc.
	 */

	/* A command can not be asynchronous AND expect an SKB to be set */
	BUG_ON((cmd->meta.flags & CMD_ASYNC)
	       && (cmd->meta.flags & CMD_WANT_SKB));

	/* The skb/callback union must be NULL if an SKB is requested */
	BUG_ON(cmd->meta.u.skb && (cmd->meta.flags & CMD_WANT_SKB));

	/* A command can not be synchronous AND have a callback set */
	BUG_ON(is_cmd_sync(cmd) && cmd->meta.u.callback);

	/* An asynchronous command MUST have a callback */
	BUG_ON((cmd->meta.flags & CMD_ASYNC)
	       && !cmd->meta.u.callback);

	/* A command can not be synchronous AND not use locks */
	BUG_ON(is_cmd_sync(cmd) && (cmd->meta.flags & CMD_NO_LOCK));

	if (cmd_needs_lock(cmd))
		spin_lock_irqsave(&priv->lock, flags);

	if (is_cmd_sync(cmd) && (priv->status & STATUS_HCMD_ACTIVE)) {
		IPW_ERROR("Error sending %s: "
			  "Already sending a host command\n",
			  get_cmd_string(cmd->id));
		if (cmd_needs_lock(cmd))
			spin_unlock_irqrestore(&priv->lock, flags);
		return -EBUSY;
	}

	if (is_cmd_sync(cmd))
		priv->status |= STATUS_HCMD_ACTIVE;

	/* When the SKB is provided in the tasklet, it needs
	 * a backpointer to the originating caller so it can
	 * actually copy the skb there */
	if (cmd->meta.flags & CMD_WANT_SKB) {
		cmd->meta.source = &cmd->meta;
		cmd->meta.magic = CMD_VAR_MAGIC;
	}

	cmd->meta.len = cmd->len;

	rc = ipw_queue_tx_hcmd(priv, cmd);
	if (rc) {
		if (is_cmd_sync(cmd))
			priv->status &= ~STATUS_HCMD_ACTIVE;
		if (cmd_needs_lock(cmd))
			spin_unlock_irqrestore(&priv->lock, flags);

		IPW_ERROR("Error sending %s: "
			  "ipw_queue_tx_hcmd failed: %d\n",
			  get_cmd_string(cmd->id), rc);

		return -ENOSPC;
	}
	if (cmd_needs_lock(cmd))
		spin_unlock_irqrestore(&priv->lock, flags);

	if (is_cmd_sync(cmd)) {
		rc = wait_event_interruptible_timeout(priv->
						      wait_command_queue,
						      !(priv->
							status &
							STATUS_HCMD_ACTIVE),
						      HOST_COMPLETE_TIMEOUT);
		if (rc == 0) {
			if (cmd_needs_lock(cmd))
				spin_lock_irqsave(&priv->lock, flags);

			if (priv->status & STATUS_HCMD_ACTIVE) {
				IPW_ERROR("Error sending %s: "
					  "time out after %dms.\n",
					  get_cmd_string(cmd->id),
					  jiffies_to_msecs
					  (HOST_COMPLETE_TIMEOUT));
				priv->status &= ~STATUS_HCMD_ACTIVE;
				if ((cmd->meta.flags & CMD_WANT_SKB)
				    && cmd->meta.u.skb) {
					dev_kfree_skb_any(cmd->meta.u.skb);
					cmd->meta.u.skb = NULL;
				}

				if (cmd_needs_lock(cmd))
					spin_unlock_irqrestore(&priv->
							       lock, flags);
				cmd->meta.magic = 0;
				return -ETIMEDOUT;
			}

			if (cmd_needs_lock(cmd))
				spin_unlock_irqrestore(&priv->lock, flags);
		}
	}

	if (priv->status & STATUS_RF_KILL_HW) {
		if ((cmd->meta.flags & CMD_WANT_SKB)
		    && cmd->meta.u.skb) {
			dev_kfree_skb_any(cmd->meta.u.skb);
			cmd->meta.u.skb = NULL;
		}

		IPW_DEBUG_INFO("Command %s aborted: RF KILL Switch\n",
			       get_cmd_string(cmd->id));

		return -ECANCELED;
	}

	if (priv->status & STATUS_FW_ERROR) {
		if ((cmd->meta.flags & CMD_WANT_SKB)
		    && cmd->meta.u.skb) {
			dev_kfree_skb_any(cmd->meta.u.skb);
			cmd->meta.u.skb = NULL;
		}

		IPW_DEBUG_INFO("Command %s failed: FW Error\n",
			       get_cmd_string(cmd->id));

		return -EIO;
	}

	if ((cmd->meta.flags & CMD_WANT_SKB) && !cmd->meta.u.skb) {
		IPW_ERROR("Error: Response NULL in '%s'\n",
			  get_cmd_string(cmd->id));
		return -EIO;
	}

	return 0;
}

static int ipw_send_cmd_pdu(struct ipw_priv *priv, u8 id, u16 len, void *data)
    __attribute__ ((warn_unused_result));
static int ipw_send_cmd_pdu(struct ipw_priv *priv, u8 id, u16 len, void *data)
{
	struct ipw_host_cmd cmd = {
		.id = id,
		.len = len,
		.data = data,
	};

	return ipw_send_cmd(priv, &cmd);
}

static int ipw_send_cmd_u32(struct ipw_priv *priv, u8 id, u32 val)
    __attribute__ ((warn_unused_result));
static int ipw_send_cmd_u32(struct ipw_priv *priv, u8 id, u32 val)
{
	struct ipw_host_cmd cmd = {
		.id = id,
		.len = sizeof(val),
		.data = &val,
	};

	return ipw_send_cmd(priv, &cmd);
}

static struct ipw_link_blink link_led_table[] = {
	[IPW_LED_LINK_RADIOOFF] = {1000, 0, 0},
	[IPW_LED_LINK_UNASSOCIATED] = {10000, 200, 2},
	[IPW_LED_LINK_ASSOCIATED] = {1000, 0, 1},
};

static void ipw_update_link_led(struct ipw_priv *priv)
{
	int state;
	struct ipw_led_cmd led_cmd = {
		.id = IPW_LED_LINK,
	};

	/* If we are in RF KILL then we can't send the LED
	 * command, so cache that the LED is in the
	 * RADIOOFF state so we'll turn it back on when
	 * we come back from RF KILL. */
	if (priv->status & STATUS_RF_KILL_MASK) {
		IPW_DEBUG_LED("Not sending LINK LED off cmd due to RF KILL.\n");
		priv->led_state = IPW_LED_LINK_RADIOOFF;
		return;
	}

	if (priv->status & STATUS_IN_SUSPEND) {
		IPW_DEBUG_LED("Not sending LINK LED off cmd due to SUSPEND.\n");
		priv->led_state = IPW_LED_LINK_RADIOOFF;
		return;
	}

	if ((priv->config & CFG_NO_LED) ||
	    (priv->status & STATUS_EXIT_PENDING) ||
	    !(priv->status & STATUS_READY))
		state = IPW_LED_LINK_RADIOOFF;
	else
		state = IPW_LED_LINK_ASSOCIATED;

	if (state == priv->led_state)
		return;

	led_cmd.interval = link_led_table[state].interval;
	led_cmd.on = link_led_table[state].on;
	led_cmd.off = link_led_table[state].off;

	priv->led_state = state;

#ifdef CONFIG_IPWRAW_DEBUG
	switch (state) {
	case IPW_LED_LINK_RADIOOFF:
		IPW_DEBUG_LED("Link state: RADIO OFF\n");
		break;
	case IPW_LED_LINK_ASSOCIATED:
		IPW_DEBUG_LED("Link state: ASSOCIATED\n");
		break;
	case IPW_LED_LINK_UNASSOCIATED:
		IPW_DEBUG_LED("Link state: UNASSOCIATED\n");
		break;
	default:
		IPW_DEBUG_LED("Link state: UNKNOWN\n");
		break;
	}
#endif

	IPW_DEBUG_LED("On: %d, Off: %d, Interval: %d\n",
		      led_cmd.on, led_cmd.off, led_cmd.interval);

	IGNORE_RETURN(ipw_send_cmd_pdu(priv, REPLY_LEDS_CMD,
				       sizeof(struct ipw_led_cmd), &led_cmd));
}

static struct ipw_activity_blink activity_led_table[] = {
	{300, 25, 25},
	{200, 40, 40},
	{100, 55, 55},
	{70, 65, 65},
	{50, 75, 75},
	{20, 85, 85},
	{10, 95, 95},
	{5, 110, 110},
	{1, 130, 130},
	{0, 167, 167},
};

/*
  set to correct blink rate. set to solid blink we can not find correct
  rate value or the blink valus exceed the blink threshold
*/
static void get_led_blink_rate(struct ipw_priv *priv,
			       struct ipw_activity_blink *blink)
{
	/* Adjust to Mbs throughput table */
	u32 bit_count = (priv->led_packets * 10) >> 17;
	u32 index = 0;

	/* If < 1mbs then just quick blink over long duration to
	 * indicate "some" activity */
	if (!bit_count) {
		blink->on = 10;
		blink->off = 200;
		return;
	}

	while ((bit_count <= activity_led_table[index].throughput) &&
	       index < ARRAY_SIZE(activity_led_table))
		index++;

	if (index == ARRAY_SIZE(activity_led_table)) {
		blink->on = 1;	/* turn on */
		blink->off = 0;	/* never turn off */
		return;
	}

	blink->on = activity_led_table[index].on;
	blink->off = activity_led_table[index].off;
}

#define IPW_ACTIVITY_PERIOD msecs_to_jiffies(100)

static void ipw_update_activity_led(struct ipw_priv *priv)
{
	static struct ipw_activity_blink last_blink = { 0, 0, 0 };
	struct ipw_activity_blink blink;
	struct ipw_led_cmd led_cmd = {
		.id = IPW_LED_ACTIVITY,
		.interval = IPW_LED_INTERVAL,
	};

	/* If configured to not use LEDs or LEDs are disabled,
	 * then we don't toggle a activity led */
	if (priv->config & CFG_NO_LED || (priv->status & STATUS_EXIT_PENDING)) {
		blink.on = blink.off = 0;
	} else {
		IPW_DEBUG_LED("total Tx/Rx bytes = %lu\n", priv->led_packets);
		get_led_blink_rate(priv, &blink);
		priv->led_packets = 0;
	}

	if (last_blink.on != blink.on || last_blink.off != blink.off) {
		last_blink = blink;
		IPW_DEBUG_LED
		    ("Blink rate is %d On, %d ms Off, at %d interval.\n",
		     blink.on, blink.off, led_cmd.interval);

		led_cmd.off = blink.off;
		led_cmd.on = blink.on;

		IGNORE_RETURN(ipw_send_cmd_pdu(priv, REPLY_LEDS_CMD,
					       sizeof(struct
						      ipw_led_cmd), &led_cmd));
	}
}

static void ipw_setup_activity_timer(struct ipw_priv *priv)
{
	if (priv->activity_timer_active)
		return;

	priv->activity_timer_active = 1;
	queue_delayed_work(priv->workqueue, &priv->activity_timer,
			   IPW_ACTIVITY_PERIOD);
}

static void ipw_bg_activity_timer(struct work_struct *work)
{
	struct ipw_priv *priv = IPW_DELAYED_DATA(work, struct ipw_priv,
						 activity_timer);

	if (priv->status & STATUS_EXIT_PENDING)
		return;

	mutex_lock(&priv->mutex);

	ipw_update_activity_led(priv);

	/* If we haven't Tx/Rx any packets, then don't bother
	 * running this timer any more until we do one of those things */
	if (!priv->led_packets)
		priv->activity_timer_active = 0;
	else
		queue_delayed_work(priv->workqueue,
				   &priv->activity_timer, IPW_ACTIVITY_PERIOD);

	mutex_unlock(&priv->mutex);
}

static void ipw_update_tech_led(struct ipw_priv *priv)
{
}

#define IPW_EEPROM_LED_FROM_EEPROM     	0x80
#define IPW_EEPROM_LED_MODE             0x03
#define IPW_EEPROM_LED_SAVE		0x04

static int ipw_block_until_driver_ready(struct ipw_priv *priv)
{
	if (!ipw_is_alive(priv))
		return -EAGAIN;

	return 0;
}

/*
 * The following adds a new attribute to the sysfs representation
 * of this device driver (i.e. a new file in /sys/bus/pci/drivers/ipw/)
 * used for controling the debug level.
 *
 * See the level definitions in ipw for details.
 */
static ssize_t show_debug_level(struct device_driver *d, char *buf)
{
	return sprintf(buf, "0x%08X\n", ipw_debug_level);
}
static ssize_t store_debug_level(struct device_driver *d,
				 const char *buf, size_t count)
{
	char *p = (char *)buf;
	u32 val;

	if (p[1] == 'x' || p[1] == 'X' || p[0] == 'x' || p[0] == 'X') {
		p++;
		if (p[0] == 'x' || p[0] == 'X')
			p++;
		val = simple_strtoul(p, &p, 16);
	} else
		val = simple_strtoul(p, &p, 10);
	if (p == buf)
		printk(KERN_INFO DRV_NAME
		       ": %s is not in hex or decimal form.\n", buf);
	else
		ipw_debug_level = val;

	return strnlen(buf, count);
}

static DRIVER_ATTR(debug_level, S_IWUSR | S_IRUGO,
		   show_debug_level, store_debug_level);

static int ipw_send_statistics_request(struct ipw_priv *priv)
{
	return ipw_send_cmd_u32(priv, REPLY_STATISTICS_CMD, 0);
}

static inline int ipw_get_temperature(struct ipw_priv *priv)
{
	return ipw_read32(priv, CSR_UCODE_DRV_GP2);
}

static ssize_t show_temperature(struct device *d,
				struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = (struct ipw_priv *)d->driver_data;
	int rc;

	rc = ipw_block_until_driver_ready(priv);
	if (rc)
		return rc;

	return sprintf(buf, "%d\n", ipw_get_temperature(priv));
}

static DEVICE_ATTR(temperature, S_IRUGO, show_temperature, NULL);

static void ipw_prom_free(struct ipw_priv *priv);
static int ipw_prom_alloc(struct ipw_priv *priv);
static ssize_t store_rtap_iface(struct device *d,
				struct device_attribute *attr,
				const char *buf, size_t count)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	int rc = 0;

	if (!ipw_is_ready(priv))
		return -EAGAIN;

	if (count < 1)
		return -EINVAL;

	switch (buf[0]) {
	case '0':
		if (!param_rtap_iface)
			return count;

		if (netif_running(priv->prom_net_dev)) {
			IPW_WARNING("Interface is up.  Cannot unregister.\n");
			return count;
		}

		ipw_prom_free(priv);
		param_rtap_iface = 0;
		break;

	case '1':
		if (param_rtap_iface)
			return count;

		rc = ipw_prom_alloc(priv);
		if (!rc)
			param_rtap_iface = 1;
		break;

	default:
		return -EINVAL;
	}

	if (rc) {
		IPW_ERROR("Failed to register promiscuous network "
			  "device (error %d).\n", rc);
	}

	return count;
}

static ssize_t show_rtap_iface(struct device *d,
			       struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	if (param_rtap_iface)
		return sprintf(buf, "%s", priv->prom_net_dev->name);
	else {
		buf[0] = '-';
		buf[1] = '1';
		buf[2] = '\0';
		return 3;
	}
}

static DEVICE_ATTR(rtap_iface, S_IWUSR | S_IRUSR, show_rtap_iface,
		   store_rtap_iface);

static ssize_t store_rtap_filter(struct device *d,
				 struct device_attribute *attr,
				 const char *buf, size_t count)
{
	struct ipw_priv *priv = dev_get_drvdata(d);

	if (!priv->prom_priv) {
		IPW_ERROR("Attempting to set filter without "
			  "rtap_iface enabled.\n");
		return -EPERM;
	}

	priv->prom_priv->filter = simple_strtol(buf, NULL, 0);

	IPW_DEBUG_INFO("Setting rtap filter to " BIT_FMT16 "\n",
		       BIT_ARG16(priv->prom_priv->filter));

	return count;
}

static ssize_t show_rtap_filter(struct device *d,
				struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	return sprintf(buf, "0x%04X",
		       priv->prom_priv ? priv->prom_priv->filter : 0);
}

static DEVICE_ATTR(rtap_filter, S_IWUSR | S_IRUSR, show_rtap_filter,
		   store_rtap_filter);

static ssize_t store_retry_rate(struct device *d,
				struct device_attribute *attr,
				const char *buf, size_t count)
{
	struct ipw_priv *priv = dev_get_drvdata(d);

	priv->retry_rate = simple_strtoul(buf, NULL, 0);
	if (priv->retry_rate <= 0)
		priv->retry_rate = 16;

	return count;
}

static ssize_t show_retry_rate(struct device *d,
			       struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	return sprintf(buf, "%d", priv->retry_rate);
}

static DEVICE_ATTR(retry_rate, S_IWUSR | S_IRUSR, show_retry_rate,
		   store_retry_rate);

static ssize_t store_bssid(struct device *d,
			   struct device_attribute *attr,
			   const char *buf, size_t count)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	u8 bssid[ETH_ALEN];
	int i;

	if (count < sizeof(bssid) * 2 + sizeof(bssid) - 1)
		return -EINVAL;

	for (i = 0; i < ETH_ALEN; i++)
		bssid[i] = simple_strtoul(&buf[i * 3], NULL, 16);

	if (!memcmp(priv->staging_config.bssid_addr, bssid, ETH_ALEN)) {
		IPW_DEBUG_INFO("BSSID already set to " MAC_FMT "\n",
			       MAC_ARG(bssid));
		return count;
	}

	memcpy(priv->staging_config.bssid_addr, bssid, ETH_ALEN);
	IPW_DEBUG_INFO("BSSID set to " MAC_FMT "\n", MAC_ARG(bssid));
	queue_delayed_work(priv->workqueue, &priv->commit, 0);

	return count;
}

static ssize_t show_bssid(struct device *d,
			  struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	return sprintf(buf, MAC_FMT, MAC_ARG(priv->active_config.bssid_addr));
}

static DEVICE_ATTR(bssid, S_IWUSR | S_IRUSR, show_bssid, store_bssid);

/* Tx/Rx simulation interface
 *
 * This will eventually be moved to being a real network interface
 * exposed via netlink.  For now, however, quickest path to prototype
 * is through sysfs... */

/* sysfs entry rx -
 * writing used to simulate receipt of a frame.
 * Format:
 * RADIOTAP HEADER
 * 802.11 FRAME
 */
static void ipw_handle_reply_rx(struct ipw_priv *, struct ipw_rx_mem_buffer *);
static ssize_t store_rx(struct device *d,
			struct device_attribute *attr,
			const char *buf, size_t count)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	struct ipw_rt_hdr *rt = (void *)buf;
	struct ipw_rx_packet *pkt;
	struct ipw_rx_mem_buffer rxb;
	struct ipw_rx_frame_stats *rx_stats;
	struct ipw_rx_frame_hdr *rx_hdr;
	struct ipw_rx_frame_end *rx_end;
	u16 len;
	unsigned long flags;

	if (priv->status & STATUS_EXIT_PENDING)
		return -EAGAIN;

	if (priv->status & STATUS_IN_SUSPEND)
		return -ERESTARTSYS;

	if (count < sizeof(*rt)) {
		IPW_WARNING
		    ("Simulated RX frame of wrong size (%zd < %zd).\n",
		     count, sizeof(*rt));
		return 0;
	}

	/* Check radiotap header... */
	if (rt->rt_hdr.it_version != PKTHDR_RADIOTAP_VERSION) {
		IPW_ERROR("driver / app protocol version "
			  "mismatch (v%d vs. v%d).\n",
			  PKTHDR_RADIOTAP_VERSION, rt->rt_hdr.it_version);
		return -EIO;
	}

	len = count - sizeof(*rt);
	if (!len) {
		IPW_WARNING("No 802.11 contents to transmit.\n");
		return count;
	}

	memset(&rxb, 0, sizeof(rxb));
	rxb.skb = dev_alloc_skb(sizeof(struct ipw_rx_frame) + len);
	if (!rxb.skb) {
		IPW_ERROR("Coult not allocate storage for simulated Rx.\n");
		return -ENOMEM;
	}
	pkt = (void *)rxb.skb->data;

	/* Fill in rx_stats */
	rx_stats = IPW_RX_STATS(pkt);
	rx_stats->mib_count = 0;
	rx_stats->rssi = rt->rt_dbmsignal + IPW_RSSI_OFFSET;
	rx_stats->noise_diff = rt->rt_dbmnoise;

	/* Fill in rx_hdr
	 * NOTE: rx_stats->mib_count is needed before IPW_RX_END can work */
	rx_hdr = IPW_RX_HDR(pkt);
	rx_hdr->len = len;

	rx_hdr->phy_flags = 0;
	rx_hdr->phy_flags |=
	    (rt->
	     rt_chbitmask & IEEE80211_CHAN_2GHZ) ?
	    RX_RES_PHY_FLAGS_BAND_24_MSK : 0;
	rx_hdr->phy_flags |=
	    (rt->
	     rt_chbitmask & IEEE80211_CHAN_CCK) ?
	    RX_RES_PHY_FLAGS_MOD_CCK_MSK : 0;
	rx_hdr->phy_flags |=
	    (rt->rt_antenna << 4) & RX_RES_PHY_FLAGS_ANTENNA_MSK;
	rx_hdr->phy_flags |=
	    (rt->
	     rt_flags & IEEE80211_RADIOTAP_F_SHORTPRE) ?
	    RX_RES_PHY_FLAGS_SHORT_PREAMBLE_MSK : 0;
	rx_hdr->channel = rt->rt_channel;

	switch (rt->rt_rate) {
	case 2:
		rx_hdr->rate = IPW_TX_RATE_1MB;
		break;
	case 4:
		rx_hdr->rate = IPW_TX_RATE_2MB;
		break;
	case 10:
		rx_hdr->rate = IPW_TX_RATE_5MB;
		break;
	case 12:
		rx_hdr->rate = IPW_TX_RATE_9MB;
		break;
	case 18:
		rx_hdr->rate = IPW_TX_RATE_11MB;
		break;
	case 22:
		rx_hdr->rate = IPW_TX_RATE_12MB;
		break;
	case 36:
		rx_hdr->rate = IPW_TX_RATE_18MB;
		break;
	case 48:
		rx_hdr->rate = IPW_TX_RATE_24MB;
		break;
	case 72:
		rx_hdr->rate = IPW_TX_RATE_36MB;
		break;
	case 96:
		rx_hdr->rate = IPW_TX_RATE_48MB;
		break;
	case 108:
		rx_hdr->rate = IPW_TX_RATE_54MB;
		break;
	default:
		rx_hdr->rate = 0;
		break;
		break;
	}

	/* Fill in rx_end
	 * NOTE: rx_hdr->len is needed before IPW_RX_END can work */
	rx_end = IPW_RX_END(pkt);
	if (rt->rt_hdr.it_present & (1 << IEEE80211_RADIOTAP_TSFT))
		rx_end->beaconTimeStamp = rt->rt_tsf;
	else {
		rx_end->beaconTimeStamp = priv->last_beacon_time +
		    jiffies_to_msecs(jiffies) -
		    jiffies_to_msecs(priv->last_rx_jiffies);
	}
	rx_end->status = RX_RES_STATUS_NO_RXE_OVERFLOW |
	    RX_RES_STATUS_NO_CRC32_ERROR;
	rx_end->timestamp = priv->last_tsf;

	memcpy(IPW_RX_DATA(pkt), rt->payload, len);

	/* ipw_handle_reply_rx is intended to be called typically from
	 * tasklet context; so we need to lock... */
	IPW_DEBUG_INFO
	    ("Simulating Rx of %ud [%zd] bytes @ %08X%08X.\n", len,
	     count, (rx_end->beaconTimeStamp & 0xffffffff) > 32,
	     (rx_end->beaconTimeStamp & 0xffffffff));

	spin_lock_irqsave(&priv->lock, flags);
	ipw_handle_reply_rx(priv, &rxb);
	spin_unlock_irqrestore(&priv->lock, flags);

	if (rxb.skb != NULL)
		dev_kfree_skb_any(rxb.skb);

	return count;
}

static DEVICE_ATTR(rx, S_IWUSR, NULL, store_rx);

static const struct ipw_channel_info *find_channel(struct ipw_priv *priv,
						   u8 channel)
{
	int i;

	for (i = 0; i < priv->channel_count; i++) {
		if (priv->channel_info[i].channel == channel)
			return &priv->channel_info[i];
	}

	return NULL;
}

static inline int is_channel_a_band(const struct ipw_channel_info *ch)
{
	return (ch->band == IEEE80211_52GHZ_BAND) ? 1 : 0;
}

static inline int is_channel_bg_band(const struct ipw_channel_info *ch)
{
	return (ch->band == IEEE80211_24GHZ_BAND) ? 1 : 0;
}

static inline int is_channel_passive(const struct ipw_channel_info *ch)
{
	return (!(ch->flags & IPW_CHANNEL_ACTIVE)) ? 1 : 0;
}

static void ipw_connection_init_rx_config(struct ipw_priv *priv)
{
	const struct ipw_channel_info *ch_info;

	memset(&priv->staging_config, 0, sizeof(priv->staging_config));

	priv->staging_config.dev_type = RXON_DEV_TYPE_ESS;
	priv->staging_config.filter_flags = RXON_FILTER_ACCEPT_GRP_MSK;
	priv->staging_config.filter_flags = RXON_FILTER_PROMISC_MSK |
	    RXON_FILTER_CTL2HOST_MSK | RXON_FILTER_ACCEPT_GRP_MSK;

	if (param_rtap_iface && netif_running(priv->prom_net_dev))
		priv->staging_config.filter_flags |=
		    (RXON_FILTER_PROMISC_MSK | RXON_FILTER_CTL2HOST_MSK);

	if (priv->config & CFG_PREAMBLE_LONG)
		priv->staging_config.flags &= ~RXON_FLG_SHORT_PREAMBLE_MSK;

	ch_info = find_channel(priv, priv->channel);
	if (ch_info == NULL)
		ch_info = &priv->channel_info[0];

	priv->staging_config.channel = ch_info->channel;
	priv->channel = ch_info->channel;

	if (is_channel_a_band(ch_info)) {
		priv->staging_config.flags &=
		    ~(RXON_FLG_BAND_24G_MSK | RXON_FLG_AUTO_DETECT_MSK
		      | RXON_FLG_CCK_MSK);
		priv->staging_config.flags |= RXON_FLG_SHORT_SLOT_MSK;
	} else {
		priv->staging_config.flags &= ~RXON_FLG_SHORT_SLOT_MSK;
		priv->staging_config.flags |= RXON_FLG_BAND_24G_MSK;
		priv->staging_config.flags |= RXON_FLG_AUTO_DETECT_MSK;
		priv->staging_config.flags &= ~RXON_FLG_CCK_MSK;
	}

	priv->staging_config.ofdm_basic_rates =
	    R_6M_MSK | R_24M_MSK | R_36M_MSK | R_48M_MSK | R_54M_MSK |
	    R_9M_MSK | R_12M_MSK | R_18M_MSK;

	priv->staging_config.cck_basic_rates =
	    R_5_5M_MSK | R_1M_MSK | R_11M_MSK | R_2M_MSK;
}

struct rate {
	int mbps;
	int plcp;
} rates[] = {
	/* cck rates */
	{
	2, 10,},		/*   1mbps */
	{
	4, 20,},		/*   2mbps */
	{
	11, 55,},		/* 5.5mbps */
	{
	22, 110,},		/*  11mbps */
	    /* ofdm rates */
	{
	12, 13,},		/*   6mbps */
	{
	18, 15,},		/*   9mbps */
	{
	24, 5,},		/*  12mbps */
	{
	36, 7,},		/*  18mbps */
	{
	48, 9,},		/*  24mbps */
	{
	72, 11,},		/*  36mbps */
	{
	96, 1,},		/*  48mbps */
	{
	108, 3,},		/*  54mbps */
};

static ssize_t store_rate(struct device *d,
			  struct device_attribute *attr,
			  const char *buf, size_t count)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	char *p = NULL;
	int i;
	int mbps = simple_strtol(buf, &p, 0);

	if (p == buf) {
		IPW_ERROR("Invalid channel format to sysfs.\n");
		return -EINVAL;
	}

	for (i = 0; i < ARRAY_SIZE(rates); i++) {
		if (rates[i].mbps == mbps) {
			IPW_DEBUG_INFO("Setting rate to %d [%d%smbps].\n",
				       mbps, mbps >> 1, (mbps & 1) ? ".5" : "");
			priv->rate_mbps = mbps;
			priv->rate_plcp = rates[i].plcp;
			return count;
		}
	}

	IPW_ERROR("Invalid rate request %d [%d%smbps].\n",
		  mbps, mbps >> 1, (mbps & 1) ? ".5" : "");

	return -EINVAL;
}

static ssize_t show_rate(struct device *d,
			 struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	return sprintf(buf, "%d", priv->rate_mbps);
}

static DEVICE_ATTR(rate, S_IWUSR | S_IRUSR, show_rate, store_rate);

static ssize_t store_channel(struct device *d,
			     struct device_attribute *attr,
			     const char *buf, size_t count)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	char *p = NULL;
	int channel;
	const struct ipw_channel_info *ch_info;

	if (priv->status & STATUS_EXIT_PENDING)
		return -EAGAIN;

	if (priv->status & STATUS_IN_SUSPEND)
		return -ERESTARTSYS;

	channel = simple_strtoul(buf, &p, 0);
	if (p == buf) {
		IPW_ERROR("Invalid channel format to sysfs.\n");
		return -EINVAL;
	}

	if (priv->channel == channel) {
		IPW_DEBUG_INFO("Already tuned to channel %d\n", channel);
	} else {
		ch_info = find_channel(priv, channel);
		if (ch_info == NULL) {
			IPW_ERROR("Invalid channel request: %d\n", channel);
			return -EINVAL;
		}

		IPW_DEBUG_INFO("Tuning to channel %d\n", channel);

		priv->channel = channel;
		priv->staging_config.channel = ch_info->channel;

		if (is_channel_a_band(ch_info)) {
			priv->staging_config.flags &=
			    ~(RXON_FLG_BAND_24G_MSK |
			      RXON_FLG_AUTO_DETECT_MSK | RXON_FLG_CCK_MSK);
			priv->staging_config.flags |= RXON_FLG_SHORT_SLOT_MSK;
			priv->band = IEEE80211_52GHZ_BAND;
		} else {
			priv->staging_config.flags &= ~RXON_FLG_SHORT_SLOT_MSK;
			priv->staging_config.flags |= RXON_FLG_BAND_24G_MSK;
			priv->staging_config.flags |= RXON_FLG_AUTO_DETECT_MSK;
			priv->staging_config.flags &= ~RXON_FLG_CCK_MSK;
			priv->band = IEEE80211_24GHZ_BAND;
		}

		queue_delayed_work(priv->workqueue, &priv->commit, 0);
	}

	return count;
}

static ssize_t show_channel(struct device *d,
			    struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = dev_get_drvdata(d);

	return sprintf(buf, "%d", priv->channel);
}

static DEVICE_ATTR(channel, S_IWUSR | S_IRUSR, show_channel, store_channel);

static ssize_t store_band(struct device *d,
			  struct device_attribute *attr,
			  const char *buf, size_t count)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	char *p = NULL;
	int band;

	if (priv->status & STATUS_EXIT_PENDING)
		return -EAGAIN;

	if (priv->status & STATUS_IN_SUSPEND)
		return -ERESTARTSYS;

	band = simple_strtoul(buf, &p, 0);
	if (p == buf) {
		IPW_ERROR("Invalid band format to sysfs.\n");
		return -EINVAL;
	}

	if (priv->band == band) {
		IPW_DEBUG_INFO("Already tuned to band %d\n", band);
	} else {
		if (band != IEEE80211_24GHZ_BAND &&
		    band != IEEE80211_52GHZ_BAND) {
			IPW_ERROR("Invalid band request: %d\n", band);
			return -EINVAL;
		}

		IPW_DEBUG_INFO("Tuning to band %d\n", band);

		priv->band = band;

		if (priv->band == IEEE80211_52GHZ_BAND) {
			priv->staging_config.flags &=
			    ~(RXON_FLG_BAND_24G_MSK |
			      RXON_FLG_AUTO_DETECT_MSK | RXON_FLG_CCK_MSK);
			priv->staging_config.flags |= RXON_FLG_SHORT_SLOT_MSK;
		} else {
			priv->staging_config.flags &= ~RXON_FLG_SHORT_SLOT_MSK;
			priv->staging_config.flags |= RXON_FLG_BAND_24G_MSK;
			priv->staging_config.flags |= RXON_FLG_AUTO_DETECT_MSK;
			priv->staging_config.flags &= ~RXON_FLG_CCK_MSK;
		}

		queue_delayed_work(priv->workqueue, &priv->commit, 0);
	}

	return count;
}

static ssize_t show_band(struct device *d,
			 struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = dev_get_drvdata(d);

	return sprintf(buf, "%d", priv->band);
}

static DEVICE_ATTR(band, S_IWUSR | S_IRUSR, show_band, store_band);

static ssize_t store_dev_type(struct device *d,
			      struct device_attribute *attr,
			      const char *buf, size_t count)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	char *p = NULL;
	int dev_type;

	if (priv->status & STATUS_EXIT_PENDING)
		return -EAGAIN;

	if (priv->status & STATUS_IN_SUSPEND)
		return -ERESTARTSYS;

	dev_type = simple_strtoul(buf, &p, 0);
	if (p == buf) {
		IPW_ERROR("Invalid dev_type format to sysfs.\n");
		return -EINVAL;
	}

	if (priv->active_config.dev_type == dev_type) {
		IPW_DEBUG_INFO("Already configured to dev_type %d\n", dev_type);
	} else {
		if (dev_type < 0 || dev_type > 10) {
			IPW_ERROR("Invalid dev_type request: %d\n", dev_type);
			return -EINVAL;
		}

		IPW_DEBUG_INFO("Configuring to dev_type %d\n", dev_type);

		priv->staging_config.dev_type = dev_type;
		queue_delayed_work(priv->workqueue, &priv->commit, 0);
	}

	return count;
}

static ssize_t show_dev_type(struct device *d,
			     struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = dev_get_drvdata(d);

	return sprintf(buf, "%d", priv->active_config.dev_type);
}

static DEVICE_ATTR(dev_type, S_IWUSR | S_IRUSR, show_dev_type, store_dev_type);

/* --- POWER CONSTRAINT STUFF --- */

#define IPW_CCK_FROM_OFDM_POWER_DIFF  -5
#define IPW_CCK_FROM_OFDM_INDEX_DIFF (10)

/* Replace requested_power and base_power_index ch_info fields for one channel.
 * Called if user or spectrum management changes power preferences.
 * Takes into account h/w and modulation limitations (clip power).
 * This does *not* send anything to NIC, just sets up ch_info for one channel.
 * NOTE:  reg_compensate_for_temperature_dif() *must* be run after this to
 *    properly fill out the scan powers, and actual h/w gain settings,
 *    and send changes to NIC */
static int reg_set_new_power(struct ipw_priv *priv,
			     struct ipw_channel_info *ch_info)
{
	struct ipw_channel_power_info *power_info;
	int power_changed = 0;
	int i;
	s8 *clip_pwrs;
	s8 power;
	s8 delta_idx;

	/* Get this chnlgrp's rate-to-max/clip-powers table */
	clip_pwrs = priv->clip_groups[ch_info->group_index].clip_powers;

	/* Get this channel's rate-to-current-power settings table */
	power_info = ch_info->power_info;

	/* update OFDM Txpower settings */
	for (i = 0; i < IPW_OFDM_RATES; i++, ++power_info) {

		/* limit new power to be no more than h/w capability */
		if (!priv->max_power_override)
			power = min(ch_info->curr_txpow, clip_pwrs[i]);
		else
			power = ch_info->curr_txpow;
		if (power == power_info->requested_power)
			continue;

		/* find difference between old and new requested powers,
		 *    update base (non-temp-compensated) power index */
		delta_idx = (power - power_info->requested_power) * 2;
		power_info->base_power_index -= delta_idx;

		/* save new requested power value */
		power_info->requested_power = power;

		power_changed = 1;
	}

	/* update CCK Txpower settings, based on OFDM 12M setting ...
	 *    ... all CCK power settings for a given channel are the *same*. */
	if (power_changed) {
		power = ch_info->power_info[RATE_SCALE_12M_INDEX].
		    requested_power + IPW_CCK_FROM_OFDM_POWER_DIFF;

		/* do all CCK rates' ipw_channel_power_info structures */
		for (i = IPW_OFDM_RATES; i < IPW_MAX_RATES; i++) {
			power_info->requested_power = power;
			power_info->base_power_index =
			    ch_info->power_info[RATE_SCALE_12M_INDEX].
			    base_power_index + IPW_CCK_FROM_OFDM_INDEX_DIFF;
			++power_info;
		}
	}

	return 0;
}

/* selects and returns new power limit for channel,
 *   which may be less (but not more) than requested,
 *   based strictly on regulatory (eeprom and spectrum mgt) limitations
 *   (no consideration for h/w clipping limitations). */
static int reg_get_channel_txpower_limit(struct ipw_channel_info *ch_info)
{
	s8 max_power = 0;

	/* if we're using TGd limits, use lower of TGd or EEPROM */
	if (ch_info->tgd.max_power != 0)
		max_power = min(ch_info->tgd.max_power,
				ch_info->eeprom.max_power_avg);

	/* else just use EEPROM limits */
	else
		max_power = ch_info->eeprom.max_power_avg;
	return min(max_power, ch_info->max_power_avg);
}

static ssize_t store_txpower(struct device *d,
			     struct device_attribute *attr,
			     const char *buf, size_t count)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	char *p = (char *)buf;
	int txpower, i, max_power;
	struct ipw_channel_info *ch_info;

	if (priv->status & STATUS_EXIT_PENDING)
		return -EAGAIN;

	if (priv->status & STATUS_IN_SUSPEND)
		return -ERESTARTSYS;

	/* This is provided to let the host attempt to override
	 * the hardware / uCode TX power enforcement -- the uCode
	 * will clamp to the appropriate value. */
	if (*p == '!') {
		priv->max_power_override = 1;
		p++;
	} else
		priv->max_power_override = 0;

	txpower = simple_strtol(p, &p, 0);

	if (!priv->max_power_override && txpower > priv->max_txpower_limit) {
		IPW_DEBUG_INFO("Clamping maximum txpower to %d\n",
			       priv->max_txpower_limit);
		txpower = priv->max_txpower_limit;
	}

	if (priv->user_txpower_limit == txpower) {
		IPW_DEBUG_INFO("Already locked maximum txpower to %d\n",
			       txpower);
	} else {
		IPW_DEBUG_INFO("Setting maximum txpower to %d\n", txpower);

		priv->user_txpower_limit = txpower;

		for (i = 0; i < priv->channel_count; i++) {
			ch_info = &priv->channel_info[i];
			/* find minimum power of all user and regulatory constraints
			 *    (does not consider h/w clipping limitations) */
			max_power = reg_get_channel_txpower_limit(ch_info);
			if (!priv->max_power_override)
				max_power = min(txpower, max_power);
			else
				max_power = txpower;
			if (max_power != ch_info->curr_txpow) {
				ch_info->curr_txpow = max_power;

				/* this considers the h/w clipping limitations */
				reg_set_new_power(priv, ch_info);
			}
		}

		/* Force a thermal recalibration */
		priv->last_temperature = 0;

		cancel_delayed_work(&priv->thermal_periodic);
		queue_delayed_work(priv->workqueue, &priv->thermal_periodic, 0);
	}

	return count;
}

static ssize_t show_txpower(struct device *d,
			    struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	const struct ipw_channel_info *ch_info;

	ch_info = find_channel(priv, priv->channel);
	if (ch_info == NULL)
		return sprintf(buf, "-/%d", priv->user_txpower_limit);

	return sprintf(buf, "%s%d/%d [channel max %d]",
		       priv->max_power_override ? "!" : "",
		       priv->user_txpower_limit,
		       priv->max_txpower_limit,
		       ch_info->max_power_avg);
}

static DEVICE_ATTR(txpower, S_IWUSR | S_IRUSR, show_txpower, store_txpower);

static inline int is_associated(struct ipw_priv *priv)
{
	return (priv->active_config.
		filter_flags & RXON_FILTER_ASSOC_MSK) ? 1 : 0;
}

static ssize_t store_assoc(struct device *d,
			   struct device_attribute *attr,
			   const char *buf, size_t count)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	char *p = NULL;
	int assoc;

	if (priv->status & STATUS_EXIT_PENDING)
		return -EAGAIN;

	if (priv->status & STATUS_IN_SUSPEND)
		return -ERESTARTSYS;

	assoc = simple_strtoul(buf, &p, 0);
	if (p == buf || (assoc != 1 && assoc != 0)) {
		IPW_ERROR("Invalid assoc format to sysfs.\n");
		return -EINVAL;
	}

	if (is_associated(priv) == assoc) {
		IPW_DEBUG_INFO("Already %sassociated\n", assoc ? "" : "un");
	} else {
		if (assoc)
			priv->staging_config.filter_flags |=
			    RXON_FILTER_ASSOC_MSK;
		else
			priv->staging_config.filter_flags &=
			    ~RXON_FILTER_ASSOC_MSK;

		IPW_DEBUG_INFO("Setting to %sassociated\n", assoc ? "" : "un");

		queue_delayed_work(priv->workqueue, &priv->commit, 0);
	}

	return count;
}

static ssize_t show_assoc(struct device *d,
			  struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = dev_get_drvdata(d);

	return sprintf(buf, "%d", is_associated(priv));
}

static DEVICE_ATTR(assoc, S_IWUSR | S_IRUSR, show_assoc, store_assoc);

static inline int is_channel_valid(const struct ipw_channel_info *ch_info)
{
	if (ch_info == NULL)
		return 0;
	return (ch_info->flags & IPW_CHANNEL_VALID) ? 1 : 0;
}

static inline int is_channel_narrow(const struct ipw_channel_info *ch_info)
{
	return (ch_info->flags & IPW_CHANNEL_NARROW) ? 1 : 0;
}

static inline int is_channel_radar(const struct ipw_channel_info *ch_info)
{
	return (ch_info->flags & IPW_CHANNEL_RADAR) ? 1 : 0;
}

static ssize_t show_channels(struct device *d,
			     struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	int len = 0, i;
	const struct ipw_channel_info *ch;

	if (!ipw_is_ready(priv))
		return -EAGAIN;

	len += sprintf(&buf[len],
		       "Displaying %d channels in 2.4Ghz band "
		       "(802.11bg):\n", priv->bg_count);

	for (i = 0; i < priv->channel_count; i++) {
		ch = &priv->channel_info[i];

		if (!is_channel_bg_band(ch))
			continue;

		if (is_channel_valid(ch)) {
			len += sprintf(&buf[len], "%d: %ddBm: %s%s.\n",
				       ch->channel, ch->max_power_avg,
				       is_channel_radar(ch) ?
				       "radar spectrum, " : "",
				       is_channel_passive(ch) ?
				       "passive only" : "active/passive");
		} else {
			len += sprintf(&buf[len], "%d: Tx disabled.\n",
				       ch->channel);
		}
	}

	len += sprintf(&buf[len],
		       "Displaying %d channels in 5.2Ghz band "
		       "(802.11a):\n", priv->a_count);

	for (i = 0; i < priv->channel_count; i++) {
		ch = &priv->channel_info[i];

		if (!is_channel_a_band(ch))
			continue;

		if (is_channel_valid(ch)) {
			len += sprintf(&buf[len], "%d: %ddBm: %s%s.\n",
				       ch->channel, ch->max_power_avg,
				       is_channel_radar(ch) ?
				       "radar spectrum, " : "",
				       is_channel_passive(ch) ?
				       "passive only" : "active/passive");
		} else {
			len += sprintf(&buf[len], "%d: Tx disabled.\n",
				       ch->channel);
		}
	}

	return len;
}

static DEVICE_ATTR(channels, S_IRUSR, show_channels, NULL);

static ssize_t show_statistics(struct device *d,
			       struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = dev_get_drvdata(d);
	u32 size = sizeof(priv->statistics), len = 0, ofs = 0;
	u8 *data = (u8 *) & priv->statistics;
	int rc = 0;

	if (!ipw_is_alive(priv))
		return -EAGAIN;

	mutex_lock(&priv->mutex);
	rc = ipw_send_statistics_request(priv);
	mutex_unlock(&priv->mutex);

	if (rc) {
		len = sprintf(buf,
			      "Error sending statistics request: 0x%08X\n", rc);
		return len;
	}

	while (size && (PAGE_SIZE - len)) {
		len +=
		    snprint_line(&buf[len], PAGE_SIZE - len,
				 &data[ofs], min(size, 16U), ofs);
		if (PAGE_SIZE - len)
			buf[len++] = '\n';

		ofs += 16;
		size -= min(size, 16U);
	}

	return len;
}

static DEVICE_ATTR(statistics, S_IRUGO, show_statistics, NULL);

static ssize_t show_antenna(struct device *d,
			    struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = dev_get_drvdata(d);

	if (!ipw_is_alive(priv))
		return -EAGAIN;

	return sprintf(buf, "%d\n", priv->antenna);
}

static ssize_t store_antenna(struct device *d,
			     struct device_attribute *attr,
			     const char *buf, size_t count)
{
	int ant;
	struct ipw_priv *priv = dev_get_drvdata(d);

	if (count == 0)
		return 0;

	if (sscanf(buf, "%1i", &ant) != 1) {
		IPW_DEBUG_INFO("not in hex or decimal form.\n");
		return count;
	}

	if ((ant >= 0) && (ant <= 2)) {
		IPW_DEBUG_INFO("Setting antenna select to %d.\n", ant);
		priv->antenna = ant;
	} else {
		IPW_DEBUG_INFO("Bad antenna select value %d.\n", ant);
	}

	return count;
}

static DEVICE_ATTR(antenna, S_IWUSR | S_IRUGO, show_antenna, store_antenna);

static ssize_t show_led(struct device *d,
			struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = dev_get_drvdata(d);

	if (!ipw_is_alive(priv))
		return -EAGAIN;

	return sprintf(buf, "%d\n", (priv->config & CFG_NO_LED) ? 0 : 1);
}

static ssize_t store_led(struct device *d,
			 struct device_attribute *attr,
			 const char *buf, size_t count)
{
	struct ipw_priv *priv = dev_get_drvdata(d);

	if (count == 0)
		return 0;

	if (*buf == '0') {
		IPW_DEBUG_LED("Disabling LED control.\n");
		priv->config |= CFG_NO_LED;
	} else {
		IPW_DEBUG_LED("Enabling LED control.\n");
		priv->config &= ~CFG_NO_LED;
	}

	ipw_update_link_led(priv);
	ipw_update_activity_led(priv);
	ipw_update_tech_led(priv);

	return count;
}

static DEVICE_ATTR(led, S_IWUSR | S_IRUGO, show_led, store_led);

static ssize_t show_status(struct device *d,
			   struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = (struct ipw_priv *)d->driver_data;
	if (!ipw_is_alive(priv))
		return -EAGAIN;
	return sprintf(buf, "0x%08x\n", (int)priv->status);
}

static DEVICE_ATTR(status, S_IRUGO, show_status, NULL);

static ssize_t show_cfg(struct device *d,
			struct device_attribute *attr, char *buf)
{
	struct ipw_priv *priv = (struct ipw_priv *)d->driver_data;

	if (!ipw_is_alive(priv))
		return -EAGAIN;

	return sprintf(buf, "0x%08x\n", (int)priv->config);
}

static DEVICE_ATTR(cfg, S_IRUGO, show_cfg, NULL);

static ssize_t dump_error_log(struct device *d,
			      struct device_attribute *attr,
			      const char *buf, size_t count)
{
	char *p = (char *)buf;

	if (p[0] == '1')
		ipw_dump_nic_error_log((struct ipw_priv *)d->driver_data);

	return strnlen(buf, count);
}

static DEVICE_ATTR(dump_errors, S_IWUSR, NULL, dump_error_log);

static ssize_t dump_event_log(struct device *d,
			      struct device_attribute *attr,
			      const char *buf, size_t count)
{
	char *p = (char *)buf;

	if (p[0] == '1')
		ipw_dump_nic_event_log((struct ipw_priv *)d->driver_data);

	return strnlen(buf, count);
}

static DEVICE_ATTR(dump_events, S_IWUSR, NULL, dump_event_log);

static ssize_t show_rf_kill(struct device *d,
			    struct device_attribute *attr, char *buf)
{
	/* 0 - RF kill not enabled
	   1 - SW based RF kill active (sysfs)
	   2 - HW based RF kill active
	   3 - Both HW and SW baed RF kill active */
	struct ipw_priv *priv = (struct ipw_priv *)d->driver_data;
	int val = ((priv->status & STATUS_RF_KILL_SW) ? 0x1 : 0x0) |
	    ((priv->status & STATUS_RF_KILL_HW) ? 0x2 : 0x0);

	return sprintf(buf, "%i\n", val);
}

static inline unsigned long elapsed_jiffies(unsigned long start,
					    unsigned long end)
{
	if (end > start)
		return end - start;

	return end + (MAX_JIFFY_OFFSET - start);
}

static void ipw_radio_kill_sw(struct ipw_priv *priv, int disable_radio)
{
	if ((disable_radio ? 1 : 0) ==
	    ((priv->status & STATUS_RF_KILL_SW) ? 1 : 0))
		return;

	IPW_DEBUG_RF_KILL("Manual SW RF KILL set to: RADIO %s\n",
			  disable_radio ? "OFF" : "ON");

	if (disable_radio) {
		ipw_update_link_led(priv);
		ipw_write32(priv, CSR_UCODE_DRV_GP1_SET,
			    CSR_UCODE_SW_BIT_RFKILL);
		ipw_send_card_state(priv, CARD_STATE_CMD_DISABLE, 0);
		priv->status |= STATUS_RF_KILL_SW;
		ipw_write32(priv, CSR_UCODE_DRV_GP1_SET,
			    CSR_UCODE_DRV_GP1_BIT_CMD_BLOCKED);
		ipw_read32(priv, CSR_INT_MASK);
		return;
	}

	ipw_write32(priv, CSR_UCODE_DRV_GP1_CLR, CSR_UCODE_SW_BIT_RFKILL);
	ipw_read32(priv, CSR_INT_MASK);

	priv->status &= ~STATUS_RF_KILL_SW;

	if (priv->status & STATUS_RF_KILL_HW) {
		IPW_DEBUG_RF_KILL("Can not turn radio back on - "
				  "disabled by HW switch\n");
		return;
	}

	queue_delayed_work(priv->workqueue, &priv->restart, 0);

	return;
}

static ssize_t store_rf_kill(struct device *d,
			     struct device_attribute *attr,
			     const char *buf, size_t count)
{
	struct ipw_priv *priv = (struct ipw_priv *)d->driver_data;

	mutex_lock(&priv->mutex);
	ipw_radio_kill_sw(priv, buf[0] == '1');
	mutex_unlock(&priv->mutex);

	return count;
}

static DEVICE_ATTR(rf_kill, S_IWUSR | S_IRUGO, show_rf_kill, store_rf_kill);

#ifdef CONFIG_IPWRAW_DEBUG
static void ipw_print_rx_config(struct ipw_rxon_cmd *rxon)
{
	IPW_DEBUG_RADIO("RX CONFIG:\n");
	printk_buf(IPW_DL_RADIO, (u8 *) rxon, sizeof(*rxon));
	IPW_DEBUG_RADIO("u16 channel: 0x%x\n", rxon->channel);
	IPW_DEBUG_RADIO("u32 flags: 0x%08X " BIT_FMT32 "\n",
			rxon->flags, BIT_ARG32(rxon->flags));
	IPW_DEBUG_RADIO("u32 filter_flags: 0x%08x " BIT_FMT32 "\n",
			rxon->filter_flags, BIT_ARG32(rxon->filter_flags));
	IPW_DEBUG_RADIO("u8 dev_type: 0x%x\n", rxon->dev_type);
	IPW_DEBUG_RADIO("u8 ofdm_basic_rates: 0x%02x " BIT_FMT8 "\n",
			rxon->ofdm_basic_rates,
			BIT_ARG8(rxon->ofdm_basic_rates));
	IPW_DEBUG_RADIO("u8 cck_basic_rates: 0x%02x " BIT_FMT8 "\n",
			rxon->cck_basic_rates, BIT_ARG8(rxon->cck_basic_rates));
	IPW_DEBUG_RADIO("u8[6] node_addr: " MAC_FMT "\n",
			MAC_ARG(rxon->node_addr));
	IPW_DEBUG_RADIO("u8[6] bssid_addr: " MAC_FMT "\n",
			MAC_ARG(rxon->bssid_addr));
	IPW_DEBUG_RADIO("u16 assoc_id: 0x%x\n", rxon->assoc_id);
}
#endif

static void ipw_irq_handle_error(struct ipw_priv *priv)
{
	/* Set the FW error flag -- cleared on ipw_down */
	priv->status |= STATUS_FW_ERROR;

	/* Cancel currently queued command. */
	priv->status &= ~STATUS_HCMD_ACTIVE;

#ifdef CONFIG_IPWRAW_DEBUG
	if (ipw_debug_level & IPW_DL_FW_ERRORS) {
		ipw_dump_nic_error_log(priv);
		ipw_dump_nic_event_log(priv);
		ipw_print_rx_config(&priv->active_config);
	}
#endif

	wake_up_interruptible(&priv->wait_command_queue);

	/* Keep the restart process from trying to send host
	 * commands by clearing the INIT status bit */
	priv->status &= ~STATUS_READY;
	if (!(priv->status & STATUS_EXIT_PENDING)) {
		IPW_DEBUG(IPW_DL_INFO | IPW_DL_FW_ERRORS,
			  "Restarting adapter due to uCode error.\n");
		queue_delayed_work(priv->workqueue, &priv->restart, 0);
	}
}

static void ipw_irq_tasklet(struct ipw_priv *priv)
{
	u32 inta, handled = 0;
	unsigned long flags;

	spin_lock_irqsave(&priv->lock, flags);

	/* Add any cached INTA values that need to be handled */
	inta = priv->isr_inta;

	if (inta & BIT_INT_ERR) {
		IPW_ERROR("Microcode HW error detected.  Restarting.\n");

		/* tell the device to stop sending interrupts */
		ipw_disable_interrupts(priv);

		ipw_irq_handle_error(priv);

		handled |= BIT_INT_ERR;

		spin_unlock_irqrestore(&priv->lock, flags);

		return;
	}

	if (inta & BIT_INT_SWERROR) {
		IPW_ERROR("Microcode SW error detected.  Restarting.\n");
		ipw_irq_handle_error(priv);
		handled |= BIT_INT_SWERROR;
	}

	if (inta & BIT_INT_WAKEUP) {
		IPW_DEBUG_ISR("Wakeup interrupt\n");
		ipw_rx_queue_update_write_ptr(priv, priv->rxq);
		ipw_tx_queue_update_write_ptr(priv, &priv->txq[0], 0);
		ipw_tx_queue_update_write_ptr(priv, &priv->txq[1], 1);
		ipw_tx_queue_update_write_ptr(priv, &priv->txq[2], 2);
		ipw_tx_queue_update_write_ptr(priv, &priv->txq[3], 3);
		ipw_tx_queue_update_write_ptr(priv, &priv->txq[4], 4);
		ipw_tx_queue_update_write_ptr(priv, &priv->txq[5], 5);

		handled |= BIT_INT_WAKEUP;
	}

	if (inta & BIT_INT_ALIVE) {
		IPW_DEBUG_ISR("Alive interrupt\n");
		handled |= BIT_INT_ALIVE;
	}

	/* handle all the justifications for the interrupt */
	if (inta & BIT_INT_RX) {
		//      IPW_DEBUG_ISR("Rx interrupt\n");
		ipw_rx_handle(priv);
		handled |= BIT_INT_RX;
	}

	if (inta & BIT_INT_TX) {
		IPW_DEBUG_ISR("Command completed.\n");
		ipw_write32(priv, CSR_FH_INT_STATUS, (1 << 6));
		if (ipw_grab_restricted_access(priv)) {
			ipw_write_restricted(priv,
					     FH_TCSR_CREDIT
					     (ALM_FH_SRVC_CHNL), 0x0);
			ipw_release_restricted_access(priv);
		}
		ipw_read32(priv, CSR_INT_MASK);

		handled |= BIT_INT_TX;
	}

	if (handled != inta) {
		IPW_ERROR("Unhandled INTA bits 0x%08x\n", inta & ~handled);
	}

	/* enable all interrupts */
	ipw_enable_interrupts(priv);

	spin_unlock_irqrestore(&priv->lock, flags);
}

/****************** 3945ABG-FUNCTIONS *********************/

static int ipw_send_rxon_assoc(struct ipw_priv *priv)
{
	int rc = 0;
	struct ipw_rx_packet *res = NULL;
	struct ipw_rxon_assoc_cmd rxon_assoc;
	struct ipw_host_cmd cmd = {
		.id = REPLY_RXON_ASSOC,
		.len = sizeof(struct ipw_rxon_assoc_cmd),
		.meta.flags = CMD_WANT_SKB,
		.data = &rxon_assoc,
	};

	rxon_assoc.flags = priv->staging_config.flags;
	rxon_assoc.filter_flags = priv->staging_config.filter_flags;
	rxon_assoc.ofdm_basic_rates = priv->staging_config.ofdm_basic_rates;
	rxon_assoc.cck_basic_rates = priv->staging_config.cck_basic_rates;
	rxon_assoc.reserved = 0;

	rc = ipw_send_cmd(priv, &cmd);
	if (rc)
		return rc;

	res = (struct ipw_rx_packet *)cmd.meta.u.skb->data;
	if (res->hdr.flags & 0x40) {
		IPW_ERROR("Bad return from REPLY_RXON_ASSOC command\n");
		rc = -EIO;
	}

	dev_kfree_skb_any(cmd.meta.u.skb);

#ifdef CONFIG_IPWRAW_DEBUG
	if (rc)
		ipw_print_rx_config(&priv->staging_config);
#endif

	return rc;
}

static int ipw_add_sta_sync_callback(struct ipw_priv *priv,
				     struct ipw_cmd *cmd, struct sk_buff *skb)
{
	struct ipw_rx_packet *res = NULL;

	if (!skb) {
		IPW_ERROR("Error: Response NULL in " "REPLY_ADD_STA.\n");
		return 1;
	}

	res = (struct ipw_rx_packet *)skb->data;
	if (res->hdr.flags & 0x40) {
		IPW_ERROR("Bad return from REPLY_ADD_STA (0x%08X)\n",
			  res->hdr.flags);
		return 1;
	}

	switch (res->u.add_sta.status) {
	case ADD_STA_SUCCESS_MSK:
		break;
	default:
		break;
	}

	return 1;		/* We didn't cache the SKB; let the caller free it */
}

static int ipw_send_add_station(struct ipw_priv *priv,
				struct ipw_addsta_cmd *sta, u8 flags)
{
	struct ipw_rx_packet *res = NULL;
	int rc = 0;
	struct ipw_host_cmd cmd = {
		.id = REPLY_ADD_STA,
		.len = sizeof(struct ipw_addsta_cmd),
		.meta.flags = flags,
		.data = sta,
	};

	if (flags & CMD_ASYNC)
		cmd.meta.u.callback = ipw_add_sta_sync_callback;
	else
		cmd.meta.flags |= CMD_WANT_SKB;

	rc = ipw_send_cmd(priv, &cmd);

	if (rc || (flags & CMD_ASYNC))
		return rc;

	res = (struct ipw_rx_packet *)cmd.meta.u.skb->data;
	if (res->hdr.flags & 0x40) {
		IPW_ERROR("Bad return from REPLY_ADD_STA (0x%08X)\n",
			  res->hdr.flags);
		rc = -EIO;
	}

	if (rc == 0) {
		switch (res->u.add_sta.status) {
		case ADD_STA_SUCCESS_MSK:
			IPW_DEBUG_INFO("REPLY_ADD_STA PASSED\n");
			break;
		default:
			rc = -EIO;
			IPW_WARNING("REPLY_ADD_STA failed\n");
			break;
		}
	}
	dev_kfree_skb_any(cmd.meta.u.skb);

	return rc;
}

static int ipw_card_state_sync_callback(struct ipw_priv *priv,
					struct ipw_cmd *cmd,
					struct sk_buff *skb)
{

	return 1;
}

/*
 * CARD_STATE_CMD
 *
 * Use: Sets the internal card state to enable, disable, or halt
 *
 * When in the 'enable' state the card operates as normal.
 * When in the 'disable' state, the card enters into a low power mode.
 * When in the 'halt' state, the card is shut down and must be fully
 * restarted to come back on.
 */
static int ipw_send_card_state(struct ipw_priv *priv, u32 flags, u8 meta_flag)
{
	struct ipw_host_cmd cmd = {
		.id = REPLY_CARD_STATE_CMD,
		.len = sizeof(u32),
		.data = &flags,
		.meta.flags = meta_flag,
	};

	if (meta_flag & CMD_ASYNC)
		cmd.meta.u.callback = ipw_card_state_sync_callback;

	return ipw_send_cmd(priv, &cmd);
}

/**
* rate  duration
**/
#define MMAC_SCHED_2_4GHZ_SIFS               10
#define MMAC_SCHED_5_2GHZ_SIFS               16
#define MMAC_SCHED_CCK_PHY_OVERHEAD_SHORT    96
#define MMAC_SCHED_CCK_PHY_OVERHEAD_LONG    192
#define MMAC_SCHED_OFDM_SYMBOL_TIME           4
#define MMAC_SCHED_OFDM_PHY_OVERHEAD         20
#define MMAC_SCHED_UCODE_OVERHEAD             8

#define IPW_INVALID_RATE                   0xFF

/* NOTE:
 * Do not reorder the rate_table_info list --
 *
 * OFDM rates must come before CCK rates in order for routines like
 * ipw_send_txpower_cmd to be compatible with the uCode
 */
static struct ipw_rate_info rate_table_info[] = {
/*  OFDM rate info   */
	{13, 6 * 2, 0, 24, 44, 52, 44, 228},	/*   6mbps */
	{15, 9 * 2, 1, 36, 36, 44, 36, 160},	/*   9mbps */
	{5, 12 * 2, 2, 48, 32, 36, 32, 124},	/*  12mbps */
	{7, 18 * 2, 3, 72, 28, 32, 28, 92},	/*  18mbps */
	{9, 24 * 2, 4, 96, 28, 32, 28, 72},	/*  24mbps */
	{11, 36 * 2, 5, 144, 24, 28, 24, 56},	/*  36mbps */
	{1, 48 * 2, 6, 192, 24, 24, 24, 48},	/*  48mbps */
	{3, 54 * 2, 7, 216, 24, 24, 24, 44},	/*  54mbps */
/*  CCK rate info   */
	{10, 2, 8, 0, 112, 160, 112, 1216},	/*   1mbps */
	{20, 4, 9, 0, 56, 80, 56, 608},	/*   2mbps */
	{55, 11, 10, 0, 21, 29, 21, 222},	/* 5.5mbps */
	{110, 22, 11, 0, 11, 15, 11, 111},	/*  11mbps */
};

static inline int ipw_rate_plcp2index(u8 x)
{
	int i;
	for (i = 0; i < ARRAY_SIZE(rate_table_info); i++) {
		if (rate_table_info[i].rate_plcp == x)
			return i;
	}
	return -1;
}

static inline int ipw_rate_scale2index(int x)
{
	int i;
	for (i = 0; i < ARRAY_SIZE(rate_table_info); i++) {
		if (rate_table_info[i].rate_scale_index == x)
			return i;
	}
	return -1;
}

static inline u8 ipw_rate_scaling2rate_plcp(int x)
{
	int i;
	for (i = 0; i < ARRAY_SIZE(rate_table_info); i++) {
		if (rate_table_info[i].rate_scale_index == x)
			return rate_table_info[i].rate_plcp;
	}
	return IPW_INVALID_RATE;
}

static inline int ipw_rate_plcp2rate_scaling(u8 x)
{
	int i;
	for (i = 0; i < ARRAY_SIZE(rate_table_info); i++) {
		if (rate_table_info[i].rate_plcp == x)
			return rate_table_info[i].rate_scale_index;
	}
	return -1;
}

static u8 ipw_rate_index2plcp(int x)
{

	if (x < ARRAY_SIZE(rate_table_info))
		return rate_table_info[x].rate_plcp;

	return IPW_INVALID_RATE;
}

/**
* RXON functions
**/
static u8 BROADCAST_ADDR[ETH_ALEN] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

/* Get antenna flags for RxOn command. */
static int ipw_get_antenna_flags(struct ipw_priv *priv)
{
	switch (priv->antenna) {
	case 0:		/* "diversity", NIC selects best antenna by itself */
		return 0;

	case 1:		/* force Main antenna */
		if (priv->eeprom.antenna_switch_type)
			return RXON_FLG_DIS_DIV_MSK | RXON_FLG_ANT_B_MSK;
		return RXON_FLG_DIS_DIV_MSK | RXON_FLG_ANT_A_MSK;

	case 2:		/* force Aux antenna */
		if (priv->eeprom.antenna_switch_type)
			return RXON_FLG_DIS_DIV_MSK | RXON_FLG_ANT_A_MSK;
		return RXON_FLG_DIS_DIV_MSK | RXON_FLG_ANT_B_MSK;
	}

	/* bad antenna selector value */
	IPW_ERROR("Bad antenna selector value (0x%x)\n", priv->antenna);
	return 0;		/* "diversity" is default if error */
}

/*
 * return index delta into power gain settings table
 */
static s32 reg_adjust_power_by_temp(s32 new_reading, s32 old_reading)
{
	s32 delta_index = (new_reading - old_reading) * (-11) / 100;
	return (u8) delta_index;
}

/* Keep temperature in sane range */
static inline int reg_temp_out_of_range(int temperature)
{
	return (((temperature < -260) || (temperature > 25)) ? 1 : 0);
}

/* get current temperature by reading from NIC via driver. */
static int reg_txpower_get_temperature(struct ipw_priv *priv)
{
	int temperature;

	temperature = ipw_get_temperature(priv);

	/* driver's okay range is -260 to +25.
	 *   human readable okay range is 0 to +285 */
	IPW_DEBUG_INFO("Temperature: %d\n", temperature + 260);

	/* handle insane temp reading */
	if (reg_temp_out_of_range(temperature)) {
		IPW_ERROR("Error bad temperature value  %d\n", temperature);

		/* if really really hot(?),
		 *   substitute the 3rd band/group's temp measured at factory */
		if (priv->last_temperature > 100)
			temperature = (s16) le16_to_cpu(
					    priv->eeprom.groups[2].temperature);
		else	/* else use most recent "sane" value from driver */
			temperature = priv->last_temperature;
	}

	return temperature;	/* raw, not "human readable" */
}

/* Adjust Txpower only if temperature variance is greater than threshold.
 * Threshold for PA measurement is lower than for timer, so
 *    we can favor the PA measurement method, and let it track along
 *    as long as we keep getting measurements with reasonable frequency.
 * Both are lower than older versions' 9 degrees */
#define IPW_TEMPERATURE_LIMIT_TIMER   6
#define IPW_TEMPERATURE_LIMIT_MEAS_PA 4

/* reads temperature and determines if new calibration is needed.
 * records new temperature in tx_mgr->curr_temperature.
 * replaces tx_mgr->last_temperature *only* if calib needed
 *    (assumes caller will actually do the calibration!). */
static int is_temp_calib_needed(struct ipw_priv *priv)
{
	int temp_diff;

	priv->curr_temperature = reg_txpower_get_temperature(priv);
	if (priv->last_temperature == 0)
		temp_diff = IPW_TEMPERATURE_LIMIT_TIMER;
	else
		temp_diff = priv->curr_temperature - priv->last_temperature;

	/* get absolute value */
	if (temp_diff < 0) {
		IPW_DEBUG_POWER("Getting cooler, delta %d,\n", temp_diff);
		temp_diff = -temp_diff;
	} else if (temp_diff == 0)
		IPW_DEBUG_POWER("Same temp,\n");
	else
		IPW_DEBUG_POWER("Getting warmer, delta %d,\n", temp_diff);

	/* if we don't need calibration, *don't* update last_temperature */
	if (temp_diff < IPW_TEMPERATURE_LIMIT_TIMER) {
		IPW_DEBUG_POWER("Timed thermal calib not needed\n");
		return 0;
	}

	IPW_DEBUG_POWER("Timed thermal calib needed\n");

	/* assume that caller will actually do calib ...
	 *   update the "last temperature" value */
	priv->last_temperature = priv->curr_temperature;
	return 1;
}

#define IPW_MAX_GAIN_ENTRIES 78
#define IPW_CCK_FROM_OFDM_POWER_DIFF  -5
#define IPW_CCK_FROM_OFDM_INDEX_DIFF (10)

/* radio and DSP power table, each step is 1/2 dB.
 * 1st number is for RF analog gain, 2nd number is for DSP pre-DAC gain. */
static struct ipw_tx_power power_gain_table[2][IPW_MAX_GAIN_ENTRIES] = {
	{
	 {251, 127},		/* 2.4 GHz, highest power */
	 {251, 127},
	 {251, 127},
	 {251, 127},
	 {251, 125},
	 {251, 110},
	 {251, 105},
	 {251, 98},
	 {187, 125},
	 {187, 115},
	 {187, 108},
	 {187, 99},
	 {243, 119},
	 {243, 111},
	 {243, 105},
	 {243, 97},
	 {243, 92},
	 {211, 106},
	 {211, 100},
	 {179, 120},
	 {179, 113},
	 {179, 107},
	 {147, 125},
	 {147, 119},
	 {147, 112},
	 {147, 106},
	 {147, 101},
	 {147, 97},
	 {147, 91},
	 {115, 107},
	 {235, 121},
	 {235, 115},
	 {235, 109},
	 {203, 127},
	 {203, 121},
	 {203, 115},
	 {203, 108},
	 {203, 102},
	 {203, 96},
	 {203, 92},
	 {171, 110},		/* 171, 127 is (was) uCode's default txpower value */
	 {171, 104},
	 {171, 98},
	 {139, 116},
	 {227, 125},
	 {227, 119},
	 {227, 113},
	 {227, 107},
	 {227, 101},
	 {227, 96},
	 {195, 113},
	 {195, 106},
	 {195, 102},
	 {195, 95},
	 {163, 113},
	 {163, 106},
	 {163, 102},
	 {163, 95},
	 {131, 113},
	 {131, 106},
	 {131, 102},
	 {131, 95},
	 {99, 113},
	 {99, 106},
	 {99, 102},
	 {99, 95},
	 {67, 113},
	 {67, 106},
	 {67, 102},
	 {67, 95},
	 {35, 113},
	 {35, 106},
	 {35, 102},
	 {35, 95},
	 {3, 113},
	 {3, 106},
	 {3, 102},
	 {3, 95}},		/* 2.4 GHz, lowest power */
	{
	 {251, 127},		/* 5.x GHz, highest power */
	 {251, 120},
	 {251, 114},
	 {219, 119},
	 {219, 101},
	 {187, 113},
	 {187, 102},
	 {155, 114},
	 {155, 103},
	 {123, 117},
	 {123, 107},
	 {123, 99},
	 {123, 92},
	 {91, 108},
	 {59, 125},
	 {59, 118},
	 {59, 109},
	 {59, 102},
	 {59, 96},
	 {59, 90},
	 {27, 104},
	 {27, 98},
	 {27, 92},
	 {115, 118},
	 {115, 111},
	 {115, 104},
	 {83, 126},
	 {83, 121},
	 {83, 113},
	 {83, 105},
	 {83, 99},
	 {51, 118},
	 {51, 111},
	 {51, 104},
	 {51, 98},
	 {19, 116},
	 {19, 109},
	 {19, 102},
	 {19, 98},
	 {19, 93},
	 {171, 113},		/* 171, 127 is (was) uCode's default txpower value */
	 {171, 107},
	 {171, 99},
	 {139, 120},
	 {139, 113},
	 {139, 107},
	 {139, 99},
	 {107, 120},
	 {107, 113},
	 {107, 107},
	 {107, 99},
	 {75, 120},
	 {75, 113},
	 {75, 107},
	 {75, 99},
	 {43, 120},
	 {43, 113},
	 {43, 107},
	 {43, 99},
	 {11, 120},
	 {11, 113},
	 {11, 107},
	 {11, 99},
	 {131, 107},
	 {131, 99},
	 {99, 120},
	 {99, 113},
	 {99, 107},
	 {99, 99},
	 {67, 120},
	 {67, 113},
	 {67, 107},
	 {67, 99},
	 {35, 120},
	 {35, 113},
	 {35, 107},
	 {35, 99},
	 {3, 120}}		/* 5.x GHz, lowest power */
};

/* keep gain index within range of the gain table */
static inline u8 reg_fix_power_index(s8 index)
{
	if (index < 0)
		return 0;
	if (index >= IPW_MAX_GAIN_ENTRIES)
		return IPW_MAX_GAIN_ENTRIES - 1;
	return (u8) index;
}

/* Kick off thermal recalibration check every 60 seconds */
#define REG_RECALIB_PERIOD (60)

/*
 * Set (in our channel info database) the direct scan Tx power for 1 Mbit (CCK)
 *    or 6 Mbit (OFDM) rates.
 */
static void reg_set_scan_power(struct ipw_priv *priv, u32 scan_tbl_index,
			       s32 rate_index, s8 * clip_pwrs,
			       struct ipw_channel_info *ch_info, int band_index)
{
	struct ipw_scan_power_info *scan_power_info;
	s8 power;
	u8 power_index;

	scan_power_info = &ch_info->scan_pwr_info[scan_tbl_index];

	/* use this channel group's 6Mbit clipping/saturation pwr,
	 *   but cap at regulatory scan power restriction (set during init
	 *   based on eeprom channel data) for this channel.  */
	power = min(ch_info->scan_power, clip_pwrs[RATE_SCALE_6M_INDEX]);

	/* further limit to user's max power preference.
	 * FIXME:  Other spectrum management power limitations do not
	 *   seem to apply?? */
	if (!priv->max_power_override)
		power = min(power, priv->user_txpower_limit);
	else
		power = priv->user_txpower_limit;
	scan_power_info->requested_power = power;

	/* find difference between new scan *power* and current "normal"
	 *   Tx *power* for 6Mb.  Use this difference (x2) to adjust the current
	 *   "normal" temperature-compensated Tx power *index* for this rate
	 *   (1Mb or 6Mb) to yield new temp-compensated scan power *index*. */
	power_index = ch_info->power_info[rate_index].power_table_index
	    - (power - ch_info->power_info
	       [RATE_SCALE_6M_INDEX].requested_power) * 2;

	/* store reference index that we use when adjusting *all* scan
	 *   powers after a PA feedback adjustment.  So we can accomodate
	 *   user (all channel) or spectrum management (single channel) power
	 *   changes "between" temperature or PA feedback compensation
	 *   procedures, back out the current cumulative PA adjustment value.
	 * when we're recalibrating *all* channels during init or using
	 *   reg_txpower_compensate_for_temperature_dif(), we should reset
	 *   the cumulative PA adjustment to 0, *before* calling
	 *   reg_set_scan_power(), for a clean start.
	 * don't force fit this reference index into gain table; it may be a
	 *   negative number.  This will help avoid errors when we're at
	 *   the lower bounds (highest gains, for warmest temperatures)
	 *   of the table. */
	scan_power_info->base_ref_index = power_index;

	/* don't exceed table bounds for "real" setting */
	power_index = reg_fix_power_index(power_index);

	scan_power_info->power_table_index = power_index;
	scan_power_info->tpc.tx_gain =
	    power_gain_table[band_index][power_index].tx_gain;
	scan_power_info->tpc.dsp_atten =
	    power_gain_table[band_index][power_index].dsp_atten;

	/* reference for use with cumulative PA adjustments */
	scan_power_info->power_table_index = power_index;
}

static struct ipw_channel_info *ipw_get_channel_info(struct ipw_priv *priv,
						     int band, int channel)
{
	int i;

	/* For channels that overlap the 2.4Ghz and 5.2Ghz band, we
	 * default to the 2.4Ghz band if both bands are currently enabled */
	if (band == IEEE80211_24GHZ_BAND) {	/* 2.4Ghz 802.11b/g */
		if (channel >= 1 && channel <= 14)
			return &priv->channel_info[channel - 1];
		else {
			IPW_ERROR("channel request misconfigured for "
				  "2.4Ghz band!\n");
			return NULL;
		}
	}

	if (band != IEEE80211_52GHZ_BAND) {
		IPW_ERROR("band misconfigured!\n");
		return NULL;
	}

	for (i = 14; i < priv->channel_count; i++) {	/* 5.2Ghz 802.11a */
		if (priv->channel_info[i].channel == channel)
			return &priv->channel_info[i];
	}

	return NULL;
}

/* fill in Tx Power command with gain settings for all rates for the current
 * channel, using values from channel info struct, and send to NIC via driver */
static int ipw_reg_send_txpower(struct ipw_priv *priv)
{
	int rate_idx;
	struct ipw_channel_info *ch_info = NULL;
	struct ipw_txpowertable_cmd txpower = {
		.channel = priv->channel,
	};

	txpower.band = (priv->channel <= IPW_MAX_CHANNEL_24) ? 1 : 0;
	ch_info = ipw_get_channel_info(priv, priv->band, priv->channel);
	if (!is_channel_valid(ch_info)) {
		if (ch_info) {
			IPW_DEBUG_INFO("Tx disabled on channel %d %d.\n",
				       priv->channel, priv->band);
			return 0;
		}

		IPW_ERROR("Failed to get channel info for channel %d %d\n",
			  priv->channel, priv->band);

		return -EINVAL;
	}

	/* fill cmd with power settings for all rates for current channel */
	for (rate_idx = 0; rate_idx < IPW_MAX_RATES; rate_idx++) {
		txpower.power[rate_idx].tpc = ch_info->power_info[rate_idx].tpc;
		txpower.power[rate_idx].rate = ipw_rate_index2plcp(rate_idx);

		IPW_DEBUG_POWER("ch %d rf %d dsp %d rate code 0x%x\n",
				ch_info->channel,
				txpower.power[rate_idx].tpc.tx_gain,
				txpower.power[rate_idx].tpc.dsp_atten,
				txpower.power[rate_idx].rate);
	}

	return ipw_send_cmd_pdu(priv, REPLY_TX_PWR_TABLE_CMD,
				sizeof(struct ipw_txpowertable_cmd), &txpower);
}

static void ipw_bg_reg_send_txpower(struct work_struct *work)
{
	struct ipw_priv *priv = IPW_DELAYED_DATA(work, struct ipw_priv, send_txpower);

	if (priv->status & STATUS_EXIT_PENDING)
		return;

	mutex_lock(&priv->mutex);
	ipw_reg_send_txpower(priv);
	mutex_unlock(&priv->mutex);
}

/* Compensate txpower settings of *all* channels for temperature.
 * This does *not* take into account any PA measurement, nor any
 *   current settings of txpowers ... only the difference between
 *   current temperature and the factory calibration temperatures,
 *   and bases the new settings on the channel's base_power_index.
 * If RxOn is "associated", this sends the new Txpower to NIC! */
static int reg_txpower_compensate_for_temperature_dif(struct ipw_priv *priv)
{
	struct ipw_channel_info *ch_info = NULL;
	s32 delta_index;
	s8 *clip_pwrs;		/* array of h/w max power levels for each rate */
	u8 a_band;
	u8 rate_index;
	u8 scan_tbl_index;
	u8 i;
	int ref_temp;
	int temperature = priv->curr_temperature;

	/* set up new Tx power info for each and every channel, 2.4 and 5.x */
	for (i = 0; i < priv->channel_count; i++) {
		ch_info = &priv->channel_info[i];
		a_band = is_channel_a_band(ch_info);

		/* Get this chnlgrp's factory calibration temperature */
		ref_temp = (s16)priv->eeprom.groups[ch_info->group_index].
		    temperature;

		/* get power index adjustment based on curr and factory temps */
		delta_index = reg_adjust_power_by_temp(temperature, ref_temp);

		/* set tx power value for all rates, OFDM and CCK */
		for (rate_index = 0; rate_index < IPW_MAX_RATES; rate_index++) {
			s32 power_idx;
			power_idx =
			    ch_info->power_info[rate_index].base_power_index;

			/* temperature compensate */
			power_idx += delta_index;

			/* stay within table range */
			power_idx = reg_fix_power_index(power_idx);
			ch_info->power_info[rate_index].
			    power_table_index = (u8) power_idx;
			ch_info->power_info[rate_index].tpc.
			    tx_gain =
			    power_gain_table[a_band][power_idx].tx_gain;
			ch_info->power_info[rate_index].tpc.
			    dsp_atten =
			    power_gain_table[a_band][power_idx].dsp_atten;
		}

		/* Get this chnlgrp's rate-to-max/clip-powers table */
		clip_pwrs = priv->clip_groups[ch_info->group_index].clip_powers;

		/* set scan tx power, 1Mbit for CCK, 6Mbit for OFDM */
		for (scan_tbl_index = 0;
		     scan_tbl_index < IPW_NUM_SCAN_RATES; scan_tbl_index++) {
			s32 actual_index = (scan_tbl_index == 0) ?
			    RATE_SCALE_1M_INDEX : RATE_SCALE_6M_INDEX;
			reg_set_scan_power(priv, scan_tbl_index,
					   actual_index, clip_pwrs,
					   ch_info, a_band);
		}
	}

	/* send Txpower command for current channel to ucode */
	return ipw_reg_send_txpower(priv);
}

/* called whenever it's time to check our temperature.
 * -- reset periodic timer
 * -- see if temp has changed enough to warrant re-calibration ... if so:
 *     -- correct coeffs for temp (can reset temp timer)
 *     -- save this temp as "last",
 *     -- build new PA vdet<->power tables
 *     -- send new set of gain settings to NIC
 *     -- clear PA measurement history
 * NOTE:  This should continue working, even when we're not associated,
 *   so we can keep our internal table of scan powers current. */
static void reg_txpower_periodic(struct ipw_priv *priv)
{
	/* This will kick in the "brute force"
	 *   reg_txpower_compensate_for_temperature_dif() below,
	 *   if we haven't been able to do a PA measurement recently.
	 *   (see reg_txpower_pa_measure()) */
	if (!is_temp_calib_needed(priv))
		goto reschedule;

	/* Set up a new set of temp-adjusted TxPowers, send to NIC.
	 * This is based *only* on current temperature,
	 * ignoring any previous power measurements */
	reg_txpower_compensate_for_temperature_dif(priv);

      reschedule:
	queue_delayed_work(priv->workqueue,
			   &priv->thermal_periodic, REG_RECALIB_PERIOD * HZ);
}

static void ipw_bg_reg_txpower_periodic(struct work_struct *work)
{
	struct ipw_priv *priv = IPW_DELAYED_DATA(work, struct ipw_priv, thermal_periodic);

	if (priv->status & STATUS_EXIT_PENDING)
		return;

	mutex_lock(&priv->mutex);
	reg_txpower_periodic(priv);
	mutex_unlock(&priv->mutex);
}

/* find the *channel-group* index (0-4) for the channel.
 *  ... used when initializing channel-info structs.
 * NOTE:  These channel groups do *NOT* match the bands above!
 *   These channel groups are based on factory-tested channels;
 *   on A-band, EEPROM's "group frequency" entries represent the top channel
 *   in each group 1-4.  Group 5 All B/G channels are in group 0.  */
static u16 reg_get_chnl_grp_index(struct ipw_priv *priv,
				  const struct ipw_channel_info *ch_info)
{
	struct ipw_eeprom_txpower_group *ch_grp = &priv->eeprom.groups[0];
	u8 group;
	u16 group_index = 0;	/* based on factory calib frequencies */
	u8 grp_channel;

	/* Find the group index for the channel ... don't use index 1(?) */
	/* FIXME:  Verify the channel group boundaries.  Windows code starts
	 *    at index "2", but that ignores the index "1" EEPROM data for
	 *    channels 44 and below.  I'll try starting at "1", to take
	 *    advantage of the factory calibration data at "1".  */
	if (ch_info->band == IEEE80211_52GHZ_BAND) {
		for (group = 1; group < 5; group++) {
			grp_channel = ch_grp[group].group_channel;
			if (ch_info->channel <= grp_channel) {
				group_index = group;
				break;
			}
		}
		/* group 4 has a few channels *above* its factory cal freq */
		if (group == 5)
			group_index = 4;
	} else
		group_index = 0;	/* 2.4 GHz, group 0 */

	IPW_DEBUG_POWER("Chnl %d mapped to grp %d\n", ch_info->channel,
			group_index);
	return group_index;
}

/*
 * Interpolate to get nominal (i.e. at factory calibration temperature) index
 *   into radio/DSP gain settings table for requested power.
 */
static int reg_get_matched_power_index(struct ipw_priv *priv,
				       s8 requested_power,
				       s32 setting_index, s32 * new_index)
{
	const struct ipw_eeprom_txpower_group *chnl_grp = NULL;
	s32 index0, index1;
	s32 rPower = 2 * requested_power;
	s32 i;
	const struct ipw_eeprom_txpower_sample *samples;
	s32 gains0, gains1;
	s32 res;
	s32 denominator;

	chnl_grp = &priv->eeprom.groups[setting_index];
	samples = chnl_grp->samples;
	for (i = 0; i < 5; i++) {
		if (rPower == samples[i].power) {
			*new_index = samples[i].gain_index;
			return 0;
		}
	}

	if (rPower > samples[1].power) {
		index0 = 0;
		index1 = 1;
	} else if (rPower > samples[2].power) {
		index0 = 1;
		index1 = 2;
	} else if (rPower > samples[3].power) {
		index0 = 2;
		index1 = 3;
	} else {
		index0 = 3;
		index1 = 4;
	}

	denominator = (s32) samples[index1].power - (s32) samples[index0].power;
	if (denominator == 0)
		return -EINVAL;
	gains0 = (s32) samples[index0].gain_index * (1 << 19);
	gains1 = (s32) samples[index1].gain_index * (1 << 19);
	res = gains0 + (gains1 - gains0) *
	    ((s32) rPower - (s32) samples[index0].power) / denominator +
	    (1 << 18);
	*new_index = res >> 19;
	return 0;
}

/*
 * read *factory calibration* tx_power data from eeprom
 * set up "tx manager" (overall Tx power calib structure)
 * and "channel group" structs within tx manager.
 */
static int reg_txpower_read_eeprom(struct ipw_priv *priv)
{
	int rc = 0;
	u32 i;
	s32 rate_index;
	struct ipw_eeprom_txpower_group *chnl_grp = NULL;

	IPW_DEBUG_POWER("Initializing factory calib info from EEPROM\n");

	for (i = 0; i < IPW_NUM_TX_CALIB_GROUPS; i++) {
		s8 *clip_pwrs;	/* table of power levels for each rate */
		s8 satur_pwr;	/* saturation power for each chnl group */
		chnl_grp = &priv->eeprom.groups[i];

		/* sanity check on factory saturation power value */
		if (chnl_grp->saturation_power < 40) {
			IPW_WARNING
			    ("Error: saturation power is %d, less than minimum expected 40\n",
			     chnl_grp->saturation_power);
			return -1;
		}

		/*
		 * Derive requested power levels for each rate, based on
		 *   hardware capabilities (saturation power for band).
		 * Basic value is 3dB down from saturation, with further
		 *   power reductions for highest 3 data rates.  These
		 *   backoffs provide headroom for high rate modulation
		 *   power peaks, without too much distortion (clipping).
		 */
		/* we'll fill in this array with h/w max power levels */
		clip_pwrs = priv->clip_groups[i].clip_powers;

		/* divide factory saturation power by 2 to find -3dB level */
		satur_pwr = (s8) (chnl_grp->saturation_power >> 1);

		/* fill in channel group's nominal powers for each rate */
		for (rate_index = 0;
		     rate_index < IPW_MAX_RATES; rate_index++, clip_pwrs++) {
			switch (rate_index) {
			case RATE_SCALE_36M_INDEX:
				if (i == 0)	/* B/G */
					*clip_pwrs = satur_pwr;
				else	/* A */
					*clip_pwrs = satur_pwr - 5;
				break;
			case RATE_SCALE_48M_INDEX:
				if (i == 0)
					*clip_pwrs = satur_pwr - 7;
				else
					*clip_pwrs = satur_pwr - 10;
				break;
			case RATE_SCALE_54M_INDEX:
				if (i == 0)
					*clip_pwrs = satur_pwr - 9;
				else
					*clip_pwrs = satur_pwr - 12;
				break;
			default:
				*clip_pwrs = satur_pwr;
				break;
			}
		}
	}

	return rc;
}

/*
 * Second pass (during init) to set up priv->channel_info
 *
 * Set up Tx-power settings in our channel info database for each VALID
 * (for this geo/SKU) channel, at all Tx data rates, based on eeprom values
 * and current temperature.
 *
 * Since this is based on current temperature (at init time), these values may
 * not be valid for very long, but it gives us a starting/default point,
 * and allows us to active (i.e. using Tx) scan.
 *
 * This does *not* write values to NIC, just sets up our internal table.
 */
static int reg_txpower_set_from_eeprom(struct ipw_priv *priv)
{
	struct ipw_channel_info *ch_info = NULL;
	struct ipw_channel_power_info *pwr_info;
	s32 delta_index;
	u8 channel;
	u8 rate_index;
	u8 scan_tbl_index;
	s8 *clip_pwrs;		/* array of power levels for each rate */
	u8 gain, dsp_atten;
	s8 power;
	u8 pwr_index, base_pwr_index, a_band;
	u8 i;
	int temperature;

	/* save temperature reference,
	 *   so we can determine next time to calibrate */
	temperature = reg_txpower_get_temperature(priv);
	priv->last_temperature = temperature;

	/* initialize Tx power info for each and every channel, 2.4 and 5.x */
	for (i = 0, ch_info = priv->channel_info; i < priv->channel_count;
	     i++, ch_info++) {
		a_band = is_channel_a_band(ch_info);
		channel = ch_info->channel;
		if (!is_channel_valid(ch_info))
			continue;

		/* find this channel's channel group (*not* "band") index */
		/* FIXME:  Check for IPW_INVALID_CHNL */
		ch_info->group_index = reg_get_chnl_grp_index(priv, ch_info);

		/* Get this chnlgrp's rate->max/clip-powers table */
		clip_pwrs = priv->clip_groups[ch_info->group_index].clip_powers;

		/* calculate power index *adjustment* value according to
		 *   diff between current temperature and factory temperature */
		delta_index = reg_adjust_power_by_temp(temperature,
			(s16)le16_to_cpu(priv->eeprom.groups[
					ch_info->group_index].temperature));

		/* set tx power value for all OFDM rates */
		for (rate_index = 0; rate_index < IPW_OFDM_RATES; rate_index++) {
			s32 power_idx;
			s32 old_power_idx;
			int rc = 0;

			/* use channel group's clip-power table,
			 *   but don't exceed channel's max power */
			s8 power = min(ch_info->max_power_avg,
				       clip_pwrs[rate_index]);

			pwr_info = &ch_info->power_info[rate_index];

			/* get base (i.e. at factory-measured temperature)
			 *    power table index for this rate's power */
			rc = reg_get_matched_power_index(priv, power,
							 ch_info->group_index,
							 &power_idx);
			if (rc)
				return rc;
			pwr_info->base_power_index = (u8) power_idx;

			/* temperature compensate */
			power_idx += delta_index;
			old_power_idx = power_idx;

			/* stay within range of gain table */
			power_idx = reg_fix_power_index(power_idx);

			/* fill 1 OFDM rate's ipw_channel_power_info struct */
			pwr_info->requested_power = power;
			pwr_info->power_table_index = (u8) power_idx;
			pwr_info->tpc.tx_gain =
			    power_gain_table[a_band][power_idx].tx_gain;
			pwr_info->tpc.dsp_atten =
			    power_gain_table[a_band][power_idx].dsp_atten;
		}

		/* set tx power for CCK rates, based on OFDM 12 Mbit settings */
		pwr_info = &ch_info->power_info[RATE_SCALE_12M_INDEX];
		power = pwr_info->requested_power
		    + IPW_CCK_FROM_OFDM_POWER_DIFF;
		pwr_index = pwr_info->power_table_index
		    + IPW_CCK_FROM_OFDM_INDEX_DIFF;
		base_pwr_index = pwr_info->base_power_index
		    + IPW_CCK_FROM_OFDM_INDEX_DIFF;

		/* stay within table range */
		pwr_index = reg_fix_power_index(pwr_index);
		gain = power_gain_table[a_band][pwr_index].tx_gain;
		dsp_atten = power_gain_table[a_band][pwr_index].dsp_atten;

		/* fill each CCK rate's ipw_channel_power_info structure
		 * NOTE:  All CCK-rate Txpwrs are the same for a given chnl!
		 * NOTE:  CCK rates start at end of OFDM rates! */
		for (rate_index = IPW_OFDM_RATES;
		     rate_index < IPW_MAX_RATES; rate_index++) {
			pwr_info = &ch_info->power_info[rate_index];
			pwr_info->requested_power = power;
			pwr_info->power_table_index = pwr_index;
			pwr_info->base_power_index = base_pwr_index;
			pwr_info->tpc.tx_gain = gain;
			pwr_info->tpc.dsp_atten = dsp_atten;
		}

		/* set scan tx power, 1Mbit for CCK, 6Mbit for OFDM */
		for (scan_tbl_index = 0;
		     scan_tbl_index < IPW_NUM_SCAN_RATES; scan_tbl_index++) {
			s32 actual_index = (scan_tbl_index == 0) ?
			    RATE_SCALE_1M_INDEX : RATE_SCALE_6M_INDEX;
			reg_set_scan_power(priv, scan_tbl_index,
					   actual_index, clip_pwrs,
					   ch_info, a_band);
		}
	}

	return 0;
}

/*
  add AP station into station table. there is only one AP
  station with id=0
*/
static int ipw_rxon_add_station(struct ipw_priv *priv, u8 * addr, int is_ap)
{
	/* Remove this station if it happens to already exist */
	ipw_remove_station(priv, addr, is_ap);

	return ipw_add_station(priv, addr, is_ap, 0);
}

static inline int check_bits(unsigned long field, unsigned long mask)
{
	return ((field & mask) == mask) ? 1 : 0;
}

/* commit_flags
 * examine staging_rxon vs. active_rxon
 * if no change from active_rxon:assoc and staging_rxon:assoc then
 * rxon_assoc.
 */
static inline int tune_required(struct ipw_priv *priv)
{
	if (memcmp
	    (priv->staging_config.bssid_addr, priv->active_config.bssid_addr,
	     ETH_ALEN))
		return 1;

	if ((priv->staging_config.dev_type != priv->active_config.dev_type) ||
	    (priv->staging_config.channel != priv->active_config.channel))
		return 1;

	/* Check if we are not switching bands */
	if (check_bits(priv->staging_config.flags, RXON_FLG_BAND_24G_MSK) !=
	    check_bits(priv->active_config.flags, RXON_FLG_BAND_24G_MSK))
		return 1;

	/* Check if we are switching association toggle */
	if (check_bits(priv->staging_config.filter_flags,
		       RXON_FILTER_ASSOC_MSK) &&
	    check_bits(priv->active_config.filter_flags, RXON_FILTER_ASSOC_MSK))
		return 0;

	return 1;
}

static int ipw_commit_config(struct ipw_priv *priv)
{
	int rc = 0;

	if (!ipw_is_alive(priv))
		return -1;

	/* always get timestamp with Rx frame */
	priv->staging_config.flags |= RXON_FLG_TSF2HOST_MSK;

	/* select antenna */
	priv->staging_config.flags &=
	    ~(RXON_FLG_DIS_DIV_MSK | RXON_FLG_ANT_SEL_MSK);
	priv->staging_config.flags |= ipw_get_antenna_flags(priv);

	/* If we don't need to retune, we can use ipw_rxon_assoc_cmd which
	 * is used to reconfigure filter and other flags for the current
	 * radio configuration.
	 *
	 * If we need to tune, we need to request the regulatory
	 * daemon to tune and configure the radio via ipw_send_rx_config. */
	if (!tune_required(priv))
		rc = ipw_send_rxon_assoc(priv);
	else {
		/* Sending the RXON command clears out the station table,
		 * so we must clear out our cached table values so we will
		 * re-add stations to the uCode for TX */
		ipw_clear_stations_table(priv);

		/* If we are currently associated and the new config requires
		 * a tune *and* the new config wants the associated mask enabled,
		 * we must clear the associated from the active configuration
		 * before we apply the new config */
		if (is_associated(priv) &&
		    (priv->staging_config.
		     filter_flags & RXON_FILTER_ASSOC_MSK)) {
			priv->active_config.filter_flags &=
			    ~RXON_FILTER_ASSOC_MSK;
			rc = ipw_send_cmd_pdu(priv, REPLY_RXON,
					      sizeof(struct ipw_rxon_cmd),
					      &priv->active_config);

			/* If the mask clearing failed then we set active_config
			 * back to what it was previously */
			if (!rc)
				priv->active_config.filter_flags |=
				    RXON_FILTER_ASSOC_MSK;

		}

		if (!rc)
			rc = ipw_send_cmd_pdu(priv, REPLY_RXON,
					      sizeof(struct ipw_rxon_cmd),
					      &priv->staging_config);
		if (!rc)
			ipw_reg_send_txpower(priv);

		/* Add the broadcast address so we can send broadcast frames */
		ipw_rxon_add_station(priv, BROADCAST_ADDR, 0);
	}

	if (rc)
		IPW_ERROR("Error setting configuration.  Reload driver.\n");
	else
		memcpy(&priv->active_config, &priv->staging_config,
		       sizeof(priv->active_config));

	return rc;
}

static void ipw_link_up(struct ipw_priv *priv)
{
	BUG_ON(!priv->netdev_registered);

	priv->last_seq_num = -1;
	priv->last_frag_num = -1;
	priv->last_packet_time = 0;

	netif_carrier_on(priv->net_dev);
	if (netif_queue_stopped(priv->net_dev)) {
		IPW_DEBUG_NOTIF("waking queue\n");
		netif_wake_queue(priv->net_dev);
	} else {
		IPW_DEBUG_NOTIF("starting queue\n");
		netif_start_queue(priv->net_dev);
	}

	ipw_update_link_led(priv);

}

static void ipw_bg_commit(struct work_struct *work)
{
	struct ipw_priv *priv = IPW_DELAYED_DATA(work, struct ipw_priv, commit);

	if (priv->status & STATUS_EXIT_PENDING)
		return;

	mutex_lock(&priv->mutex);
	ipw_commit_config(priv);
	ipw_link_up(priv);
	mutex_unlock(&priv->mutex);
}

static int ipw_send_bt_config(struct ipw_priv *priv)
{
	struct ipw_bt_cmd bt_cmd = {
		.flags = 3,
		.leadTime = 0xAA,
		.maxKill = 1,
		.killAckMask = 0,
		.killCTSMask = 0,
	};

	return ipw_send_cmd_pdu(priv, REPLY_BT_CONFIG,
				sizeof(struct ipw_bt_cmd), &bt_cmd);
}

/***************** END ***********************************/

#define MAX_UCODE_BEACON_INTERVAL	1024
#define INTEL_CONN_LISTEN_INTERVAL	0xA

static int ipw_send_power_mode(struct ipw_priv *priv, u32 mode)
{
	u32 final_mode = mode;
	int rc = 0;
	unsigned long flags;
	struct ipw_powertable_cmd cmd;

	/* If on battery, set to 3, if AC set to CAM, else user
	 * level */
	switch (mode) {
	case IPW_POWER_BATTERY:
		final_mode = IPW_POWER_INDEX_3;
		break;
	case IPW_POWER_AC:
		final_mode = IPW_POWER_MODE_CAM;
		break;
	default:
		final_mode = mode;
		break;
	}

	ipw_update_power_cmd(priv, &cmd, final_mode);

	rc = ipw_send_cmd_pdu(priv, POWER_TABLE_CMD, sizeof(cmd), &cmd);

	spin_lock_irqsave(&priv->lock, flags);

	if (final_mode == IPW_POWER_MODE_CAM) {
		priv->status &= ~STATUS_POWER_PMI;
	} else {
		priv->status |= STATUS_POWER_PMI;
	}

	spin_unlock_irqrestore(&priv->lock, flags);
	return rc;
}

/* The ipw_eeprom_band definitions below provide the mapping from the
 * EEPROM contents to the specific channel number supported for each
 * band.
 *
 * For example, ipw_priv->eeprom.band_3_channels[4] from the band_3
 * definition below maps to physical channel 42 in the 5.2Ghz spectrum.
 * The specific geography and calibration information for that channel
 * is contained in the eeprom map itself.
 *
 * During init, we copy the eeprom information and channel map
 * information into priv->channel_info_24/52 and priv->channel_map_24/52
 *
 * channel_map_24/52 provides the index in the channel_info array for a
 * given channel.  We have to have two separate maps as there is channel
 * overlap with the 2.4Ghz and 5.2Ghz spectrum as seen in band_1 and
 * band_2
 *
 * A value of 0xff stored in the channel_map indicates that the channel
 * is not supported by the hardware at all.
 *
 * A valud of 0xfe in the channel_map indicates that the channel is not
 * valid for Tx with the current hardware.  This means that
 * while the system can tune and receive on a given channel, it may not
 * be able to associate or transmit any frames on that
 * channel.  There is no corresponding channel information for that
 * entry.
 *
 */

/* 2.4 GHz */
static u8 ipw_eeprom_band_1[] = {
	1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14
};

/* 5.2 Ghz bands */
static u8 ipw_eeprom_band_2[] = {
	183, 184, 185, 187, 188, 189, 192, 196, 7, 8, 11, 12, 16
};

static u8 ipw_eeprom_band_3[] = {	/* 5205-5320MHz */
	34, 36, 38, 40, 42, 44, 46, 48, 52, 56, 60, 64
};

static u8 ipw_eeprom_band_4[] = {	/* 5500-5700MHz */
	100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140
};

static u8 ipw_eeprom_band_5[] = {	/* 5725-5825MHz */
	145, 149, 153, 157, 161, 165
};

static void ipw_init_band_reference(struct ipw_priv *priv, int band,
				    int *eeprom_ch_count,
				    const struct ipw_eeprom_channel
				    **eeprom_ch_info,
				    const u8 ** eeprom_ch_index)
{
	switch (band) {
	case 1:		/* 2.4Ghz band */
		*eeprom_ch_count = ARRAY_SIZE(ipw_eeprom_band_1);
		*eeprom_ch_info = priv->eeprom.band_1_channels;
		*eeprom_ch_index = ipw_eeprom_band_1;
		break;
	case 2:		/* 5.2Ghz band */
		*eeprom_ch_count = ARRAY_SIZE(ipw_eeprom_band_2);
		*eeprom_ch_info = priv->eeprom.band_2_channels;
		*eeprom_ch_index = ipw_eeprom_band_2;
		break;
	case 3:		/* 5.2Ghz band */
		*eeprom_ch_count = ARRAY_SIZE(ipw_eeprom_band_3);
		*eeprom_ch_info = priv->eeprom.band_3_channels;
		*eeprom_ch_index = ipw_eeprom_band_3;
		break;
	case 4:		/* 5.2Ghz band */
		*eeprom_ch_count = ARRAY_SIZE(ipw_eeprom_band_4);
		*eeprom_ch_info = priv->eeprom.band_4_channels;
		*eeprom_ch_index = ipw_eeprom_band_4;
		break;
	case 5:		/* 5.2Ghz band */
		*eeprom_ch_count = ARRAY_SIZE(ipw_eeprom_band_5);
		*eeprom_ch_info = priv->eeprom.band_5_channels;
		*eeprom_ch_index = ipw_eeprom_band_5;
		break;
	default:
		BUG();
		return;
	}
}

#define IPW_INVALID_CHANNEL                   0xFF
#define IPW_INVALID_TX_CHANNEL                0xFE

#define CHECK_AND_PRINT(x) ((eeprom_ch_info[c].flags & IPW_CHANNEL_##x) ? # x " " : "")

static int ipw_init_channel_map(struct ipw_priv *priv)
{
	int eeprom_ch_count = 0;
	const u8 *eeprom_ch_index = NULL;
	const struct ipw_eeprom_channel *eeprom_ch_info = NULL;
	int b, c;
	struct ipw_channel_info *ch_info;

	if (priv->eeprom.version < 0x2f) {
		IPW_WARNING("Unsupported EEPROM version: 0x%04X\n",
			    priv->eeprom.version);
		return -EINVAL;
	}

	IPW_DEBUG_INFO("Initializing regulatory info from EEPROM\n");

	priv->channel_count =
	    ARRAY_SIZE(ipw_eeprom_band_1) +
	    ARRAY_SIZE(ipw_eeprom_band_2) +
	    ARRAY_SIZE(ipw_eeprom_band_3) +
	    ARRAY_SIZE(ipw_eeprom_band_4) + ARRAY_SIZE(ipw_eeprom_band_5);

	IPW_DEBUG_INFO("Parsing data for %d channels.\n", priv->channel_count);

	kfree(priv->channel_info);
	priv->channel_info = kzalloc(sizeof(struct ipw_channel_info) *
				     priv->channel_count, GFP_KERNEL);
	if (!priv->channel_info)
		return -ENOMEM;

	ch_info = priv->channel_info;
	priv->a_count = priv->bg_count = 0;

	/* Loop through the 5 EEPROM bands adding them in order to the
	 * channel map we maintain (that contains additional information than
	 * what just in the EEPROM) */
	for (b = 1; b <= 5; b++) {

		ipw_init_band_reference(priv, b, &eeprom_ch_count,
					&eeprom_ch_info, &eeprom_ch_index);

		/* Loop through each band adding each of the channels */
		for (c = 0; c < eeprom_ch_count; c++) {
			ch_info->channel = eeprom_ch_index[c];
			ch_info->band = (b == 1) ? IEEE80211_24GHZ_BAND :
			    IEEE80211_52GHZ_BAND;

			/* permanently store EEPROM's channel regulatory flags
			 *   and max power in channel info database. */
			ch_info->eeprom = eeprom_ch_info[c];

			/* Copy the run-time flags so they are there even on
			 * invalid channels */
			ch_info->flags = eeprom_ch_info[c].flags;

			if (!(is_channel_valid(ch_info))) {
				IPW_DEBUG_INFO("Ch. %d [%sGhz] - No Tx\n",
					       ch_info->channel,
					       (ch_info->band ==
						IEEE80211_24GHZ_BAND) ?
					       "2.4" : "5.2");
				ch_info++;
				continue;
			}

			if (ch_info->band == IEEE80211_24GHZ_BAND)
				priv->bg_count++;
			else
				priv->a_count++;

			/* Initialize regulatory-based run-time data */
			ch_info->max_power_avg = ch_info->curr_txpow =
			    eeprom_ch_info[c].max_power_avg;
			ch_info->scan_power = eeprom_ch_info[c].max_power_avg;
			ch_info->min_power = 0;

			if (is_channel_passive(ch_info) ||
			    is_channel_radar(ch_info)) {
				ch_info->tx_locked = 1;
				ch_info->rx_unlock = 0;
			}

			IPW_DEBUG_INFO("Ch. %d [%sGhz] %s%s%s%s%s%s(" BIT_FMT8
				       " %ddBm): Ad-Hoc %ssupported\n",
				       ch_info->channel,
				       (ch_info->band == IEEE80211_24GHZ_BAND) ?
				       "2.4" : "5.2",
				       CHECK_AND_PRINT(IBSS),
				       CHECK_AND_PRINT(ACTIVE),
				       CHECK_AND_PRINT(RADAR),
				       CHECK_AND_PRINT(WIDE),
				       CHECK_AND_PRINT(NARROW),
				       CHECK_AND_PRINT(DFS),
				       BIT_ARG8(eeprom_ch_info[c].flags),
				       eeprom_ch_info[c].
				       max_power_avg,
				       ((eeprom_ch_info[c].
					 flags & IPW_CHANNEL_IBSS)
					&& !(eeprom_ch_info[c].
					     flags & IPW_CHANNEL_RADAR))
				       ? "" : "not ");

			/* Set the user_txpower_limit to the highest power
			 * supported by any channel */
			if (eeprom_ch_info[c].max_power_avg >
			    priv->user_txpower_limit)
				priv->user_txpower_limit =
				    eeprom_ch_info[c].max_power_avg;

			if (ch_info->max_power_avg > priv->max_txpower_limit)
				priv->max_txpower_limit =
				    ch_info->max_power_avg;

			ch_info++;
		}
	}

	reg_txpower_read_eeprom(priv);
	reg_txpower_set_from_eeprom(priv);

	return 0;
}

static void ipw_set_supported_rates_mask(struct ipw_priv *priv, int rates_mask)
{
	priv->active_rate = rates_mask & 0xffff;
	priv->active_rate_basic = (rates_mask >> 16) & 0xffff;
}

static void ipw_post_alive_work(struct ipw_priv *priv)
{
	int rc = 0;

	rc = ipw_init_channel_map(priv);
	if (rc) {
		IPW_ERROR("initializing regulatory failed: %d\n", rc);
		return;
	}

	if ((priv->a_count == 0) && priv->is_abg) {
		printk(KERN_INFO DRV_NAME
		       ": Incorrectly detected BG card as ABG.  Please send "
		       "your PCI ID 0x%04X:0x%04X to maintainer.\n",
		       priv->pci_dev->device, priv->pci_dev->subsystem_device);
		priv->is_abg = 0;
	}

	printk(KERN_INFO DRV_NAME
	       ": Detected geography %cBG (%d 802.11bg "
	       "channels, %d 802.11a channels)\n",
	       priv->is_abg ? 'A' : 'x', priv->bg_count, priv->a_count);

	if (!priv->netdev_registered) {
		mutex_unlock(&priv->mutex);
		rc = register_netdev(priv->net_dev);
		if (rc) {
			IPW_ERROR("Failed to register network "
				  "device (error %d) - Scheduling re-init "
				  "in 5 seconds.\n", rc);
			queue_delayed_work(priv->workqueue,
					   &priv->restart, 5 * HZ);
			return;
		}
		if (param_rtap_iface) {
			rc = ipw_prom_alloc(priv);
			if (rc) {
				IPW_ERROR
				    ("Failed to register promiscuous network "
				     "device (error %d) - Scheduling re-init "
				     "in 5 seconds.\n", rc);
				queue_delayed_work(priv->workqueue,
						   &priv->restart, 5 * HZ);
				unregister_netdev(priv->net_dev);
				return;
			}
		}
		mutex_lock(&priv->mutex);
		priv->netdev_registered = 1;
	}

	ipw_set_supported_rates_mask(priv,
				     IEEE80211_OFDM_DEFAULT_RATES_MASK |
				     (IEEE80211_OFDM_BASIC_RATES_MASK << 16) |
				     IEEE80211_CCK_DEFAULT_RATES_MASK |
				     (IEEE80211_CCK_BASIC_RATES_MASK << 16));

	ipw_send_power_mode(priv, IPW_POWER_LEVEL(priv->power_mode));

	/* Initialize our rx_config data */
	ipw_connection_init_rx_config(priv);
	memcpy(priv->staging_config.node_addr, priv->net_dev->dev_addr,
	       ETH_ALEN);

	/* Configure BT coexistence */
	ipw_send_bt_config(priv);

	/* Configure the adapter for unassociated operation */
	ipw_commit_config(priv);

	/* At this point, the NIC is initialized and operational */
	priv->status |= STATUS_READY;

	ipw_update_link_led(priv);

	reg_txpower_periodic(priv);
}

static void eeprom_parse_mac(struct ipw_priv *priv, u8 * mac)
{
	memcpy(mac, priv->eeprom.mac_address, 6);
}

/*
 * Read EEPROM contents
 */
static int ipw_eeprom_init(struct ipw_priv *priv)
{
	u16 *e = (u16 *) & priv->eeprom;
	u32 r;
	int to;
	u32 gp = ipw_read32(priv, CSR_EEPROM_GP);
	u16 sz = sizeof(priv->eeprom);
	int rc;
	u16 addr;

	if (sizeof(priv->eeprom) != 1024) {
		IPW_ERROR("EEPROM structure size incorrect!\n");
		return -EINVAL;
	}

	if ((gp & 0x00000007) == 0x00000000) {
		IPW_ERROR("EEPROM not found, EEPROM_GP=0x%08x", gp);
		return -ENOENT;
	}

	ipw_clear_bit(priv, CSR_EEPROM_GP, 0x00000180);
	for (addr = 0, r = 0; addr < sz; addr += 2) {
		ipw_write32(priv, CSR_EEPROM_REG, addr << 1);
		ipw_clear_bit(priv, CSR_EEPROM_REG, 0x00000002);
		rc = ipw_grab_restricted_access(priv);
		if (rc)
			return rc;

		for (to = 0; to < 10; to++) {
			r = ipw_read_restricted(priv, CSR_EEPROM_REG);
			if (r & 1)
				break;
			udelay(5);
		}

		ipw_release_restricted_access(priv);

		if (!(r & 1)) {
			IPW_ERROR("Time out reading EEPROM[%d]", addr);
			return -ETIMEDOUT;
		}

		e[addr / 2] = r >> 16;
	}

	return 0;
}

/************************** RX-FUNCTIONS ****************************/
/*
 * Rx theory of operation
 *
 * The host allocates 32 DMA target addresses and passes the host address
 * to the firmware at register IPW_RFDS_TABLE_LOWER + N * RFD_SIZE where N is
 * 0 to 31
 *
 * Rx Queue Indexes
 * The host/firmware share two index registers for managing the Rx buffers.
 *
 * The READ index maps to the first position that the firmware may be writing
 * to -- the driver can read up to (but not including) this position and get
 * good data.
 * The READ index is managed by the firmware once the card is enabled.
 *
 * The WRITE index maps to the last position the driver has read from -- the
 * position preceding WRITE is the last slot the firmware can place a packet.
 *
 * The queue is empty (no good data) if WRITE = READ - 1, and is full if
 * WRITE = READ.
 *
 * During initialization the host sets up the READ queue position to the first
 * INDEX position, and WRITE to the last (READ - 1 wrapped)
 *
 * When the firmware places a packet in a buffer it will advance the READ index
 * and fire the RX interrupt.  The driver can then query the READ index and
 * process as many packets as possible, moving the WRITE index forward as it
 * resets the Rx queue buffers with new memory.
 *
 * The management in the driver is as follows:
 * + A list of pre-allocated SKBs is stored in ipw->rxq->rx_free.  When
 *   ipw->rxq->free_count drops to or below RX_LOW_WATERMARK, work is scheduled
 *   to replensish the ipw->rxq->rx_free.
 * + In ipw_rx_queue_replenish (scheduled) if 'processed' != 'read' then the
 *   ipw->rxq is replenished and the READ INDEX is updated (updating the
 *   'processed' and 'read' driver indexes as well)
 * + A received packet is processed and handed to the kernel network stack,
 *   detached from the ipw->rxq.  The driver 'processed' index is updated.
 * + The Host/Firmware ipw->rxq is replenished at tasklet time from the rx_free
 *   list. If there are no allocated buffers in ipw->rxq->rx_free, the READ
 *   INDEX is not incremented and ipw->status(RX_STALLED) is set.  If there
 *   were enough free buffers and RX_STALLED is set it is cleared.
 *
 *
 * Driver sequence:
 *
 * ipw_rx_queue_alloc()       Allocates rx_free
 * ipw_rx_queue_replenish()   Replenishes rx_free list from rx_used, and calls
 *                            ipw_rx_queue_restock
 * ipw_rx_queue_restock()     Moves available buffers from rx_free into Rx
 *                            queue, updates firmware pointers, and updates
 *                            the WRITE index.  If insufficient rx_free buffers
 *                            are available, schedules ipw_rx_queue_replenish
 *
 * -- enable interrupts --
 * ISR - ipw_rx()             Detach ipw_rx_mem_buffers from pool up to the
 *                            READ INDEX, detaching the SKB from the pool.
 *                            Moves the packet buffer from queue to rx_used.
 *                            Calls ipw_rx_queue_restock to refill any empty
 *                            slots.
 * ...
 *
 */

static inline int ipw_rx_queue_space(struct ipw_rx_queue *q)
{
	int s = q->read - q->write;
	if (s <= 0)
		s += RX_QUEUE_SIZE;
	s -= 2;			// keep some buffer to not confuse full and empty queue
	if (s < 0)
		s = 0;
	return s;
}

static int ipw_rx_queue_update_write_ptr(struct ipw_priv *priv,
					 struct ipw_rx_queue *q)
{
	u32 reg = 0;
	int rc = 0;
	unsigned long flags;

	spin_lock_irqsave(&q->lock, flags);

	if (q->need_update == 0)
		goto exit_unlock;

	if (priv->status & STATUS_POWER_PMI) {
		reg = ipw_read32(priv, CSR_UCODE_DRV_GP1);

		if (reg & CSR_UCODE_DRV_GP1_BIT_MAC_SLEEP) {
			ipw_set_bit(priv, CSR_GP_CNTRL,
				    CSR_GP_CNTRL_REG_FLAG_MAC_ACCESS_REQ);
			goto exit_unlock;
		}

		rc = ipw_grab_restricted_access(priv);
		if (rc)
			goto exit_unlock;

		ipw_write_restricted(priv, FH_RCSR_WPTR(0), q->write & ~0x7);
		ipw_release_restricted_access(priv);
	} else {
		ipw_write32(priv, FH_RCSR_WPTR(0), q->write & ~0x7);
		ipw_read32(priv, CSR_INT_MASK);
	}

	q->need_update = 0;

      exit_unlock:
	spin_unlock_irqrestore(&q->lock, flags);
	return rc;
}

/*
 * If there are slots in the RX queue that  need to be restocked,
 * and we have free pre-allocated buffers, fill the ranks as much
 * as we can pulling from rx_free.
 *
 * This moves the 'write' index forward to catch up with 'processed', and
 * also updates the memory address in the firmware to reference the new
 * target buffer.
 */
static int ipw_rx_queue_restock(struct ipw_priv *priv)
{
	struct ipw_rx_queue *rxq = priv->rxq;
	struct list_head *element;
	struct ipw_rx_mem_buffer *rxb;
	unsigned long flags;
	int write;
	int counter = 0;
	int rc;

	spin_lock_irqsave(&rxq->lock, flags);
	write = rxq->write & ~0x7;
	while ((ipw_rx_queue_space(rxq) > 0) && (rxq->free_count)) {
		element = rxq->rx_free.next;
		rxb = list_entry(element, struct ipw_rx_mem_buffer, list);
		list_del(element);

		((u32 *) rxq->bd)[rxq->write] = (u32) rxb->dma_addr;

		rxq->queue[rxq->write] = rxb;
		rxq->write = (rxq->write + 1) % RX_QUEUE_SIZE;
		rxq->free_count--;
		counter++;
	}
	spin_unlock_irqrestore(&rxq->lock, flags);
	/* If the pre-allocated buffer pool is dropping low, schedule to
	 * refill it */
	if (rxq->free_count <= RX_LOW_WATERMARK) {
		queue_delayed_work(priv->workqueue, &priv->rx_replenish, 0);
	}

	counter = ipw_rx_queue_space(rxq);
	/* If we've added more space for the firmware to place data, tell it */
	if ((write != (rxq->write & ~0x7))
	    || (abs(rxq->write - rxq->read) > 7)) {
		spin_lock_irqsave(&rxq->lock, flags);
		rxq->need_update = 1;
		spin_unlock_irqrestore(&rxq->lock, flags);
		rc = ipw_rx_queue_update_write_ptr(priv, rxq);
		if (rc) {
			return rc;
		}
	}

	return 0;
}

/*
 * Move all used packet from rx_used to rx_free, allocating a new SKB for each.
 * Also restock the Rx queue via ipw_rx_queue_restock.
 *
 * This is called as a scheduled work item (except for during intialization)
 */
static void ipw_rx_queue_replenish(struct ipw_priv *priv)
{
	struct ipw_rx_queue *rxq = priv->rxq;
	struct list_head *element;
	struct ipw_rx_mem_buffer *rxb;
	unsigned long flags;
	spin_lock_irqsave(&rxq->lock, flags);
	while (!list_empty(&rxq->rx_used)) {
		element = rxq->rx_used.next;
		rxb = list_entry(element, struct ipw_rx_mem_buffer, list);
		rxb->skb = alloc_skb(IPW_RX_BUF_SIZE, GFP_ATOMIC);
		if (!rxb->skb) {
			printk(KERN_CRIT
			       "%s: Can not allocate SKB buffers.\n",
			       priv->net_dev->name);
			/* We don't reschedule replenish work here -- we will
			 * call the restock method and if it still needs
			 * more buffers it will schedule replenish */
			break;
		}
		list_del(element);
		rxb->dma_addr =
		    pci_map_single(priv->pci_dev, rxb->skb->data,
				   IPW_RX_BUF_SIZE, PCI_DMA_FROMDEVICE);
		list_add_tail(&rxb->list, &rxq->rx_free);
		rxq->free_count++;
	}
	spin_unlock_irqrestore(&rxq->lock, flags);

	spin_lock_irqsave(&priv->lock, flags);
	ipw_rx_queue_restock(priv);
	spin_unlock_irqrestore(&priv->lock, flags);
}

static void ipw_bg_rx_queue_replenish(struct work_struct *work)
{
	struct ipw_priv *priv = IPW_DELAYED_DATA(work, struct ipw_priv, rx_replenish);

	if (priv->status & STATUS_EXIT_PENDING)
		return;

	mutex_lock(&priv->mutex);
	ipw_rx_queue_replenish(priv);
	mutex_unlock(&priv->mutex);
}

/* Assumes that the skb field of the buffers in 'pool' is kept accurate.
 * If an SKB has been detached, the POOL needs to have it's SKB set to NULL
 * This free routine walks the list of POOL entries and if SKB is set to
 * non NULL it is unmapped and freed
 */
static void ipw_rx_queue_free(struct ipw_priv *priv, struct ipw_rx_queue *rxq)
{
	int i;
	if (!rxq)
		return;
	for (i = 0; i < RX_QUEUE_SIZE + RX_FREE_BUFFERS; i++) {
		if (rxq->pool[i].skb != NULL) {
			pci_unmap_single(priv->pci_dev,
					 rxq->pool[i].dma_addr,
					 IPW_RX_BUF_SIZE, PCI_DMA_FROMDEVICE);
			dev_kfree_skb(rxq->pool[i].skb);
		}
	}

	pci_free_consistent(priv->pci_dev, 4 * RX_QUEUE_SIZE, rxq->bd,
			    rxq->dma_addr);
	kfree(rxq);
}

static int ipw_rxq_stop(struct ipw_priv *priv)
{
	int rc;
	unsigned long flags;

	spin_lock_irqsave(&priv->lock, flags);
	rc = ipw_grab_restricted_access(priv);
	if (rc) {
		spin_unlock_irqrestore(&priv->lock, flags);
		return rc;
	}

	ipw_write_restricted(priv, FH_RCSR_CONFIG(0), 0);
	rc = ipw_poll_restricted_bit(priv, FH_RSSR_STATUS, (1 << 24), 1000);
	if (rc < 0)
		IPW_ERROR("Can't stop Rx DMA.\n");

	ipw_release_restricted_access(priv);
	spin_unlock_irqrestore(&priv->lock, flags);

	return 0;
}

static int ipw_rx_init(struct ipw_priv *priv, struct ipw_rx_queue *rxq)
{
	int rc;
	unsigned long flags;

	spin_lock_irqsave(&priv->lock, flags);
	rc = ipw_grab_restricted_access(priv);
	if (rc) {
		spin_unlock_irqrestore(&priv->lock, flags);
		return rc;
	}

	ipw_write_restricted(priv, FH_RCSR_RBD_BASE(0), rxq->dma_addr);
	ipw_write_restricted(priv, FH_RCSR_RPTR_ADDR(0),
			     priv->shared_phys +
			     offsetof(struct ipw_shared_t, rx_read_ptr[0]));
	ipw_write_restricted(priv, FH_RCSR_WPTR(0), 0);
	ipw_write_restricted(priv, FH_RCSR_CONFIG(0),
			     ALM_FH_RCSR_RX_CONFIG_REG_VAL_DMA_CHNL_EN_ENABLE
			     |
			     ALM_FH_RCSR_RX_CONFIG_REG_VAL_RDRBD_EN_ENABLE
			     |
			     ALM_FH_RCSR_RX_CONFIG_REG_BIT_WR_STTS_EN
			     |
			     ALM_FH_RCSR_RX_CONFIG_REG_VAL_MAX_FRAG_SIZE_128
			     | (RX_QUEUE_SIZE_LOG <<
				ALM_FH_RCSR_RX_CONFIG_REG_POS_RBDC_SIZE)
			     |
			     ALM_FH_RCSR_RX_CONFIG_REG_VAL_IRQ_DEST_INT_HOST
			     | (1 << ALM_FH_RCSR_RX_CONFIG_REG_POS_IRQ_RBTH)
			     | ALM_FH_RCSR_RX_CONFIG_REG_VAL_MSG_MODE_FH);

	/* fake read to flush all prev I/O */
	ipw_read_restricted(priv, FH_RSSR_CTRL);

	ipw_release_restricted_access(priv);

	spin_unlock_irqrestore(&priv->lock, flags);

	return 0;
}

static struct ipw_rx_queue *ipw_rx_queue_alloc(struct ipw_priv *priv)
{
	struct ipw_rx_queue *rxq;
	struct pci_dev *dev = priv->pci_dev;
	int i;
	rxq = (struct ipw_rx_queue *)kmalloc(sizeof(*rxq), GFP_ATOMIC);
	memset(rxq, 0, sizeof(*rxq));

	spin_lock_init(&rxq->lock);
	INIT_LIST_HEAD(&rxq->rx_free);
	INIT_LIST_HEAD(&rxq->rx_used);
	rxq->bd = pci_alloc_consistent(dev, 4 * RX_QUEUE_SIZE, &rxq->dma_addr);
	/* Fill the rx_used queue with _all_ of the Rx buffers */
	for (i = 0; i < RX_FREE_BUFFERS + RX_QUEUE_SIZE; i++)
		list_add_tail(&rxq->pool[i].list, &rxq->rx_used);
	/* Set us so that we have processed and used all buffers, but have
	 * not restocked the Rx queue with fresh buffers */
	rxq->read = rxq->write = 0;
	rxq->free_count = 0;
	rxq->need_update = 0;
	return rxq;
}

static inline void ipw_rx_queue_reset(struct ipw_priv *priv,
				      struct ipw_rx_queue *rxq)
{
	unsigned long flags;
	int i;
	spin_lock_irqsave(&rxq->lock, flags);
	INIT_LIST_HEAD(&rxq->rx_free);
	INIT_LIST_HEAD(&rxq->rx_used);
	/* Fill the rx_used queue with _all_ of the Rx buffers */
	for (i = 0; i < RX_FREE_BUFFERS + RX_QUEUE_SIZE; i++) {
		/* In the reset function, these buffers may have been allocated
		 * to an SKB, so we need to unmap and free potential storage */
		if (rxq->pool[i].skb != NULL) {
			pci_unmap_single(priv->pci_dev,
					 rxq->pool[i].dma_addr,
					 IPW_RX_BUF_SIZE, PCI_DMA_FROMDEVICE);
			dev_kfree_skb(rxq->pool[i].skb);
			rxq->pool[i].skb = NULL;
		}
		list_add_tail(&rxq->pool[i].list, &rxq->rx_used);
	}

	/* Set us so that we have processed and used all buffers, but have
	 * not restocked the Rx queue with fresh buffers */
	rxq->read = rxq->write = 0;
	rxq->free_count = 0;
	spin_unlock_irqrestore(&rxq->lock, flags);
}

/************************ NIC-FUNCTIONS *******************/
/**
 * Check that card is still alive.
 * Reads debug register from domain0.
 * If card is present, pre-defined value should
 * be found there.
 *
 * @param priv
 * @return 1 if card is present, 0 otherwise
 */
static inline int ipw_alive(struct ipw_priv *priv)
{
	u32 val = ipw_read32(priv, CSR_RESET);
	if (val & CSR_RESET_REG_FLAG_NEVO_RESET)
		return 0;

	IPW_DEBUG_INFO("Card is alive \n");
	return 1;
}

/* These functions load the firmware and micro code for the operation of
 * the ipw hardware.  It assumes the buffer has all the bits for the
 * image and the caller is handling the memory allocation and clean up.
 */

static int ipw_nic_stop_master(struct ipw_priv *priv)
{
	int rc = 0;
	u32 reg_val;
	unsigned long flags;

	spin_lock_irqsave(&priv->lock, flags);

	/* set stop master bit */
	ipw_set_bit(priv, CSR_RESET, CSR_RESET_REG_FLAG_STOP_MASTER);

	reg_val = ipw_read32(priv, CSR_GP_CNTRL);

	if (CSR_GP_CNTRL_REG_FLAG_MAC_POWER_SAVE ==
	    (reg_val & CSR_GP_CNTRL_REG_MSK_POWER_SAVE_TYPE)) {
		IPW_DEBUG_INFO
		    ("Card in power save, master is already stopped\n");
	} else {
		rc = ipw_poll_bit(priv,
				  CSR_RESET,
				  CSR_RESET_REG_FLAG_MASTER_DISABLED,
				  CSR_RESET_REG_FLAG_MASTER_DISABLED, 100);
		if (rc < 0) {
			spin_unlock_irqrestore(&priv->lock, flags);
			return rc;
		}
	}

	spin_unlock_irqrestore(&priv->lock, flags);
	IPW_DEBUG_INFO("stop master\n");

	return rc;
}

static int ipw_nic_set_pwr_src(struct ipw_priv *priv, int pwr_max)
{
	int rc = 0;
	unsigned long flags;

	//return 0;
	spin_lock_irqsave(&priv->lock, flags);
	rc = ipw_grab_restricted_access(priv);
	if (rc) {
		spin_unlock_irqrestore(&priv->lock, flags);
		return rc;
	}

	if (!pwr_max) {
		u32 val;
		rc = pci_read_config_dword(priv->pci_dev, 0x0C8, &val);
		if (val & PCI_CFG_PMC_PME_FROM_D3COLD_SUPPORT) {
			ipw_set_bits_mask_restricted_reg(priv,
							 ALM_APMG_PS_CTL,
							 APMG_PS_CTRL_REG_VAL_POWER_SRC_VAUX,
							 ~APMG_PS_CTRL_REG_MSK_POWER_SRC);
			ipw_release_restricted_access(priv);

			ipw_poll_bit(priv, CSR_GPIO_IN, CSR_GPIO_IN_VAL_VAUX_PWR_SRC, CSR_GPIO_IN_BIT_AUX_POWER, 5000);	//uS
		}

	} else {
		ipw_set_bits_mask_restricted_reg(priv,
						 ALM_APMG_PS_CTL,
						 APMG_PS_CTRL_REG_VAL_POWER_SRC_VMAIN,
						 ~APMG_PS_CTRL_REG_MSK_POWER_SRC);

		ipw_release_restricted_access(priv);
		ipw_poll_bit(priv, CSR_GPIO_IN, CSR_GPIO_IN_VAL_VMAIN_PWR_SRC, CSR_GPIO_IN_BIT_AUX_POWER, 5000);	//uS
	}
	spin_unlock_irqrestore(&priv->lock, flags);

	return rc;
}



static int ipw_nic_init(struct ipw_priv *priv)
{
	u8 rev_id;
	int rc;
	unsigned long flags;

	ipw_power_init_handle(priv);

	spin_lock_irqsave(&priv->lock, flags);
	ipw_set_bit(priv, CSR_ANA_PLL_CFG, (1 << 24));
	ipw_set_bit(priv, CSR_GIO_CHICKEN_BITS,
		    CSR_GIO_CHICKEN_BITS_REG_BIT_L1A_NO_L0S_RX);

	ipw_set_bit(priv, CSR_GP_CNTRL, CSR_GP_CNTRL_REG_FLAG_INIT_DONE);
	rc = ipw_poll_bit(priv, CSR_GP_CNTRL,
			  CSR_GP_CNTRL_REG_FLAG_MAC_CLOCK_READY,
			  CSR_GP_CNTRL_REG_FLAG_MAC_CLOCK_READY, 25000);
	if (rc < 0) {
		spin_unlock_irqrestore(&priv->lock, flags);
		IPW_DEBUG_INFO("Failed to init the card\n");
		return rc;
	}

	rc = ipw_grab_restricted_access(priv);
	if (rc) {
		spin_unlock_irqrestore(&priv->lock, flags);
		return rc;
	}
	ipw_write_restricted_reg(priv, ALM_APMG_CLK_EN,
				 APMG_CLK_REG_VAL_DMA_CLK_RQT |
				 APMG_CLK_REG_VAL_BSM_CLK_RQT);
	udelay(20);
	ipw_set_bits_restricted_reg(priv, ALM_APMG_PCIDEV_STT,
				    APMG_DEV_STATE_REG_VAL_L1_ACTIVE_DISABLE);
	ipw_release_restricted_access(priv);
	spin_unlock_irqrestore(&priv->lock, flags);

	/* Determine HW type */
	rc = pci_read_config_byte(priv->pci_dev, PCI_REVISION_ID, &rev_id);
	if (rc)
		return rc;
	IPW_DEBUG_INFO("HW Revision ID = 0x%X\n", rev_id);

	ipw_nic_set_pwr_src(priv, 1);
	spin_lock_irqsave(&priv->lock, flags);

	if (rev_id & PCI_CFG_REV_ID_BIT_RTP)
		IPW_DEBUG_INFO("RTP type \n");
	else if (rev_id & PCI_CFG_REV_ID_BIT_BASIC_SKU) {
		IPW_DEBUG_INFO("ALM-MB type\n");
		ipw_set_bit(priv, CSR_HW_IF_CONFIG_REG,
			    CSR_HW_IF_CONFIG_REG_BIT_ALMAGOR_MB);
	} else {
		IPW_DEBUG_INFO("ALM-MM type\n");
		ipw_set_bit(priv, CSR_HW_IF_CONFIG_REG,
			    CSR_HW_IF_CONFIG_REG_BIT_ALMAGOR_MM);
	}

	spin_unlock_irqrestore(&priv->lock, flags);

	/* Initialize the EEPROM */
	rc = ipw_eeprom_init(priv);
	if (rc)
		return rc;

	spin_lock_irqsave(&priv->lock, flags);
	if (EEPROM_SKU_CAP_OP_MODE_MRC == priv->eeprom.sku_cap) {
		IPW_DEBUG_INFO("SKU OP mode is mrc\n");
		ipw_set_bit(priv, CSR_HW_IF_CONFIG_REG,
			    CSR_HW_IF_CONFIG_REG_BIT_SKU_MRC);
	} else {
		IPW_DEBUG_INFO("SKU OP mode is basic\n");
	}

	if ((priv->eeprom.board_revision & 0xF0) == 0xD0) {
		IPW_DEBUG_INFO("3945ABG revision is 0x%X\n",
			       priv->eeprom.board_revision);
		ipw_set_bit(priv, CSR_HW_IF_CONFIG_REG,
			    CSR_HW_IF_CONFIG_REG_BIT_BOARD_TYPE);
	} else {
		IPW_DEBUG_INFO("3945ABG revision is 0x%X\n",
			       priv->eeprom.board_revision);
		ipw_clear_bit(priv, CSR_HW_IF_CONFIG_REG,
			      CSR_HW_IF_CONFIG_REG_BIT_BOARD_TYPE);
	}

	if (priv->eeprom.almgor_m_version <= 1) {
		ipw_set_bit(priv, CSR_HW_IF_CONFIG_REG,
			    CSR_HW_IF_CONFIG_REG_BITS_SILICON_TYPE_A);
		IPW_DEBUG_INFO("Card M type A version is 0x%X\n",
			       priv->eeprom.almgor_m_version);
	} else {
		IPW_DEBUG_INFO("Card M type B version is 0x%X\n",
			       priv->eeprom.almgor_m_version);
		ipw_set_bit(priv, CSR_HW_IF_CONFIG_REG,
			    CSR_HW_IF_CONFIG_REG_BITS_SILICON_TYPE_B);
	}
	spin_unlock_irqrestore(&priv->lock, flags);

	switch (priv->eeprom.sku_cap & (EEPROM_SKU_CAP_SW_RF_KILL_ENABLE |
					EEPROM_SKU_CAP_HW_RF_KILL_ENABLE)) {
	case EEPROM_SKU_CAP_HW_RF_KILL_ENABLE:
		IPW_DEBUG_RF_KILL("HW RF KILL supported in EEPROM.\n");
		break;
	case EEPROM_SKU_CAP_SW_RF_KILL_ENABLE:
		IPW_DEBUG_RF_KILL("SW RF KILL supported in EEPROM.\n");
		break;
	case (EEPROM_SKU_CAP_SW_RF_KILL_ENABLE | EEPROM_SKU_CAP_HW_RF_KILL_ENABLE):
		IPW_DEBUG_RF_KILL("HW & HW RF KILL supported in EEPROM.\n");
		break;
	default:
		IPW_DEBUG_RF_KILL("NO RF KILL supported in EEPROM.\n");
		break;
	}

	/* Allocate the RX queue, or reset if it is already allocated */
	if (!priv->rxq)
		priv->rxq = ipw_rx_queue_alloc(priv);
	else
		ipw_rx_queue_reset(priv, priv->rxq);

	if (!priv->rxq) {
		IPW_ERROR("Unable to initialize Rx queue\n");
		return -ENOMEM;
	}
	ipw_rx_queue_replenish(priv);

	ipw_rx_init(priv, priv->rxq);

	spin_lock_irqsave(&priv->lock, flags);
	rc = ipw_grab_restricted_access(priv);
	if (rc) {
		spin_unlock_irqrestore(&priv->lock, flags);
		return rc;
	}
	ipw_write_restricted(priv, FH_RCSR_WPTR(0), priv->rxq->write & ~7);
	ipw_release_restricted_access(priv);

	spin_unlock_irqrestore(&priv->lock, flags);

	rc = ipw_queue_reset(priv);
	if (rc)
		return rc;

	priv->status |= STATUS_INIT;

	return 0;
}

/* Call this function from process context, it will sleeps */
static int ipw_nic_reset(struct ipw_priv *priv)
{
	int rc = 0;
	unsigned long flags;

	ipw_nic_stop_master(priv);

	spin_lock_irqsave(&priv->lock, flags);
	ipw_set_bit(priv, CSR_RESET, CSR_RESET_REG_FLAG_SW_RESET);

	rc = ipw_poll_bit(priv, CSR_GP_CNTRL,
			  CSR_GP_CNTRL_REG_FLAG_MAC_CLOCK_READY,
			  CSR_GP_CNTRL_REG_FLAG_MAC_CLOCK_READY, 25000);

	rc = ipw_grab_restricted_access(priv);
	if (rc) {
		spin_unlock_irqrestore(&priv->lock, flags);
		return rc;
	}

	ipw_write_restricted_reg(priv, APMG_CLK_CTRL_REG,
				 APMG_CLK_REG_VAL_BSM_CLK_RQT);

	udelay(10);

	ipw_set_bit(priv, CSR_GP_CNTRL, CSR_GP_CNTRL_REG_FLAG_INIT_DONE);

	ipw_write_restricted_reg(priv, ALM_APMG_LARC_INT_MSK, 0x0);
	ipw_write_restricted_reg(priv, ALM_APMG_LARC_INT, 0xFFFFFFFF);

	/* enable DMA */
	ipw_write_restricted_reg(priv, ALM_APMG_CLK_EN,
				 APMG_CLK_REG_VAL_DMA_CLK_RQT |
				 APMG_CLK_REG_VAL_BSM_CLK_RQT);
	udelay(10);

	ipw_set_bits_restricted_reg(priv, ALM_APMG_PS_CTL,
				    APMG_PS_CTRL_REG_VAL_ALM_R_RESET_REQ);
	udelay(5);
	ipw_clear_bits_restricted_reg(priv, ALM_APMG_PS_CTL,
				      APMG_PS_CTRL_REG_VAL_ALM_R_RESET_REQ);
	ipw_release_restricted_access(priv);

	/* Clear the 'host command active' bit... */
	priv->status &= ~STATUS_HCMD_ACTIVE;

	wake_up_interruptible(&priv->wait_command_queue);
	spin_unlock_irqrestore(&priv->lock, flags);

	return rc;
}

/********************* UCODE-DOWNLOAD-FUNCTIONS ***********************/

static int attach_buffer_to_tfd_frame(struct tfd_frame *tfd,
				      dma_addr_t addr, u16 len)
{
	int count = 0;
	u32 pad;

	count = TFD_CTL_COUNT_GET(tfd->control_flags);
	pad = TFD_CTL_PAD_GET(tfd->control_flags);

	if ((count >= NUM_TFD_CHUNKS) || (count < 0)) {
		IPW_ERROR("Error can not send more than %d chunks\n",
			  NUM_TFD_CHUNKS);
		return -EINVAL;
	}

	tfd->pa[count].addr = (u32) addr;
	tfd->pa[count].len = len;

	count++;

	tfd->control_flags = TFD_CTL_COUNT_SET(count) | TFD_CTL_PAD_SET(pad);

	return 0;
}




/* verify runtime uCode image in card vs. host */
static int ipw_verify_inst_full(struct ipw_priv *priv, u32* image, u32 len)
{
	u32 val;
	u32 save_len = len;
	int rc = 0;
	u32 errcnt;

	/* Since data memory has already been modified by running uCode,
	 * we can't really verify the data image, but we'll show its size. */
	IPW_DEBUG_INFO("ucode data image size is %u\n", priv->ucode_data.len);

	/* read from instruction memory to verify instruction image */
	image = priv->ucode_code.v_addr;
	len = priv->ucode_code.len;

	IPW_DEBUG_INFO("ucode inst image size is %u\n", len);

	rc = ipw_grab_restricted_access(priv);
	if (rc)
		return rc;

	ipw_write_restricted(priv, HBUS_TARG_MEM_RADDR, RTC_INST_LOWER_BOUND);

	errcnt = 0;
	for (; len > 0; len -= sizeof(u32), image++) {
		/* read data comes through single port, auto-incr addr */
		val = ipw_read_restricted(priv, HBUS_TARG_MEM_RDAT);
		if (val !=le32_to_cpu(*image)) {
			IPW_ERROR("uCode INST section is invalid at "
				  "offset 0x%x, is 0x%x, s/b 0x%x\n",
				  save_len - len, val, le32_to_cpu(*image));
			rc = -EIO;
			errcnt++;
			if (errcnt >= 20)
				break;
		}
	}

	ipw_release_restricted_access(priv);

	if (!errcnt)
		IPW_DEBUG_INFO
		    ("ucode image in INSTRUCTION memory is good\n");

	return rc;
}



static void ipw_dealloc_ucode_pci(struct ipw_priv *priv)
{
	if (priv->ucode_code.v_addr != NULL) {
		pci_free_consistent(priv->pci_dev,
				    priv->ucode_code.len,
				    priv->ucode_code.v_addr,
				    priv->ucode_code.p_addr);
		priv->ucode_code.v_addr = NULL;
	}
	if (priv->ucode_data.v_addr != NULL) {
		pci_free_consistent(priv->pci_dev,
				    priv->ucode_data.len,
				    priv->ucode_data.v_addr,
				    priv->ucode_data.p_addr);
		priv->ucode_data.v_addr = NULL;
	}
	if (priv->ucode_data_backup.v_addr != NULL) {
		pci_free_consistent(priv->pci_dev,
				    priv->ucode_data_backup.len,
				    priv->ucode_data_backup.v_addr,
				    priv->ucode_data_backup.p_addr);
		priv->ucode_data_backup.v_addr = NULL;
	}
	if (priv->ucode_init.v_addr != NULL) {
		pci_free_consistent(priv->pci_dev,
				    priv->ucode_init.len,
				    priv->ucode_init.v_addr,
				    priv->ucode_init.p_addr);
		priv->ucode_init.v_addr = NULL;
	}
	if (priv->ucode_init_data.v_addr != NULL) {
		pci_free_consistent(priv->pci_dev,
				    priv->ucode_init_data.len,
				    priv->ucode_init_data.v_addr,
				    priv->ucode_init_data.p_addr);
		priv->ucode_init_data.v_addr = NULL;
	}
	if (priv->ucode_boot.v_addr != NULL) {
		pci_free_consistent(priv->pci_dev,
				    priv->ucode_boot.len,
				    priv->ucode_boot.v_addr,
				    priv->ucode_boot.p_addr);
		priv->ucode_boot.v_addr = NULL;
	}
}


/* Read uCode images from disk file.
 * Copy into buffers for card to fetch via bus-mastering */
static int ipw_verify_inst_sparse(struct ipw_priv *priv, u32* image, u32 len)
{
	u32 val;
	int rc = 0;

	u32 errcnt = 0;
	u32 i;



	IPW_DEBUG_INFO("ucode inst image size is %u\n", len);

	rc = ipw_grab_restricted_access(priv);
	if (rc)
		return rc;

	for (i = 0; i < len; i += 100, image += 100/sizeof(u32)) {
		/* read data comes through single port, auto-incr addr */
		/* NOTE: Use the debugless read so we don't flood kernel log
		* if IPW_DL_IO is set */
		ipw_write_restricted(priv, HBUS_TARG_MEM_RADDR,
		i + RTC_INST_LOWER_BOUND);
		val = _ipw_read_restricted(priv, HBUS_TARG_MEM_RDAT);
		if (val != *image) {
			rc = -EIO;
			errcnt++;
			if (errcnt >= 3)
			break;
		}
	}

	ipw_release_restricted_access(priv);


	return rc;
}

/**
 * ipw_verify_ucode - determine which instruction image is in SRAM,
 *    and verify its contents
 */
static int ipw_verify_ucode(struct ipw_priv *priv)
{
	u32 *image;
	u32 len;
	int rc = 0;

	/* Try bootstrap */
	image = priv->ucode_boot.v_addr;
	len = priv->ucode_boot.len;
	rc = ipw_verify_inst_sparse(priv, image, len);
	if (rc == 0) {
		IPW_DEBUG_INFO("Bootstrap uCode is good in inst SRAM\n");
		return 0;
	}
	/* Try initialize */
	image = priv->ucode_init.v_addr;
	len = priv->ucode_init.len;
	rc = ipw_verify_inst_sparse(priv, image, len);
	if (rc == 0) {
		IPW_DEBUG_INFO("Initialize uCode is good in inst SRAM\n");
		return 0;
	}

	/* Try runtime/protocol */
	image = priv->ucode_code.v_addr;
	len = priv->ucode_code.len;
	rc = ipw_verify_inst_sparse(priv, image, len);
	if (rc == 0) {
		IPW_DEBUG_INFO("Runtime uCode is good in inst SRAM\n");
		return 0;
	}

	IPW_ERROR("NO VALID UCODE IMAGE IN INSTRUCTION SRAM!!\n");

	/* Show first several data entries in instruction SRAM.
	 * Selection of bootstrap image is arbitrary. */
	image = priv->ucode_boot.v_addr;
	len = priv->ucode_boot.len;
	rc = ipw_verify_inst_full(priv, image, len);

	return rc;
}

/* check contents of special bootstrap uCode SRAM */
static int ipw_verify_bsm(struct ipw_priv *priv)
{
	u8 *image = priv->ucode_boot.v_addr;
	u32 len = priv->ucode_boot.len;
	u32 reg;
	u32 val;
	int rc;

	IPW_DEBUG_INFO("Begin verify bsm\n");

	rc = ipw_grab_restricted_access(priv);
	if (rc)
		return rc;

	/* verify BSM SRAM contents */
	val = ipw_read_restricted_reg(priv, BSM_WR_DWCOUNT_REG);
	for (reg = BSM_SRAM_LOWER_BOUND;
	     reg < BSM_SRAM_LOWER_BOUND + len;
	     reg += sizeof(u32), image += sizeof(u32)) {
		val = ipw_read_restricted_reg(priv, reg);
		if (val != *(u32 *) image) {
			IPW_ERROR("BSM uCode verification failed at "
				  "addr 0x%08X+%u (of %u), is 0x%x, s/b 0x%x\n",
				  BSM_SRAM_LOWER_BOUND,
				  reg - BSM_SRAM_LOWER_BOUND, len,
				  val, *(u32 *) image);
			ipw_release_restricted_access(priv);
			return -EIO;
		}
	}

	ipw_release_restricted_access(priv);

	IPW_DEBUG_INFO("BSM bootstrap uCode image OK\n");

	return 0;
}
/**
 * ipw_load_bsm - Load bootstrap instructions
 *
 * BSM operation:
 *
 * The Bootstrap State Machine (BSM) stores a short bootstrap uCode program
 * in special SRAM that does not power down during RFKILL.  When powering back
 * up after power-saving sleeps (or during initial uCode load), the BSM loads
 * the bootstrap program into the on-board processor, and starts it.
 *
 * The bootstrap program loads (via DMA) instructions and data for a new
 * program from host DRAM locations indicated by the host driver in the
 * BSM_DRAM_* registers.  Once the new program is loaded, it starts
 * automatically.
 *
 * When initializing the NIC, the host driver points the BSM to the
 * "initialize" uCode image.  This uCode sets up some internal data, then
 * notifies host via "initialize alive" that it is complete.
 *
 * The host then replaces the BSM_DRAM_* pointer values to point to the
 * normal runtime uCode instructions and a backup uCode data cache buffer
 * (filled initially with starting data values for the on-board processor),
 * then triggers the "initialize" uCode to load and launch the runtime uCode,
 * which begins normal operation.
 *
 * When doing a power-save shutdown, runtime uCode saves data SRAM into
 * the backup data cache in DRAM before SRAM is powered down.
 *
 * When powering back up, the BSM loads the bootstrap program.  This reloads
 * the runtime uCode instructions and the backup data cache into SRAM,
 * and re-launches the runtime uCode from where it left off.
 */
static int ipw_load_bsm(struct ipw_priv *priv)
{
	u8 *image = priv->ucode_boot.v_addr;
	u32 len = priv->ucode_boot.len;
	dma_addr_t pinst;
	dma_addr_t pdata;
	u32 inst_len;
	u32 data_len;
	int rc;
	int i;
	u32 done;

	IPW_DEBUG_INFO("Begin load bsm\n");

	/* make sure bootstrap program is no larger than BSM's SRAM size */
	if (len > IPW_MAX_BSM_SIZE)
		return -EINVAL;

	/* Tell bootstrap uCode where to find the "Initialize" uCode
	 *   in host DRAM ... bits 31:0 for 3945, bits 35:4 for 4965.
	 * NOTE:  ipw_initialize_alive_start() will replace these values,
	 *        after the "initialize" uCode has run, to point to
	 *        runtime/protocol instructions and backup data cache. */

	pinst = priv->ucode_init.p_addr;
	pdata = priv->ucode_init_data.p_addr;
	//endnote

	inst_len = priv->ucode_init.len;
	data_len = priv->ucode_init_data.len;

	rc = ipw_grab_restricted_access(priv);
	if (rc)
		return rc;

	ipw_write_restricted_reg(priv, BSM_DRAM_INST_PTR_REG, pinst);
	ipw_write_restricted_reg(priv, BSM_DRAM_DATA_PTR_REG, pdata);
	ipw_write_restricted_reg(priv, BSM_DRAM_INST_BYTECOUNT_REG, inst_len);
	ipw_write_restricted_reg(priv, BSM_DRAM_DATA_BYTECOUNT_REG, data_len);

	/* Fill BSM memory with bootstrap instructions */
	ipw_write_restricted_reg_buffer(priv, BSM_SRAM_LOWER_BOUND, len, image);

	ipw_release_restricted_access(priv);
	rc = ipw_verify_bsm(priv);
	if (rc)
		return rc;
	rc = ipw_grab_restricted_access(priv);
	if (rc)
		return rc;

	/* Tell BSM to copy from BSM SRAM into instruction SRAM, when asked */
	ipw_write_restricted_reg(priv, BSM_WR_MEM_SRC_REG, 0x0);
	ipw_write_restricted_reg(priv, BSM_WR_MEM_DST_REG, RTC_INST_LOWER_BOUND);
	ipw_write_restricted_reg(priv, BSM_WR_DWCOUNT_REG, len / sizeof(u32));

	/* Load bootstrap code into instruction SRAM now,
	 *   to prepare to load "initialize" uCode */
	ipw_write_restricted_reg(priv, BSM_WR_CTRL_REG,
		BSM_WR_CTRL_REG_BIT_START);

	/* Wait for load of bootstrap uCode to finish */
	i = 0;
	while (i < 1000) {
		i++;
		done = ipw_read_restricted_reg(priv, BSM_WR_CTRL_REG);
		if (!(done & BSM_WR_CTRL_REG_BIT_START))
			break;
	}
	if (i < 1000) {
		IPW_DEBUG_INFO("BSM write complete, poll %d iterations\n", i);
	} else {
		IPW_ERROR("BSM write did not complete!\n");
		return -EIO;
	}

	/* Enable future boot loads whenever power management unit triggers it
	 *   (e.g. when powering back up after power-save shutdown) */
	ipw_write_restricted_reg(priv, BSM_WR_CTRL_REG,
		BSM_WR_CTRL_REG_BIT_START_EN);

	ipw_release_restricted_access(priv);

	return 0;
}

static void ipw_nic_start(struct ipw_priv *priv)
{
	unsigned long flags;

	spin_lock_irqsave(&priv->lock, flags);

	/* Remove all resets to allow NIC to operate */
	ipw_write32(priv, CSR_RESET, 0);

	spin_unlock_irqrestore(&priv->lock, flags);
}

/**
 * ipw_read_ucode - Read uCode images from disk file.
 *
 * Copy into buffers for card to fetch via bus-mastering
 */
static int ipw_read_ucode(struct ipw_priv *priv)
{
	struct ipw_ucode *ucode;
	int rc = 0;
	const struct firmware *ucode_raw;

	const char *name = "iwlwifi-3945.ucode";	/* firmware file name */


	u8 *src;
	size_t len;

	/* Ask kernel firmware_class module to get the boot firmware off disk.
	 * request_firmware() is synchronous call, file is in memory on return.
	 * TODO:  Would it be more polite to use asynchronous
	 *        request_firmware_nowait()??  If so, put request back into
	 *        ipw_pci_probe(), and rest of this function would serve as
	 *        the callback for request_firmware_nowait().  Also need to
	 *        make sure everything waits for this callback to complete! */
	rc = request_firmware(&ucode_raw, name, &priv->pci_dev->dev);
	if (rc < 0) {
		IPW_ERROR("%s firmware file req failed: Reason %d\n", name, rc);
		goto error;
	}

	IPW_DEBUG_INFO("Got firmware '%s' file (%zd bytes) from disk\n",
		       name, ucode_raw->size);

	/* Make sure that we got at least our header! */
	if (ucode_raw->size < sizeof(*ucode)) {
		IPW_ERROR("File size way too small!\n");
		rc = -EINVAL;
		goto err_release;
	}

	/* Data from ucode file:  header followed by uCode images */
	ucode = (void *)ucode_raw->data;

	ucode->ver = le32_to_cpu(ucode->ver);
	ucode->inst_size = le32_to_cpu(ucode->inst_size);
	ucode->data_size = le32_to_cpu(ucode->data_size);
	ucode->init_size = le32_to_cpu(ucode->init_size);
	ucode->init_data_size = le32_to_cpu(ucode->init_data_size);
	ucode->boot_size = le32_to_cpu(ucode->boot_size);

	IPW_DEBUG_INFO("f/w package hdr ucode version = 0x%x\n", ucode->ver);
	IPW_DEBUG_INFO("f/w package hdr runtime inst size = %u\n",
		       ucode->inst_size);
	IPW_DEBUG_INFO("f/w package hdr runtime data size = %u\n",
		       ucode->data_size);
	IPW_DEBUG_INFO("f/w package hdr init inst size = %u\n",
		       ucode->init_size);
	IPW_DEBUG_INFO("f/w package hdr init data size = %u\n",
		       ucode->init_data_size);
	IPW_DEBUG_INFO("f/w package hdr boot inst size = %u\n",
		       ucode->boot_size);

	/* Verify size of file vs. image size info in file's header */
	if (ucode_raw->size < sizeof(*ucode) +
	    ucode->inst_size + ucode->data_size +
	    ucode->init_size + ucode->init_data_size +
	    ucode->boot_size) {

		IPW_DEBUG_INFO("uCode file size %d too small\n",
			       (int)ucode_raw->size);
		rc = -EINVAL;
		goto err_release;
	}

	/* Verify that uCode images will fit in card's SRAM */
	if (ucode->inst_size > IPW_MAX_INST_SIZE) {
		IPW_DEBUG_INFO("uCode instr len %d too large to fit in card\n",
			       (int)ucode->inst_size);
		rc = -EINVAL;
		goto err_release;
	}

	if (ucode->data_size > IPW_MAX_DATA_SIZE) {
		IPW_DEBUG_INFO("uCode data len %d too large to fit in card\n",
			       (int)ucode->data_size);
		rc = -EINVAL;
		goto err_release;
	}
	if (ucode->init_size > IPW_MAX_INST_SIZE) {
		IPW_DEBUG_INFO
		    ("uCode init instr len %d too large to fit in card\n",
		     (int)ucode->init_size);
		rc = -EINVAL;
		goto err_release;
	}
	if (ucode->init_data_size > IPW_MAX_DATA_SIZE) {
		IPW_DEBUG_INFO
		    ("uCode init data len %d too large to fit in card\n",
		     (int)ucode->init_data_size);
		rc = -EINVAL;
		goto err_release;
	}
	if (ucode->boot_size > IPW_MAX_BSM_SIZE) {
		IPW_DEBUG_INFO
		    ("uCode boot instr len %d too large to fit in bsm\n",
		     (int)ucode->boot_size);
		rc = -EINVAL;
		goto err_release;
	}

	/* Allocate ucode buffers for card's bus-master loading ... */

	/* Runtime instructions and 2 copies of data:
	 * 1) unmodified from disk
	 * 2) backup cache for save/restore during power-downs */
	priv->ucode_code.len = ucode->inst_size;
	priv->ucode_code.v_addr =
	     pci_alloc_consistent(priv->pci_dev,
				 priv->ucode_code.len,
				 &(priv->ucode_code.p_addr));

	priv->ucode_data.len = ucode->data_size;
	priv->ucode_data.v_addr =
	     pci_alloc_consistent(priv->pci_dev,
				 priv->ucode_data.len,
				 &(priv->ucode_data.p_addr));

	priv->ucode_data_backup.len = ucode->data_size;
	priv->ucode_data_backup.v_addr =
	     pci_alloc_consistent(priv->pci_dev,
				 priv->ucode_data_backup.len,
				 &(priv->ucode_data_backup.p_addr));


	/* Initialization instructions and data */
	priv->ucode_init.len = ucode->init_size;
	priv->ucode_init.v_addr =
	    pci_alloc_consistent(priv->pci_dev,
				 priv->ucode_init.len,
				 &(priv->ucode_init.p_addr));

	priv->ucode_init_data.len = ucode->init_data_size;
	priv->ucode_init_data.v_addr =
	    pci_alloc_consistent(priv->pci_dev,
				 priv->ucode_init_data.len,
				 &(priv->ucode_init_data.p_addr));

	/* Bootstrap (instructions only, no data) */
	priv->ucode_boot.len = ucode->boot_size;
	priv->ucode_boot.v_addr =
	    pci_alloc_consistent(priv->pci_dev,
				 priv->ucode_boot.len,
				 &(priv->ucode_boot.p_addr));

	if (!priv->ucode_code.v_addr || !priv->ucode_data.v_addr ||
	    !priv->ucode_init.v_addr || !priv->ucode_init_data.v_addr ||
	    !priv->ucode_boot.v_addr || !priv->ucode_data_backup.v_addr)
		goto err_pci_alloc;

	/* Copy images into buffers for card's bus-master reads ... */

	/* Runtime instructions (first block of data in file) */
	src = &ucode->data[0];
	len = priv->ucode_code.len;
	IPW_DEBUG_INFO("Copying (but not loading) uCode instr len %d\n",
		       (int)len);
	memcpy(priv->ucode_code.v_addr, src, len);
	IPW_DEBUG_INFO("uCode instr buf vaddr = 0x%p, paddr = 0x%08x\n",
		priv->ucode_code.v_addr, (u32)priv->ucode_code.p_addr);

	/* Runtime data (2nd block)
	 * NOTE:  Copy into backup buffer will be done in ipw_up()  */
	src = &ucode->data[ucode->inst_size];
	len = priv->ucode_data.len;
	IPW_DEBUG_INFO("Copying (but not loading) uCode data len %d\n",
		       (int)len);
	memcpy(priv->ucode_data.v_addr, src, len);
	memcpy(priv->ucode_data_backup.v_addr, src, len);

	/* Initialization instructions (3rd block) */
	if (ucode->init_size) {
		src = &ucode->data[ucode->inst_size + ucode->data_size];
		len = priv->ucode_init.len;
		IPW_DEBUG_INFO("Copying (but not loading) init instr len %d\n",
			       (int)len);
		memcpy(priv->ucode_init.v_addr, src, len);
	}

	/* Initialization data (4th block) */
	if (ucode->init_data_size) {
		src = &ucode->data[ucode->inst_size + ucode->data_size
				   + ucode->init_size];
		len = priv->ucode_init_data.len;
		IPW_DEBUG_INFO("Copying (but not loading) init data len %d\n",
			       (int)len);
		memcpy(priv->ucode_init_data.v_addr, src, len);
	}

	/* Bootstrap instructions (5th block) */
	src = &ucode->data[ucode->inst_size + ucode->data_size
				+ ucode->init_size + ucode->init_data_size];
	len = priv->ucode_boot.len;
	IPW_DEBUG_INFO("Copying (but not loading) boot instr len %d\n",
		       (int)len);
	memcpy(priv->ucode_boot.v_addr, src, len);

	/* We have our copies now, allow OS release its copies */
	release_firmware(ucode_raw);
	return 0;

 err_pci_alloc:
	IPW_ERROR("failed to allocate pci memory\n");
	rc = -ENOMEM;
	ipw_dealloc_ucode_pci(priv);

 err_release:
	release_firmware(ucode_raw);

 error:
	return rc;
}

/**
 * ipw_set_ucode_ptrs - Set uCode address location
 *
 * Tell initialization uCode where to find runtime uCode.
 *
 * BSM registers initially contain pointers to initialization uCode.
 * We need to replace them to load runtime uCode inst and data,
 * and to save runtime data when powering down.
 */
static int ipw_set_ucode_ptrs(struct ipw_priv *priv)
{
	dma_addr_t pinst;
	dma_addr_t pdata;
	int rc = 0;
	unsigned long flags;

	/* bits 31:0 for 3945 */
	pinst = priv->ucode_code.p_addr;
	pdata = priv->ucode_data_backup.p_addr;


	spin_lock_irqsave(&priv->lock, flags);
	rc = ipw_grab_restricted_access(priv);
	if (rc) {
		spin_unlock_irqrestore(&priv->lock, flags);
		return rc;
	}

	/* Tell bootstrap uCode where to find image to load */
	ipw_write_restricted_reg(priv, BSM_DRAM_INST_PTR_REG, pinst);
	ipw_write_restricted_reg(priv, BSM_DRAM_DATA_PTR_REG, pdata);
	ipw_write_restricted_reg(priv, BSM_DRAM_DATA_BYTECOUNT_REG,
				 priv->ucode_data.len);

	/* Inst bytecount must be last to set up, bit 31 signals uCode
	 *   that all new ptr/size info is in place */
	ipw_write_restricted_reg(priv, BSM_DRAM_INST_BYTECOUNT_REG,
				 priv->ucode_code.len | 0x80000000);

	ipw_release_restricted_access(priv);

	spin_unlock_irqrestore(&priv->lock, flags);

	IPW_DEBUG_INFO("Runtime uCode pointers are set.\n");

	return rc;
}

/**
 * ipw_init_alive_start - Called after REPLY_ALIVE notification receieved
 *
 * Called after REPLY_ALIVE notification received from "initialize" uCode.
 *
 * Tell "initialize" uCode to go ahead and load the runtime uCode.
*/
static void ipw_init_alive_start(struct ipw_priv *priv)
{
	/* Check alive response for "valid" sign from uCode */
	if (priv->card_alive_init.is_valid != 1) {
		/* We had an error bringing up the hardware, so take it
		 * all the way back down so we can try again */
		IPW_DEBUG_INFO("Initialize Alive failed.\n");
		goto restart;
	}

	/* Bootstrap uCode has loaded initialize uCode ... verify inst image.
	 * This is a paranoid check, because we would not have gotten the
	 * "initialize" alive if code weren't properly loaded.  */
	if (ipw_verify_ucode(priv)) {
		/* Runtime instruction load was bad;
		 * take it all the way back down so we can try again */
		IPW_DEBUG_INFO("Bad \"initialize\" uCode load.\n");
		goto restart;
	}

	/* Send pointers to protocol/runtime uCode image ... init code will
	 * load and launch runtime uCode, which will send us another "Alive"
	 * notification. */
	IPW_DEBUG_INFO("Initialization Alive received.\n");
	if (ipw_set_ucode_ptrs(priv)) {
		/* Runtime instruction load won't happen;
		 * take it all the way back down so we can try again */
		IPW_DEBUG_INFO("Couldn't set up uCode pointers.\n");
		goto restart;
	}
	return;

 restart:
	queue_delayed_work(priv->workqueue, &priv->restart, 0);
}



/*************** DMA-QUEUE-GENERAL-FUNCTIONS  *****/
/**
 * DMA services
 *
 * Theory of operation
 *
 * A queue is a circular buffers with 'Read' and 'Write' pointers.
 * 2 empty entries always kept in the buffer to protect from overflow.
 *
 * For Tx queue, there are low mark and high mark limits. If, after queuing
 * the packet for Tx, free space become < low mark, Tx queue stopped. When
 * reclaiming packets (on 'tx done IRQ), if free space become > high mark,
 * Tx queue resumed.
 *
 * The IPW operates with six queues, one receive queue in the device's
 * sram, one transmit queue for sending commands to the device firmware,
 * and four transmit queues for data.
 *
 * The four transmit queues allow for performing quality of service (qos)
 * transmissions as per the 802.11 protocol.  Currently Linux does not
 * provide a mechanism to the user for utilizing prioritized queues, so
 * we only utilize the first data transmit queue (queue1).
 */

/**
 * Driver allocates buffers of this size for Rx
 */

static inline int ipw_queue_space(const struct ipw_queue *q)
{
	int s = q->last_used - q->first_empty;
	if (q->last_used > q->first_empty)
		s -= q->n_bd;

	if (s <= 0)
		s += q->n_window;
	s -= 2;			/* keep some reserve to not confuse empty and full situations */
	if (s < 0)
		s = 0;
	return s;
}

static inline int ipw_queue_inc_wrap(int index, int n_bd)
{
	return (++index == n_bd) ? 0 : index;
}

static inline int x2_queue_used(const struct ipw_queue *q, int i)
{
	return q->first_empty > q->last_used ?
	    (i >= q->last_used && i < q->first_empty) :
	    !(i < q->last_used && i >= q->first_empty);
}

static inline u8 get_next_cmd_index(struct ipw_queue *q, u32 index, int is_huge)
{
	if (is_huge)
		return q->n_window;

	return (u8) (index % q->n_window);
}

/***************** END **************************/

/********************* TX-QUEUE-FUNCTIONS ******************/
static int ipw_stop_tx_queue(struct ipw_priv *priv)
{
	int queueId;
	int rc;
	unsigned long flags;

	spin_lock_irqsave(&priv->lock, flags);
	rc = ipw_grab_restricted_access(priv);
	if (rc) {
		spin_unlock_irqrestore(&priv->lock, flags);
		return rc;
	}

	/* stop SCD */
	ipw_write_restricted_reg(priv, SCD_MODE_REG, 0);

	/* reset TFD queues */
	for (queueId = TFD_QUEUE_MIN; queueId < TFD_QUEUE_MAX; queueId++) {
		ipw_write_restricted(priv, FH_TCSR_CONFIG(queueId), 0x0);
		ipw_poll_restricted_bit(priv, FH_TSSR_TX_STATUS,
					ALM_FH_TSSR_TX_STATUS_REG_MSK_CHNL_IDLE
					(queueId), 1000);
	}

	ipw_release_restricted_access(priv);
	spin_unlock_irqrestore(&priv->lock, flags);

	return 0;
}

/**
 * Initialize common DMA queue structure
 *
 * @param q                queue to init
 * @param count            Number of BD's to allocate. Should be power of 2
 * @param read_register    Address for 'read' register
 *                         (not offset within BAR, full address)
 * @param write_restrictedister   Address for 'write' register
 *                         (not offset within BAR, full address)
 * @param base_register    Address for 'base' register
 *                         (not offset within BAR, full address)
 * @param size             Address for 'size' register
 *                         (not offset within BAR, full address)
 */

static int ipw_tx_reset(struct ipw_priv *priv)
{
	int rc;
	unsigned long flags;

	spin_lock_irqsave(&priv->lock, flags);
	rc = ipw_grab_restricted_access(priv);
	if (rc) {
		spin_unlock_irqrestore(&priv->lock, flags);
		return rc;
	}

	ipw_write_restricted_reg(priv, SCD_MODE_REG, 0x2);	// bypass mode
	ipw_write_restricted_reg(priv, SCD_ARASTAT_REG, 0x01);	// RA 0 is active
	ipw_write_restricted_reg(priv, SCD_TXFACT_REG, 0x3f);	// all 6 fifo are active
	ipw_write_restricted_reg(priv, SCD_SBYP_MODE_1_REG, 0x010000);
	ipw_write_restricted_reg(priv, SCD_SBYP_MODE_2_REG, 0x030002);
	ipw_write_restricted_reg(priv, SCD_TXF4MF_REG, 0x000004);
	ipw_write_restricted_reg(priv, SCD_TXF5MF_REG, 0x000005);

	ipw_write_restricted(priv, FH_TSSR_CBB_BASE, priv->shared_phys);

	ipw_write_restricted(priv,
			     FH_TSSR_MSG_CONFIG,
			     ALM_FH_TSSR_TX_MSG_CONFIG_REG_VAL_SNOOP_RD_TXPD_ON
			     |
			     ALM_FH_TSSR_TX_MSG_CONFIG_REG_VAL_ORDER_RD_TXPD_ON
			     |
			     ALM_FH_TSSR_TX_MSG_CONFIG_REG_VAL_MAX_FRAG_SIZE_128B
			     |
			     ALM_FH_TSSR_TX_MSG_CONFIG_REG_VAL_SNOOP_RD_TFD_ON
			     |
			     ALM_FH_TSSR_TX_MSG_CONFIG_REG_VAL_ORDER_RD_CBB_ON
			     |
			     ALM_FH_TSSR_TX_MSG_CONFIG_REG_VAL_ORDER_RSP_WAIT_TH
			     | ALM_FH_TSSR_TX_MSG_CONFIG_REG_VAL_RSP_WAIT_TH);

	ipw_release_restricted_access(priv);

	spin_unlock_irqrestore(&priv->lock, flags);

	return 0;
}

static int ipw_queue_init(struct ipw_priv *priv, struct ipw_queue *q,
			  int count, int size, u32 id)
{
	int rc;
	unsigned long flags;

	q->n_bd = count;
	q->n_window = size;
	q->id = id;

	q->low_mark = q->n_window / 4;
	if (q->low_mark < 4)
		q->low_mark = 4;

	q->high_mark = q->n_window / 8;
	if (q->high_mark < 2)
		q->high_mark = 2;

	q->first_empty = q->last_used = 0;
	priv->shared_virt->tx_base_ptr[id] = (u32) q->dma_addr;

	spin_lock_irqsave(&priv->lock, flags);
	rc = ipw_grab_restricted_access(priv);
	if (rc) {
		spin_unlock_irqrestore(&priv->lock, flags);
		return rc;
	}
	ipw_write_restricted(priv, FH_CBCC_CTRL(id), 0);
	ipw_write_restricted(priv, FH_CBCC_BASE(id), 0);

	ipw_write_restricted(priv, FH_TCSR_CONFIG(id),
			     ALM_FH_TCSR_TX_CONFIG_REG_VAL_CIRQ_RTC_NOINT
			     |
			     ALM_FH_TCSR_TX_CONFIG_REG_VAL_MSG_MODE_TXF
			     |
			     ALM_FH_TCSR_TX_CONFIG_REG_VAL_CIRQ_HOST_IFTFD
			     |
			     ALM_FH_TCSR_TX_CONFIG_REG_VAL_DMA_CREDIT_ENABLE_VAL
			     | ALM_FH_TCSR_TX_CONFIG_REG_VAL_DMA_CHNL_ENABLE);
	ipw_release_restricted_access(priv);

	ipw_read32(priv, FH_TSSR_CBB_BASE);	/* fake read to flush all prev. writes */

	spin_unlock_irqrestore(&priv->lock, flags);
	return 0;
}

static int ipw_queue_tx_init(struct ipw_priv *priv,
			     struct ipw_tx_queue *q, int count, u32 id)
{
	struct pci_dev *dev = priv->pci_dev;
	int len;

	q->skb = kmalloc(sizeof(q->skb[0]) * TFD_QUEUE_SIZE_MAX, GFP_ATOMIC);
	if (!q->skb) {
		IPW_ERROR("kmalloc for auxilary BD structures failed\n");
		return -ENOMEM;
	}

	q->bd =
	    pci_alloc_consistent(dev,
				 sizeof(q->bd[0]) *
				 TFD_QUEUE_SIZE_MAX, &q->q.dma_addr);
	if (!q->bd) {
		IPW_ERROR("pci_alloc_consistent(%zd) failed\n",
			  sizeof(q->bd[0]) * count);
		kfree(q->skb);
		q->skb = NULL;
		return -ENOMEM;
	}

	/* alocate command space + one big command for scan since scan
	 * command is very huge the system will not have two scan at the
	 * same time */
	len = (sizeof(struct ipw_cmd) * count) + IPW_MAX_CMD_SIZE;
	q->cmd = pci_alloc_consistent(dev, len, &q->dma_addr_cmd);
	if (!q->cmd) {
		IPW_ERROR("pci_alloc_consistent(%zd) failed\n",
			  sizeof(q->cmd[0]) * count);
		kfree(q->skb);
		q->skb = NULL;

		pci_free_consistent(dev,
				    sizeof(q->bd[0]) *
				    TFD_QUEUE_SIZE_MAX, q->bd, q->q.dma_addr);

		return -ENOMEM;
	}

	q->need_update = 0;
	ipw_queue_init(priv, &q->q, TFD_QUEUE_SIZE_MAX, count, id);
	return 0;
}

/**
 * Free one TFD, those at index [txq->q.last_used].
 * Do NOT advance any indexes
 */
static void ipw_queue_tx_free_tfd(struct ipw_priv *priv,
				  struct ipw_tx_queue *txq)
{
	struct tfd_frame *bd = &txq->bd[txq->q.last_used];
	struct pci_dev *dev = priv->pci_dev;
	int i;
	int counter = 0;
	/* classify bd */
	if (txq->q.id == CMD_QUEUE_NUM)
		/* nothing to cleanup after for host commands */
		return;

	/* sanity check */
	counter = TFD_CTL_COUNT_GET(le32_to_cpu(bd->control_flags));
	if (counter > NUM_TFD_CHUNKS) {
		IPW_ERROR("Too many chunks: %i\n", counter);
		/** @todo issue fatal error, it is quite serious situation */
		return;
	}

	/* unmap chunks if any */

	for (i = 1; i < counter; i++) {
		pci_unmap_single(dev, le32_to_cpu(bd->pa[i].addr),
				 le16_to_cpu(bd->pa[i].len), PCI_DMA_TODEVICE);
		if (txq->skb[txq->q.last_used]) {
			struct sk_buff *skb = txq->skb[txq->q.last_used];
			struct ieee80211_hdr *hdr = (void *)skb->data;
			priv->tx_bytes += skb->len -
			    ieee80211_get_hdrlen(hdr->frame_ctl);
			dev_kfree_skb_any(skb);
			txq->skb[txq->q.last_used] = NULL;
		}
	}
}

/**
 * Deallocate DMA queue.
 *
 * Empty queue by removing and destroying all BD's.
 * Free all buffers.
 *
 * @param dev
 * @param q
 */
static void ipw_queue_tx_free(struct ipw_priv *priv, struct ipw_tx_queue *txq)
{
	struct ipw_queue *q = &txq->q;
	struct pci_dev *dev = priv->pci_dev;
	int len;

	if (q->n_bd == 0)
		return;
	/* first, empty all BD's */
	for (; q->first_empty != q->last_used;
	     q->last_used = ipw_queue_inc_wrap(q->last_used, q->n_bd)) {
		ipw_queue_tx_free_tfd(priv, txq);
	}

	len = (sizeof(txq->cmd[0]) * q->n_window) + IPW_MAX_CMD_SIZE;
	pci_free_consistent(dev, len, txq->cmd, txq->dma_addr_cmd);

	/* free buffers belonging to queue itself */
	pci_free_consistent(dev, sizeof(txq->bd[0]) * q->n_bd,
			    txq->bd, q->dma_addr);

	kfree(txq->skb);

	/* 0 fill whole structure */
	memset(txq, 0, sizeof(*txq));
}

/**
 * Destroy all DMA queues and structures
 *
 * @param priv
 */
static void ipw_tx_queue_free(struct ipw_priv *priv)
{

	/* Tx queues */
	ipw_queue_tx_free(priv, &priv->txq[0]);
	ipw_queue_tx_free(priv, &priv->txq[1]);
	ipw_queue_tx_free(priv, &priv->txq[2]);
	ipw_queue_tx_free(priv, &priv->txq[3]);
	ipw_queue_tx_free(priv, &priv->txq[4]);
	ipw_queue_tx_free(priv, &priv->txq[5]);
}

static int ipw_tx_queue_update_write_ptr(struct ipw_priv *priv,
					 struct ipw_tx_queue *txq, int tx_id)
{
	u32 reg = 0;
	int rc = 0;

	if (txq->need_update == 0)
		return rc;

	if (priv->status & STATUS_POWER_PMI) {
		reg = ipw_read32(priv, CSR_UCODE_DRV_GP1);

		if (reg & CSR_UCODE_DRV_GP1_BIT_MAC_SLEEP) {
			ipw_set_bit(priv, CSR_GP_CNTRL,
				    CSR_GP_CNTRL_REG_FLAG_MAC_ACCESS_REQ);
			return rc;
		}

		rc = ipw_grab_restricted_access(priv);
		if (rc)
			return rc;
		ipw_write_restricted(priv, HBUS_TARG_WRPTR,
				     txq->q.first_empty | (tx_id << 8));
		ipw_release_restricted_access(priv);
	} else {
		ipw_write32(priv, HBUS_TARG_WRPTR,
			    txq->q.first_empty | (tx_id << 8));
		ipw_read32(priv, CSR_INT_MASK);
	}

	txq->need_update = 0;

	return rc;
}

/**
 * Destroys all DMA structures and initialise them again
 *
 * @param priv
 * @return error code
 */
static int ipw_queue_reset(struct ipw_priv *priv)
{
	int rc = 0;

	ipw_tx_queue_free(priv);

	/* Tx CMD queue */
	ipw_tx_reset(priv);

	/* Tx queue(s) */
	rc = ipw_queue_tx_init(priv, &priv->txq[0], TFD_TX_CMD_SLOTS, 0);
	if (rc) {
		IPW_ERROR("Tx 0 queue init failed\n");
		goto error;
	}

	rc = ipw_queue_tx_init(priv, &priv->txq[1], TFD_TX_CMD_SLOTS, 1);
	if (rc) {
		IPW_ERROR("Tx 1 queue init failed\n");
		goto error;
	}
	rc = ipw_queue_tx_init(priv, &priv->txq[2], TFD_TX_CMD_SLOTS, 2);
	if (rc) {
		IPW_ERROR("Tx 2 queue init failed\n");
		goto error;
	}
	rc = ipw_queue_tx_init(priv, &priv->txq[3], TFD_TX_CMD_SLOTS, 3);
	if (rc) {
		IPW_ERROR("Tx 3 queue init failed\n");
		goto error;
	}

	rc = ipw_queue_tx_init(priv, &priv->txq[4], TFD_CMD_SLOTS,
			       CMD_QUEUE_NUM);
	if (rc) {
		IPW_ERROR("Tx Cmd queue init failed\n");
		goto error;
	}

	rc = ipw_queue_tx_init(priv, &priv->txq[5], TFD_TX_CMD_SLOTS, 5);
	if (rc) {
		IPW_ERROR("Tx service queue init failed\n");
		goto error;
	}

	return rc;

      error:
	ipw_tx_queue_free(priv);
	return rc;
}

/**************************************************************/

static inline void ipw_create_bssid(struct ipw_priv *priv, u8 * bssid)
{
	/* First 3 bytes are manufacturer */
	bssid[0] = priv->mac_addr[0];
	bssid[1] = priv->mac_addr[1];
	bssid[2] = priv->mac_addr[2];

	/* Last bytes are random */
	get_random_bytes(&bssid[3], ETH_ALEN - 3);

	bssid[0] &= 0xfe;	/* clear multicast bit */
	bssid[0] |= 0x02;	/* set local assignment bit (IEEE802) */
}

static inline int ipw_is_broadcast_ether_addr(const u8 * addr)
{
	return (addr[0] & addr[1] & addr[2] & addr[3] & addr[4] &
		addr[5]) == 0xff;
}

static u8 ipw_remove_station(struct ipw_priv *priv, u8 * bssid, int is_ap)
{
	int index = IPW_INVALID_STATION;
	int i;

	if (is_ap) {
		index = AP_ID;
		if ((priv->stations[index].used))
			priv->stations[index].used = 0;
	} else if (ipw_is_broadcast_ether_addr(bssid)) {
		index = BROADCAST_ID;
		if ((priv->stations[index].used))
			priv->stations[index].used = 0;
	} else {
		for (i = STA_ID; i < STA_ID + max(priv->num_stations,
					(u8)(NUM_OF_STATIONS - 3)); i++) {
			if ((priv->stations[i].used)
			    &&
			    (!memcmp
			     (priv->stations[i].sta.sta.MACAddr,
			      bssid, ETH_ALEN))) {
				index = i;
				priv->stations[index].used = 0;
				break;
			}
		}
	}

	if (index != IPW_INVALID_STATION) {
		if (priv->num_stations > 0)
			priv->num_stations--;
	}

	return 0;
}

static void ipw_delete_stations_table(struct ipw_priv *priv)
{

	priv->num_stations = 0;
	memset(priv->stations, 0,
	       NUM_OF_STATIONS * sizeof(struct ipw_station_entry));
}

static void ipw_clear_stations_table(struct ipw_priv *priv)
{

	priv->num_stations = 0;
	memset(priv->stations, 0,
	       NUM_OF_STATIONS * sizeof(struct ipw_station_entry));
}

//todoG port to 3945 card
static u8 ipw_add_station(struct ipw_priv *priv, const u8 * bssid,
			  int is_ap, u8 flags)
{
	int i = NUM_OF_STATIONS;
	int index = IPW_INVALID_STATION;

	if (is_ap) {
		index = AP_ID;
		if ((priv->stations[index].used) &&
		    (!memcmp
		     (priv->stations[index].sta.sta.MACAddr, bssid, ETH_ALEN)))
			return index;
	} else if (ipw_is_broadcast_ether_addr(bssid)) {
		index = BROADCAST_ID;
		if ((priv->stations[index].used) &&
		    (!memcmp
		     (priv->stations[index].sta.sta.MACAddr, bssid, ETH_ALEN)))
			return index;
	} else
		for (i = STA_ID; i < (priv->num_stations + STA_ID); i++) {
			if ((priv->stations[i].used)
			    &&
			    (!memcmp
			     (priv->stations[i].sta.sta.MACAddr,
			      bssid, ETH_ALEN)))
				return i;

			if ((priv->stations[i].used == 0) &&
			    (index == IPW_INVALID_STATION))
				index = i;
		}

	if (index != IPW_INVALID_STATION)
		i = index;

	if (i == NUM_OF_STATIONS)
		return IPW_INVALID_STATION;

	priv->stations[i].used = 1;

	memset(&priv->stations[i].sta, 0, sizeof(struct ipw_addsta_cmd));
	memcpy(priv->stations[i].sta.sta.MACAddr, bssid, ETH_ALEN);
	priv->stations[i].sta.ctrlAddModify = 0;
	priv->stations[i].sta.sta.staID = i;
	priv->stations[i].sta.station_flags = 0;

	//todoG do we need this
//      priv->stations[i].sta.tid_disable_tx = 0xffff;  /* all TID's disabled */
	if (priv->staging_config.flags & RXON_FLG_BAND_24G_MSK)
		priv->stations[i].sta.tx_rate = R_1M;
	else
		priv->stations[i].sta.tx_rate = R_6M;

	priv->stations[i].current_rate = priv->stations[i].sta.tx_rate;

	priv->num_stations++;
	ipw_send_add_station(priv, &priv->stations[i].sta, flags);
	return i;
}

static u8 ipw_find_station(struct ipw_priv *priv, const u8 * bssid, int create)
{
	int i;
	int start = 0;

	if (ipw_is_broadcast_ether_addr(bssid))
		return BROADCAST_ID;

	for (i = start; i < (start + priv->num_stations); i++)
		if ((priv->stations[i].used) &&
		    (!memcmp
		     (priv->stations[i].sta.sta.MACAddr, bssid, ETH_ALEN)))
			return i;

	if (!create)
		return IPW_INVALID_STATION;

	return ipw_add_station(priv, bssid, 0, 0);
}

static void ipw_remove_all_sta(struct ipw_priv *priv)
{
	int i;
	for (i = 0; i < NUM_OF_STATIONS; i++)
		priv->stations[i].used = 0;
}

static int ipw_queue_tx_hcmd(struct ipw_priv *priv, struct ipw_host_cmd *cmd)
{
	struct ipw_tx_queue *txq = &priv->txq[CMD_QUEUE_NUM];
	struct ipw_queue *q = &txq->q;
	struct tfd_frame *tfd;
	struct ipw_cmd *out_cmd;
	u32 idx = 0;
	u16 fix_size = (u16) (cmd->meta.len + sizeof(out_cmd->hdr));
	int pad;
	dma_addr_t phys_addr;
	u8 fifo = CMD_QUEUE_NUM;
	u16 count;
	int rc;

	/* If any of the command structures end up being larger than
	 * the TFD_MAX_PAYLOAD_SIZE, and it sent as a 'small' command then
	 * we will need to increase the size of the TFD entries */
	BUG_ON((fix_size > TFD_MAX_PAYLOAD_SIZE)
	       && is_cmd_small(cmd));
	if (ipw_queue_space(q) < (is_cmd_sync(cmd) ? 1 : 2)) {
		IPW_ERROR("No space for Tx\n");
		return -ENOSPC;
	}

	tfd = &txq->bd[q->first_empty];
	memset(tfd, 0, sizeof(*tfd));

	txq->skb[q->first_empty] = NULL;

	idx = get_next_cmd_index(q, q->first_empty,
				 cmd->meta.flags & CMD_SIZE_HUGE);
	out_cmd = &txq->cmd[idx];

	out_cmd->hdr.cmd = cmd->id;
	memcpy(&out_cmd->meta, &cmd->meta, sizeof(cmd->meta));
	memcpy(&out_cmd->cmd.payload, cmd->data, cmd->meta.len);

	/* At this point, the out_cmd now has all of the incoming cmd
	 * information */

	out_cmd->hdr.flags = 0;
	out_cmd->hdr.sequence = FIFO_TO_SEQ(fifo) |
	    INDEX_TO_SEQ(q->first_empty);
	if (out_cmd->meta.flags & CMD_SIZE_HUGE)
		out_cmd->hdr.sequence |= SEQ_HUGE_FRAME;

	/* physical buffer passed to the hardware starts at the header --
	 * past the meta */
	phys_addr = txq->dma_addr_cmd + sizeof(txq->cmd[0]) * idx +
	    offsetof(struct ipw_cmd, hdr);
	attach_buffer_to_tfd_frame(tfd, phys_addr, fix_size);

	pad = U32_PAD(out_cmd->meta.len);
	count = TFD_CTL_COUNT_GET(tfd->control_flags);
	tfd->control_flags = TFD_CTL_COUNT_SET(count) | TFD_CTL_PAD_SET(pad);

/*	IPW_DEBUG_TX("TFD %d - %d chunks for %d bytes (%d pad) at 0x%08X:\n",
		     q->first_empty, TFD_CTL_COUNT_GET(tfd->control_flags),
		     tfd->pa[0].len, TFD_CTL_PAD_GET(tfd->control_flags),
		     tfd->pa[0].addr);
		     printk_buf(IPW_DL_TX, (u8 *) tfd, sizeof(*tfd));*/

	if (out_cmd->hdr.cmd != 0x23 &&
	    out_cmd->hdr.cmd != 0x24 && out_cmd->hdr.cmd != 0x22) {
		IPW_DEBUG_HC("Sending command %s (#%x), seq: 0x%04X, "
			     "%d bytes at %d[%d]:%d\n",
			     get_cmd_string(out_cmd->hdr.cmd),
			     out_cmd->hdr.cmd, out_cmd->hdr.sequence,
			     fix_size, q->first_empty, idx, fifo);
		printk_buf(IPW_DL_HOST_COMMAND, cmd->data, cmd->len);
	}

	q->first_empty = ipw_queue_inc_wrap(q->first_empty, q->n_bd);

	txq->need_update = 1;
	rc = ipw_tx_queue_update_write_ptr(priv, txq, CMD_QUEUE_NUM);
	if (rc)
		return rc;

	return 0;
}

static int ipw_get_tx_queue_number(struct ipw_priv *priv, u16 priority)
{
	if (priority > 7)
		priority = 0;

	return from_priority_to_tx_queue[priority] - 1;
}

#define IPW_FORCE_TXPOWR_CALIB  (180 * HZ)

/* All received frames are sent to this function. @skb contains the frame in
 * IEEE 802.11 format, i.e., in the format it was sent over air.
 * This function is called only as a tasklet (software IRQ). */
static int raw_rx(struct net_device *dev, struct sk_buff *skb)
{
	struct ieee80211_hdr_4addr *hdr;
	u16 fc;

	if (skb->len < 10) {
		printk(KERN_INFO "%s: SKB length < 10\n", dev->name);
		return 0;
	}

	hdr = (struct ieee80211_hdr_4addr *)skb->data;

	fc = le16_to_cpu(hdr->frame_ctl);

	skb->dev = dev;
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,21)
	skb_reset_mac_header(skb);
#else
	skb->mac.raw = skb->data;
#endif
	skb_pull(skb, frame_get_hdrlen(fc));
	skb->pkt_type = PACKET_OTHERHOST;
	skb->protocol = __constant_htons(ETH_P_80211_RAW);
	memset(skb->cb, 0, sizeof(skb->cb));
	netif_rx(skb);

	/* We handled the SKB so return 1 */
	return 1;
}

static void ipw_handle_data_packet(struct ipw_priv *priv,
				   struct ipw_rx_mem_buffer *rxb,
				   struct ieee80211_rx_stats *stats)
{
	struct ieee80211_hdr *hdr;
	struct ipw_rx_packet *pkt = (struct ipw_rx_packet *)rxb->skb->data;
	struct ipw_rx_frame_hdr *rx_hdr = IPW_RX_HDR(pkt);

	/* We received data from the HW, so stop the watchdog */
	priv->net_dev->trans_start = jiffies;
	if (unlikely((le16_to_cpu(rx_hdr->len) + IPW_RX_FRAME_SIZE) >
		     skb_tailroom(rxb->skb))) {
		IPW_DEBUG_DROP("Corruption detected! Oh no!\n");
		return;
	}

	/* We only process data packets if the interface is open */
	if (unlikely(!priv->netdev_registered || !netif_running(priv->net_dev))) {
		IPW_DEBUG_DROP("Dropping packet while interface is not up.\n");
		return;
	}

	skb_reserve(rxb->skb, (void *)rx_hdr->payload - (void *)pkt);
	/* Set the size of the skb to the size of the frame */
	skb_put(rxb->skb, le16_to_cpu(rx_hdr->len));

	hdr = (void *)rxb->skb->data;
	priv->rx_bytes += rxb->skb->len - frame_get_hdrlen(hdr->frame_ctl);
	priv->rx_packets++;

	if (!raw_rx(priv->net_dev, rxb->skb)) {
	} else {
		/* raw_rx succeeded, so it now owns the SKB */
		rxb->skb = NULL;
		priv->led_packets += rx_hdr->len;
		ipw_setup_activity_timer(priv);
	}
}

#define IPW_PACKET_RETRY_TIME HZ

static inline int is_duplicate_packet(struct ipw_priv *priv,
				      struct ieee80211_hdr_4addr *header)
{
	u16 sc = le16_to_cpu(header->seq_ctl);
	u16 seq = WLAN_GET_SEQ_SEQ(sc);
	u16 frag = WLAN_GET_SEQ_FRAG(sc);
	u16 *last_seq, *last_frag;
	unsigned long *last_time;

	last_seq = &priv->last_seq_num;
	last_frag = &priv->last_frag_num;
	last_time = &priv->last_packet_time;

	if ((*last_seq == seq) &&
	    time_after(*last_time + IPW_PACKET_RETRY_TIME, jiffies)) {
		if (*last_frag == frag)
			goto drop;
		if (*last_frag + 1 != frag)
			/* out-of-order fragment */
			goto drop;
	} else
		*last_seq = seq;

	*last_frag = frag;
	*last_time = jiffies;
	return 0;

      drop:
	/* Comment this line now since we observed the card receives
	 * duplicate packets but the FCTL_RETRY bit is not set in the
	 * IBSS mode with fragmentation enabled.
	 BUG_ON(!(le16_to_cpu(header->frame_ctl) & IEEE80211_FCTL_RETRY)); */
	return 1;
}

static void ipw_handle_promiscuous_tx(struct ipw_priv *priv,
				      struct ieee80211_hdr *frame,
				      int frame_len)
{
	u16 filter = priv->prom_priv->filter;
	int hdr_only = 0;
	struct sk_buff *dst;
	struct ieee80211_radiotap_header *rt_hdr;
	int len;

	if (filter & IPW_PROM_NO_TX)
		return;

	/* Filtering of fragment chains is done agains the first fragment */
	if (frame_is_management(frame->frame_ctl)) {
		if (filter & IPW_PROM_NO_MGMT)
			return;
		if (filter & IPW_PROM_MGMT_HEADER_ONLY)
			hdr_only = 1;
	} else if (frame_is_control(frame->frame_ctl)) {
		if (filter & IPW_PROM_NO_CTL)
			return;
		if (filter & IPW_PROM_CTL_HEADER_ONLY)
			hdr_only = 1;
	} else if (frame_is_data(frame->frame_ctl)) {
		if (filter & IPW_PROM_NO_DATA)
			return;
		if (filter & IPW_PROM_DATA_HEADER_ONLY)
			hdr_only = 1;
	}

	if (hdr_only) {
		len = frame_get_hdrlen(frame->frame_ctl);
	} else
		len = frame_len;

	dst = alloc_skb(len + IEEE80211_RADIOTAP_HDRLEN, GFP_ATOMIC);
	if (!dst)
		return;

	rt_hdr = (void *)skb_put(dst, sizeof(*rt_hdr));

	rt_hdr->it_version = PKTHDR_RADIOTAP_VERSION;
	rt_hdr->it_pad = 0;
	rt_hdr->it_present = 0;	/* after all, it's just an idea */
	rt_hdr->it_present |= (1 << IEEE80211_RADIOTAP_CHANNEL);

	*(u16 *) skb_put(dst, sizeof(u16)) =
	    cpu_to_le16(ieee80211chan2mhz(priv->channel));

	if (priv->band == IEEE80211_52GHZ_BAND)	/* 802.11a */
		*(u16 *) skb_put(dst, sizeof(u16)) =
		    cpu_to_le16(IEEE80211_CHAN_OFDM | IEEE80211_CHAN_5GHZ);
	else			/* 802.11g */
		*(u16 *) skb_put(dst, sizeof(u16)) =
		    cpu_to_le16(IEEE80211_CHAN_OFDM | IEEE80211_CHAN_2GHZ);

	rt_hdr->it_len = dst->len;

	memcpy(skb_put(dst, len), frame, len);

	/* We are *dumping the Tx frames* as if they were received
	 * hence the use of 'raw_rx' in what looks like a tx code
	 * path */
	if (!raw_rx(priv->prom_net_dev, dst)) {
		/* If the packet isn't owned by the stack then free it */
		dev_kfree_skb_any(dst);
	}
}

static void ipw_handle_promiscuous_rx(struct ipw_priv *priv,
				      struct ipw_rx_mem_buffer *rxb,
				      struct ieee80211_rx_stats
				      *stats)
{
	struct ipw_rx_packet *pkt = (struct ipw_rx_packet *)rxb->skb->data;
	struct ipw_rx_frame_stats *rx_stats = IPW_RX_STATS(pkt);
	struct ipw_rx_frame_hdr *rx_hdr = IPW_RX_HDR(pkt);
	struct ipw_rx_frame_end *rx_end = IPW_RX_END(pkt);
	struct ipw_rt_hdr *ipw_rt;
	u16 filter = priv->prom_priv->filter;

	/* First cache any information we need before we overwrite
	 * the information provided in the skb from the hardware */
	struct ieee80211_hdr *hdr;
	s8 signal = rx_stats->rssi - IPW_RSSI_OFFSET;
	s8 noise = rx_stats->noise_diff;
	u16 phy_flags = rx_hdr->phy_flags;
	u16 channel = cpu_to_le16(ieee80211chan2mhz(rx_hdr->channel));
	u8 rate = rx_hdr->rate;
	short len = le16_to_cpu(rx_hdr->len);
	u64 tsf = rx_end->beaconTimeStamp;
	struct sk_buff *skb;
	int hdr_only = 0;
	u16 snr = 0;

	if (rx_stats->noise_diff)
		snr = rx_stats->sig_avg / rx_stats->noise_diff;

	noise = signal - snr;

	/* If the filter is set to not include Rx frames then return */
	if (filter & IPW_PROM_NO_RX)
		return;

	/* We received data from the HW, so stop the watchdog */
	priv->prom_net_dev->trans_start = jiffies;

	if (unlikely((len + IPW_RX_FRAME_SIZE) > skb_tailroom(rxb->skb))) {
		IPW_DEBUG_DROP("Corruption detected! Oh no!\n");
		return;
	}

	/* We only process data packets if the interface is open */
	if (unlikely(!netif_running(priv->prom_net_dev))) {
		IPW_DEBUG_DROP("Dropping packet while interface is not up.\n");
		return;
	}

	/* Libpcap 0.9.3+ can handle variable length radiotap, so we'll use
	 * that now */
	if (len > IPW_RX_BUF_SIZE - sizeof(struct ipw_rt_hdr)) {
		/* FIXME: Should alloc bigger skb instead */
		IPW_DEBUG_DROP("Dropping too large packet in monitor\n");
		return;
	}

	hdr = (void *)IPW_RX_DATA(pkt);
	if (frame_is_management(hdr->frame_ctl)) {
		if (filter & IPW_PROM_NO_MGMT)
			return;
		if (filter & IPW_PROM_MGMT_HEADER_ONLY)
			hdr_only = 1;
	} else if (frame_is_control(hdr->frame_ctl)) {
		if (filter & IPW_PROM_NO_CTL)
			return;
		if (filter & IPW_PROM_CTL_HEADER_ONLY)
			hdr_only = 1;
	} else if (frame_is_data(hdr->frame_ctl)) {
		if (filter & IPW_PROM_NO_DATA)
			return;
		if (filter & IPW_PROM_DATA_HEADER_ONLY)
			hdr_only = 1;
	}

	/* Copy the SKB since this is for the promiscuous side */
	skb = skb_copy(rxb->skb, GFP_ATOMIC);
	if (skb == NULL) {
		IPW_ERROR("skb_clone failed for promiscuous copy.\n");
		return;
	}

	/* copy the frame data to write after where the radiotap header goes */
	ipw_rt = (void *)skb->data;

	if (hdr_only)
		len = frame_get_hdrlen(hdr->frame_ctl);

	memcpy(ipw_rt->payload, IPW_RX_DATA(pkt), len);

	/* Zero the radiotap static buffer  ...  We only need to zero the bytes
	 * NOT part of our real header, saves a little time.
	 *
	 * No longer necessary since we fill in all our data.  Purge before
	 * merging patch officially.
	 * memset(rxb->skb->data + sizeof(struct ipw_rt_hdr), 0,
	 *        IEEE80211_RADIOTAP_HDRLEN - sizeof(struct ipw_rt_hdr));
	 */

	ipw_rt->rt_hdr.it_version = PKTHDR_RADIOTAP_VERSION;
	ipw_rt->rt_hdr.it_pad = 0;	/* always good to zero */
	ipw_rt->rt_hdr.it_len = sizeof(*ipw_rt);	/* total header+data */

	/* Set the size of the skb to the size of the frame */
	skb_put(skb, ipw_rt->rt_hdr.it_len + len);

	/* Big bitfield of all the fields we provide in radiotap */
	ipw_rt->rt_hdr.it_present =
	    ((1 << IEEE80211_RADIOTAP_FLAGS) |
	     (1 << IEEE80211_RADIOTAP_TSFT) |
	     (1 << IEEE80211_RADIOTAP_RATE) |
	     (1 << IEEE80211_RADIOTAP_CHANNEL) |
	     (1 << IEEE80211_RADIOTAP_DBM_ANTSIGNAL) |
	     (1 << IEEE80211_RADIOTAP_DBM_ANTNOISE) |
	     (1 << IEEE80211_RADIOTAP_ANTENNA));

	/* Zero the flags, we'll add to them as we go */
	ipw_rt->rt_flags = 0;

	ipw_rt->rt_tsf = tsf;

	/* Convert to DBM */
	ipw_rt->rt_dbmsignal = signal;
	ipw_rt->rt_dbmnoise = noise;

	/* Convert the channel data and set the flags */
	ipw_rt->rt_channel = channel;
	if (!(phy_flags & RX_RES_PHY_FLAGS_BAND_24_MSK)) {
		ipw_rt->rt_chbitmask =
		    cpu_to_le16((IEEE80211_CHAN_OFDM | IEEE80211_CHAN_5GHZ));
	} else if (phy_flags & RX_RES_PHY_FLAGS_MOD_CCK_MSK) {
		ipw_rt->rt_chbitmask =
		    cpu_to_le16((IEEE80211_CHAN_CCK | IEEE80211_CHAN_2GHZ));
	} else {		/* 802.11g */
		ipw_rt->rt_chbitmask =
		    cpu_to_le16((IEEE80211_CHAN_OFDM | IEEE80211_CHAN_2GHZ));
	}

	/* set the rate in multiples of 500k/s */
	switch (rate) {
	case IPW_TX_RATE_1MB:
		ipw_rt->rt_rate = 2;
		break;
	case IPW_TX_RATE_2MB:
		ipw_rt->rt_rate = 4;
		break;
	case IPW_TX_RATE_5MB:
		ipw_rt->rt_rate = 10;
		break;
	case IPW_TX_RATE_6MB:
		ipw_rt->rt_rate = 12;
		break;
	case IPW_TX_RATE_9MB:
		ipw_rt->rt_rate = 18;
		break;
	case IPW_TX_RATE_11MB:
		ipw_rt->rt_rate = 22;
		break;
	case IPW_TX_RATE_12MB:
		ipw_rt->rt_rate = 24;
		break;
	case IPW_TX_RATE_18MB:
		ipw_rt->rt_rate = 36;
		break;
	case IPW_TX_RATE_24MB:
		ipw_rt->rt_rate = 48;
		break;
	case IPW_TX_RATE_36MB:
		ipw_rt->rt_rate = 72;
		break;
	case IPW_TX_RATE_48MB:
		ipw_rt->rt_rate = 96;
		break;
	case IPW_TX_RATE_54MB:
		ipw_rt->rt_rate = 108;
		break;
	default:
		ipw_rt->rt_rate = 0;
		break;
	}

	/* antenna number */
	ipw_rt->rt_antenna = (phy_flags & RX_RES_PHY_FLAGS_ANTENNA_MSK) >> 4;

	/* set the preamble flag if we have it */
	if (phy_flags & RX_RES_PHY_FLAGS_SHORT_PREAMBLE_MSK)
		ipw_rt->rt_flags |= IEEE80211_RADIOTAP_F_SHORTPRE;

	IPW_DEBUG_RX("Rx packet of %d bytes.\n", skb->len);

	if (!raw_rx(priv->prom_net_dev, skb)) {
		dev_kfree_skb_any(skb);
	}
}

/*
 * Reclaim Tx queue entries no more used by NIC.
 *
 * When FW adwances 'R' index, all entries between old and
 * new 'R' index need to be reclaimed. As result, some free space
 * forms. If there is enough free space (> low mark), wake Tx queue.
 */
static int ipw_queue_tx_reclaim(struct ipw_priv *priv, int fifo, int index)
{
	struct ipw_tx_queue *txq = &priv->txq[fifo];
	struct ipw_queue *q = &txq->q;
	u8 is_next = 0;
	int used;
	if ((index >= q->n_bd) || (x2_queue_used(q, index) == 0)) {
		IPW_ERROR
		    ("Read index for DMA queue (%d) is out of range [0-%d) %d %d\n",
		     index, q->n_bd, q->first_empty, q->last_used);
		goto done;
	}
	index = ipw_queue_inc_wrap(index, q->n_bd);
	for (; q->last_used != index;
	     q->last_used = ipw_queue_inc_wrap(q->last_used, q->n_bd)) {
		if (is_next) {
			IPW_WARNING("XXXL we have skipped command\n");
			queue_delayed_work(priv->workqueue, &priv->restart, 0);
		}
		if (fifo != CMD_QUEUE_NUM) {
			ipw_queue_tx_free_tfd(priv, txq);
			priv->tx_packets++;
		}

		is_next = 1;
	}
      done:
	if (ipw_queue_space(q) > q->low_mark && (fifo >= 0)
	    && (fifo != CMD_QUEUE_NUM)
	    && priv->netdev_registered && netif_running(priv->net_dev))
		netif_wake_queue(priv->net_dev);
	used = q->first_empty - q->last_used;
	if (used < 0)
		used += q->n_window;
	return used;
}

static void ipw_tx_complete(struct ipw_priv *priv,
			    struct ipw_rx_mem_buffer *rxb)
{
	struct ipw_rx_packet *pkt = (struct ipw_rx_packet *)rxb->skb->data;
	int fifo = SEQ_TO_FIFO(pkt->hdr.sequence);
	int index = SEQ_TO_INDEX(pkt->hdr.sequence);
	int is_huge = (pkt->hdr.sequence & SEQ_HUGE_FRAME);
	int cmd_index;
	struct ipw_cmd *cmd;
	if (fifo > MAX_REAL_TX_QUEUE_NUM)
		return;
	if (fifo != CMD_QUEUE_NUM) {
		ipw_queue_tx_reclaim(priv, fifo, index);
		return;
	}

	cmd_index = get_next_cmd_index(&priv->txq[CMD_QUEUE_NUM].q,
				       index, is_huge);
	cmd = &priv->txq[CMD_QUEUE_NUM].cmd[cmd_index];
	/* Input error checking is done when commands are added to queue. */
	if (cmd->meta.flags & CMD_WANT_SKB) {
		/* FIXME: we use cmd->meta.magic to indicate the
		* memory cmd->meta.source points to is still valid or
		* not at this point since caller may pass a local
		* variable to us and returned before we get here.
		* In this case, caller must ensure the ->magic field
		* is set correctly to indicate the availability of the
		* pointer cmd->meta.source. */
		if (cmd->meta.source->magic == CMD_VAR_MAGIC) {
			cmd->meta.source->u.skb = rxb->skb;
			cmd->meta.source->magic = 0;
			rxb->skb = NULL;
		}
	} else if (cmd->meta.u.callback &&
		   !cmd->meta.u.callback(priv, cmd, rxb->skb))
		rxb->skb = NULL;

	ipw_queue_tx_reclaim(priv, fifo, index);

	/* is_cmd_sync(cmd) works with ipw_host_cmd... here we only have ipw_cmd */
	if (!(cmd->meta.flags & CMD_ASYNC)) {
		priv->status &= ~STATUS_HCMD_ACTIVE;
		wake_up_interruptible(&priv->wait_command_queue);
	}
}

static void ipw_handle_reply_rx(struct ipw_priv *priv,
				struct ipw_rx_mem_buffer *rxb)
{
	struct ipw_rx_packet *pkt = (void *)rxb->skb->data;
	struct ipw_rx_frame_stats *rx_stats = IPW_RX_STATS(pkt);
	struct ipw_rx_frame_hdr *rx_hdr = IPW_RX_HDR(pkt);
	struct ipw_rx_frame_end *rx_end = IPW_RX_END(pkt);
	struct ieee80211_rx_stats stats = {
		.rssi = rx_stats->rssi - IPW_RSSI_OFFSET,
		.signal = le16_to_cpu(rx_stats->sig_avg),
		.noise = le16_to_cpu(rx_stats->noise_diff),
		.mac_time = rx_end->beaconTimeStamp,
		.rate = rx_hdr->rate,
		.received_channel = rx_hdr->channel,
		.len = le16_to_cpu(rx_hdr->len),
		.freq =
		    (rx_hdr->
		     phy_flags & RX_RES_PHY_FLAGS_BAND_24_MSK) ?
		    IEEE80211_24GHZ_BAND : IEEE80211_52GHZ_BAND,
		.tsf = rx_end->timestamp,
		.beacon_time = rx_end->beaconTimeStamp,
	};
	u16 snr = 0;

	if (rx_stats->noise_diff)
		snr = (rx_stats->sig_avg << 7) / rx_stats->noise_diff;
	stats.noise = stats.rssi - (snr >> 7);

	if (unlikely(rx_stats->mib_count > 20)) {
		IPW_DEBUG_DROP
		    ("dsp size out of range [0,20]: "
		     "%d", rx_stats->mib_count);
		return;
	}

	if (stats.rssi != 0)
		stats.mask |= IEEE80211_STATMASK_RSSI;
	stats.signal = stats.rssi;

	if (stats.signal != 0)
		stats.mask |= IEEE80211_STATMASK_SIGNAL;
	if (stats.noise != 0)
		stats.mask |= IEEE80211_STATMASK_NOISE;
	if (stats.rate != 0)
		stats.mask |= IEEE80211_STATMASK_RATE;

	if (!(rx_end->status & RX_RES_STATUS_NO_CRC32_ERROR)
	    || !(rx_end->status & RX_RES_STATUS_NO_RXE_OVERFLOW)) {
		IPW_DEBUG_RX("Bad CRC or FIFO: 0x%08X.\n", rx_end->status);
		return;
	}
	if (param_rtap_iface && netif_running(priv->prom_net_dev))
		ipw_handle_promiscuous_rx(priv, rxb, &stats);

	ipw_handle_data_packet(priv, rxb, &stats);
}

#define TX_STATUS_ENTRY(x) case TX_STATUS_FAIL_ ## x: return #x

static const char *get_tx_fail_reason(u32 status)
{
	switch (status & TX_STATUS_MSK) {
	case TX_STATUS_SUCCESS:
		return "SUCCESS";
		TX_STATUS_ENTRY(SHORT_LIMIT);
		TX_STATUS_ENTRY(LONG_LIMIT);
		TX_STATUS_ENTRY(FIFO_UNDERRUN);
		TX_STATUS_ENTRY(MGMNT_ABORT);
		TX_STATUS_ENTRY(NEXT_FRAG);
		TX_STATUS_ENTRY(LIFE_EXPIRE);
		TX_STATUS_ENTRY(DEST_PS);
		TX_STATUS_ENTRY(ABORTED);
		TX_STATUS_ENTRY(BT_RETRY);
		TX_STATUS_ENTRY(STA_INVALID);
		TX_STATUS_ENTRY(FRAG_DROPPED);
		TX_STATUS_ENTRY(TID_DISABLE);
		TX_STATUS_ENTRY(FRAME_FLUSHED);
		TX_STATUS_ENTRY(INSUFFICIENT_CF_POLL);
		TX_STATUS_ENTRY(TX_LOCKED);
	}

	return "UNKNOWN";
}

static void ipw_handle_reply_tx(struct ipw_priv *priv,
				struct ipw_tx_resp *resp, u16 sequence)
{
	int fifo = SEQ_TO_FIFO(sequence);

	IPW_DEBUG_TX("Tx fifo %d Status %s (0x%08x) plcp rate %d retries %d\n",
		     fifo, get_tx_fail_reason(resp->status),
		     resp->status, resp->rate, resp->failure_frame);

	if (check_bits(resp->status, TX_ABORT_REQUIRED_MSK)) {
		IPW_ERROR("Tx ABORT REQUIRED!!!  Scheduling restart.\n");
		queue_delayed_work(priv->workqueue, &priv->restart, 0);
	}

	/* We don't do rate scaling or measurements with microcode commands */
	if (fifo == CMD_QUEUE_NUM)
		return;

	/* If the packet had any retries or status does not indicate success,
	 * then we don't do anything with this status info.
	 *
	 * NOTE : this is dead code, but kept here as code to indicate how
	 * to detect it a Tx actually succeeded w/out failure of any kind.
	 *
	 if (resp->failure_frame || resp->failure_rts
	 || resp->bt_kill_count || ((resp->status & 0xff) != 1))
	 return;
	 *
	 */
}

//todoG fix this
/*
 * Main entry function for recieving a packet with 80211 headers.  This
 * should be called when ever the FW has notified us that there is a new
 * skb in the recieve queue.
 */
static void ipw_rx_handle(struct ipw_priv *priv)
{
	struct ipw_rx_mem_buffer *rxb;
	struct ipw_rx_packet *pkt;
	u32 r, i;
	int pkt_from_hardware;
	struct delayed_work *pwork;

	r = priv->shared_virt->rx_read_ptr[0];
	i = priv->rxq->read;
	while (i != r) {
		rxb = priv->rxq->queue[i];
		BUG_ON(rxb == NULL);
		priv->rxq->queue[i] = NULL;
		pci_dma_sync_single_for_cpu(priv->pci_dev,
					    rxb->dma_addr,
					    IPW_RX_BUF_SIZE,
					    PCI_DMA_FROMDEVICE);
		pkt = (struct ipw_rx_packet *)rxb->skb->data;

		/* If this frame wasn't received then it is a response from
		 * a host request */
		pkt_from_hardware = !(pkt->hdr.sequence & SEQ_RX_FRAME);

		/* Don't report replies covered by debug messages below ...
		 * switch statement for readability ... compiler may optimize.
		 * Hack at will to see/not-see what you want in logs. */
		switch (pkt->hdr.cmd) {
		case REPLY_STATISTICS_CMD:
		case STATISTICS_NOTIFICATION:
		case REPLY_RX:
		case REPLY_ALIVE:
		case REPLY_ADD_STA:
		case REPLY_ERROR:
			break;
		default:
			IPW_DEBUG_RX
			    ("Received %s command (#%x), seq:0x%04X, "
			     "flags=0x%02X, len = %d\n",
			     get_cmd_string(pkt->hdr.cmd),
			     pkt->hdr.cmd, pkt->hdr.sequence,
			     pkt->hdr.flags, le16_to_cpu(pkt->len));
		}

		switch (pkt->hdr.cmd) {
		case REPLY_RX:	/* 802.11 frame */
			ipw_handle_reply_rx(priv, rxb);
			break;

		case REPLY_ALIVE:{
				ipw_disable_events(priv);

				IPW_DEBUG_INFO
				    ("Alive ucode status 0x%08X revision "
				     "0x%01X 0x%01X\n",
				     pkt->u.alive_frame.is_valid,
				     pkt->u.alive_frame.ver_type,
				     pkt->u.alive_frame.ver_subtype);

				if (pkt->u.alive_frame.ver_subtype == INITIALIZE_SUBTYPE) {
					IPW_DEBUG_INFO("Initialization Alive received.\n");
					memcpy(&priv->card_alive_init,
					&pkt->u.alive_frame,
					sizeof(struct ipw_init_alive_resp));
					pwork = &priv->init_alive_start;
				} else {
					IPW_DEBUG_INFO("Runtime Alive received.\n");
					memcpy(&priv->card_alive, &pkt->u.alive_frame,
					sizeof(struct ipw_alive_resp));
					pwork = &priv->alive_start;
				}

				/* We delay the ALIVE response by 5ms to
				 * give the HW RF Kill time to activate... */
				if (pkt->u.alive_frame.is_valid == UCODE_VALID_OK)
					queue_delayed_work(priv->
							   workqueue,
							   pwork,
							   msecs_to_jiffies(5));
				else
					IPW_WARNING
					    ("uCode did not respond OK.\n");
				break;
			}

		case REPLY_ADD_STA:{
				IPW_DEBUG_RX
				    ("Received REPLY_ADD_STA: 0x%02X\n",
				     pkt->u.status);
				break;
			}

		case REPLY_ERROR:{
				u32 err_type = pkt->u.err_resp.enumErrorType;
				u8 cmd_id = pkt->u.err_resp.currentCmdID;
				u16 seq = pkt->u.err_resp.erroneousCmdSeqNum;
				u32 info = pkt->u.err_resp.errorInfo;
				IPW_ERROR("Error Reply type 0x%08X "
					  "cmd %s (0x%02X) "
					  "seq 0x%04X info 0x%08X\n",
					  err_type,
					  get_cmd_string(cmd_id),
					  cmd_id, seq, info);
				break;
			}
		case REPLY_TX:
			ipw_handle_reply_tx(priv, &pkt->u.tx_resp,
					    pkt->hdr.sequence);
			break;

		case CHANNEL_SWITCH_NOTIFICATION:{
				struct ipw_csa_notification *csa =
				    &(pkt->u.csa_notif);
				IPW_DEBUG_NOTIF
				    ("CSA notif: channel %d, status %d\n",
				     csa->channel, csa->status);
				priv->channel = csa->channel;
				break;
			}

		case SPECTRUM_MEASURE_NOTIFICATION:{
				struct ipw_spectrum_notification
				*report = &(pkt->u.spectrum_notif);

				if (!report->state) {
					IPW_DEBUG_NOTIF
					    ("Spectrum Measure Notification: "
					     "Start\n");
					break;
				}

				memcpy(&priv->measure_report, report,
				       sizeof(*report));

				break;
			}

		case QUIET_NOTIFICATION:
			IPW_DEBUG_INFO("UNHANDLED - Quiet Notification.\n");
			break;

		case MEASURE_ABORT_NOTIFICATION:
			IPW_DEBUG_INFO
			    ("UNHANDLED - Measure Abort Notification.\n");
			break;

		case RADAR_NOTIFICATION:
			IPW_DEBUG_INFO("UNHANDLED - Radar Notification.\n");
			break;

		case PM_SLEEP_NOTIFICATION:{
#ifdef CONFIG_IPWRAW_DEBUG
				struct ipw_sleep_notification *sleep =
				    &(pkt->u.sleep_notif);
				IPW_DEBUG_RX
				    ("sleep mode: %d, src: %d\n",
				     sleep->pm_sleep_mode,
				     sleep->pm_wakeup_src);
#endif
				break;
			}

		case PM_DEBUG_STATISTIC_NOTIFIC:
			IPW_DEBUG_RADIO
			    ("Dumping %d bytes of unhandled "
			     "notification for %s:\n",
			     le16_to_cpu(pkt->len),
			     get_cmd_string(pkt->hdr.cmd));
			printk_buf(IPW_DL_RADIO, pkt->u.raw,
				   le16_to_cpu(pkt->len));
			break;

		case REPLY_STATISTICS_CMD:
		case STATISTICS_NOTIFICATION:
			IPW_DEBUG_RX
			    ("Statistics notification received (%zd vs %d).\n",
			     sizeof(priv->statistics), pkt->len);
			memcpy(&priv->statistics, pkt->u.raw,
			       sizeof(priv->statistics));
			break;

		case WHO_IS_AWAKE_NOTIFICATION:
			IPW_DEBUG_RX("Notification from the card \n");
			break;

		case CARD_STATE_NOTIFICATION:{
				u32 flags =
				    le32_to_cpu(pkt->u.card_state_notif.flags);
				u32 status = priv->status;
				IPW_DEBUG_RF_KILL
				    ("Card state received: "
				     "HW:%s SW:%s\n",
				     (flags & HW_CARD_DISABLED) ?
				     "Off" : "On",
				     (flags & SW_CARD_DISABLED) ? "Off" : "On");

				if (flags & HW_CARD_DISABLED) {
					ipw_write32(priv,
						    CSR_UCODE_DRV_GP1_SET,
						    CSR_UCODE_DRV_GP1_BIT_CMD_BLOCKED);
					ipw_read32(priv, CSR_INT_MASK);

					priv->status |= STATUS_RF_KILL_HW;
				} else
					priv->status &= ~STATUS_RF_KILL_HW;

				/* uCode cannot tell us the SW kill switch status. -YZ
				if (flags & SW_CARD_DISABLED)
					priv->status |= STATUS_RF_KILL_SW;
				else
					priv->status &= ~STATUS_RF_KILL_SW;
				*/

				if (((status & STATUS_RF_KILL_HW) !=
				     (priv->status & STATUS_RF_KILL_HW))
				    || ((status & STATUS_RF_KILL_SW)
					!= (priv->status & STATUS_RF_KILL_SW))) {

					queue_delayed_work(priv->workqueue,
						   &priv->rf_kill, 0);
				} else
					wake_up_interruptible(&priv->
							      wait_command_queue);

				break;
			}
		default:
			break;
		}

		if (pkt_from_hardware) {
			/* Invoke any callbacks, transfer the skb to
			 * caller, and fire off the (possibly) blocking
			 * ipw_send_cmd() via as we reclaim the queue... */
			if (rxb && rxb->skb)
				ipw_tx_complete(priv, rxb);
			else
				IPW_WARNING("Claim null rxb?\n");
		}

		/* For now we just don't re-use anything.  We can tweak this
		 * later to try and re-use notification packets and SKBs that
		 * fail to Rx correctly */
		if (rxb->skb != NULL) {
			dev_kfree_skb_any(rxb->skb);
			rxb->skb = NULL;
		}

		pci_unmap_single(priv->pci_dev, rxb->dma_addr,
				 IPW_RX_BUF_SIZE, PCI_DMA_FROMDEVICE);
		list_add_tail(&rxb->list, &priv->rxq->rx_used);
		i = (i + 1) % RX_QUEUE_SIZE;
	}

	/* Backtrack one entry */
	priv->rxq->read = i;
	ipw_rx_queue_restock(priv);
}

/* net device stuff */

static int ipw_net_open(struct net_device *dev)
{
	struct ipw_priv *priv = netdev_priv(dev);
	IPW_DEBUG_INFO("dev->open\n");
	/* we should be verifying the device is ready to be opened */
	mutex_lock(&priv->mutex);
	if (!(priv->status & STATUS_RF_KILL_MASK))
		netif_start_queue(dev);
	mutex_unlock(&priv->mutex);
	return 0;
}

static int ipw_net_stop(struct net_device *dev)
{
	IPW_DEBUG_INFO("dev->close\n");
	netif_stop_queue(dev);
	return 0;
}

/*
  TX command functions
*/

/*
  handle build REPLY_TX command notification. it is responsible for rates fields.
*/
static int ipw_build_tx_cmd_rate(struct ipw_priv *priv,
				 struct ipw_tx_cmd *tx_cmd,
				 struct ieee80211_hdr_4addr *hdr,
				 int sta_id, int tx_id)
{
	u16 rate_mask, ctrl_rate;

	rate_mask = priv->active_rate;
	ctrl_rate = priv->active_rate;

	if ((is_multicast_ether_addr(hdr->addr1) ||
	     ipw_is_broadcast_ether_addr(hdr->addr1)) &&
	    (priv->active_rate_basic)) {
		rate_mask = priv->active_rate_basic;
		ctrl_rate = priv->active_rate_basic;
	}

	tx_cmd->rate = priv->rate_plcp;
/*	tx_cmd->rate = ipw_get_tx_rate(priv, rate_mask,
				       priv->stations[sta_id].
				       current_rate, sta_id);*/
	if (tx_cmd->rate == IPW_INVALID_RATE) {
		IPW_ERROR("ERROR: No TX rate available.\n");
		return -1;
	}

	priv->stations[sta_id].current_rate = tx_cmd->rate;

	if (tx_id >= CMD_QUEUE_NUM)
		tx_cmd->rts_retry_limit = 3;
	else
		tx_cmd->rts_retry_limit = 7;

	if (frame_is_probe_response(hdr->frame_ctl)) {
		tx_cmd->data_retry_limit = 3;
		if (tx_cmd->data_retry_limit < tx_cmd->rts_retry_limit)
			tx_cmd->rts_retry_limit = tx_cmd->data_retry_limit;
	} else
		tx_cmd->data_retry_limit = IPW_DEFAULT_TX_RETRY;

	tx_cmd->data_retry_limit = priv->retry_rate;

	if (check_bits(hdr->frame_ctl, IEEE80211_FCTL_FTYPE)) {
		IPW_DEBUG_TX("Tx of a management frame\n");
/*		if (priv->active_config.flags & RXON_FLG_BAND_24G_MSK)
			tx_cmd->rate = R_1M;
		else
			tx_cmd->_rate = R_6M;
*/
		switch (hdr->frame_ctl & IEEE80211_FCTL_STYPE) {
		case IEEE80211_STYPE_AUTH:
		case IEEE80211_STYPE_DEAUTH:
		case IEEE80211_STYPE_ASSOC_REQ:
		case IEEE80211_STYPE_REASSOC_REQ:
			if (tx_cmd->tx_flags & TX_CMD_FLG_RTS_MSK) {
				tx_cmd->tx_flags &= ~TX_CMD_FLG_RTS_MSK;
				tx_cmd->tx_flags |= TX_CMD_FLG_CTS_MSK;
			}
			break;
		default:
			break;
		}
	}

	/* OFDM */
	tx_cmd->supp_rates[0] = rate_mask >> 4;

	/* CCK */
	tx_cmd->supp_rates[1] = rate_mask & 0xF;

	tx_cmd->tx_flags &= ~(TX_CMD_FLG_ANT_A_MSK | TX_CMD_FLG_ANT_B_MSK);
	return 0;
}

/*
  handle build REPLY_TX command notification.
*/
static void ipw_build_tx_cmd_basic(struct ipw_priv *priv,
				   struct ipw_tx_cmd *tx_cmd,
				   struct ieee80211_hdr_4addr *hdr,
				   int is_unicast)
{
	ulong nominal_length;

	tx_cmd->u.life_time = 0xFFFFFFFF;
	if (is_unicast == 1) {
		tx_cmd->tx_flags |= TX_CMD_FLG_ACK_MSK;
		if ((hdr->frame_ctl & IEEE80211_FCTL_FTYPE) ==
		    IEEE80211_FTYPE_MGMT) {
			tx_cmd->tx_flags |= TX_CMD_FLG_SEQ_CTL_MSK;
		}
		if (frame_is_probe_response(hdr->frame_ctl)) {
			if ((hdr->seq_ctl & 0x000F) == 0) {
				tx_cmd->tx_flags |= TX_CMD_FLG_TSF_MSK;
			}
		}
	} else {
		tx_cmd->tx_flags &= (~TX_CMD_FLG_ACK_MSK);
		tx_cmd->tx_flags |= TX_CMD_FLG_SEQ_CTL_MSK;
	}

	if (hdr->frame_ctl & IEEE80211_FCTL_MOREFRAGS)
		tx_cmd->tx_flags |= TX_CMD_FLG_MORE_FRAG_MSK;
	tx_cmd->tx_flags |= TX_CMD_FLG_SEQ_CTL_MSK;
	tx_cmd->next_frame_len = 0;
	nominal_length = tx_cmd->len;
	nominal_length += IEEE80211_FCS_LEN;
	//todoG need to count security over head;
	/*
	   nominal_length += usMpduSecOverhead + IEEE80211_FCS_LEN;
	   if( (hdr->frame_ctl & IEEE80211_FCTL_MOREFRAGS) == 0)
	   nominal_length += usMsduSecOverhead;
	 */
	if (hdr->frame_ctl & IEEE80211_FCTL_PROTECTED)
		nominal_length += 4;

	/* When calculatin RTS threshold, we add 4 to account for the
	 * FCS bytes that the uCode will append to the MPDU */
	if (((priv->rts_threshold == 0) ||	/* Always RTS */
	     (nominal_length + 4) >= priv->rts_threshold) &&
	    (priv->rts_threshold <= MAX_MSDU_SIZE)) {
		tx_cmd->tx_flags |= TX_CMD_FLG_RTS_MSK;
		tx_cmd->tx_flags &= ~TX_CMD_FLG_CTS_MSK;
	}

	if ((tx_cmd->tx_flags & TX_CMD_FLG_RTS_MSK) ||
	    (tx_cmd->tx_flags & TX_CMD_FLG_CTS_MSK))
		tx_cmd->tx_flags |= TX_CMD_FLG_FULL_TXOP_PROT_MSK;
	tx_cmd->tx_flags &= ~(TX_CMD_FLG_ANT_SEL_MSK);
	if ((hdr->frame_ctl & IEEE80211_FCTL_FTYPE) == IEEE80211_FTYPE_MGMT) {
		if (((hdr->frame_ctl & IEEE80211_FCTL_STYPE) ==
		     IEEE80211_STYPE_ASSOC_REQ)
		    || ((hdr->frame_ctl & IEEE80211_FCTL_STYPE) ==
			IEEE80211_STYPE_REASSOC_REQ)) {
			tx_cmd->u2.pm_frame_timeout = 3;
		} else {
			tx_cmd->u2.pm_frame_timeout = 2;
		}
	} else
		tx_cmd->u2.pm_frame_timeout = 0;
	tx_cmd->driver_txop = 0;
}

//todoG need to move this to ieee code
#define IEEE80211_STYPE_BA_REQ  0x0080

#define IS_PLCP_RATE_CCK(rate) (((rate) & 0xf0) || (!((rate) & 0x1)))

#define IS_PLCP_RATE_OFDM(rate) (((rate) & 0x1) && (!((rate) & 0xf0)))

/* Incoming 802.11 strucure is converted to a TXB
 * a block of 802.11 fragment packets (stored as skbs) */
static int tx_skb(struct ipw_priv *priv, struct sk_buff *skb)
{
	int frame_len = skb->len;
	struct ieee80211_hdr_4addr *hdr = (void *)skb->data;
	struct tfd_frame *tfd;
	int tx_id = ipw_get_tx_queue_number(priv, 0);
	struct ipw_tx_queue *txq = &priv->txq[tx_id];
	struct ipw_queue *q = &txq->q;
	dma_addr_t phys_addr;
	struct ipw_cmd *out_cmd = NULL;
	u16 len, idx;
	u8 id, unicast;
	u8 sta_id;
	u16 seq_number;
	int rc;
	int hdr_len = frame_get_hdrlen(hdr->frame_ctl);

	unicast = !ipw_is_broadcast_ether_addr(hdr->addr1) &&
	    !is_multicast_ether_addr(hdr->addr1);
	id = 0;

	if (unicast) {
		if (frame_is_probe_response(hdr->frame_ctl))
			sta_id = ipw_find_station(priv, BROADCAST_ADDR, 1);
		else
			sta_id = ipw_find_station(priv, hdr->addr1, 1);
	} else
		sta_id = ipw_find_station(priv, BROADCAST_ADDR, 1);

	if (sta_id == IPW_INVALID_STATION) {
		IPW_DEBUG_DROP("Station " MAC_FMT
			       " not in station map. "
			       "Defaulting to broadcast...\n",
			       MAC_ARG(hdr->addr1));
		printk_buf(IPW_DL_DROP, (u8 *) hdr, sizeof(*hdr));
		sta_id = ipw_find_station(priv, BROADCAST_ADDR, 1);
		if (sta_id == IPW_INVALID_STATION)
			goto drop;
	}

	seq_number = priv->stations[sta_id].tid[tx_id].seq_number++;

	tfd = &txq->bd[q->first_empty];
	memset(tfd, 0, sizeof(*tfd));
	idx = get_next_cmd_index(q, q->first_empty, 0);

	txq->skb[q->first_empty] = skb;

	out_cmd = &txq->cmd[idx];
	memset(&out_cmd->hdr, 0, sizeof(out_cmd->hdr));
	memset(&out_cmd->cmd.tx, 0, sizeof(out_cmd->cmd.tx));
	out_cmd->hdr.cmd = REPLY_TX;
	out_cmd->hdr.sequence = FIFO_TO_SEQ(tx_id) |
	    INDEX_TO_SEQ(q->first_empty);
	/* copy frags header */
	memcpy(out_cmd->cmd.tx.hdr, hdr, hdr_len);

	hdr = (struct ieee80211_hdr_4addr *)out_cmd->cmd.tx.hdr;
	len = sizeof(struct ipw_tx_cmd) + sizeof(struct ipw_cmd_header)
	    + hdr_len;
	len = (len + 3) & ~3;

	phys_addr = txq->dma_addr_cmd + sizeof(struct ipw_cmd) * idx +
	    offsetof(struct ipw_cmd, hdr);
	attach_buffer_to_tfd_frame(tfd, phys_addr, cpu_to_le16(len));

	IPW_DEBUG_TX
	    ("Tx %d byte %scast packet in the clear to %d (hdrlen=%d)\n",
	     frame_len, unicast ? "uni" : "broad/multi", sta_id, hdr_len);
	printk_buf(IPW_DL_TX, skb->data, frame_len);

	phys_addr = cpu_to_le32(pci_map_single(priv->pci_dev,
					       skb->data + hdr_len,
					       frame_len - hdr_len,
					       PCI_DMA_TODEVICE));
	len = frame_len - hdr_len;
	attach_buffer_to_tfd_frame(tfd, phys_addr, cpu_to_le16(len));

	/* total length of frame */
	out_cmd->cmd.tx.len = frame_len;

	tfd->control_flags =
	    TFD_CTL_COUNT_SET(2) | TFD_CTL_PAD_SET(U32_PAD(len));

	out_cmd->cmd.tx.sta_id = sta_id;

	ipw_build_tx_cmd_basic(priv, &(out_cmd->cmd.tx), hdr, unicast);

	if (ipw_build_tx_cmd_rate(priv, &(out_cmd->cmd.tx), hdr, sta_id, tx_id)) {
		IPW_ERROR("tx cmd rate scale  failed.\n");
		goto drop;
	}

	IPW_DEBUG_TX("Tx rate %d (%02X:%02X)\n",
		     out_cmd->cmd.tx.rate,
		     out_cmd->cmd.tx.supp_rates[0],
		     out_cmd->cmd.tx.supp_rates[1]);

	out_cmd->cmd.tx.tx_flags |= TX_CMD_FLG_SEQ_CTL_MSK;

	printk_buf(IPW_DL_TX, out_cmd->cmd.payload, sizeof(out_cmd->cmd.tx));

	IPW_DEBUG_TX("Just header (%d bytes):\n",
		     frame_get_hdrlen(out_cmd->cmd.tx.hdr->frame_ctl));
	printk_buf(IPW_DL_TX, (u8 *) out_cmd->cmd.tx.hdr,
		   frame_get_hdrlen(out_cmd->cmd.tx.hdr->frame_ctl));

	q->first_empty = ipw_queue_inc_wrap(q->first_empty, q->n_bd);

	/* kick DMA */

	txq->need_update = 1;
	rc = ipw_tx_queue_update_write_ptr(priv, txq, tx_id);
	if (rc) {
		IPW_ERROR("Error adding packet to queue -- may leak SKB!\n");
		return rc;
	}

	if ((ipw_queue_space(q) < q->high_mark)
	    && priv->netdev_registered)
		netif_stop_queue(priv->net_dev);

	priv->led_packets += frame_len;
	ipw_setup_activity_timer(priv);

	return 0;

      drop:
	IPW_DEBUG_DROP("Silently dropping Tx packet.\n");
	dev_kfree_skb_any(skb);
	return 0;
}

#if 0
/* Used for sending frames internal to the driver; wraps the frame in a
 * skb */
static int raw_tx(struct ipw_priv *priv, struct ieee80211_hdr *frame,
		  int frame_len)
{
	struct sk_buff *skb = alloc_skb(frame_len, GFP_ATOMIC);
	if (!skb)
		return -ENOMEM;

	memcpy(skb_put(skb, frame_len), frame, frame_len);

	tx_skb(priv, skb);

	return 0;
}
#endif

int ipw_net_hard_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	struct ipw_priv *priv = netdev_priv(dev);
	unsigned long flags;
	const struct ieee80211_hdr_4addr *hdr = (void *)skb->data;

	if (skb->len < sizeof(struct ieee80211_hdr_1addr)) {
		IPW_DEBUG_DROP("too small for any legal 802.11 frame; drop\n");
		/* too small for any legal 802.11 frames; drop */
		goto drop;
	}

	spin_lock_irqsave(&priv->lock, flags);

	if (priv->status & STATUS_RF_KILL_MASK) {
		IPW_DEBUG_DROP("in rf kill state; drop\n");
		spin_unlock_irqrestore(&priv->lock, flags);
		goto drop;
	}

	if (param_rtap_iface && netif_running(priv->prom_net_dev))
		ipw_handle_promiscuous_tx(priv, (void *)skb->data, skb->len);

	/* Make sure that the target has a station entry (we have to do it
	 * here to keep from having to play unlock games in tx_skb) */
	if (!frame_is_probe_response(hdr->frame_ctl)) {
		if (!ipw_is_broadcast_ether_addr(hdr->addr1) &&
		    !is_multicast_ether_addr(hdr->addr1)) {
			spin_unlock_irqrestore(&priv->lock, flags);

			ipw_find_station(priv, hdr->addr1, 1);

			spin_lock_irqsave(&priv->lock, flags);
		}
	}

	tx_skb(priv, skb);

	spin_unlock_irqrestore(&priv->lock, flags);

	return 0;

      drop:
	netif_stop_queue(dev);
	return 1;
}

static struct net_device_stats *ipw_net_get_stats(struct net_device
						  *dev)
{
	struct ipw_priv *priv = netdev_priv(dev);
	priv->stats.tx_packets = priv->tx_packets;
	priv->stats.rx_packets = priv->rx_packets;
	priv->stats.rx_bytes = priv->rx_bytes;
	priv->stats.tx_bytes = priv->tx_bytes;
	return &priv->stats;
}

static int ipw_net_set_mac_address(struct net_device *dev, void *p)
{
	struct ipw_priv *priv = netdev_priv(dev);
	struct sockaddr *addr = p;
	if (!is_valid_ether_addr(addr->sa_data))
		return -EADDRNOTAVAIL;
	mutex_lock(&priv->mutex);
	priv->config |= CFG_CUSTOM_MAC;
	memcpy(priv->mac_addr, addr->sa_data, ETH_ALEN);
	printk(KERN_INFO "%s: Setting MAC to " MAC_FMT "\n",
	       priv->net_dev->name, MAC_ARG(priv->mac_addr));
	IPW_DEBUG_INFO("Restarting adapter to set new MAC.\n");
	queue_delayed_work(priv->workqueue, &priv->restart, 0);
	mutex_unlock(&priv->mutex);
	return 0;
}

static void ipw_ethtool_get_drvinfo(struct net_device *dev,
				    struct ethtool_drvinfo *info)
{
	struct ipw_priv *priv = netdev_priv(dev);
	char ver_string[9];

	strcpy(info->driver, DRV_NAME);
	strcpy(info->version, DRV_VERSION);

	memcpy(ver_string, priv->card_alive.sw_rev, 8);
	ver_string[8] = '\0';

	snprintf(info->fw_version, sizeof(info->fw_version),
		 "%s%d.%d %d:%d (%s)",
		 priv->card_alive.ucode_major & 0x80 ? "P" : "",
		 priv->card_alive.ucode_major & 0x7f,
		 priv->card_alive.ucode_minor,
		 priv->card_alive.ver_type,
		 priv->card_alive.ver_subtype, ver_string);

	strcpy(info->bus_info, pci_name(priv->pci_dev));
	info->eedump_len = sizeof(priv->eeprom);
}

static u32 ipw_ethtool_get_link(struct net_device *dev)
{
	return ipw_is_ready(netdev_priv(dev));
}

static int ipw_ethtool_get_eeprom_len(struct net_device *dev)
{
	return sizeof(struct ipw_eeprom);
}

static int ipw_ethtool_get_eeprom(struct net_device *dev,
				  struct ethtool_eeprom *eeprom, u8 * bytes)
{
	struct ipw_priv *priv = netdev_priv(dev);
	if (eeprom->offset + eeprom->len > sizeof(priv->eeprom))
		return -EINVAL;

	mutex_lock(&priv->mutex);
	memcpy(bytes, &((u8 *) & priv->eeprom)[eeprom->offset], eeprom->len);
	mutex_unlock(&priv->mutex);
	return 0;
}

static struct ethtool_ops ipw_ethtool_ops = {
	.get_link = ipw_ethtool_get_link,
	.get_drvinfo = ipw_ethtool_get_drvinfo,
	.get_eeprom_len = ipw_ethtool_get_eeprom_len,
	.get_eeprom = ipw_ethtool_get_eeprom,
};


#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19) )
static irqreturn_t ipw_isr(int irq, void *data, struct pt_regs *regs)
#else
static irqreturn_t ipw_isr(int irq, void *data)
#endif
{
	struct ipw_priv *priv = data;
	u32 inta, inta_mask;
	if (!priv)
		return IRQ_NONE;

	spin_lock(&priv->lock);
	if (!(priv->status & STATUS_INT_ENABLED)) {
		/* Shared IRQ */
		goto none;
	}

	inta = ipw_read32(priv, CSR_INT);
	inta_mask = ipw_read32(priv, CSR_INT_MASK);
	if (inta == 0xFFFFFFFF) {
		/* Hardware disappeared */
		IPW_WARNING("IRQ INTA == 0xFFFFFFFF\n");
		/* the device may have raised the interrupt */
		goto unplugged;
	}

	if (!(inta & (CSR_INI_SET_MASK & inta_mask))) {
		if (inta) {
			ipw_write32(priv, CSR_INT, inta);
			ipw_read32(priv, CSR_INT_MASK);
		}
		/* Shared interrupt */
		goto none;
	}

	/* tell the device to stop sending interrupts */

	IPW_DEBUG_ISR
	    ("interrupt recieved 0x%08x masked 0x%08x card mask 0x%08x\n",
	     inta, inta_mask, CSR_INI_SET_MASK);

	priv->status &= ~STATUS_INT_ENABLED;
	ipw_write32(priv, CSR_INT_MASK, 0x00000000);
	/* ack current interrupts */
	ipw_write32(priv, CSR_INT, inta);
	ipw_read32(priv, CSR_INT_MASK);
	inta &= (CSR_INI_SET_MASK & inta_mask);
	/* Cache INTA value for our tasklet */
	priv->isr_inta = inta;
	tasklet_schedule(&priv->irq_tasklet);
	unplugged:
	spin_unlock(&priv->lock);

	return IRQ_HANDLED;

      none:
	spin_unlock(&priv->lock);
	return IRQ_NONE;
}

static void ipw_bg_rf_kill(struct work_struct *work)
{
	struct ipw_priv *priv = IPW_DELAYED_DATA(work, struct ipw_priv,
						 rf_kill);

	wake_up_interruptible(&priv->wait_command_queue);

	if (priv->status & STATUS_EXIT_PENDING)
		return;

	mutex_lock(&priv->mutex);

	if (!(priv->status & STATUS_RF_KILL_MASK)) {
		IPW_DEBUG(IPW_DL_INFO | IPW_DL_RF_KILL,
			  "HW RF Kill no longer active, restarting "
			  "device\n");
		if (!(priv->status & STATUS_EXIT_PENDING))
			ipw_down(priv);
	} else {
		priv->led_state = IPW_LED_LINK_RADIOOFF;

		if (!(priv->status & STATUS_RF_KILL_HW))
			IPW_DEBUG_RF_KILL
			    ("Can not turn radio back on - "
			     "disabled by SW switch\n");
		else
			IPW_WARNING
			    ("Radio Frequency Kill Switch is On:\n"
			     "Kill switch must be turned off for "
			     "wireless networking to work.\n");
	}
	mutex_unlock(&priv->mutex);
}

static void ipw_bg_resume_work(struct work_struct *work)
{
	struct ipw_priv *priv = IPW_DELAYED_DATA(work, struct ipw_priv,
						 resume_work);
	unsigned long flags;

	mutex_lock(&priv->mutex);

	/* The following it a temporary work around due to the
	 * suspend / resume not fully initializing the NIC correctly.
	 * Without all of the following, resume will not attempt to take
	 * down the NIC (it shouldn't really need to) and will just try
	 * and bring the NIC back up.  However that fails during the
	 * ucode verification process.  This then causes ipw_down to be
	 * called *after* ipw_nic_init() has succeedded -- which
	 * then lets the next init sequence succeed.  So, we've
	 * replicated all of that NIC init code here... */

	ipw_write32(priv, CSR_INT, 0xFFFFFFFF);

	ipw_nic_init(priv);

	ipw_write32(priv, CSR_UCODE_DRV_GP1_CLR, CSR_UCODE_SW_BIT_RFKILL);
	ipw_write32(priv, CSR_UCODE_DRV_GP1_CLR,
		    CSR_UCODE_DRV_GP1_BIT_CMD_BLOCKED);
	ipw_write32(priv, CSR_INT, 0xFFFFFFFF);
	ipw_write32(priv, CSR_UCODE_DRV_GP1_CLR, CSR_UCODE_SW_BIT_RFKILL);
	ipw_write32(priv, CSR_UCODE_DRV_GP1_CLR, CSR_UCODE_SW_BIT_RFKILL);

	/* tell the device to stop sending interrupts */
	ipw_disable_interrupts(priv);

	spin_lock_irqsave(&priv->lock, flags);
	ipw_clear_bit(priv, CSR_GP_CNTRL, CSR_GP_CNTRL_REG_FLAG_MAC_ACCESS_REQ);
	spin_unlock_irqrestore(&priv->lock, flags);

	spin_lock_irqsave(&priv->lock, flags);
	if (!ipw_grab_restricted_access(priv)) {
		ipw_write_restricted_reg(priv, ALM_APMG_CLK_DIS,
					 APMG_CLK_REG_VAL_DMA_CLK_RQT);
		ipw_release_restricted_access(priv);
	}
	spin_unlock_irqrestore(&priv->lock, flags);

	udelay(5);

	ipw_nic_reset(priv);

	/* Bring the device back up */
	priv->status &= ~STATUS_IN_SUSPEND;

	mutex_unlock(&priv->mutex);
}

static void ipw_bg_reg_txpower_periodic(struct work_struct *work);

static int ipw_setup_deferred_work(struct ipw_priv *priv)
{
	int ret = 0;
	priv->workqueue = create_workqueue(DRV_NAME);

	init_waitqueue_head(&priv->wait_command_queue);

	IPW_INIT_DELAYED_WORK(&priv->commit, ipw_bg_commit);

	IPW_INIT_DELAYED_WORK(&priv->rx_replenish, ipw_bg_rx_queue_replenish);
	IPW_INIT_DELAYED_WORK(&priv->rf_kill, ipw_bg_rf_kill);
	IPW_INIT_DELAYED_WORK(&priv->up, ipw_bg_up);
	IPW_INIT_DELAYED_WORK(&priv->restart, ipw_bg_restart);

	IPW_INIT_DELAYED_WORK(&priv->alive_start, ipw_bg_alive_start);
	IPW_INIT_DELAYED_WORK(&priv->init_alive_start,
					ipw_bg_init_alive_start);
	IPW_INIT_DELAYED_WORK(&priv->resume_work, ipw_bg_resume_work);

	IPW_INIT_DELAYED_WORK(&priv->activity_timer, ipw_bg_activity_timer);
	IPW_INIT_DELAYED_WORK(&priv->thermal_periodic, ipw_bg_reg_txpower_periodic);
	IPW_INIT_DELAYED_WORK(&priv->send_txpower, ipw_bg_reg_send_txpower);

	tasklet_init(&priv->irq_tasklet, (void (*)(unsigned long))
		     ipw_irq_tasklet, (unsigned long)priv);

	return ret;
}

/*
  Power management (not Tx power!) functions
*/
#define PCI_LINK_CTRL      0x0F0

static int ipw_power_init_handle(struct ipw_priv *priv)
{
	int rc = 0, i;
	struct ipw_power_mgr *pow_data;
	int size = sizeof(struct ipw_power_vec_entry) * IPW_POWER_AC;
	u16 pci_pm;

	IPW_DEBUG_POWER("Intialize power \n");

	pow_data = &(priv->power_data);

	memset(pow_data, 0, sizeof(*pow_data));

	pow_data->active_index = IPW_POWER_RANGE_0;
	pow_data->dtim_val = 0xffff;

	memcpy(&pow_data->pwr_range_0[0], &range_0[0], size);
	memcpy(&pow_data->pwr_range_1[0], &range_1[0], size);

	rc = pci_read_config_word(priv->pci_dev, PCI_LINK_CTRL, &pci_pm);
	if (rc != 0)
		return 0;
	else {
		struct ipw_powertable_cmd *cmd;

		IPW_DEBUG_POWER("adjust power command flags\n");

		for (i = 0; i < IPW_POWER_AC; i++) {
			cmd = &pow_data->pwr_range_0[i].cmd;

			if (pci_pm & 0x1)
				cmd->flags &= ~0x8;
			else
				cmd->flags |= 0x8;
		}
	}

	return rc;
}

int ipw_update_power_cmd(struct ipw_priv *priv,
			 struct ipw_powertable_cmd *cmd, u32 mode)
{
	int rc = 0, i;
	u8 skip = 0;
	u32 max_sleep = 0;
	struct ipw_power_vec_entry *range;
	u8 period = 0;
	struct ipw_power_mgr *pow_data;

	if ((mode < IPW_POWER_MODE_CAM) || (mode > IPW_POWER_INDEX_5)) {
		IPW_DEBUG_POWER("Error invalid power mode \n");
		return -1;
	}
	pow_data = &(priv->power_data);

	if (pow_data->active_index == IPW_POWER_RANGE_0)
		range = &pow_data->pwr_range_0[0];
	else
		range = &pow_data->pwr_range_1[1];

	memcpy(cmd, &range[mode].cmd, sizeof(struct ipw_powertable_cmd));

	skip = range[mode].no_dtim;

	if (period == 0) {
		period = 1;
		skip = 0;
	}

	if (skip == 0) {
		max_sleep = period;
		cmd->flags &= ~PMC_TCMD_FLAG_SLEEP_OVER_DTIM_MSK;
	} else {
		max_sleep =
		    (cmd->
		     SleepInterval[PMC_TCMD_SLEEP_INTRVL_TABLE_SIZE -
				   1] / period) * period;
		cmd->flags |= PMC_TCMD_FLAG_SLEEP_OVER_DTIM_MSK;
	}

	for (i = 0; i < PMC_TCMD_SLEEP_INTRVL_TABLE_SIZE; i++) {
		if (cmd->SleepInterval[i] > max_sleep)
			cmd->SleepInterval[i] = max_sleep;
	}

	IPW_DEBUG_POWER("Flags value = 0x%08X\n", cmd->flags);
	IPW_DEBUG_POWER("Tx timeout = %u\n", cmd->TxDataTimeout);
	IPW_DEBUG_POWER("Rx timeout = %u\n", cmd->RxDataTimeout);
	IPW_DEBUG_POWER
	    ("Sleep interval vector = { %d , %d , %d , %d , %d }\n",
	     cmd->SleepInterval[0], cmd->SleepInterval[1],
	     cmd->SleepInterval[2], cmd->SleepInterval[3],
	     cmd->SleepInterval[4]);

	return rc;
}

/************* CARD-FUNCTION ************/
/* Print card information like version number other eeprom values */
int ipw_card_show_info(struct ipw_priv *priv)
{
	IPW_DEBUG_INFO("3945ABG HW Version %u.%u.%u\n",
		       ((priv->eeprom.board_revision >> 8) & 0x0F),
		       ((priv->eeprom.board_revision >> 8) >> 4),
		       (priv->eeprom.board_revision & 0x00FF));

	IPW_DEBUG_INFO("3945ABG PBA Number %.*s\n",
		       (int)sizeof(priv->eeprom.board_pba_number),
		       priv->eeprom.board_pba_number);

	IPW_DEBUG_INFO("EEPROM_ANTENNA_SWITCH_TYPE is 0x%02X\n",
		       priv->eeprom.antenna_switch_type);

	return 0;
}

/*
  Call all registered function for card RXON status
  this function called after we call REPLY_RXON command.
*/
/*
  called at ipw_pci_remove to clean the driver data
*/
static int ipw_card_remove_notify(struct ipw_priv *priv)
{
	int rc = 0;
	return rc;
}

/*
  called after we recieved REPLY_ALIVE notification.
  this function starts the calibration then start the process
  of transfering the card to recieving state
*/
static void ipw_alive_start(struct ipw_priv *priv)
{
	int thermal_spin = 0;
	int rc;
	u32 rfkill;

	if (priv->card_alive.is_valid != 1) {
		/* We had an error bringing up the hardware, so take it
		 * all the way back down so we can try again */
		IPW_DEBUG_INFO("Alive failed.\n");
		ipw_down(priv);
		return;
	}

	/* Initialize uCode has loaded Runtime uCode... verify inst image.
	 * This is a paranoid check, because we would not have gotten the
	 * "runtime" alive if code weren't propery loaded. */
	if (ipw_verify_ucode(priv)) {
		/* Runtime instruction load was bad;
		 * take it all the way back down so we can try again */
		IPW_DEBUG_INFO("Bad runtime uCode load.\n");
		ipw_down(priv);
		return;
	}

	/* After the ALIVE response, we can process host commands */
	priv->status |= STATUS_ALIVE;

	IPW_DEBUG_INFO("Alive received.\n");

	rc = ipw_grab_restricted_access(priv);
	if (rc) {
		IPW_WARNING("Can not read rfkill status from adapter\n");
		return;
	}

	rfkill = ipw_read_restricted_reg(priv, ALM_APMG_RFKILL);
	IPW_DEBUG_INFO("RFKILL status: 0x%x\n", rfkill);
	ipw_release_restricted_access(priv);

	if (rfkill & 0x1) {
		priv->status &= ~STATUS_RF_KILL_HW;
		/* if rfkill is not on, then wait for thermal
		* sensor in adapter to kick in */
		while (ipw_get_temperature(priv) == 0) {
			thermal_spin++;
			udelay(10);
		}
		if (thermal_spin)
			IPW_DEBUG_INFO("Thermal calibration took %dus\n",
					thermal_spin * 10);
	} else
		priv->status |= STATUS_RF_KILL_HW;

	ipw_clear_stations_table(priv);

	if (!(priv->status & STATUS_RF_KILL_MASK))
		ipw_post_alive_work(priv);
}

#define MAX_HW_RESTARTS 5

static int ipw_up(struct ipw_priv *priv)
{
	int rc, i;

	IPW_DEBUG_INFO("in ipw_up...\n");
	if (priv->status & STATUS_EXIT_PENDING) {
		IPW_WARNING("Exit pending will not bring the NIC up\n");
		return -EIO;
	}

	if (priv->status & STATUS_RF_KILL_SW) {
		IPW_WARNING("Radio disabled by module parameter.\n");
		return 0;
	} else if (priv->status & STATUS_RF_KILL_HW)
		IPW_WARNING("Radio disabled by HW RF Kill Switch button.\n");

	ipw_write32(priv, CSR_INT, 0xFFFFFFFF);

	rc = ipw_nic_init(priv);
	if (rc) {
		IPW_ERROR("Unable to int nic\n");
		return rc;
	}

	ipw_write32(priv, CSR_UCODE_DRV_GP1_CLR, CSR_UCODE_SW_BIT_RFKILL);
	ipw_write32(priv, CSR_UCODE_DRV_GP1_CLR,
		    CSR_UCODE_DRV_GP1_BIT_CMD_BLOCKED);
	ipw_write32(priv, CSR_INT, 0xFFFFFFFF);

	ipw_enable_interrupts(priv);

	ipw_write32(priv, CSR_UCODE_DRV_GP1_CLR, CSR_UCODE_SW_BIT_RFKILL);
	ipw_write32(priv, CSR_UCODE_DRV_GP1_CLR, CSR_UCODE_SW_BIT_RFKILL);

	//TODO is this useful?
	memcpy(priv->ucode_data_backup.v_addr, priv->ucode_data.v_addr,
		priv->ucode_data.len);

	for (i = 0; i < MAX_HW_RESTARTS; i++) {

		ipw_clear_stations_table(priv);

		rc = ipw_load_bsm(priv);

		if (rc) {
			IPW_ERROR("Unable to set up bootstrap uCode: %d\n", rc);
			continue;
		}

		/* start card; bootstrap will load runtime ucode */
		ipw_nic_start(priv);

		ipw_card_show_info(priv);

		if (!(priv->config & CFG_CUSTOM_MAC)) {
			eeprom_parse_mac(priv, priv->mac_addr);
			IPW_DEBUG_INFO("MAC address: " MAC_FMT "\n",
				       MAC_ARG(priv->mac_addr));
		}
		memcpy(priv->net_dev->dev_addr, priv->mac_addr, ETH_ALEN);
		ipw_read32(priv, CSR_INT_MASK);
		return 0;
	}

	priv->status |= STATUS_EXIT_PENDING;
	ipw_down(priv);

	/* tried to restart and config the device for as long as our
	 * patience could withstand */
	IPW_ERROR("Unable to initialize device after %d attempts.\n", i);
	return -EIO;
}


static void ipw_down(struct ipw_priv *priv)
{
	unsigned long flags;
	int exit_pending = priv->status & STATUS_EXIT_PENDING;

	priv->status |= STATUS_EXIT_PENDING;

	/* If we are coming down due to a microcode error, then
	 * don't bother trying to do anything that results in sending
	 * host commands... */
	if (!(priv->status & STATUS_FW_ERROR) && ipw_is_alive(priv)) {
		ipw_update_link_led(priv);
		ipw_update_activity_led(priv);
		ipw_update_tech_led(priv);
	}

	ipw_remove_all_sta(priv);

	/* Unblock any waiting calls */
	wake_up_interruptible_all(&priv->wait_command_queue);

	cancel_delayed_work(&priv->resume_work);
	cancel_delayed_work(&priv->up);
	cancel_delayed_work(&priv->restart);
	cancel_delayed_work(&priv->activity_timer);
	cancel_delayed_work(&priv->rf_kill);
	cancel_delayed_work(&priv->thermal_periodic);
	cancel_delayed_work(&priv->send_txpower);
	cancel_delayed_work(&priv->alive_start);
	flush_workqueue(priv->workqueue);

	/* Wipe out the EXIT_PENDING status bit if we are not actually
	 * exiting the module */
	if (!exit_pending)
		priv->status &= ~STATUS_EXIT_PENDING;

	/* tell the device to stop sending interrupts */
	ipw_disable_interrupts(priv);

	if (priv->netdev_registered) {
		netif_carrier_off(priv->net_dev);
		netif_stop_queue(priv->net_dev);
	}

	/* If we have not previously called ipw_init() then
	 * clear all bits but the RF Kill and SUSPEND bits and return */
	if (!ipw_is_init(priv)) {
		priv->status &= (STATUS_RF_KILL_MASK | STATUS_IN_SUSPEND);
		goto exit;
	}

	/* ...otherwise clear out all the status bits but the RF Kill and
	 * SUSPEND bits and continue taking the NIC down. */
	priv->status &= (STATUS_RF_KILL_MASK | STATUS_IN_SUSPEND);

	spin_lock_irqsave(&priv->lock, flags);
	ipw_clear_bit(priv, CSR_GP_CNTRL, CSR_GP_CNTRL_REG_FLAG_MAC_ACCESS_REQ);
	spin_unlock_irqrestore(&priv->lock, flags);

	ipw_stop_tx_queue(priv);
	ipw_rxq_stop(priv);

	spin_lock_irqsave(&priv->lock, flags);
	if (!ipw_grab_restricted_access(priv)) {
		ipw_write_restricted_reg(priv, ALM_APMG_CLK_DIS,
					 APMG_CLK_REG_VAL_DMA_CLK_RQT);
		ipw_release_restricted_access(priv);
	}
	spin_unlock_irqrestore(&priv->lock, flags);

	udelay(5);

	ipw_nic_stop_master(priv);

	spin_lock_irqsave(&priv->lock, flags);
	ipw_set_bit(priv, CSR_RESET, CSR_RESET_REG_FLAG_SW_RESET);
	spin_unlock_irqrestore(&priv->lock, flags);

	ipw_nic_reset(priv);

      exit:
	memset(&priv->card_alive, 0, sizeof(struct ipw_alive_resp));

}

static void ipw_bg_init_alive_start(struct work_struct *work)
{
	struct ipw_priv *priv = IPW_DELAYED_DATA(work, struct ipw_priv,
						 init_alive_start);

	if (priv->status & STATUS_EXIT_PENDING)
		return;

	mutex_lock(&priv->mutex);
	ipw_init_alive_start(priv);
	mutex_unlock(&priv->mutex);
}

static void ipw_bg_alive_start(struct work_struct *work)
{
	struct ipw_priv *priv = IPW_DELAYED_DATA(work, struct ipw_priv,
						 alive_start);

	if (priv->status & STATUS_EXIT_PENDING)
		return;

	mutex_lock(&priv->mutex);
	ipw_alive_start(priv);
	mutex_unlock(&priv->mutex);
}

#define MAX_HW_RESTARTS 5

static void ipw_bg_up(struct work_struct *work)
{
	struct ipw_priv *priv = IPW_DELAYED_DATA(work, struct ipw_priv, up);

	if (priv->status & STATUS_EXIT_PENDING)
		return;

	mutex_lock(&priv->mutex);
	ipw_up(priv);
	mutex_unlock(&priv->mutex);
}


static void ipw_bg_restart(struct work_struct *work)
{
	struct ipw_priv *priv = IPW_DELAYED_DATA(work, struct ipw_priv, restart);

	if (priv->status & STATUS_EXIT_PENDING)
		return;

	mutex_lock(&priv->mutex);
	ipw_down(priv);
	ipw_up(priv);
	mutex_unlock(&priv->mutex);
}

static struct pci_device_id card_ids[] __devinitdata = {
	{0x8086, 0x4222, PCI_ANY_ID, PCI_ANY_ID, 0, 0, 0},
	{0x8086, 0x4227, PCI_ANY_ID, PCI_ANY_ID, 0, 0, 0},
	{0}
};

MODULE_DEVICE_TABLE(pci, card_ids);
static struct attribute *ipw_sysfs_entries[] = {
	&dev_attr_dev_type.attr,
	&dev_attr_txpower.attr,
	&dev_attr_assoc.attr,
	&dev_attr_rate.attr,
	&dev_attr_band.attr,
	&dev_attr_channel.attr,
	&dev_attr_rf_kill.attr,
	&dev_attr_status.attr,
	&dev_attr_cfg.attr,
	&dev_attr_dump_errors.attr,
	&dev_attr_dump_events.attr,
	&dev_attr_led.attr,
	&dev_attr_antenna.attr,
	&dev_attr_statistics.attr,
	&dev_attr_temperature.attr,
	&dev_attr_channels.attr,
	&dev_attr_bssid.attr,
	&dev_attr_retry_rate.attr,
	&dev_attr_rtap_iface.attr,
	&dev_attr_rtap_filter.attr,
	&dev_attr_rx.attr,
	NULL
};

/*
static struct attribute *ipw_sysfs_entries[] = {
	NULL
};
*/
static struct attribute_group ipw_attribute_group = {
	.name = NULL,		/* put in device directory */
	.attrs = ipw_sysfs_entries,
};

static int ipw_prom_open(struct net_device *dev)
{
	/*     struct ipw_prom_priv *prom_priv = netdev_priv(dev);
	   struct ipw_priv *priv = prom_priv->priv; */

	IPW_DEBUG_INFO("prom dev->open\n");
	netif_carrier_off(dev);
	netif_stop_queue(dev);

	return 0;
}

static int ipw_prom_stop(struct net_device *dev)
{
/*	struct ipw_prom_priv *prom_priv = netdev_priv(dev);
	struct ipw_priv *priv = prom_priv->priv;*/

	IPW_DEBUG_INFO("prom dev->stop\n");

	return 0;
}

static int ipw_prom_hard_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	IPW_DEBUG_INFO("prom dev->xmit\n");
	netif_stop_queue(dev);
	return -EOPNOTSUPP;
}

static struct net_device_stats *ipw_prom_get_stats(struct net_device
						   *dev)
{
	struct ipw_prom_priv *prom_priv = netdev_priv(dev);
	return &prom_priv->stats;
}

static int ipw_prom_alloc(struct ipw_priv *priv)
{
	int rc = 0;

	if (priv->prom_net_dev)
		return -EPERM;

	priv->prom_net_dev = alloc_etherdev(sizeof(struct ipw_prom_priv));
	if (priv->prom_net_dev == NULL)
		return -ENOMEM;

	priv->prom_priv = netdev_priv(priv->prom_net_dev);
	priv->prom_priv->priv = priv;

	strcpy(priv->prom_net_dev->name, "rtap%d");

	priv->prom_net_dev->type = ARPHRD_IEEE80211_RADIOTAP;
	priv->prom_net_dev->open = ipw_prom_open;
	priv->prom_net_dev->stop = ipw_prom_stop;
	priv->prom_net_dev->get_stats = ipw_prom_get_stats;
	priv->prom_net_dev->hard_start_xmit = ipw_prom_hard_start_xmit;
	memcpy(priv->prom_net_dev->dev_addr, priv->mac_addr, ETH_ALEN);
	ipw_read32(priv, CSR_INT_MASK);

	rc = register_netdev(priv->prom_net_dev);
	if (rc) {
		free_netdev(priv->prom_net_dev);
		priv->prom_net_dev = NULL;
		return rc;
	}

	return 0;
}

static int ipw_wx_get_name(struct net_device *dev,
			   struct iw_request_info *info,
			   union iwreq_data *wrqu, char *extra)
{
	strcpy(wrqu->name, "unassociated");
	return 0;
}

static int ipw_wx_get_essid(struct net_device *dev,
			    struct iw_request_info *info,
			    union iwreq_data *wrqu, char *extra)
{
	wrqu->essid.length = 0;
	wrqu->essid.flags = 0;
	return 0;
}

static int ipw_wx_get_freq(struct net_device *dev,
			   struct iw_request_info *info,
			   union iwreq_data *wrqu, char *extra)
{
	struct ipw_priv *priv = netdev_priv(dev);

	if (!ipw_is_ready(priv))
		return -EAGAIN;

	mutex_lock(&priv->mutex);
	wrqu->freq.m = priv->channel;
	wrqu->freq.e = 0;
	mutex_unlock(&priv->mutex);
	return 0;
}

#define 	MAP_KHZ_TO_CHANNEL_ID(khz, ch)	{				\
				switch (khz)								\
				{											\
					case 2412000:	 ch = 1;	 break; 	\
					case 2417000:	 ch = 2;	 break; 	\
					case 2422000:	 ch = 3;	 break; 	\
					case 2427000:	 ch = 4;	 break; 	\
					case 2432000:	 ch = 5;	 break; 	\
					case 2437000:	 ch = 6;	 break; 	\
					case 2442000:	 ch = 7;	 break; 	\
					case 2447000:	 ch = 8;	 break; 	\
					case 2452000:	 ch = 9;	 break; 	\
					case 2457000:	 ch = 10;	 break; 	\
					case 2462000:	 ch = 11;	 break; 	\
					case 2467000:	 ch = 12;	 break; 	\
					case 2472000:	 ch = 13;	 break; 	\
					case 2484000:	 ch = 14;	 break; 	\
					case 5180000:	 ch = 36;  /* UNII */  break;	  \
					case 5200000:	 ch = 40;  /* UNII */  break;	  \
					case 5220000:	 ch = 44;  /* UNII */  break;	  \
					case 5240000:	 ch = 48;  /* UNII */  break;	  \
					case 5260000:	 ch = 52;  /* UNII */  break;	  \
					case 5280000:	 ch = 56;  /* UNII */  break;	  \
					case 5300000:	 ch = 60;  /* UNII */  break;	  \
					case 5320000:	 ch = 64;  /* UNII */  break;	  \
					case 5745000:	 ch = 149; /* UNII */  break;	  \
					case 5765000:	 ch = 153; /* UNII */  break;	  \
					case 5785000:	 ch = 157; /* UNII */  break;	  \
					case 5805000:	 ch = 161; /* UNII */  break;	  \
					case 5500000:	 ch = 100; /* HiperLAN2 */	break;	   \
					case 5520000:	 ch = 104; /* HiperLAN2 */	break;	   \
					case 5540000:	 ch = 108; /* HiperLAN2 */	break;	   \
					case 5560000:	 ch = 112; /* HiperLAN2 */	break;	   \
					case 5580000:	 ch = 116; /* HiperLAN2 */	break;	   \
					case 5600000:	 ch = 120; /* HiperLAN2 */	break;	   \
					case 5620000:	 ch = 124; /* HiperLAN2 */	break;	   \
					case 5640000:	 ch = 128; /* HiperLAN2 */	break;	   \
					case 5660000:	 ch = 132; /* HiperLAN2 */	break;	   \
					case 5680000:	 ch = 136; /* HiperLAN2 */	break;	   \
					case 5700000:	 ch = 140; /* HiperLAN2 */	break;	   \
					case 5170000:	 ch = 34;  /* Japan MMAC */   break;   \
					case 5190000:	 ch = 38;  /* Japan MMAC */   break;   \
					case 5210000:	 ch = 42;  /* Japan MMAC */   break;   \
					case 5230000:	 ch = 46;  /* Japan MMAC */   break;   \
					default:		 ch = -1;	 break; 	\
				}											\
			}


static int ipw_wx_set_freq(struct net_device *dev,
			   struct iw_request_info *info,
			   union iwreq_data *wrqu, char *extra)
{
	struct ipw_priv *priv = netdev_priv(dev);
	const struct ipw_channel_info *ch_info;
	int 	chan = -1;

	if (!ipw_is_ready(priv))
		return -EAGAIN;

	if (priv->status & STATUS_EXIT_PENDING)
		return -EAGAIN;

	if (priv->status & STATUS_IN_SUSPEND)
		return -ERESTARTSYS;

	if((wrqu->freq.e == 0) && (wrqu->freq.m <= 1000))
		chan = wrqu->freq.m;	// Setting by channel number
	else
		MAP_KHZ_TO_CHANNEL_ID( (wrqu->freq.m /100) , chan); // Setting by frequency - search the table , like 2.412G, 2.422G,

	mutex_lock(&priv->mutex);
	if (priv->channel == chan) {
		IPW_DEBUG_INFO("Already tuned to channel %d\n", chan);
	} else {
		ch_info = find_channel(priv, chan);
		if (ch_info == NULL) {
			IPW_ERROR("Invalid channel request: %d\n", chan);
			mutex_unlock(&priv->mutex);
			return -EINVAL;
		}

		IPW_DEBUG_INFO("Tuning to channel %d\n", chan);

		priv->channel = chan;
		priv->staging_config.channel = ch_info->channel;

		if (is_channel_a_band(ch_info)) {
			priv->staging_config.flags &=
			    ~(RXON_FLG_BAND_24G_MSK |
			      RXON_FLG_AUTO_DETECT_MSK | RXON_FLG_CCK_MSK);
			priv->staging_config.flags |= RXON_FLG_SHORT_SLOT_MSK;
			priv->band = IEEE80211_52GHZ_BAND;
		} else {
			priv->staging_config.flags &= ~RXON_FLG_SHORT_SLOT_MSK;
			priv->staging_config.flags |= RXON_FLG_BAND_24G_MSK;
			priv->staging_config.flags |= RXON_FLG_AUTO_DETECT_MSK;
			priv->staging_config.flags &= ~RXON_FLG_CCK_MSK;
			priv->band = IEEE80211_24GHZ_BAND;
		}

		queue_delayed_work(priv->workqueue, &priv->commit, 0);
	}
	mutex_unlock(&priv->mutex);
	return 0;
}

static int ipw_wx_set_mode(struct net_device *dev,
			   struct iw_request_info *info,
			   union iwreq_data *wrqu, char *extra)
{
	return 0;
}

static int ipw_wx_get_mode(struct net_device *dev,
			   struct iw_request_info *info,
			   union iwreq_data *wrqu, char *extra)
{
	wrqu->mode = IW_MODE_MONITOR;
	return 0;
}

static int ipw_wx_get_rate(struct net_device *dev,
			   struct iw_request_info *info,
			   union iwreq_data *wrqu, char *extra)
{
	struct ipw_priv *priv = netdev_priv(dev);
	if (!ipw_is_ready(priv))
		return -EAGAIN;

	mutex_lock(&priv->mutex);
	wrqu->bitrate.value = priv->rate_mbps*1000000/2;
	mutex_unlock(&priv->mutex);
	return 0;
}

static int ipw_wx_set_rate(struct net_device *dev,
			   struct iw_request_info *info,
			   union iwreq_data *wrqu, char *extra)
{
	struct ipw_priv *priv = netdev_priv(dev);
	int i;
	int mbps;

	if (!ipw_is_ready(priv))
		return -EAGAIN;

	mutex_lock(&priv->mutex);
	if (wrqu->bitrate.value >= 1000)
		mbps = wrqu->bitrate.value*2/1000000;
	else
		mbps = wrqu->bitrate.value;

	for (i = 0; i < ARRAY_SIZE(rates); i++) {
		if (rates[i].mbps == mbps) {
			IPW_DEBUG_INFO("Setting rate to %d [%d%smbps].\n",
				       mbps, mbps >> 1, (mbps & 1) ? ".5" : "");
			priv->rate_mbps = mbps;
			priv->rate_plcp = rates[i].plcp;
			mutex_unlock(&priv->mutex);
			return 0;
		}
	}

	IPW_ERROR("Invalid rate request %d [%d%smbps].\n",
		  mbps, mbps >> 1, (mbps & 1) ? ".5" : "");

	mutex_unlock(&priv->mutex);
	return -EINVAL;
}

static int ipw_wx_get_txpow(struct net_device *dev,
			    struct iw_request_info *info,
			    union iwreq_data *wrqu, char *extra)
{
	struct ipw_priv *priv = netdev_priv(dev);

	if (!ipw_is_ready(priv))
		return -EAGAIN;

	mutex_lock(&priv->mutex);
	wrqu->txpower.value = (int)priv->user_txpower_limit;
	wrqu->txpower.fixed = 1;
	wrqu->txpower.flags = IW_TXPOW_DBM;
	wrqu->txpower.disabled = 0;
	mutex_unlock(&priv->mutex);
	return 0;
}

static int ipw_wx_set_txpow(struct net_device *dev,
			    struct iw_request_info *info,
			    union iwreq_data *wrqu, char *extra)
{
	struct ipw_priv *priv = netdev_priv(dev);

	int txpower, i, max_power;
	struct ipw_channel_info *ch_info;

	if (!ipw_is_ready(priv))
		return -EAGAIN;

	if (priv->status & STATUS_EXIT_PENDING)
		return -EAGAIN;

	if (priv->status & STATUS_IN_SUSPEND)
		return -ERESTARTSYS;

	mutex_lock(&priv->mutex);
	priv->max_power_override = 1;
	txpower = wrqu->txpower.value;

	if (txpower < -12 || txpower > 16) {
		IPW_ERROR("Invalid txpower (%d).\n", txpower);
		mutex_unlock(&priv->mutex);
		return -EINVAL;
	}

	if (priv->user_txpower_limit == txpower) {
		IPW_DEBUG_INFO("Already locked maximum txpower to %d\n",
			       txpower);
	} else {
		IPW_DEBUG_INFO("Setting maximum txpower to %d\n", txpower);

		priv->user_txpower_limit = txpower;

		for (i = 0; i < priv->channel_count; i++) {
			ch_info = &priv->channel_info[i];
			/* find minimum power of all user and regulatory constraints
			 *    (does not consider h/w clipping limitations) */
			max_power = reg_get_channel_txpower_limit(ch_info);
			if (!priv->max_power_override)
				max_power = min(txpower, max_power);
			else
				max_power = txpower;
			if (max_power != ch_info->curr_txpow) {
				ch_info->curr_txpow = max_power;

				/* this considers the h/w clipping limitations */
				reg_set_new_power(priv, ch_info);
			}
		}

		/* Force a thermal recalibration */
		priv->last_temperature = 0;

		cancel_delayed_work(&priv->thermal_periodic);
		queue_delayed_work(priv->workqueue, &priv->thermal_periodic, 0);
	}

	mutex_unlock(&priv->mutex);
	return 0;

}

#define IW_IOCTL(x) [(x)-SIOCSIWCOMMIT]
#define MAX_WX_STRING 80

static int ipw_priv_drvname(struct net_device *dev,
				struct iw_request_info *info,
				union iwreq_data *wrqu, char *extra)
{
	return 0;
}

static iw_handler ipw_priv_handler[] = {

	ipw_priv_drvname,
};


static iw_handler ipw_wx_handlers[] = {
	IW_IOCTL(SIOCGIWNAME) = ipw_wx_get_name,
	IW_IOCTL(SIOCSIWFREQ) = ipw_wx_set_freq,
	IW_IOCTL(SIOCGIWFREQ) = ipw_wx_get_freq,
	IW_IOCTL(SIOCSIWMODE) = ipw_wx_set_mode,
	IW_IOCTL(SIOCGIWMODE) = ipw_wx_get_mode,
	IW_IOCTL(SIOCGIWESSID) = ipw_wx_get_essid,
	IW_IOCTL(SIOCGIWRATE) = ipw_wx_get_rate,
	IW_IOCTL(SIOCSIWRATE) = ipw_wx_set_rate,
/*
	IW_IOCTL(SIOCSIWRTS) = ipw_wx_set_rts,
	IW_IOCTL(SIOCGIWRTS) = ipw_wx_get_rts,
	IW_IOCTL(SIOCSIWFRAG) = ipw_wx_set_frag,
	IW_IOCTL(SIOCGIWFRAG) = ipw_wx_get_frag,
*/
	IW_IOCTL(SIOCSIWTXPOW) = ipw_wx_set_txpow,
	IW_IOCTL(SIOCGIWTXPOW) = ipw_wx_get_txpow,
/*
	IW_IOCTL(SIOCSIWRETRY) = ipw_wx_set_retry,
	IW_IOCTL(SIOCGIWRETRY) = ipw_wx_get_retry,
	IW_IOCTL(SIOCSIWPOWER) = ipw_wx_set_power,
	IW_IOCTL(SIOCGIWPOWER) = ipw_wx_get_power,
	IW_IOCTL(SIOCSIWSPY) = iw_handler_set_spy,
	IW_IOCTL(SIOCGIWSPY) = iw_handler_get_spy,
	IW_IOCTL(SIOCSIWTHRSPY) = iw_handler_set_thrspy,
	IW_IOCTL(SIOCGIWTHRSPY) = iw_handler_get_thrspy,
*/
};

enum {
	IPW_PRIV_DNAME = SIOCIWFIRSTPRIV,
};

static struct iw_priv_args ipw_priv_args[] = {
	{
	 .cmd = IPW_PRIV_DNAME,
	 .set_args = 0,
	 .get_args = IW_PRIV_TYPE_CHAR | IW_PRIV_SIZE_FIXED | IFNAMSIZ,
	 .name = "ipwraw-ng"},
};

static struct iw_handler_def ipw_wx_handler_def = {
	.standard = ipw_wx_handlers,
	.num_standard = ARRAY_SIZE(ipw_wx_handlers),
	.num_private = ARRAY_SIZE(ipw_priv_handler),
	.num_private_args = ARRAY_SIZE(ipw_priv_args),
	.private = ipw_priv_handler,
	.private_args = ipw_priv_args,
};

static void ipw_prom_free(struct ipw_priv *priv)
{
	if (!priv->prom_net_dev)
		return;

	unregister_netdev(priv->prom_net_dev);
	free_netdev(priv->prom_net_dev);

	priv->prom_net_dev = NULL;
}

static int ipw_pci_probe(struct pci_dev *pdev, const struct pci_device_id *ent)
{
	int err = 0;
	struct net_device *net_dev;
	void __iomem *base;
	u32 length;
	u32 pci_id;
	struct ipw_priv *priv;

	net_dev = alloc_etherdev(sizeof(struct ipw_priv));
	if (net_dev == NULL) {
		err = -ENOMEM;
		goto out;
	}

	IPW_DEBUG_INFO("*** LOAD DRIVER ***\n");
	priv = netdev_priv(net_dev);
	priv->net_dev = net_dev;
	strcpy(priv->net_dev->name, "wifi%d");

	priv->pci_dev = pdev;
	priv->rxq = NULL;
	priv->antenna = param_antenna;
#ifdef CONFIG_IPWRAW_DEBUG
	ipw_debug_level = param_debug;
#endif
	priv->retry_rate = 16;

	spin_lock_init(&priv->lock);
	spin_lock_init(&priv->power_data.lock);

	mutex_init(&priv->mutex);

	if (pci_enable_device(pdev)) {
		err = -ENODEV;
		goto out_free_ieee80211;
	}

	pci_set_master(pdev);

	ipw_clear_stations_table(priv);

	priv->data_retry_limit = -1;

	err = pci_set_dma_mask(pdev, DMA_32BIT_MASK);
	if (!err)
		err = pci_set_consistent_dma_mask(pdev, DMA_32BIT_MASK);
	if (err) {
		printk(KERN_WARNING DRV_NAME ": No suitable DMA available.\n");
		goto out_pci_disable_device;
	}

	pci_set_drvdata(pdev, priv);
	err = pci_request_regions(pdev, DRV_NAME);
	if (err)
		goto out_pci_disable_device;
	/* We disable the RETRY_TIMEOUT register (0x41) to keep
	 * PCI Tx retries from interfering with C3 CPU state */
	pci_write_config_byte(pdev, 0x41, 0x00);

	length = pci_resource_len(pdev, 0);
	base = ioremap_nocache(pci_resource_start(pdev, 0), length);
	if (!base) {
		err = -ENODEV;
		goto out_pci_release_regions;
	}

	priv->hw_base = base;
	IPW_DEBUG_INFO("pci_resource_len = 0x%08x\n", length);
	IPW_DEBUG_INFO("pci_resource_base = %p\n", base);
	err = ipw_setup_deferred_work(priv);
	if (err) {
		IPW_ERROR("Unable to setup deferred work\n");
		goto out_iounmap;
	}

	/* Initialize module parameter values here */

	if (!param_led)
		priv->config |= CFG_NO_LED;
	if (param_disable) {
		priv->status |= STATUS_RF_KILL_SW;
		IPW_DEBUG_INFO("Radio disabled.\n");
	}

	pci_id =
	    (priv->pci_dev->device << 16) | priv->pci_dev->subsystem_device;

	switch (pci_id) {
	case 0x42221005:	/* 0x4222 0x8086 0x1005 is BG SKU */
	case 0x42221034:	/* 0x4222 0x8086 0x1034 is BG SKU */
	case 0x42271014:	/* 0x4227 0x8086 0x1014 is BG SKU */
	case 0x42221044:	/* 0x4222 0x8086 0x1044 is BG SKU */
		priv->is_abg = 0;
		break;

	default:		/* Rest are assumed ABG SKU -- if this is not the
				 * case then the card will get the wrong 'Detected'
				 * line in the kernel log however the code that
				 * initializes the GEO table will detect no A-band
				 * channels and remove the is_abg mask. */
		priv->is_abg = 1;
		break;
	}

	printk(KERN_INFO DRV_NAME
	       ": Detected Intel PRO/Wireless 3945%s Network Connection\n",
	       priv->is_abg ? "ABG" : "BG");

	if (param_channel != 0) {
		priv->channel = param_channel;
		IPW_DEBUG_INFO("Bind to static channel %d\n", param_channel);
		/* TODO: Validate that provided channel is in range */
		if (priv->channel > 14)
			priv->band = IEEE80211_52GHZ_BAND;
		else
			priv->band = IEEE80211_24GHZ_BAND;

	} else {
		priv->channel = 1;
		priv->band = IEEE80211_24GHZ_BAND;
	}

	priv->rts_threshold = DEFAULT_RTS_THRESHOLD;
	/* If power management is turned on, default to AC mode */
	priv->power_mode = IPW_POWER_AC;
	priv->actual_txpower_limit = IPW_DEFAULT_TX_POWER;

	priv->rate_mbps = 108;	/* default to 54mbps for fun */
	priv->rate_plcp = 3;

	err = request_irq(pdev->irq, ipw_isr, IRQF_SHARED, DRV_NAME, priv);

	if (err) {
		IPW_ERROR("Error allocating IRQ %d\n", pdev->irq);
		goto out_destroy_workqueue;
	}

	SET_MODULE_OWNER(net_dev);
	SET_NETDEV_DEV(net_dev, &pdev->dev);
	mutex_lock(&priv->mutex);
	net_dev->hard_start_xmit = ipw_net_hard_start_xmit;
	net_dev->open = ipw_net_open;
	net_dev->stop = ipw_net_stop;
	net_dev->get_stats = ipw_net_get_stats;
	net_dev->set_mac_address = ipw_net_set_mac_address;
	net_dev->type = ARPHRD_IEEE80211;
	net_dev->mtu = 2346;
	net_dev->wireless_handlers = &ipw_wx_handler_def;
	net_dev->ethtool_ops = &ipw_ethtool_ops;
	net_dev->irq = pdev->irq;
	net_dev->base_addr = (unsigned long)priv->hw_base;
	net_dev->mem_start = pci_resource_start(pdev, 0);
	net_dev->mem_end = net_dev->mem_start + pci_resource_len(pdev, 0) - 1;

	err = sysfs_create_group(&pdev->dev.kobj, &ipw_attribute_group);
	if (err) {
		IPW_ERROR("failed to create sysfs device attributes\n");
		mutex_unlock(&priv->mutex);
		goto out_release_irq;
	}

	/* fetch uCode file from disk, copy to bus-master buffers */
	err = ipw_read_ucode(priv);
	if (err) {
		IPW_ERROR("Could not read microcode from disk: %d\n", err);
		goto out_remove_sysfs;
	}
	priv->shared_virt =
	    pci_alloc_consistent(priv->pci_dev,
				 sizeof(struct ipw_shared_t),
				 &priv->shared_phys);
	if (!priv->shared_virt) {
		IPW_ERROR("failed to allocate pci memory\n");
		mutex_unlock(&priv->mutex);
		goto out_pci_alloc;
	}

	mutex_unlock(&priv->mutex);

	IPW_DEBUG_INFO("Queue up transitioning to the UP state.\n");
	queue_delayed_work(priv->workqueue, &priv->up, 0);

	return 0;

      out_pci_alloc:
	ipw_dealloc_ucode_pci(priv);

	if (priv->shared_virt != NULL)
		pci_free_consistent(priv->pci_dev,
				    sizeof(struct ipw_shared_t),
				    priv->shared_virt, priv->shared_phys);
	priv->shared_virt = NULL;

      out_remove_sysfs:
	sysfs_remove_group(&pdev->dev.kobj, &ipw_attribute_group);
	if (priv->workqueue)
		cancel_delayed_work(&priv->rf_kill);

      out_release_irq:
	free_irq(pdev->irq, priv);

      out_destroy_workqueue:
	destroy_workqueue(priv->workqueue);
	priv->workqueue = NULL;
      out_iounmap:
	iounmap(priv->hw_base);
      out_pci_release_regions:
	pci_release_regions(pdev);
      out_pci_disable_device:
	pci_disable_device(pdev);
	pci_set_drvdata(pdev, NULL);
      out_free_ieee80211:
	free_netdev(priv->net_dev);
      out:
	return err;
}

static void ipw_pci_remove(struct pci_dev *pdev)
{
	struct ipw_priv *priv = pci_get_drvdata(pdev);

	if (!priv)
		return;

	IPW_DEBUG_INFO("*** UNLOAD DRIVER ***\n");

	mutex_lock(&priv->mutex);

	priv->status |= STATUS_EXIT_PENDING;

	ipw_down(priv);

	mutex_unlock(&priv->mutex);

	destroy_workqueue(priv->workqueue);

	priv->workqueue = NULL;

	sysfs_remove_group(&pdev->dev.kobj, &ipw_attribute_group);

	ipw_dealloc_ucode_pci(priv);

	if (priv->shared_virt != NULL)
		pci_free_consistent(priv->pci_dev,
				    sizeof(*priv->shared_virt),
				    priv->shared_virt, priv->shared_phys);

	priv->shared_virt = NULL;
	if (priv->rxq)
		ipw_rx_queue_free(priv, priv->rxq);
	ipw_tx_queue_free(priv);
	ipw_card_remove_notify(priv);

	ipw_delete_stations_table(priv);

	if (priv->netdev_registered) {
		unregister_netdev(priv->net_dev);
	}
	ipw_prom_free(priv);

	free_irq(pdev->irq, priv);
	iounmap(priv->hw_base);
	pci_release_regions(pdev);
	pci_disable_device(pdev);
	pci_set_drvdata(pdev, NULL);
	free_netdev(priv->net_dev);
}

#ifdef CONFIG_PM
static int ipw_pci_suspend(struct pci_dev *pdev, pm_message_t state)
{
	struct ipw_priv *priv = pci_get_drvdata(pdev);
	struct net_device *dev = priv->net_dev;
	printk(KERN_INFO "%s: Going into suspend...\n", dev->name);

	mutex_lock(&priv->mutex);

	priv->status |= STATUS_IN_SUSPEND;

	/* Take down the device; powers it off, etc. */
	ipw_down(priv);

	/* Remove the PRESENT state of the device */
	if (priv->netdev_registered)
		netif_device_detach(dev);
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,10)
	pci_save_state(pdev, priv->pm_state);
#else
	pci_save_state(pdev);
#endif
	pci_disable_device(pdev);
	pci_set_power_state(pdev, PCI_D3hot);

	mutex_unlock(&priv->mutex);

	return 0;
}

static int ipw_pci_resume(struct pci_dev *pdev)
{
	struct ipw_priv *priv = pci_get_drvdata(pdev);
	struct net_device *dev = priv->net_dev;

	printk(KERN_INFO "%s: Coming out of suspend...\n", dev->name);

	mutex_lock(&priv->mutex);

	pci_set_power_state(pdev, PCI_D0);

	if (pci_enable_device(pdev)) {
		mutex_unlock(&priv->mutex);
		return -ENODEV;
	}
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,10)
	pci_restore_state(pdev, priv->pm_state);
#else
	pci_restore_state(pdev);
#endif
	/*
	 * Suspend/Resume resets the PCI configuration space, so we have to
	 * re-disable the RETRY_TIMEOUT register (0x41) to keep PCI Tx retries
	 * from interfering with C3 CPU state. pci_restore_state won't help
	 * here since it only restores the first 64 bytes pci config header.
	 */
	pci_write_config_byte(pdev, 0x41, 0x00);

	/* Set the device back into the PRESENT state; this will also wake
	 * the queue of needed */
	if (priv->netdev_registered)
		netif_device_attach(dev);

	queue_delayed_work(priv->workqueue, &priv->resume_work, 3 * HZ);

	mutex_unlock(&priv->mutex);

	return 0;
}
#endif

/* driver initialization stuff */
static struct pci_driver ipw_driver = {
	.name = DRV_NAME,.id_table = card_ids,.probe =
	    ipw_pci_probe,.remove = __devexit_p(ipw_pci_remove),
#ifdef CONFIG_PM
	.suspend = ipw_pci_suspend,.resume = ipw_pci_resume,
#endif
};
static int __init ipw_init(void)
{

	int ret;
	printk(KERN_INFO DRV_NAME ": " DRV_DESCRIPTION ", " DRV_VERSION "\n");
	printk(KERN_INFO DRV_NAME ": " DRV_COPYRIGHT "\n");
	ret = pci_register_driver(&ipw_driver);
	if (ret) {
		IPW_ERROR("Unable to initialize PCI module\n");
		return ret;
	}

	ret = driver_create_file(&ipw_driver.driver, &driver_attr_debug_level);
	if (ret) {
		IPW_ERROR("Unable to create driver sysfs file\n");
		pci_unregister_driver(&ipw_driver);
		return ret;
	}

	return ret;
}

static void __exit ipw_exit(void)
{
	driver_remove_file(&ipw_driver.driver, &driver_attr_debug_level);
	pci_unregister_driver(&ipw_driver);
}

module_param_named(antenna, param_antenna, int, 0444);
MODULE_PARM_DESC(antenna, "select antenna (1=Main, 2=Aux, default 0 [both])");
module_param_named(disable, param_disable, int, 0444);
MODULE_PARM_DESC(disable, "manually disable the radio (default 0 [radio on])");
module_param_named(led, param_led, int, 0444);
MODULE_PARM_DESC(led, "enable led control (default 1 [on])\n");
#ifdef CONFIG_IPWRAW_DEBUG
module_param_named(debug, param_debug, int, 0444);
MODULE_PARM_DESC(debug, "debug output mask");
#endif
module_param_named(channel, param_channel, int, 0444);
MODULE_PARM_DESC(channel, "channel to limit associate to (default 0 [ANY])");

module_param_named(rtap_iface, param_rtap_iface, int, 0444);
MODULE_PARM_DESC(rtap_iface,
		 "create the rtap interface (default 1 [create])");

module_exit(ipw_exit);
module_init(ipw_init);
