/**
 * system-layout.h
 * zen-system-layout
 *
 * Copyright (C) 2008 Ryan Hope <rmh3093@gmail.com>
 *
 * Released under the GPL version 2 only.
 *
 */

#define ZEN_SERVER 0
#define ZEN_FILE_SERVER 1
#define ZEN_GAMING 2
#define ZEN_DESKTOP 3
#define ZEN_LL_DESKTOP 4
#define ZEN_CUSTOM 5
#define ZEN_LAST 6

struct layout
{
	char *name;
	int sched_latency;
	int sched_min_granularity;
	int sched_wakeup_granularity;
	int vm_swappiness;
	int vm_dirty_ratio;
};
