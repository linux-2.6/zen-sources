#ifndef _COMPAT_H_
#define _COMPAT_H_

/*
 * Even if AppArmor isn't enabled, it still has different prototypes.
 * Add more distro/version pairs here to declare which has AppArmor applied.
 */
#if defined(CONFIG_SUSE_KERNEL)
#define REMOVE_SUID_PATH 1
#endif

#define btrfs_drop_nlink(inode) drop_nlink(inode)
#define btrfs_inc_nlink(inode)	inc_nlink(inode)

/*
 * catch any other distros that have patched in apparmor.  This isn't
 * 100% reliable because it won't catch people that hand compile their
 * own distro kernels without apparmor compiled in.  But, it is better
 * than nothing.
 */
#ifdef CONFIG_SECURITY_APPARMOR
# define REMOVE_SUID_PATH 1
#endif

#endif /* _COMPAT_H_ */
