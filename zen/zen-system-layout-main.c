/**
 * zen-system-layout-main.c
 * zen-system-layout
 *
 * Copyright (C) 2008 Ryan Hope <rmh3093@gmail.com>
 *
 * Released under the GPL version 2 only.
 *
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/mm.h>

#include "zen-system-layout-main.h"

extern unsigned int sysctl_sched_latency;
extern unsigned int sysctl_sched_min_granularity;
extern unsigned int sysctl_sched_wakeup_granularity;
extern unsigned int sched_nr_latency;
extern int vm_dirty_ratio;
extern int vm_swappiness;

#ifdef CONFIG_ZEN_SERVER
int system_layout = ZEN_SERVER;
#endif
#ifdef CONFIG_ZEN_FILE_SERVER
int system_layout = ZEN_FILE_SERVER;
#endif
#ifdef CONFIG_ZEN_GAMING
int system_layout = ZEN_GAMING;
#endif
#ifdef CONFIG_ZEN_DESKTOP
int system_layout = ZEN_DESKTOP;
#endif
#ifdef CONFIG_ZEN_LL_DESKTOP
int system_layout = ZEN_LL_DESKTOP;
#endif
#ifdef CONFIG_ZEN_CUSTOM
int system_layout = ZEN_CUSTOM;
#endif

struct layout available_layouts[ZEN_LAST] =
{
	{
		/* ZEN_SERVER */
		.name = "server",
		.sched_latency = 20,
		.sched_min_granularity = 4000,
		.sched_wakeup_granularity = 10,
		.vm_swappiness = 33,
		.vm_dirty_ratio = 66,
	},
	{
		/* ZEN_FILE_SERVER */
		.name = "file-server",
		.sched_latency = 20,
		.sched_min_granularity = 4000,
		.sched_wakeup_granularity = 10,
		.vm_swappiness = 0,
		.vm_dirty_ratio = 66,
	},
	{
		/* ZEN_GAMING */
		.name = "gaming",
		.sched_latency = 20,
		.sched_min_granularity = 4000,
		.sched_wakeup_granularity = 10,
		.vm_swappiness = 33,
		.vm_dirty_ratio = 66,
	},
	{
		/* ZEN_DESKTOP */
		.name = "desktop",
		.sched_latency = 15,
		.sched_min_granularity = 3000,
		.sched_wakeup_granularity = 5,
		.vm_swappiness = 66,
		.vm_dirty_ratio = 1,
	},
	{
		/* ZEN_LL_DESKTOP */
		.name = "ll-desktop",
		.sched_latency = 10,
		.sched_min_granularity = 2000,
		.sched_wakeup_granularity = 1,
		.vm_swappiness = 66,
		.vm_dirty_ratio = 1,
	},
	{
		/* ZEN_CUSTOM */
		.name = "custom",
#ifdef CONFIG_ZEN_CUSTOM
		.sched_latency = CONFIG_CUSTOM_SCHED_LATENCY,
		.sched_min_granularity = CONFIG_CUSTOM_SCHED_MIN_GRANULARITY,
		.sched_wakeup_granularity = CONFIG_CUSTOM_SCHED_WAKEUP_GRANULARITY,
		.vm_swappiness = CONFIG_CUSTOM_VM_SWAPPINESS,
		.vm_dirty_ratio = CONFIG_CUSTOM_VM_DIRTY_RATIO,
#else
		.sched_latency = sysctl_sched_latency_default,
		.sched_min_granularity = sysctl_sched_min_granularity_default,
		.sched_wakeup_granularity = sysctl_sched_wakeup_granularity_default,
		.vm_swappiness = vm_dirty_ratio_default,
		.vm_dirty_ratio = vm_swappiness_default,
#endif
	},
};

int zen_set_system_layout(int layout)
{
        sysctl_sched_latency =
		available_layouts[layout].sched_latency * NSEC_PER_MSEC;
        sysctl_sched_min_granularity =
		available_layouts[layout].sched_min_granularity * NSEC_PER_USEC;
	sched_nr_latency = (sysctl_sched_latency * USEC_PER_MSEC) /
		available_layouts[layout].sched_wakeup_granularity;
	sysctl_sched_wakeup_granularity =
		available_layouts[layout].sched_wakeup_granularity * NSEC_PER_MSEC;
        vm_swappiness = available_layouts[layout].vm_swappiness;
        vm_dirty_ratio = available_layouts[layout].vm_dirty_ratio;

        printk(KERN_INFO "Zen System Layout [%s]\n", available_layouts[layout].name);

	return 0;
}

static int zen_system_layout_init(void)
{
	return zen_set_system_layout(system_layout);
}

static void zen_system_layout_exit(void)
{
}

module_init(zen_system_layout_init);
module_exit(zen_system_layout_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ryan Hope <rmh3093@gmail.com>");

EXPORT_SYMBOL(system_layout);
EXPORT_SYMBOL(available_layouts);
EXPORT_SYMBOL(zen_set_system_layout);
