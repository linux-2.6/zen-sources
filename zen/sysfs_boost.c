/**
 * sysfs_boost.c
 * zen-tune-boost
 *
 * Copyright (C) 2008 Ryan Hope <rmh3093@gmail.com>
 *
 * Released under the GPL version 2 only.
 *
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/sched.h>

#include "zen-system-layout-main.h"

extern struct kobject *zen_kobj;
int sched_privileged_nice_level;

static ssize_t boost_renice_setting_show(struct kobject *kobj,
	struct kobj_attribute *attr, char *buf)
{
        return sprintf(buf, "%d\n", sysctl_sched_privileged_nice_level);
}

static ssize_t boost_renice_setting_store(struct kobject *kobj,
	struct kobj_attribute *attr, const char *buf, size_t count)
{
        sscanf(buf, "%du", &sysctl_sched_privileged_nice_level);
        return count;
}

static struct kobj_attribute boost_renice_setting =
        __ATTR(sched_privileged_nice_level, 0666,
		boost_renice_setting_show, boost_renice_setting_store);

static struct attribute *boost_attrs[] = {
	&boost_renice_setting.attr,
        NULL,
};

static struct attribute_group boost_group = {
        .attrs = boost_attrs,
};

int init_sysfs_interface_boost(void)
{
        int ret = 0;

	ret = sysfs_create_group(zen_kobj, &boost_group);
	if (!ret)
	        printk(KERN_INFO "Zen-Tune Sysfs [Zen System Boost Support].\n");

        return ret;
}

void cleanup_sysfs_interface_boost(void)
{
}

module_init(init_sysfs_interface_boost);
module_exit(cleanup_sysfs_interface_boost);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ryan Hope <rmh3093@gmail.com>");

