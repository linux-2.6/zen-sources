/**
 * sysfs.c
 * zen-tune
 *
 * Copyright (C) 2008 Ryan Hope <rmh3093@gmail.com>
 *
 * Released under the GPL version 2 only.
 *
 */

#include <linux/module.h>
#include <linux/kobject.h>
#include <linux/sysfs.h>

#include "sysfs.h"

struct kobject *zen_kobj;
EXPORT_SYMBOL(zen_kobj);

int init_sysfs_interface(void)
{
        int ret = 0;

	zen_kobj = kobject_create_and_add("zen", kernel_kobj);
	if (!zen_kobj)
		ret = -ENOMEM;
	else
		printk(KERN_INFO "Zen-Tune Sysfs Interface Loaded.\n");

        return ret;
}

void cleanup_sysfs_interface(void)
{
	kobject_put(zen_kobj);
}
