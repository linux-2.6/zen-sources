/*
 * BFQ, or Budget Fair Queueing, disk scheduler.
 *
 * Based on ideas and code from CFQ:
 * Copyright (C) 2003 Jens Axboe <axboe@kernel.dk>
 *
 * Copyright (C) 2008 Fabio Checconi <fabio@gandalf.sssup.it>
 *		      Paolo Valente <paolo.valente@unimore.it>
 */
#include <linux/module.h>
#include <linux/blkdev.h>
#include <linux/elevator.h>
#include <linux/rbtree.h>
#include <linux/ioprio.h>

/*
 * tunables
 */
/* max queue in one round of service */
static const int bfq_quantum = 4;
static const int bfq_fifo_expire[2] = { HZ / 4, HZ / 8 };
/* maximum backwards seek, in KiB */
static const int bfq_back_max = 16 * 1024;
/* penalty of a backwards seek */
static const int bfq_back_penalty = 2;
static const int bfq_slice_async_rq = 2;
static int bfq_slice_idle = HZ / 125;
static const int bfq_max_budget = 4096;

/*
 * below this threshold, we consider thinktime immediate
 */
#define BFQ_MIN_TT		(2)
#define BFQ_HW_QUEUE_MIN	(5)

#define RQ_CIC(rq)		\
	((struct cfq_io_context *) (rq)->elevator_private)
#define RQ_BFQQ(rq)		((rq)->elevator_private2)

static struct kmem_cache *bfq_pool;
static struct kmem_cache *bfq_ioc_pool;

static DEFINE_PER_CPU(unsigned long, ioc_count);
static struct completion *ioc_gone;

#define BFQ_PRIO_LISTS		IOPRIO_BE_NR
#define bfq_class_idle(bfqq)	((bfqq)->ioprio_class == IOPRIO_CLASS_IDLE)

#define ASYNC			(0)
#define SYNC			(1)

#define sample_valid(samples)	((samples) > 80)

#define BFQ_IOPRIO_CLASSES	3

#define BFQ_BUDGET_STEP		128

typedef u64 bfq_timestamp_t;
typedef unsigned long bfq_weight_t;
typedef unsigned long bfq_service_t;

struct bfq_wfqdata {
	struct rb_root active;
	struct rb_root idle;

	struct bfq_queue *first_idle;
	struct bfq_queue *last_idle;

	bfq_timestamp_t vtime;
	bfq_weight_t wsum;
};

#define BFQ_WFQDATA_INIT	((struct bfq_wfqdata)			\
				{ RB_ROOT, RB_ROOT, NULL, NULL, 0, 0 })

/*
 * Per block device queue structure
 */
struct bfq_data {
	struct request_queue *queue;

	struct bfq_wfqdata service_tree[BFQ_IOPRIO_CLASSES];
	unsigned int busy_queues;

	int queued;
	int rq_in_driver;
	int sync_flight;

	/*
	 * queue-depth detection
	 */
	int rq_queued;
	int hw_tag;
	int hw_tag_samples;
	int rq_in_driver_peak;

	/*
	 * idle window management
	 */
	struct timer_list idle_slice_timer;
	struct work_struct unplug_work;

	struct bfq_queue *active_queue;
	struct cfq_io_context *active_cic;

	/*
	 * async queue for each priority case
	 */
	struct bfq_queue *async_bfqq[2][IOPRIO_BE_NR];
	struct bfq_queue *async_idle_bfqq;

	sector_t last_position;

	/*
	 * tunables, see top of file
	 */
	unsigned int bfq_quantum;
	unsigned int bfq_fifo_expire[2];
	unsigned int bfq_back_penalty;
	unsigned int bfq_back_max;
	unsigned int bfq_slice_async_rq;
	unsigned int bfq_slice_idle;
	unsigned int bfq_max_budget;

	struct list_head cic_list;
};

/*
 * Per process-grouping structure
 */
struct bfq_queue {
	/* reference count */
	atomic_t ref;
	/* parent bfq_data */
	struct bfq_data *bfqd;
	/* service_tree member */
	struct rb_node rb_node;

	/* sorted list of pending requests */
	struct rb_root sort_list;
	/* if fifo isn't expired, next request to serve */
	struct request *next_rq;
	/* requests queued in sort_list */
	int queued[2];
	/* currently allocated requests */
	int allocated[2];
	/* pending metadata requests */
	int meta_pending;
	/* fifo list of requests in sort_list */
	struct list_head fifo;

	/* wfq timestamps */
	bfq_timestamp_t finish;
	bfq_timestamp_t start;

	/* wfq tree the queue belongs to */
	struct rb_root *tree;

	/* minimum start time of the subtree rooted at this queue */
	bfq_timestamp_t min_start;

	/* service received and budget for the current run */
	bfq_service_t service, budget, act_budget;
	/* effective weight of the queue */
	bfq_weight_t weight;

	/* number of requests that are on the dispatch list or inside driver */
	int dispatched;

	/* io prio of this group */
	unsigned short ioprio, org_ioprio, act_ioprio;
	unsigned short ioprio_class, org_ioprio_class, act_ioprio_class;

	/* various state flags, see below */
	unsigned int flags;
};

static inline unsigned int bfq_bfqq_tree_index(struct bfq_queue *bfqq)
{
	unsigned int idx = bfqq->act_ioprio_class - 1;

	BUG_ON(idx >= BFQ_IOPRIO_CLASSES);

	return idx;
}

static inline struct bfq_wfqdata *bfq_bfqq_wfqdata(struct bfq_data *bfqd,
						   struct bfq_queue *bfqq)
{
	return &bfqd->service_tree[bfq_bfqq_tree_index(bfqq)];
}

enum bfqq_state_flags {
	BFQ_BFQQ_FLAG_busy = 0,		/* has requests or is under service */
	BFQ_BFQQ_FLAG_wait_request,	/* waiting for a request */
	BFQ_BFQQ_FLAG_must_alloc,	/* must be allowed rq alloc */
	BFQ_BFQQ_FLAG_fifo_expire,	/* FIFO checked in this slice */
	BFQ_BFQQ_FLAG_idle_window,	/* slice idling enabled */
	BFQ_BFQQ_FLAG_prio_changed,	/* task priority has changed */
	BFQ_BFQQ_FLAG_sync,		/* synchronous queue */
};

#define BFQ_BFQQ_FNS(name)						\
static inline void bfq_mark_bfqq_##name(struct bfq_queue *bfqq)		\
{									\
	(bfqq)->flags |= (1 << BFQ_BFQQ_FLAG_##name);			\
}									\
static inline void bfq_clear_bfqq_##name(struct bfq_queue *bfqq)	\
{									\
	(bfqq)->flags &= ~(1 << BFQ_BFQQ_FLAG_##name);			\
}									\
static inline int bfq_bfqq_##name(const struct bfq_queue *bfqq)		\
{									\
	return ((bfqq)->flags & (1 << BFQ_BFQQ_FLAG_##name)) != 0;	\
}

BFQ_BFQQ_FNS(busy);
BFQ_BFQQ_FNS(wait_request);
BFQ_BFQQ_FNS(must_alloc);
BFQ_BFQQ_FNS(fifo_expire);
BFQ_BFQQ_FNS(idle_window);
BFQ_BFQQ_FNS(prio_changed);
BFQ_BFQQ_FNS(sync);
#undef BFQ_BFQQ_FNS

static void bfq_dispatch_insert(struct request_queue *, struct request *);
static struct bfq_queue *bfq_get_queue(struct bfq_data *, int,
				       struct io_context *, gfp_t);
static void bfq_put_queue(struct bfq_queue *bfqq);
static void bfq_forget_idle(struct bfq_wfqdata *wfqd);

static struct cfq_io_context *bfq_cic_lookup(struct bfq_data *,
					     struct io_context *);

static inline struct bfq_queue *cic_to_bfqq(struct cfq_io_context *cic,
					    int is_sync)
{
	return cic->cfqq[!!is_sync];
}

static inline void cic_set_bfqq(struct cfq_io_context *cic,
				struct bfq_queue *bfqq, int is_sync)
{
	cic->cfqq[!!is_sync] = bfqq;
}

/*
 * We regard a request as SYNC, if it's either a read or has the SYNC bit
 * set (in which case it could also be direct WRITE).
 */
static inline int bfq_bio_sync(struct bio *bio)
{
	if (bio_data_dir(bio) == READ || bio_sync(bio))
		return 1;

	return 0;
}

/*
 * Scheduler run of queue, if there are requests pending and no one in the
 * driver that will restart queueing.
 */
static inline void bfq_schedule_dispatch(struct bfq_data *bfqd)
{
	if (bfqd->queued != 0)
		kblockd_schedule_work(bfqd->queue, &bfqd->unplug_work);
}

static int bfq_queue_empty(struct request_queue *q)
{
	struct bfq_data *bfqd = q->elevator->elevator_data;

	return bfqd->queued == 0;
}

/*
 * Lifted from AS - choose which of rq1 and rq2 that is best served now.
 * We choose the request that is closest to the head right now.  Distance
 * behind the head is penalized and only allowed to a certain extent.
 */
static struct request *
bfq_choose_req(struct bfq_data *bfqd, struct request *rq1, struct request *rq2)
{
	sector_t last, s1, s2, d1 = 0, d2 = 0;
	unsigned long back_max;
#define BFQ_RQ1_WRAP	0x01 /* request 1 wraps */
#define BFQ_RQ2_WRAP	0x02 /* request 2 wraps */
	unsigned wrap = 0; /* bit mask: requests behind the disk head? */

	if (rq1 == NULL || rq1 == rq2)
		return rq2;
	if (rq2 == NULL)
		return rq1;

	if (rq_is_sync(rq1) && !rq_is_sync(rq2))
		return rq1;
	else if (rq_is_sync(rq2) && !rq_is_sync(rq1))
		return rq2;
	if (rq_is_meta(rq1) && !rq_is_meta(rq2))
		return rq1;
	else if (rq_is_meta(rq2) && !rq_is_meta(rq1))
		return rq2;

	s1 = rq1->sector;
	s2 = rq2->sector;

	last = bfqd->last_position;

	/*
	 * by definition, 1KiB is 2 sectors
	 */
	back_max = bfqd->bfq_back_max * 2;

	/*
	 * Strict one way elevator _except_ in the case where we allow
	 * short backward seeks which are biased as twice the cost of a
	 * similar forward seek.
	 */
	if (s1 >= last)
		d1 = s1 - last;
	else if (s1 + back_max >= last)
		d1 = (last - s1) * bfqd->bfq_back_penalty;
	else
		wrap |= BFQ_RQ1_WRAP;

	if (s2 >= last)
		d2 = s2 - last;
	else if (s2 + back_max >= last)
		d2 = (last - s2) * bfqd->bfq_back_penalty;
	else
		wrap |= BFQ_RQ2_WRAP;

	/* Found required data */

	/*
	 * By doing switch() on the bit mask "wrap" we avoid having to
	 * check two variables for all permutations: --> faster!
	 */
	switch (wrap) {
	case 0: /* common case for CFQ: rq1 and rq2 not wrapped */
		if (d1 < d2)
			return rq1;
		else if (d2 < d1)
			return rq2;
		else {
			if (s1 >= s2)
				return rq1;
			else
				return rq2;
		}

	case BFQ_RQ2_WRAP:
		return rq1;
	case BFQ_RQ1_WRAP:
		return rq2;
	case (BFQ_RQ1_WRAP|BFQ_RQ2_WRAP): /* both rqs wrapped */
	default:
		/*
		 * Since both rqs are wrapped,
		 * start with the one that's further behind head
		 * (--> only *one* back seek required),
		 * since back seek takes more time than forward.
		 */
		if (s1 <= s2)
			return rq1;
		else
			return rq2;
	}
}

/*
 * would be nice to take fifo expire time into account as well
 */
static struct request *
bfq_find_next_rq(struct bfq_data *bfqd, struct bfq_queue *bfqq,
		  struct request *last)
{
	struct rb_node *rbnext = rb_next(&last->rb_node);
	struct rb_node *rbprev = rb_prev(&last->rb_node);
	struct request *next = NULL, *prev = NULL;

	BUG_ON(RB_EMPTY_NODE(&last->rb_node));

	if (rbprev != NULL)
		prev = rb_entry_rq(rbprev);

	if (rbnext != NULL)
		next = rb_entry_rq(rbnext);
	else {
		rbnext = rb_first(&bfqq->sort_list);
		if (rbnext && rbnext != &last->rb_node)
			next = rb_entry_rq(rbnext);
	}

	return bfq_choose_req(bfqd, next, prev);
}

/*
 * Shift for timestamp calculations.  This actually limits the maximum
 * service allowed in one go (small shift values increase it), and the
 * maximum total weight of a queue (big shift values increase it), and
 * the period of virtual time wraparounds.
 */
#define WFQ_SERVICE_SHIFT	22

/**
 * bfq_gt - compare two timestamps.
 * @a: first ts.
 * @b: second ts.
 *
 * Return @a > @b, dealing with wrapping correctly.
 */
static inline int bfq_gt(bfq_timestamp_t a, bfq_timestamp_t b)
{
	return (s64)(a - b) > 0;
}

/**
 * bfq_delta - map service into the virtual time domain.
 * @service: amount of service.
 * @weight: scale factor.
 */
static inline bfq_timestamp_t bfq_delta(bfq_service_t service,
					bfq_weight_t weight)
{
	bfq_timestamp_t d = (bfq_timestamp_t)service << WFQ_SERVICE_SHIFT;

	do_div(d, weight);
	return d;
}

/**
 * bfq_calc_finish - assign the finish time to a queue.
 * @bfqq: the queue to act upon.
 */
static inline void bfq_calc_finish(struct bfq_queue *bfqq)
{
	BUG_ON(bfqq->budget == 0);

	bfqq->finish = bfqq->start + bfq_delta(bfqq->act_budget, bfqq->weight);
}

/**
 * bfq_bfqq_of - get a bfqq from a node.
 * @node: the node field of the bfqq.
 *
 * Convert a node pointer to the relative queue.  This is used only
 * to simplify the logic of some functions and not as the generic
 * conversion mechanism because, e.g., in the tree walking functions,
 * the check for a %NULL value would be redundant.
 */
static inline struct bfq_queue *bfq_bfqq_of(struct rb_node *node)
{
	struct bfq_queue *bfqq = NULL;

	if (node != NULL)
		bfqq = rb_entry(node, struct bfq_queue, rb_node);

	return bfqq;
}

/**
 * bfq_extract - remove a queue from a tree.
 * @root: the tree root.
 * @bfqq: the queue to remove.
 */
static inline void bfq_extract(struct rb_root *root,
			       struct bfq_queue *bfqq)
{
	BUG_ON(bfqq->tree != root);

	bfqq->tree = NULL;
	rb_erase(&bfqq->rb_node, root);
}

/**
 * bfq_idle_extract - extract a queue from the idle tree.
 * @wfqd: the wfqdata of the device owning @bfqq.
 * @bfqq: the queue being removed.
 */
static void bfq_idle_extract(struct bfq_wfqdata *wfqd,
			     struct bfq_queue *bfqq)
{
	struct rb_node *next;

	BUG_ON(bfqq->tree != &wfqd->idle);

	if (bfqq == wfqd->first_idle) {
		next = rb_next(&bfqq->rb_node);
		wfqd->first_idle = bfq_bfqq_of(next);
	}

	if (bfqq == wfqd->last_idle) {
		next = rb_prev(&bfqq->rb_node);
		wfqd->last_idle = bfq_bfqq_of(next);
	}

	bfq_extract(&wfqd->idle, bfqq);
}

/**
 * bfq_update_finish - resync the finish time with the service received
 * @bfqq: the queue to update.
 *
 * The queue may have received less service than allocated, decrease its
 * finish time.  This is called only for the queue under service.
 */
static inline void bfq_update_finish(struct bfq_queue *bfqq)
{
	BUG_ON(bfqq->finish < bfqq->start +
	       bfq_delta(bfqq->service, bfqq->weight));

	bfqq->finish = bfqq->start + bfq_delta(bfqq->service, bfqq->weight);
}

/**
 * bfq_insert - generic tree insertion.
 * @root: tree root.
 * @bfqq: queue to insert.
 *
 * This is used for the idle and the active tree, since they are both
 * ordered by finish time.
 */
static void bfq_insert(struct rb_root *root, struct bfq_queue *bfqq)
{
	struct bfq_queue *entry;
	struct rb_node **node = &root->rb_node;
	struct rb_node *parent = NULL;

	while (*node != NULL) {
		parent = *node;
		entry = rb_entry(parent, struct bfq_queue, rb_node);

		if (bfq_gt(entry->finish, bfqq->finish))
			node = &parent->rb_left;
		else
			node = &parent->rb_right;
	}

	rb_link_node(&bfqq->rb_node, parent, node);
	rb_insert_color(&bfqq->rb_node, root);

	bfqq->tree = root;
}

/**
 * bfq_update_min - update the min_start field of a queue.
 * @bfqq: the queue to update.
 * @node: one of its children.
 *
 * This function is called when @bfqq may store an invalid value for
 * min_start due to updates to the active tree.  It assumes that the subtree
 * rooted at @node (that may be its left or its right child) has a valid
 * min_start value.
 */
static inline void bfq_update_min(struct bfq_queue *bfqq,
				  struct rb_node *node)
{
	struct bfq_queue *child;

	if (node != NULL) {
		child = rb_entry(node, struct bfq_queue, rb_node);
		if (bfq_gt(bfqq->min_start, child->min_start))
			bfqq->min_start = child->min_start;
	}
}

/**
 * bfq_update_active_node - recalculate min_start.
 * @node: the node to update.
 *
 * @node may have changed position or one of its children can have moved,
 * this function updates its min_start value.  The left and right subtrees
 * are assumed to hold a correct min_start value.
 */
static inline void bfq_update_active_node(struct rb_node *node)
{
	struct bfq_queue *bfqq = rb_entry(node, struct bfq_queue, rb_node);

	bfqq->min_start = bfqq->start;
	bfq_update_min(bfqq, node->rb_right);
	bfq_update_min(bfqq, node->rb_left);
}

/**
 * bfq_update_active_tree - update min_start for the whole active tree.
 * @node: the starting node.
 *
 * @node must be the deepest modified node after an update.  This function
 * updates its min_start using the values held by its children, assuming
 * that they did not change, and then updates all the nodes that may have
 * changed in the path to the root.  The only nodes that may have changed
 * are those in the path or their siblings.
 */
static void bfq_update_active_tree(struct rb_node *node)
{
	struct rb_node *parent;

up:
	bfq_update_active_node(node);

	parent = rb_parent(node);
	if (parent == NULL)
		return;

	if (node == parent->rb_left && parent->rb_right != NULL)
		bfq_update_active_node(parent->rb_right);
	else if (parent->rb_left != NULL)
		bfq_update_active_node(parent->rb_left);

	node = parent;
	goto up;
}

/**
 * bfq_active_insert - insert a queue in the active tree of its device.
 * @wfqd: the wfqdata of the device data containing the tree.
 * @bfqq: the queue being inserted.
 *
 * The active tree is ordered by finish time, but an extra key is kept
 * per each node, containing the minimum value for the start times of
 * its children (and the node itself), so it's possible to search for
 * the eligible node with the lowest finish time in logarithmic time.
 */
static void bfq_active_insert(struct bfq_wfqdata *wfqd,
			      struct bfq_queue *bfqq)
{
	struct rb_node *node = &bfqq->rb_node;

	bfq_insert(&wfqd->active, bfqq);

	if (node->rb_left != NULL)
		node = node->rb_left;
	else if (node->rb_right != NULL)
		node = node->rb_right;

	bfq_update_active_tree(node);
}

/**
 * bfq_ioprio_to_weight - calc the weight for a queue.
 * @bfqq: the queue to act upon.
 */
static bfq_weight_t bfq_ioprio_to_weight(struct bfq_queue *bfqq)
{
	WARN_ON(bfqq->act_ioprio >= IOPRIO_BE_NR);
	return IOPRIO_BE_NR - bfqq->act_ioprio;
}

/**
 * bfq_update_weight - update the weight of a queue.
 * @wfqd: wfqdata for the device.
 * @bfqq: queue to act upon.
 * @old_weight: weight @bfqq had on @wfqdata.
 */
static void bfq_update_weight(struct bfq_wfqdata **wfqd,
			      struct bfq_queue *bfqq,
			      bfq_weight_t old_weight)
{
	struct bfq_data *bfqd = bfqq->bfqd;
	struct bfq_wfqdata *new_wfqd = *wfqd;

	if (bfqq->act_ioprio != bfqq->ioprio ||
	    bfqq->act_ioprio_class != bfqq->ioprio_class) {
		bfqq->act_ioprio = bfqq->ioprio;
		bfqq->act_ioprio_class = bfqq->ioprio_class;
		bfqq->weight = bfq_ioprio_to_weight(bfqq);
		new_wfqd = &bfqd->service_tree[bfq_bfqq_tree_index(bfqq)];
		if (new_wfqd != *wfqd)
			bfqq->start = new_wfqd->vtime;
	} else if (old_weight != 0)
		/* Already enqueued with the same weight. */
		return;

	(*wfqd)->wsum -= old_weight;
	new_wfqd->wsum += bfqq->weight;
	*wfqd = new_wfqd;
}

/**
 * bfq_activate_bfqq - activate a queue.
 * @bfqd: the device data.
 * @bfqq: the queue being activated.
 *
 * Called whenever a queue is activated, i.e., it is not active and
 * receives a new request, or has to be reactivated due to budget
 * exhaustion.  It uses the current budget of the queue (and the service
 * received if @bfqq is active) of the queue to calculate its timestamps.
 */
static void bfq_activate_bfqq(struct bfq_data *bfqd, struct bfq_queue *bfqq)
{
	struct bfq_wfqdata *wfqd = bfq_bfqq_wfqdata(bfqd, bfqq);
	bfq_weight_t old_weight;

	if (bfqq == bfqd->active_queue) {
		BUG_ON(bfqq->tree != NULL);
		/*
		 * If we are requeueing the current queue we have
		 * to take care of not charging to it service it has
		 * not received.
		 */
		bfq_update_finish(bfqq);
		bfqq->start = bfqq->finish;
		old_weight = bfqq->weight;
	} else if (bfqq->tree != NULL) {
		/*
		 * Must be on the idle tree, bfq_idle_extract() will
		 * check for that.
		 */
		bfq_idle_extract(wfqd, bfqq);
		bfqq->start = bfq_gt(wfqd->vtime, bfqq->finish) ?
			      wfqd->vtime : bfqq->finish;
		old_weight = bfqq->weight;
	} else {
		/*
		 * The finish time of the queue can be invalid, and
		 * it is in the past for sure, otherwise the queue
		 * would have been on the idle tree.
		 */
		bfqq->start = wfqd->vtime;
		atomic_inc(&bfqq->ref);
		old_weight = 0;
	}

	bfq_update_weight(&wfqd, bfqq, old_weight);
	bfq_calc_finish(bfqq);
	bfq_active_insert(wfqd, bfqq);
}

/**
 * bfq_find_deepest - find the deepest node that an extraction can modify.
 * @node: the node being removed.
 *
 * Do the first step of an extraction in an rb tree, looking for the
 * node that will replace @node, and returning the deepest node that
 * the following modifications to the tree can touch.  If @node is the
 * last node in the tree return %NULL.
 */
static struct rb_node *bfq_find_deepest(struct rb_node *node)
{
	struct rb_node *deepest;

	if (node->rb_right == NULL && node->rb_left == NULL)
		deepest = rb_parent(node);
	else if (node->rb_right == NULL)
		deepest = node->rb_left;
	else if (node->rb_left == NULL)
		deepest = node->rb_right;
	else {
		deepest = rb_next(node);
		if (deepest->rb_right != NULL)
			deepest = deepest->rb_right;
		else if (rb_parent(deepest) != node)
			deepest = rb_parent(deepest);
	}

	return deepest;
}

/**
 * bfq_active_extract - remove an entity from the active tree.
 * @wfqd: the wfqdata containing the tree.
 * @bfqq: the queue being removed.
 */
static void bfq_active_extract(struct bfq_wfqdata *wfqd,
			       struct bfq_queue *bfqq)
{
	struct rb_node *node;

	node = bfq_find_deepest(&bfqq->rb_node);
	bfq_extract(&wfqd->active, bfqq);

	if (node != NULL)
		bfq_update_active_tree(node);
}

/**
 * bfq_idle_insert - insert an entity into the idle tree.
 * @wfqd: the queue containing the tree.
 * @bfqq: the queue to insert.
 */
static void bfq_idle_insert(struct bfq_wfqdata *wfqd,
			    struct bfq_queue *bfqq)
{
	struct bfq_queue *first_idle = wfqd->first_idle;
	struct bfq_queue *last_idle = wfqd->last_idle;

	if (first_idle == NULL || bfq_gt(first_idle->finish, bfqq->finish))
		wfqd->first_idle = bfqq;
	if (last_idle == NULL || bfq_gt(bfqq->finish, last_idle->finish))
		wfqd->last_idle = bfqq;

	bfq_insert(&wfqd->idle, bfqq);
}

/**
 * bfq_forget_queue - remove a queue from the wfq trees.
 * @wfqd: the wfqdata.
 * @bfqq: the queue being removed.
 *
 * Update the device status and forget everything about @bfqq, putting
 * the device reference to it.
 */
static void bfq_forget_queue(struct bfq_wfqdata *wfqd,
			     struct bfq_queue *bfqq)
{
	wfqd->wsum -= bfqq->weight;
	bfq_put_queue(bfqq);
}

static void bfq_deactivate_bfqq(struct bfq_data *bfqd, struct bfq_queue *bfqq)
{
	struct bfq_wfqdata *wfqd = bfq_bfqq_wfqdata(bfqd, bfqq);

	if (bfqq == bfqd->active_queue) {
		BUG_ON(bfqq->tree != NULL);
		bfq_update_finish(bfqq);
		bfqd->active_queue = NULL;
	} else
		bfq_active_extract(wfqd, bfqq);

	if (bfq_gt(bfqq->finish, wfqd->vtime))
		bfq_idle_insert(wfqd, bfqq);
	else
		bfq_forget_queue(wfqd, bfqq);
}

/**
 * bfq_put_idle_queue - release the idle tree ref of a queue.
 * @wfqd: wfqdata of the device.
 * @bfqq: the queue being released.
 */
static void bfq_put_idle_queue(struct bfq_wfqdata *wfqd,
			       struct bfq_queue *bfqq)
{
	bfq_idle_extract(wfqd, bfqq);
	bfq_forget_queue(wfqd, bfqq);
}

/**
 * bfq_bfqq_served - update the scheduler status after service.
 * @bfqd: the device data.
 * @bfqq: the queue being served.
 * @served: bytes transfered/to transfer.
 */
static void bfq_bfqq_served(struct bfq_data *bfqd, struct bfq_queue *bfqq,
			    bfq_service_t served)
{
	struct bfq_wfqdata *wfqd = bfq_bfqq_wfqdata(bfqd, bfqq);

	WARN_ON_ONCE(bfqq->service > bfqq->act_budget);

	bfqq->service += served;
	WARN_ON_ONCE(bfqq->service > bfqq->act_budget);
	wfqd->vtime += bfq_delta(served, wfqd->wsum);

	bfq_forget_idle(wfqd);
}

/*
 * Called when an inactive queue receives a new request.
 */
static void bfq_add_bfqq_busy(struct bfq_data *bfqd, struct bfq_queue *bfqq)
{
	BUG_ON(bfq_bfqq_busy(bfqq));
	BUG_ON(bfqq == bfqd->active_queue);
	bfq_mark_bfqq_busy(bfqq);
	bfqd->busy_queues++;

	bfq_activate_bfqq(bfqd, bfqq);
}

/*
 * Called when the bfqq no longer has requests pending, remove it from
 * the service tree.
 */
static void bfq_del_bfqq_busy(struct bfq_data *bfqd, struct bfq_queue *bfqq)
{
	BUG_ON(!bfq_bfqq_busy(bfqq));
	BUG_ON(!RB_EMPTY_ROOT(&bfqq->sort_list));

	bfq_clear_bfqq_busy(bfqq);
	bfq_deactivate_bfqq(bfqd, bfqq);

	BUG_ON(bfqd->busy_queues == 0);
	bfqd->busy_queues--;
}

/*
 * rb tree support functions
 */
static void bfq_del_rq_rb(struct request *rq)
{
	struct bfq_queue *bfqq = RQ_BFQQ(rq);
	struct bfq_data *bfqd = bfqq->bfqd;
	const int sync = rq_is_sync(rq);

	BUG_ON(bfqq->queued[sync] == 0);
	bfqq->queued[sync]--;
	bfqd->queued--;

	elv_rb_del(&bfqq->sort_list, rq);

	if (bfq_bfqq_busy(bfqq) && bfqq != bfqd->active_queue &&
	    RB_EMPTY_ROOT(&bfqq->sort_list))
		bfq_del_bfqq_busy(bfqd, bfqq);
}

/**
 * bfq_updated_next_req - update the queue after a new next_rq selection.
 * @bfqd: the device data the queue belongs to.
 * @bfqq: the queue to update.
 *
 * Whenever the first request of a queue changes we try to allocate it
 * enough service (if it has grown), or to anticipate its finish time
 * (if it has shrinked), to reduce the time it has to wait, still taking
 * into account the queue budget.  We try to avoid the queue having not
 * enough service allocated for its first request, thus having to go
 * through two dispatch rounds to actually dispatch the request.
 */
static void bfq_updated_next_req(struct bfq_data *bfqd,
				 struct bfq_queue *bfqq)
{
	struct bfq_wfqdata *wfqd = bfq_bfqq_wfqdata(bfqd, bfqq);
	struct request *next_rq = bfqq->next_rq;
	bfq_service_t new_budget;

	if (next_rq == NULL)
		return;

	if (bfqq == bfqd->active_queue)
		/*
		 * In order not to break guarantees, budgets cannot be
		 * changed after an activity has been selected.
		 */
		return;

	BUG_ON(bfqq->tree != &wfqd->active);

	new_budget = max(bfqq->budget, next_rq->hard_nr_sectors);
	if (new_budget <= bfqq->act_budget)
		/*
		 * Finish times cannot be decreased while the queue
		 * is either schedulable or not eligible, as it would
		 * invalidate previous scheduling decisions.  The
		 * current budget is enough to satisfy the first req
		 * anyway.
		 */
		return;

	bfqq->act_budget = new_budget;
	bfq_active_extract(wfqd, bfqq);
	bfq_calc_finish(bfqq);
	bfq_active_insert(wfqd, bfqq);
}

static void bfq_add_rq_rb(struct request *rq)
{
	struct bfq_queue *bfqq = RQ_BFQQ(rq);
	struct bfq_data *bfqd = bfqq->bfqd;
	struct request *__alias, *next_rq;

	bfqq->queued[rq_is_sync(rq)]++;
	bfqd->queued++;

	/*
	 * looks a little odd, but the first insert might return an alias.
	 * if that happens, put the alias on the dispatch list
	 */
	while ((__alias = elv_rb_add(&bfqq->sort_list, rq)) != NULL)
		bfq_dispatch_insert(bfqd->queue, __alias);

	/*
	 * check if this request is a better next-serve candidate
	 */
	next_rq = bfq_choose_req(bfqd, bfqq->next_rq, rq);
	BUG_ON(next_rq == NULL);
	bfqq->next_rq = next_rq;

	if (!bfq_bfqq_busy(bfqq)) {
		bfqq->act_budget = max(bfqq->budget, next_rq->hard_nr_sectors);
		bfq_add_bfqq_busy(bfqd, bfqq);
	} else
		bfq_updated_next_req(bfqd, bfqq);
}

static void bfq_reposition_rq_rb(struct bfq_queue *bfqq, struct request *rq)
{
	elv_rb_del(&bfqq->sort_list, rq);
	bfqq->queued[rq_is_sync(rq)]--;
	bfqq->bfqd->queued--;
	bfq_add_rq_rb(rq);
}

static struct request *
bfq_find_rq_fmerge(struct bfq_data *bfqd, struct bio *bio)
{
	struct task_struct *tsk = current;
	struct cfq_io_context *cic;
	struct bfq_queue *bfqq;

	cic = bfq_cic_lookup(bfqd, tsk->io_context);
	if (cic == NULL)
		return NULL;

	bfqq = cic_to_bfqq(cic, bfq_bio_sync(bio));
	if (bfqq != NULL) {
		sector_t sector = bio->bi_sector + bio_sectors(bio);

		return elv_rb_find(&bfqq->sort_list, sector);
	}

	return NULL;
}

static void bfq_activate_request(struct request_queue *q, struct request *rq)
{
	struct bfq_data *bfqd = q->elevator->elevator_data;

	bfqd->rq_in_driver++;

	bfqd->last_position = rq->hard_sector + rq->hard_nr_sectors;
}

static void bfq_deactivate_request(struct request_queue *q, struct request *rq)
{
	struct bfq_data *bfqd = q->elevator->elevator_data;

	WARN_ON(bfqd->rq_in_driver == 0);
	bfqd->rq_in_driver--;
}

static void bfq_remove_request(struct request *rq)
{
	struct bfq_queue *bfqq = RQ_BFQQ(rq);
	struct bfq_data *bfqd = bfqq->bfqd;

	if (bfqq->next_rq == rq) {
		bfqq->next_rq = bfq_find_next_rq(bfqd, bfqq, rq);
		bfq_updated_next_req(bfqd, bfqq);
	}

	list_del_init(&rq->queuelist);
	bfq_del_rq_rb(rq);

	bfqq->bfqd->rq_queued--;
	if (rq_is_meta(rq)) {
		WARN_ON(bfqq->meta_pending == 0);
		bfqq->meta_pending--;
	}
}

static int bfq_merge(struct request_queue *q, struct request **req,
		     struct bio *bio)
{
	struct bfq_data *bfqd = q->elevator->elevator_data;
	struct request *__rq;

	__rq = bfq_find_rq_fmerge(bfqd, bio);
	if (__rq != NULL && elv_rq_merge_ok(__rq, bio)) {
		*req = __rq;
		return ELEVATOR_FRONT_MERGE;
	}

	return ELEVATOR_NO_MERGE;
}

static void bfq_merged_request(struct request_queue *q, struct request *req,
			       int type)
{
	if (type == ELEVATOR_FRONT_MERGE) {
		struct bfq_queue *bfqq = RQ_BFQQ(req);

		bfq_reposition_rq_rb(bfqq, req);
	}
}

static void
bfq_merged_requests(struct request_queue *q, struct request *rq,
		    struct request *next)
{
	/*
	 * reposition in fifo if next is older than rq
	 */
	if (!list_empty(&rq->queuelist) && !list_empty(&next->queuelist) &&
	    time_before(next->start_time, rq->start_time))
		list_move(&rq->queuelist, &next->queuelist);

	bfq_remove_request(next);
}

static int bfq_allow_merge(struct request_queue *q, struct request *rq,
			   struct bio *bio)
{
	struct bfq_data *bfqd = q->elevator->elevator_data;
	struct cfq_io_context *cic;
	struct bfq_queue *bfqq;

	/*
	 * Disallow merge of a sync bio into an async request.
	 */
	if (bfq_bio_sync(bio) && !rq_is_sync(rq))
		return 0;

	/*
	 * Lookup the bfqq that this bio will be queued with. Allow
	 * merge only if rq is queued there.
	 */
	cic = bfq_cic_lookup(bfqd, current->io_context);
	if (cic == NULL)
		return 0;

	bfqq = cic_to_bfqq(cic, bfq_bio_sync(bio));
	if (bfqq == RQ_BFQQ(rq))
		return 1;

	return 0;
}

static void __bfq_set_active_queue(struct bfq_data *bfqd,
				   struct bfq_queue *bfqq)
{
	if (bfqq != NULL) {
		bfq_mark_bfqq_must_alloc(bfqq);
		bfq_clear_bfqq_fifo_expire(bfqq);
		bfqq->service = 0;
	}

	bfqd->active_queue = bfqq;
}

/**
 * bfq_forget_idle - update the idle tree if necessary.
 * @wfqd: the wfqdata to act upon.
 *
 * To preserve the global O(log N) complexity we only remove one entry here;
 * as the idle tree will not grow indefinitely this can be done safely.
 */
static void bfq_forget_idle(struct bfq_wfqdata *wfqd)
{
	struct bfq_queue *first_idle = wfqd->first_idle;
	struct bfq_queue *last_idle = wfqd->last_idle;

	if (RB_EMPTY_ROOT(&wfqd->active) && last_idle != NULL) {
		/*
		 * Forget the whole idle tree, increasing the vtime past
		 * the last finish time of idle entities.
		 */
		wfqd->vtime = last_idle->finish;
	}

	if (first_idle != NULL && !bfq_gt(first_idle->finish, wfqd->vtime))
		bfq_put_idle_queue(wfqd, first_idle);
}

/**
 * bfq_update_vtime - update vtime if necessary.
 * @queue: the wfqdata to act upon.
 *
 * If necessary update the device vtime to have at least one eligible
 * entity, skipping to its start time.  Assumes that the active tree
 * of the device is not empty.
 */
static void bfq_update_vtime(struct bfq_wfqdata *wfqd)
{
	struct bfq_queue *entry;
	struct rb_node *node = wfqd->active.rb_node;

	entry = rb_entry(node, struct bfq_queue, rb_node);
	if (bfq_gt(entry->min_start, wfqd->vtime)) {
		wfqd->vtime = entry->min_start;
		bfq_forget_idle(wfqd);
	}
}

/**
 * bfq_first_active - find the eligible entity with the smallest finish time
 * @wfqd: the wfqdata to select from.
 *
 * This function searches the first schedulable queue, starting from the
 * root of the tree and going on the left every time on this side there is
 * a subtree with at least one eligible (start >= vtime) entity.  The path
 * on the right is followed only if a) the left subtree contains no eligible
 * queue and b) no eligible queue has been found yet.
 */
static struct bfq_queue *bfq_first_active(struct bfq_wfqdata *wfqd)
{
	struct bfq_queue *entry, *first = NULL;
	struct rb_node *node = wfqd->active.rb_node;

	while (node != NULL) {
		entry = rb_entry(node, struct bfq_queue, rb_node);
left:
		if (!bfq_gt(entry->start, wfqd->vtime))
			first = entry;

		BUG_ON(bfq_gt(entry->min_start, wfqd->vtime));

		if (node->rb_left != NULL) {
			entry = rb_entry(node->rb_left,
					 struct bfq_queue, rb_node);
			if (!bfq_gt(entry->min_start, wfqd->vtime)) {
				node = node->rb_left;
				goto left;
			}
		}
		if (first != NULL)
			break;
		node = node->rb_right;
	}

	return first;
}

static struct bfq_queue *bfq_wfqnext(struct bfq_wfqdata *wfqd)
{
	struct bfq_queue *bfqq;

	if (RB_EMPTY_ROOT(&wfqd->active))
		return NULL;

	bfq_update_vtime(wfqd);
	bfqq = bfq_first_active(wfqd);
	bfq_active_extract(wfqd, bfqq);

	BUG_ON(bfq_gt(bfqq->start, wfqd->vtime));

	return bfqq;
}

/*
 * Get next queue for service.
 */
static struct bfq_queue *bfq_get_next_queue(struct bfq_data *bfqd)
{
	struct bfq_queue *bfqq;
	int i;

	BUG_ON(bfqd->active_queue != NULL);

	for (i = 0; i < BFQ_IOPRIO_CLASSES; i++) {
		bfqq = bfq_wfqnext(&bfqd->service_tree[i]);
		if (bfqq != NULL)
			break;
	}

	return bfqq;
}

/*
 * Get and set a new active queue for service.
 */
static struct bfq_queue *bfq_set_active_queue(struct bfq_data *bfqd)
{
	struct bfq_queue *bfqq;

	bfqq = bfq_get_next_queue(bfqd);
	__bfq_set_active_queue(bfqd, bfqq);
	return bfqq;
}

static inline sector_t bfq_dist_from_last(struct bfq_data *bfqd,
					  struct request *rq)
{
	if (rq->sector >= bfqd->last_position)
		return rq->sector - bfqd->last_position;
	else
		return bfqd->last_position - rq->sector;
}

static inline int bfq_rq_close(struct bfq_data *bfqd, struct request *rq)
{
	struct cfq_io_context *cic = bfqd->active_cic;

	if (!sample_valid(cic->seek_samples))
		return 0;

	return bfq_dist_from_last(bfqd, rq) <= cic->seek_mean;
}

static int bfq_close_cooperator(struct bfq_data *bfq_data,
				struct bfq_queue *bfqq)
{
	/*
	 * We should notice if some of the queues are cooperating, eg
	 * working closely on the same area of the disk. In that case,
	 * we can group them together and don't waste time idling.
	 */
	return 0;
}

#define CIC_SEEKY(cic) ((cic)->seek_mean > (8 * 1024))

static void bfq_arm_slice_timer(struct bfq_data *bfqd)
{
	struct bfq_queue *bfqq = bfqd->active_queue;
	struct cfq_io_context *cic;
	unsigned long sl;

        /*
         * SSD device without seek penalty, disable idling. But only do so
         * for devices that support queuing, otherwise we still have a problem
         * with sync vs async workloads.
         */
        if (blk_queue_nonrot(bfqd->queue) && bfqd->hw_tag)
                return;

	WARN_ON(!RB_EMPTY_ROOT(&bfqq->sort_list));

	/*
	 * idle is disabled, either manually or by past process history
	 */
	if (bfqd->bfq_slice_idle == 0 || !bfq_bfqq_idle_window(bfqq))
		return;

	/*
	 * task has exited, don't wait
	 */
	cic = bfqd->active_cic;
	if (cic == NULL || atomic_read(&cic->ioc->nr_tasks) == 0)
		return;

	/*
	 * See if this prio level has a good candidate
	 */
	if (bfq_close_cooperator(bfqd, bfqq) &&
	    (sample_valid(cic->ttime_samples) && cic->ttime_mean > 2))
		return;

	bfq_mark_bfqq_wait_request(bfqq);

	/*
	 * we don't want to idle for seeks, but we do want to allow
	 * fair distribution of slice time for a process doing back-to-back
	 * seeks. so allow a little bit of time for him to submit a new rq
	 */
	sl = bfqd->bfq_slice_idle;
	if (sample_valid(cic->seek_samples) && CIC_SEEKY(cic))
		sl = min(sl, msecs_to_jiffies(BFQ_MIN_TT));

	mod_timer(&bfqd->idle_slice_timer, jiffies + sl);
}

/*
 * Move request from internal lists to the request queue dispatch list.
 */
static void bfq_dispatch_insert(struct request_queue *q, struct request *rq)
{
	struct bfq_data *bfqd = q->elevator->elevator_data;
	struct bfq_queue *bfqq = RQ_BFQQ(rq);

	bfq_remove_request(rq);
	bfqq->dispatched++;
	elv_dispatch_sort(q, rq);

	if (bfq_bfqq_sync(bfqq))
		bfqd->sync_flight++;
}

/*
 * return expired entry, or NULL to just start from scratch in rbtree
 */
static struct request *bfq_check_fifo(struct bfq_queue *bfqq)
{
	struct bfq_data *bfqd = bfqq->bfqd;
	struct request *rq;
	int fifo;

	if (bfq_bfqq_fifo_expire(bfqq))
		return NULL;

	bfq_mark_bfqq_fifo_expire(bfqq);

	if (list_empty(&bfqq->fifo))
		return NULL;

	fifo = bfq_bfqq_sync(bfqq);
	rq = rq_entry_fifo(bfqq->fifo.next);

	if (time_before(jiffies, rq->start_time + bfqd->bfq_fifo_expire[fifo]))
		return NULL;

	return rq;
}

static inline int
bfq_prio_to_maxrq(struct bfq_data *bfqd, struct bfq_queue *bfqq)
{
	const int base_rq = bfqd->bfq_slice_async_rq;

	WARN_ON(bfqq->ioprio >= IOPRIO_BE_NR);

	return 2 * (base_rq + base_rq * (BFQ_PRIO_LISTS - 1 - bfqq->ioprio));
}

static inline bfq_service_t bfq_bfqq_budget_left(struct bfq_queue *bfqq)
{
	return bfqq->act_budget - bfqq->service;
}

static void __bfq_bfqq_expire(struct bfq_data *bfqd, struct bfq_queue *bfqq)
{
	BUG_ON(bfqq != bfqd->active_queue);

	if (bfqd->active_cic != NULL) {
		put_io_context(bfqd->active_cic->ioc);
		bfqd->active_cic = NULL;
	}

	if (RB_EMPTY_ROOT(&bfqq->sort_list))
		bfq_del_bfqq_busy(bfqd, bfqq);
	else
		bfq_activate_bfqq(bfqd, bfqq);

	bfqd->active_queue = NULL;
	del_timer(&bfqd->idle_slice_timer);
}

static void __bfq_bfqq_recalc_budget(struct bfq_data *bfqd,
				     struct bfq_queue *bfqq,
				     int timed_out)
{
	struct request *next_rq;

	BUG_ON(bfqq != bfqd->active_queue);

	if (timed_out == 0) {
		bfqq->budget = min(bfqq->budget + BFQ_BUDGET_STEP,
				   (bfq_service_t)bfqd->bfq_max_budget);

		/*
		 * This is to be sure that we have enough budget for the
		 * next request, and is correct only because we are sure
		 * that the the active queue will be requeued immediately,
		 * since the queue may not be the one to serve (its finish
		 * timestamp needs to be updated to the new budget.)
		 * IOW __bfq_bfqq_recalc_budget() must be followed by
		 * __bfq_bfqq_expire().
		 */
		next_rq = bfqq->next_rq;
		bfqq->act_budget = max(bfqq->budget, next_rq->hard_nr_sectors);
	} else
		bfqq->budget = max(bfqq->service, (bfq_service_t)4);
}

static void bfq_bfqq_expire(struct bfq_data *bfqd,
			    struct bfq_queue *bfqq,
			    int timed_out)
{
	__bfq_bfqq_recalc_budget(bfqd, bfqq, timed_out);
	__bfq_bfqq_expire(bfqd, bfqq);
}

/*
 * Select a queue for service. If we have a current active queue,
 * check whether to continue servicing it, or retrieve and set a new one.
 */
static struct bfq_queue *bfq_select_queue(struct bfq_data *bfqd)
{
	struct bfq_queue *bfqq;
	struct request *next_rq;

	bfqq = bfqd->active_queue;
	if (bfqq == NULL)
		goto new_queue;

	next_rq = bfqq->next_rq;
	/*
	 * If bfqq has requests queued and it has enough budget left to
	 * serve it keep the queue, otherwise expire it.
	 */
	if (next_rq != NULL) {
		if (next_rq->hard_nr_sectors > bfq_bfqq_budget_left(bfqq)) {
			__bfq_bfqq_recalc_budget(bfqd, bfqq, 0);
			goto expire;
		} else
			goto keep_queue;
	}

	/*
	 * No requests pending. If the active queue still has requests in
	 * flight or is idling for a new request, allow either of these
	 * conditions to happen (or time out) before selecting a new queue.
	 */
	if (timer_pending(&bfqd->idle_slice_timer) ||
	    (bfqq->dispatched != 0 && bfq_bfqq_idle_window(bfqq))) {
		bfqq = NULL;
		goto keep_queue;
	}

expire:
	__bfq_bfqq_expire(bfqd, bfqq);
new_queue:
	bfqq = bfq_set_active_queue(bfqd);
keep_queue:
	return bfqq;
}

/*
 * Dispatch some requests from bfqq, moving them to the request queue
 * dispatch list.
 */
static int
__bfq_dispatch_requests(struct bfq_data *bfqd, struct bfq_queue *bfqq,
			int max_dispatch)
{
	int dispatched = 0;

	BUG_ON(RB_EMPTY_ROOT(&bfqq->sort_list));

	do {
		struct request *rq;

		/*
		 * follow expired path, else get first next available
		 */
		rq = bfq_check_fifo(bfqq);
		if (rq == NULL)
			rq = bfqq->next_rq;

		if (rq->hard_nr_sectors > bfq_bfqq_budget_left(bfqq)) {
			/*
			 * Expire the queue for budget exhaustion, and
			 * make sure that the next act_budget is enough
			 * to serve the next request, even if it comes
			 * from the fifo expired path.
			 */
			bfqq->next_rq = rq;
			bfq_bfqq_expire(bfqd, bfqq, 0);
			goto out;
		}

		/*
		 * finally, insert request into driver dispatch list
		 */
		bfq_bfqq_served(bfqd, bfqq, rq->hard_nr_sectors);
		bfq_dispatch_insert(bfqd->queue, rq);

		dispatched++;

		if (bfqd->active_cic == NULL) {
			atomic_inc(&RQ_CIC(rq)->ioc->refcount);
			bfqd->active_cic = RQ_CIC(rq);
		}

		if (RB_EMPTY_ROOT(&bfqq->sort_list))
			break;
	} while (dispatched < max_dispatch);

	/*
	 * Expire an async queue immediately if it has used up its slice.
	 * Idle queues always expire after 1 dispatch round.  A better
	 * approach to handle async queues would be to use a max_async_budget
	 * instead of slice_asyn_rq.
	 */
	if (bfqd->busy_queues > 1 && ((!bfq_bfqq_sync(bfqq) &&
	    dispatched >= bfq_prio_to_maxrq(bfqd, bfqq)) ||
	    bfq_class_idle(bfqq)))
		__bfq_bfqq_expire(bfqd, bfqq);

out:
	return dispatched;
}

static int __bfq_forced_dispatch_bfqq(struct bfq_queue *bfqq)
{
	int dispatched = 0;

	while (bfqq->next_rq != NULL) {
		bfq_dispatch_insert(bfqq->bfqd->queue, bfqq->next_rq);
		dispatched++;
	}

	BUG_ON(!list_empty(&bfqq->fifo));
	return dispatched;
}

/*
 * Drain our current requests.  Used for barriers and when switching
 * io schedulers on-the-fly.
 */
static int bfq_forced_dispatch(struct bfq_data *bfqd)
{
	struct bfq_wfqdata *wfqd;
	struct bfq_queue *bfqq;
	int dispatched = 0, i;
	struct rb_node *n;

	bfqq = bfqd->active_queue;
	if (bfqq != NULL)
		__bfq_bfqq_expire(bfqd, bfqq);

	/*
	 * Loop through classes, and be careful to leave the scheduler
	 * in a consistent state, as feedback mechanisms and vtime
	 * updates cannot be disabled during the process.
	 */
	for (i = 0; i < BFQ_IOPRIO_CLASSES; i++) {
		wfqd = &bfqd->service_tree[i];
		while ((n = rb_first(&wfqd->active)) != NULL) {
			bfqq = rb_entry(n, struct bfq_queue, rb_node);
			dispatched += __bfq_forced_dispatch_bfqq(bfqq);
			bfqq->budget = bfqd->bfq_max_budget;
		}
		bfq_forget_idle(wfqd);
	}

	BUG_ON(bfqd->busy_queues != 0);

	return dispatched;
}

static int bfq_dispatch_requests(struct request_queue *q, int force)
{
	struct bfq_data *bfqd = q->elevator->elevator_data;
	struct bfq_queue *bfqq;
	int dispatched;

	if (bfqd->busy_queues == 0)
		return 0;

	if (unlikely(force))
		return bfq_forced_dispatch(bfqd);

	dispatched = 0;
	while ((bfqq = bfq_select_queue(bfqd)) != NULL) {
		int max_dispatch;

		max_dispatch = bfqd->bfq_quantum;
		if (bfq_class_idle(bfqq))
			max_dispatch = 1;

		if (bfqq->dispatched >= max_dispatch) {
			if (bfqd->busy_queues > 1)
				break;
			if (bfqq->dispatched >= 4 * max_dispatch)
				break;
		}

		if (bfqd->sync_flight != 0 && !bfq_bfqq_sync(bfqq))
			break;

		bfq_clear_bfqq_wait_request(bfqq);
		BUG_ON(timer_pending(&bfqd->idle_slice_timer));

		dispatched += __bfq_dispatch_requests(bfqd, bfqq, max_dispatch);
	}

	return dispatched;
}

/*
 * task holds one reference to the queue, dropped when task exits. each rq
 * in-flight on this queue also holds a reference, dropped when rq is freed.
 *
 * queue lock must be held here.
 */
static void bfq_put_queue(struct bfq_queue *bfqq)
{
	struct bfq_data *bfqd = bfqq->bfqd;

	BUG_ON(atomic_read(&bfqq->ref) <= 0);

	if (!atomic_dec_and_test(&bfqq->ref))
		return;

	BUG_ON(rb_first(&bfqq->sort_list) != NULL);
	BUG_ON(bfqq->allocated[READ] + bfqq->allocated[WRITE] != 0);
	BUG_ON(bfq_bfqq_busy(bfqq));
	BUG_ON(bfqd->active_queue == bfqq);

	kmem_cache_free(bfq_pool, bfqq);
}

/*
 * Call func for each cic attached to this ioc.
 */
static void
call_for_each_cic(struct io_context *ioc,
		  void (*func)(struct io_context *, struct cfq_io_context *))
{
	struct cfq_io_context *cic;
	struct hlist_node *n;

	rcu_read_lock();
	hlist_for_each_entry_rcu(cic, n, &ioc->bfq_cic_list, cic_list)
		func(ioc, cic);
	rcu_read_unlock();
}

static void bfq_cic_free_rcu(struct rcu_head *head)
{
	struct cfq_io_context *cic;

	cic = container_of(head, struct cfq_io_context, rcu_head);

	kmem_cache_free(bfq_ioc_pool, cic);
	elv_ioc_count_dec(ioc_count);

	if (ioc_gone && !elv_ioc_count_read(ioc_count))
		complete(ioc_gone);
}

static void bfq_cic_free(struct cfq_io_context *cic)
{
	call_rcu(&cic->rcu_head, bfq_cic_free_rcu);
}

static void cic_free_func(struct io_context *ioc, struct cfq_io_context *cic)
{
	unsigned long flags;

	BUG_ON(cic->dead_key == 0);

	spin_lock_irqsave(&ioc->lock, flags);
	radix_tree_delete(&ioc->radix_root, cic->dead_key);
	hlist_del_rcu(&cic->cic_list);
	spin_unlock_irqrestore(&ioc->lock, flags);

	bfq_cic_free(cic);
}

static void bfq_free_io_context(struct io_context *ioc)
{
	/*
	 * ioc->refcount is zero here, or we are called from elv_unregister(),
	 * so no more cic's are allowed to be linked into this ioc.  So it
	 * should be ok to iterate over the known list, we will see all cic's
	 * since no new ones are added.
	 */
	call_for_each_cic(ioc, cic_free_func);
}

static void bfq_exit_bfqq(struct bfq_data *bfqd, struct bfq_queue *bfqq)
{
	if (bfqq == bfqd->active_queue) {
		__bfq_bfqq_expire(bfqd, bfqq);
		bfq_schedule_dispatch(bfqd);
	}

	bfq_put_queue(bfqq);
}

static void __bfq_exit_single_io_context(struct bfq_data *bfqd,
					 struct cfq_io_context *cic)
{
	struct io_context *ioc = cic->ioc;

	list_del_init(&cic->queue_list);

	/*
	 * Make sure key == NULL is seen for dead queues
	 */
	smp_wmb();
	cic->dead_key = (unsigned long)cic->key;
	cic->key = NULL;

	if (ioc->ioc_data == cic)
		rcu_assign_pointer(ioc->ioc_data, NULL);

	if (cic->cfqq[ASYNC] != NULL) {
		bfq_exit_bfqq(bfqd, cic->cfqq[ASYNC]);
		cic->cfqq[ASYNC] = NULL;
	}

	if (cic->cfqq[SYNC] != NULL) {
		bfq_exit_bfqq(bfqd, cic->cfqq[SYNC]);
		cic->cfqq[SYNC] = NULL;
	}
}

static void bfq_exit_single_io_context(struct io_context *ioc,
				       struct cfq_io_context *cic)
{
	struct bfq_data *bfqd = cic->key;

	if (bfqd != NULL) {
		struct request_queue *q = bfqd->queue;
		unsigned long flags;

		spin_lock_irqsave(q->queue_lock, flags);
		__bfq_exit_single_io_context(bfqd, cic);
		spin_unlock_irqrestore(q->queue_lock, flags);
	}
}

/*
 * The process that ioc belongs to has exited, we need to clean up
 * and put the internal structures we have that belongs to that process.
 */
static void bfq_exit_io_context(struct io_context *ioc)
{
	call_for_each_cic(ioc, bfq_exit_single_io_context);
}

static struct cfq_io_context *
bfq_alloc_io_context(struct bfq_data *bfqd, gfp_t gfp_mask)
{
	struct cfq_io_context *cic;

	cic = kmem_cache_alloc_node(bfq_ioc_pool, gfp_mask | __GFP_ZERO,
							bfqd->queue->node);
	if (cic != NULL) {
		cic->last_end_request = jiffies;
		INIT_LIST_HEAD(&cic->queue_list);
		INIT_HLIST_NODE(&cic->cic_list);
		cic->dtor = bfq_free_io_context;
		cic->exit = bfq_exit_io_context;
		elv_ioc_count_inc(ioc_count);
	}

	return cic;
}

/*
 * With BFQ priorities cannot change anywhere, so the values used to store
 * the actual ioprio/class of a queue are old_ioprio and old_ioprio_class,
 * that are synced with the ones assigned here (and by the boosting code)
 * only when the queue can change its priority.  This function must be
 * called in the context of the task owning ioc so we cannot delay it to
 * the next (re-)activation of the queue.
 */
static void bfq_init_prio_data(struct bfq_queue *bfqq, struct io_context *ioc)
{
	struct task_struct *tsk = current;
	int ioprio_class;

	if (!bfq_bfqq_prio_changed(bfqq))
		return;

	ioprio_class = IOPRIO_PRIO_CLASS(ioc->ioprio);
	switch (ioprio_class) {
	default:
		printk(KERN_ERR "bfq: bad prio %x\n", ioprio_class);
	case IOPRIO_CLASS_NONE:
		/*
		 * no prio set, place us in the middle of the BE classes
		 */
		bfqq->ioprio = task_nice_ioprio(tsk);
		bfqq->ioprio_class = IOPRIO_CLASS_BE;
		break;
	case IOPRIO_CLASS_RT:
		bfqq->ioprio = task_ioprio(ioc);
		bfqq->ioprio_class = IOPRIO_CLASS_RT;
		break;
	case IOPRIO_CLASS_BE:
		bfqq->ioprio = task_ioprio(ioc);
		bfqq->ioprio_class = IOPRIO_CLASS_BE;
		break;
	case IOPRIO_CLASS_IDLE:
		bfqq->ioprio_class = IOPRIO_CLASS_IDLE;
		bfqq->ioprio = 7;
		bfq_clear_bfqq_idle_window(bfqq);
		break;
	}

	/*
	 * keep track of original prio settings in case we have to temporarily
	 * elevate the priority of this queue
	 */
	bfqq->org_ioprio = bfqq->ioprio;
	bfqq->org_ioprio_class = bfqq->ioprio_class;
	bfq_clear_bfqq_prio_changed(bfqq);
}

static void changed_ioprio(struct io_context *ioc, struct cfq_io_context *cic)
{
	struct bfq_data *bfqd = cic->key;
	struct bfq_queue *bfqq;
	unsigned long flags;

	if (unlikely(bfqd == NULL))
		return;

	spin_lock_irqsave(bfqd->queue->queue_lock, flags);

	bfqq = cic->cfqq[ASYNC];
	if (bfqq != NULL) {
		struct bfq_queue *new_bfqq;
		new_bfqq = bfq_get_queue(bfqd, ASYNC, cic->ioc, GFP_ATOMIC);
		if (new_bfqq != NULL) {
			cic->cfqq[ASYNC] = new_bfqq;
			bfq_put_queue(bfqq);
		}
	}

	bfqq = cic->cfqq[SYNC];
	if (bfqq != NULL)
		bfq_mark_bfqq_prio_changed(bfqq);

	spin_unlock_irqrestore(bfqd->queue->queue_lock, flags);
}

static void bfq_ioc_set_ioprio(struct io_context *ioc)
{
	call_for_each_cic(ioc, changed_ioprio);
	ioc->ioprio_changed = 0;
}

static struct bfq_queue *
bfq_find_alloc_queue(struct bfq_data *bfqd, int is_sync,
		     struct io_context *ioc, gfp_t gfp_mask)
{
	struct bfq_queue *bfqq, *new_bfqq = NULL;
	struct cfq_io_context *cic;

retry:
	cic = bfq_cic_lookup(bfqd, ioc);
	/* cic always exists here */
	bfqq = cic_to_bfqq(cic, is_sync);

	if (bfqq == NULL) {
		if (new_bfqq != NULL) {
			bfqq = new_bfqq;
			new_bfqq = NULL;
		} else if (gfp_mask & __GFP_WAIT) {
			/*
			 * Inform the allocator of the fact that we will
			 * just repeat this allocation if it fails, to allow
			 * the allocator to do whatever it needs to attempt to
			 * free memory.
			 */
			spin_unlock_irq(bfqd->queue->queue_lock);
			new_bfqq = kmem_cache_alloc_node(bfq_pool,
					gfp_mask | __GFP_NOFAIL | __GFP_ZERO,
					bfqd->queue->node);
			spin_lock_irq(bfqd->queue->queue_lock);
			goto retry;
		} else {
			bfqq = kmem_cache_alloc_node(bfq_pool,
					gfp_mask | __GFP_ZERO,
					bfqd->queue->node);
			if (bfqq == NULL)
				goto out;
		}

		RB_CLEAR_NODE(&bfqq->rb_node);
		INIT_LIST_HEAD(&bfqq->fifo);

		atomic_set(&bfqq->ref, 0);
		bfqq->bfqd = bfqd;
		bfqq->budget = bfqd->bfq_max_budget;

		bfq_mark_bfqq_prio_changed(bfqq);

		bfq_init_prio_data(bfqq, ioc);
		bfqq->act_ioprio = bfqq->ioprio;
		bfqq->act_ioprio_class = bfqq->ioprio_class;
		bfqq->weight = bfq_ioprio_to_weight(bfqq);

		if (is_sync) {
			if (!bfq_class_idle(bfqq))
				bfq_mark_bfqq_idle_window(bfqq);
			bfq_mark_bfqq_sync(bfqq);
		}
	}

	if (new_bfqq != NULL)
		kmem_cache_free(bfq_pool, new_bfqq);

out:
	WARN_ON((gfp_mask & __GFP_WAIT) && bfqq == NULL);
	return bfqq;
}

static struct bfq_queue **
bfq_async_queue_prio(struct bfq_data *bfqd, int ioprio_class, int ioprio)
{
	switch (ioprio_class) {
	case IOPRIO_CLASS_RT:
		return &bfqd->async_bfqq[0][ioprio];
	case IOPRIO_CLASS_BE:
		return &bfqd->async_bfqq[1][ioprio];
	case IOPRIO_CLASS_IDLE:
		return &bfqd->async_idle_bfqq;
	default:
		BUG();
	}
}

static struct bfq_queue *
bfq_get_queue(struct bfq_data *bfqd, int is_sync, struct io_context *ioc,
	      gfp_t gfp_mask)
{
	const int ioprio = task_ioprio(ioc);
	const int ioprio_class = task_ioprio_class(ioc);
	struct bfq_queue **async_bfqq = NULL;
	struct bfq_queue *bfqq = NULL;

	if (!is_sync) {
		async_bfqq = bfq_async_queue_prio(bfqd, ioprio_class, ioprio);
		bfqq = *async_bfqq;
	}

	if (bfqq == NULL) {
		bfqq = bfq_find_alloc_queue(bfqd, is_sync, ioc, gfp_mask);
		if (bfqq == NULL)
			return NULL;
	}

	/*
	 * pin the queue now that it's allocated, scheduler exit will prune it
	 */
	if (!is_sync && *async_bfqq == NULL) {
		atomic_inc(&bfqq->ref);
		*async_bfqq = bfqq;
	}

	atomic_inc(&bfqq->ref);
	return bfqq;
}

/*
 * We drop cfq io contexts lazily, so we may find a dead one.
 */
static void
bfq_drop_dead_cic(struct bfq_data *bfqd, struct io_context *ioc,
		  struct cfq_io_context *cic)
{
	unsigned long flags;

	WARN_ON(!list_empty(&cic->queue_list));

	spin_lock_irqsave(&ioc->lock, flags);

	BUG_ON(ioc->ioc_data == cic);

	radix_tree_delete(&ioc->radix_root, (unsigned long)bfqd);
	hlist_del_rcu(&cic->cic_list);
	spin_unlock_irqrestore(&ioc->lock, flags);

	bfq_cic_free(cic);
}

static struct cfq_io_context *
bfq_cic_lookup(struct bfq_data *bfqd, struct io_context *ioc)
{
	struct cfq_io_context *cic;
	void *k;

	if (unlikely(ioc == NULL))
		return NULL;

	/*
	 * we maintain a last-hit cache, to avoid browsing over the tree
	 */
	cic = rcu_dereference(ioc->ioc_data);
	if (cic != NULL && cic->key == bfqd)
		return cic;

	do {
		rcu_read_lock();
		cic = radix_tree_lookup(&ioc->radix_root, (unsigned long)bfqd);
		rcu_read_unlock();
		if (cic == NULL)
			break;
		/* ->key must be copied to avoid race with bfq_exit_queue() */
		k = cic->key;
		if (unlikely(k == NULL)) {
			bfq_drop_dead_cic(bfqd, ioc, cic);
			continue;
		}

		rcu_assign_pointer(ioc->ioc_data, cic);
		break;
	} while (1);

	return cic;
}

/*
 * Add cic into ioc, using bfqd as the search key. This enables us to lookup
 * the process specific cfq io context when entered from the block layer.
 * Also adds the cic to a per-bfqd list, used when this queue is removed.
 */
static int bfq_cic_link(struct bfq_data *bfqd, struct io_context *ioc,
			struct cfq_io_context *cic, gfp_t gfp_mask)
{
	unsigned long flags;
	int ret;

	ret = radix_tree_preload(gfp_mask);
	if (ret == 0) {
		cic->ioc = ioc;
		cic->key = bfqd;

		spin_lock_irqsave(&ioc->lock, flags);
		ret = radix_tree_insert(&ioc->radix_root,
					(unsigned long)bfqd, cic);
		if (ret == 0)
			hlist_add_head_rcu(&cic->cic_list, &ioc->bfq_cic_list);
		spin_unlock_irqrestore(&ioc->lock, flags);

		radix_tree_preload_end();

		if (ret == 0) {
			spin_lock_irqsave(bfqd->queue->queue_lock, flags);
			list_add(&cic->queue_list, &bfqd->cic_list);
			spin_unlock_irqrestore(bfqd->queue->queue_lock, flags);
		}
	}

	if (ret != 0)
		printk(KERN_ERR "bfq: cic link failed!\n");

	return ret;
}

/*
 * Setup general io context and cfq io context. There can be several cfq
 * io contexts per general io context, if this process is doing io to more
 * than one device managed by cfq.
 * Since bfq uses the same io contexts as cfq, we use the same tree to store
 * either cfq and bfq contexts; the lookup is done using a bfqd/bfqd key, so
 * we cannot have clashes and the key identifies the scheduler type too.
 */
static struct cfq_io_context *
bfq_get_io_context(struct bfq_data *bfqd, gfp_t gfp_mask)
{
	struct io_context *ioc = NULL;
	struct cfq_io_context *cic;

	might_sleep_if(gfp_mask & __GFP_WAIT);

	ioc = get_io_context(gfp_mask, bfqd->queue->node);
	if (ioc == NULL)
		return NULL;

	cic = bfq_cic_lookup(bfqd, ioc);
	if (cic != NULL)
		goto out;

	cic = bfq_alloc_io_context(bfqd, gfp_mask);
	if (cic == NULL)
		goto err;

	if (bfq_cic_link(bfqd, ioc, cic, gfp_mask) != 0)
		goto err_free;

out:
	if (unlikely(ioc->ioprio_changed)) {
		/* pairs with wmb() in set_task_ioprio() in fs/ioprio.c */
		rmb();
		bfq_ioc_set_ioprio(ioc);
	}

	return cic;
err_free:
	bfq_cic_free(cic);
err:
	put_io_context(ioc);
	return NULL;
}

static void
bfq_update_io_thinktime(struct bfq_data *bfqd, struct cfq_io_context *cic)
{
	unsigned long elapsed = jiffies - cic->last_end_request;
	unsigned long ttime = min(elapsed, 2UL * bfqd->bfq_slice_idle);

	cic->ttime_samples = (7*cic->ttime_samples + 256) / 8;
	cic->ttime_total = (7*cic->ttime_total + 256*ttime) / 8;
	cic->ttime_mean = (cic->ttime_total + 128) / cic->ttime_samples;
}

static void
bfq_update_io_seektime(struct bfq_data *bfqd, struct cfq_io_context *cic,
		       struct request *rq)
{
	sector_t sdist;
	u64 total;

	if (cic->last_request_pos < rq->sector)
		sdist = rq->sector - cic->last_request_pos;
	else
		sdist = cic->last_request_pos - rq->sector;

	/*
	 * Don't allow the seek distance to get too large from the
	 * odd fragment, pagein, etc
	 */
	if (cic->seek_samples <= 60) /* second&third seek */
		sdist = min(sdist, (cic->seek_mean * 4) + 2*1024*1024);
	else
		sdist = min(sdist, (cic->seek_mean * 4)	+ 2*1024*64);

	cic->seek_samples = (7*cic->seek_samples + 256) / 8;
	cic->seek_total = (7*cic->seek_total + (u64)256*sdist) / 8;
	total = cic->seek_total + (cic->seek_samples/2);
	do_div(total, cic->seek_samples);
	cic->seek_mean = (sector_t)total;
}

/*
 * Disable idle window if the process thinks too long or seeks so much that
 * it doesn't matter
 */
static void
bfq_update_idle_window(struct bfq_data *bfqd, struct bfq_queue *bfqq,
		       struct cfq_io_context *cic)
{
	int enable_idle;

	/*
	 * Don't idle for async or idle io prio class
	 */
	if (!bfq_bfqq_sync(bfqq) || bfq_class_idle(bfqq))
		return;

	enable_idle = bfq_bfqq_idle_window(bfqq);

	if (atomic_read(&cic->ioc->nr_tasks) == 0 ||
	    bfqd->bfq_slice_idle == 0 || (bfqd->hw_tag && CIC_SEEKY(cic)))
		enable_idle = 0;
	else if (sample_valid(cic->ttime_samples)) {
		if (cic->ttime_mean > bfqd->bfq_slice_idle)
			enable_idle = 0;
		else
			enable_idle = 1;
	}

	if (enable_idle)
		bfq_mark_bfqq_idle_window(bfqq);
	else
		bfq_clear_bfqq_idle_window(bfqq);
}

/*
 * Called when a new fs request (rq) is added (to bfqq). Check if there's
 * something we should do about it
 */
static void
bfq_rq_enqueued(struct bfq_data *bfqd, struct bfq_queue *bfqq,
		struct request *rq)
{
	struct cfq_io_context *cic = RQ_CIC(rq);

	bfqd->rq_queued++;
	if (rq_is_meta(rq))
		bfqq->meta_pending++;

	bfq_update_io_thinktime(bfqd, cic);
	bfq_update_io_seektime(bfqd, cic, rq);
	bfq_update_idle_window(bfqd, bfqq, cic);

	cic->last_request_pos = rq->sector + rq->nr_sectors;

	if (bfqq == bfqd->active_queue && bfq_bfqq_wait_request(bfqq)) {
		/*
		 * If we are waiting for a request for this queue, let it rip
		 * immediately and flag that we must not expire this queue
		 * just now.
		 */
		bfq_clear_bfqq_wait_request(bfqq);
		del_timer(&bfqd->idle_slice_timer);
		blk_start_queueing(bfqd->queue);
	}
}

static void bfq_insert_request(struct request_queue *q, struct request *rq)
{
	struct bfq_data *bfqd = q->elevator->elevator_data;
	struct bfq_queue *bfqq = RQ_BFQQ(rq);

	bfq_init_prio_data(bfqq, RQ_CIC(rq)->ioc);

	bfq_add_rq_rb(rq);

	list_add_tail(&rq->queuelist, &bfqq->fifo);

	bfq_rq_enqueued(bfqd, bfqq, rq);
}

/*
 * Update hw_tag based on peak queue depth over 50 samples under
 * sufficient load.
 */
static void bfq_update_hw_tag(struct bfq_data *bfqd)
{
	if (bfqd->rq_in_driver > bfqd->rq_in_driver_peak)
		bfqd->rq_in_driver_peak = bfqd->rq_in_driver;

	if (bfqd->rq_queued <= BFQ_HW_QUEUE_MIN &&
	    bfqd->rq_in_driver <= BFQ_HW_QUEUE_MIN)
		return;

	if (bfqd->hw_tag_samples++ < 50)
		return;

	if (bfqd->rq_in_driver_peak >= BFQ_HW_QUEUE_MIN)
		bfqd->hw_tag = 1;
	else
		bfqd->hw_tag = 0;

	bfqd->hw_tag_samples = 0;
	bfqd->rq_in_driver_peak = 0;
}

static void bfq_completed_request(struct request_queue *q, struct request *rq)
{
	struct bfq_queue *bfqq = RQ_BFQQ(rq);
	struct bfq_data *bfqd = bfqq->bfqd;
	const int sync = rq_is_sync(rq);
	unsigned long now;

	now = jiffies;

	bfq_update_hw_tag(bfqd);

	WARN_ON(!bfqd->rq_in_driver);
	WARN_ON(!bfqq->dispatched);
	bfqd->rq_in_driver--;
	bfqq->dispatched--;

	if (bfq_bfqq_sync(bfqq))
		bfqd->sync_flight--;

	if (sync)
		RQ_CIC(rq)->last_end_request = now;

	/*
	 * If this is the active queue, check if it needs to be expired,
	 * or if we want to idle in case it has no pending requests.
	 */
	if (bfqd->active_queue == bfqq && sync &&
	    RB_EMPTY_ROOT(&bfqq->sort_list))
		bfq_arm_slice_timer(bfqd);

	if (!bfqd->rq_in_driver)
		bfq_schedule_dispatch(bfqd);
}

/*
 * we temporarily boost lower priority queues if they are holding fs exclusive
 * resources. they are boosted to normal prio (CLASS_BE/4)
 */
static void bfq_prio_boost(struct bfq_queue *bfqq)
{
	if (has_fs_excl()) {
		/*
		 * boost idle prio on transactions that would lock out other
		 * users of the filesystem
		 */
		if (bfq_class_idle(bfqq))
			bfqq->ioprio_class = IOPRIO_CLASS_BE;
		if (bfqq->ioprio > IOPRIO_NORM)
			bfqq->ioprio = IOPRIO_NORM;
	} else {
		/*
		 * check if we need to unboost the queue
		 */
		if (bfqq->ioprio_class != bfqq->org_ioprio_class)
			bfqq->ioprio_class = bfqq->org_ioprio_class;
		if (bfqq->ioprio != bfqq->org_ioprio)
			bfqq->ioprio = bfqq->org_ioprio;
	}
}

static inline int __bfq_may_queue(struct bfq_queue *bfqq)
{
	if (bfq_bfqq_wait_request(bfqq) && bfq_bfqq_must_alloc(bfqq)) {
		bfq_clear_bfqq_must_alloc(bfqq);
		return ELV_MQUEUE_MUST;
	}

	return ELV_MQUEUE_MAY;
}

static int bfq_may_queue(struct request_queue *q, int rw)
{
	struct bfq_data *bfqd = q->elevator->elevator_data;
	struct task_struct *tsk = current;
	struct cfq_io_context *cic;
	struct bfq_queue *bfqq;

	/*
	 * don't force setup of a queue from here, as a call to may_queue
	 * does not necessarily imply that a request actually will be queued.
	 * so just lookup a possibly existing queue, or return 'may queue'
	 * if that fails
	 */
	cic = bfq_cic_lookup(bfqd, tsk->io_context);
	if (cic == NULL)
		return ELV_MQUEUE_MAY;

	bfqq = cic_to_bfqq(cic, rw & REQ_RW_SYNC);
	if (bfqq != NULL) {
		bfq_init_prio_data(bfqq, cic->ioc);
		bfq_prio_boost(bfqq);

		return __bfq_may_queue(bfqq);
	}

	return ELV_MQUEUE_MAY;
}

/*
 * queue lock held here
 */
static void bfq_put_request(struct request *rq)
{
	struct bfq_queue *bfqq = RQ_BFQQ(rq);

	if (bfqq != NULL) {
		const int rw = rq_data_dir(rq);

		BUG_ON(!bfqq->allocated[rw]);
		bfqq->allocated[rw]--;

		put_io_context(RQ_CIC(rq)->ioc);

		rq->elevator_private = NULL;
		rq->elevator_private2 = NULL;

		bfq_put_queue(bfqq);
	}
}

/*
 * Allocate cfq data structures associated with this request.
 */
static int
bfq_set_request(struct request_queue *q, struct request *rq, gfp_t gfp_mask)
{
	struct bfq_data *bfqd = q->elevator->elevator_data;
	struct cfq_io_context *cic;
	const int rw = rq_data_dir(rq);
	const int is_sync = rq_is_sync(rq);
	struct bfq_queue *bfqq;
	unsigned long flags;

	might_sleep_if(gfp_mask & __GFP_WAIT);

	cic = bfq_get_io_context(bfqd, gfp_mask);

	spin_lock_irqsave(q->queue_lock, flags);

	if (cic == NULL)
		goto queue_fail;

	bfqq = cic_to_bfqq(cic, is_sync);
	if (bfqq == NULL) {
		bfqq = bfq_get_queue(bfqd, is_sync, cic->ioc, gfp_mask);

		if (bfqq == NULL)
			goto queue_fail;

		cic_set_bfqq(cic, bfqq, is_sync);
	}

	bfqq->allocated[rw]++;
	atomic_inc(&bfqq->ref);

	spin_unlock_irqrestore(q->queue_lock, flags);

	rq->elevator_private = cic;
	rq->elevator_private2 = bfqq;

	return 0;

queue_fail:
	if (cic != NULL)
		put_io_context(cic->ioc);

	bfq_schedule_dispatch(bfqd);
	spin_unlock_irqrestore(q->queue_lock, flags);

	return 1;
}

static void bfq_kick_queue(struct work_struct *work)
{
	struct bfq_data *bfqd =
		container_of(work, struct bfq_data, unplug_work);
	struct request_queue *q = bfqd->queue;
	unsigned long flags;

	spin_lock_irqsave(q->queue_lock, flags);
	blk_start_queueing(q);
	spin_unlock_irqrestore(q->queue_lock, flags);
}

/*
 * Timer running if the active_queue is currently idling inside its time slice
 */
static void bfq_idle_slice_timer(unsigned long data)
{
	struct bfq_data *bfqd = (struct bfq_data *)data;
	struct bfq_queue *bfqq;
	unsigned long flags;

	spin_lock_irqsave(bfqd->queue->queue_lock, flags);

	bfqq = bfqd->active_queue;
	/*
	 * Theoretical race here: active_queue can be NULL or different
	 * from the queue that was idling if the timer handler spins on
	 * the queue_lock and a new request arrives for the current
	 * queue and there is a full dispatch cycle that changes the
	 * active_queue.  This can hardly happen, but in the worst case
	 * we just expire a queue too early.
	 */
	if (bfqq != NULL && bfq_bfqq_wait_request(bfqq)) {
		BUG_ON(!RB_EMPTY_ROOT(&bfqq->sort_list));

		bfq_bfqq_expire(bfqd, bfqq, 1);
		bfq_schedule_dispatch(bfqd);
	}

	BUG_ON(bfqq == NULL && bfqd->busy_queues != 0);

	spin_unlock_irqrestore(bfqd->queue->queue_lock, flags);
}

static void bfq_shutdown_timer_wq(struct bfq_data *bfqd)
{
	del_timer_sync(&bfqd->idle_slice_timer);
	kblockd_flush_work(&bfqd->unplug_work);
}

static void bfq_put_async_queues(struct bfq_data *bfqd)
{
	int i;

	for (i = 0; i < IOPRIO_BE_NR; i++) {
		if (bfqd->async_bfqq[0][i] != NULL)
			bfq_put_queue(bfqd->async_bfqq[0][i]);
		if (bfqd->async_bfqq[1][i] != NULL)
			bfq_put_queue(bfqd->async_bfqq[1][i]);
	}

	if (bfqd->async_idle_bfqq != NULL)
		bfq_put_queue(bfqd->async_idle_bfqq);
}

static void bfq_exit_queue(elevator_t *e)
{
	struct bfq_data *bfqd = e->elevator_data;
	struct request_queue *q = bfqd->queue;
	struct bfq_wfqdata *wfqd;
	struct bfq_queue *bfqq, *next;
	int i;

	bfq_shutdown_timer_wq(bfqd);

	spin_lock_irq(q->queue_lock);

	while (!list_empty(&bfqd->cic_list)) {
		struct cfq_io_context *cic = list_entry(bfqd->cic_list.next,
							struct cfq_io_context,
							queue_list);

		__bfq_exit_single_io_context(bfqd, cic);
	}

	bfq_put_async_queues(bfqd);

	BUG_ON(bfqd->active_queue != NULL);
	for (i = 0; i < BFQ_IOPRIO_CLASSES; i++) {
		wfqd = &bfqd->service_tree[i];
		BUG_ON(!RB_EMPTY_ROOT(&wfqd->active));
		bfqq = wfqd->first_idle;
		while (bfqq != NULL) {
			next = bfq_bfqq_of(rb_next(&bfqq->rb_node));
			bfq_put_idle_queue(wfqd, bfqq);
			bfqq = next;
		}
	}
	spin_unlock_irq(q->queue_lock);

	bfq_shutdown_timer_wq(bfqd);

	kfree(bfqd);
}

static void *bfq_init_queue(struct request_queue *q)
{
	struct bfq_data *bfqd;
	int i;

	bfqd = kmalloc_node(sizeof(*bfqd), GFP_KERNEL | __GFP_ZERO, q->node);
	if (bfqd == NULL)
		return NULL;

	INIT_LIST_HEAD(&bfqd->cic_list);

	bfqd->queue = q;

	for (i = 0; i < BFQ_IOPRIO_CLASSES; i++)
		bfqd->service_tree[i] = BFQ_WFQDATA_INIT;

	init_timer(&bfqd->idle_slice_timer);
	bfqd->idle_slice_timer.function = bfq_idle_slice_timer;
	bfqd->idle_slice_timer.data = (unsigned long)bfqd;

	INIT_WORK(&bfqd->unplug_work, bfq_kick_queue);

	bfqd->bfq_quantum = bfq_quantum;
	bfqd->bfq_fifo_expire[0] = bfq_fifo_expire[0];
	bfqd->bfq_fifo_expire[1] = bfq_fifo_expire[1];
	bfqd->bfq_back_max = bfq_back_max;
	bfqd->bfq_back_penalty = bfq_back_penalty;
	bfqd->bfq_slice_async_rq = bfq_slice_async_rq;
	bfqd->bfq_slice_idle = bfq_slice_idle;
	bfqd->bfq_max_budget = bfq_max_budget;
	bfqd->hw_tag = 1;

	return bfqd;
}

static void bfq_slab_kill(void)
{
	if (bfq_pool != NULL)
		kmem_cache_destroy(bfq_pool);
	if (bfq_ioc_pool != NULL)
		kmem_cache_destroy(bfq_ioc_pool);
}

static int __init bfq_slab_setup(void)
{
	bfq_pool = KMEM_CACHE(bfq_queue, 0);
	if (bfq_pool == NULL)
		goto fail;

	bfq_ioc_pool = kmem_cache_create("bfq_io_context",
					 sizeof(struct cfq_io_context),
					 __alignof__(struct cfq_io_context),
					 0, NULL);
	if (bfq_ioc_pool == NULL)
		goto fail;

	return 0;
fail:
	bfq_slab_kill();
	return -ENOMEM;
}

/*
 * sysfs parts below -->
 */
static ssize_t
bfq_var_show(unsigned int var, char *page)
{
	return sprintf(page, "%d\n", var);
}

static ssize_t
bfq_var_store(unsigned int *var, const char *page, size_t count)
{
	char *p = (char *)page;

	*var = simple_strtoul(p, &p, 10);
	return count;
}

#define SHOW_FUNCTION(__FUNC, __VAR, __CONV)				\
static ssize_t __FUNC(elevator_t *e, char *page)			\
{									\
	struct bfq_data *bfqd = e->elevator_data;			\
	unsigned int __data = __VAR;					\
	if (__CONV)							\
		__data = jiffies_to_msecs(__data);			\
	return bfq_var_show(__data, (page));				\
}
SHOW_FUNCTION(bfq_quantum_show, bfqd->bfq_quantum, 0);
SHOW_FUNCTION(bfq_fifo_expire_sync_show, bfqd->bfq_fifo_expire[1], 1);
SHOW_FUNCTION(bfq_fifo_expire_async_show, bfqd->bfq_fifo_expire[0], 1);
SHOW_FUNCTION(bfq_back_seek_max_show, bfqd->bfq_back_max, 0);
SHOW_FUNCTION(bfq_back_seek_penalty_show, bfqd->bfq_back_penalty, 0);
SHOW_FUNCTION(bfq_slice_idle_show, bfqd->bfq_slice_idle, 1);
SHOW_FUNCTION(bfq_slice_async_rq_show, bfqd->bfq_slice_async_rq, 0);
SHOW_FUNCTION(bfq_max_budget_show, bfqd->bfq_max_budget, 0);
#undef SHOW_FUNCTION

#define STORE_FUNCTION(__FUNC, __PTR, MIN, MAX, __CONV)			\
static ssize_t __FUNC(elevator_t *e, const char *page, size_t count)	\
{									\
	struct bfq_data *bfqd = e->elevator_data;			\
	unsigned int __data;						\
	int ret = bfq_var_store(&__data, (page), count);		\
	if (__data < (MIN))						\
		__data = (MIN);						\
	else if (__data > (MAX))					\
		__data = (MAX);						\
	if (__CONV)							\
		*(__PTR) = msecs_to_jiffies(__data);			\
	else								\
		*(__PTR) = __data;					\
	return ret;							\
}
STORE_FUNCTION(bfq_quantum_store, &bfqd->bfq_quantum, 1, UINT_MAX, 0);
STORE_FUNCTION(bfq_fifo_expire_sync_store, &bfqd->bfq_fifo_expire[1], 1,
		UINT_MAX, 1);
STORE_FUNCTION(bfq_fifo_expire_async_store, &bfqd->bfq_fifo_expire[0], 1,
		UINT_MAX, 1);
STORE_FUNCTION(bfq_back_seek_max_store, &bfqd->bfq_back_max, 0, UINT_MAX, 0);
STORE_FUNCTION(bfq_back_seek_penalty_store, &bfqd->bfq_back_penalty, 1,
		UINT_MAX, 0);
STORE_FUNCTION(bfq_slice_idle_store, &bfqd->bfq_slice_idle, 0, UINT_MAX, 1);
STORE_FUNCTION(bfq_slice_async_rq_store, &bfqd->bfq_slice_async_rq, 1,
		UINT_MAX, 0);
STORE_FUNCTION(bfq_max_budget_store, &bfqd->bfq_max_budget, 0, UINT_MAX, 0);
#undef STORE_FUNCTION

#define BFQ_ATTR(name) \
	__ATTR(name, S_IRUGO|S_IWUSR, bfq_##name##_show, bfq_##name##_store)

static struct elv_fs_entry bfq_attrs[] = {
	BFQ_ATTR(quantum),
	BFQ_ATTR(fifo_expire_sync),
	BFQ_ATTR(fifo_expire_async),
	BFQ_ATTR(back_seek_max),
	BFQ_ATTR(back_seek_penalty),
	BFQ_ATTR(slice_async_rq),
	BFQ_ATTR(slice_idle),
	BFQ_ATTR(max_budget),
	__ATTR_NULL
};

static struct elevator_type iosched_bfq = {
	.ops = {
		.elevator_merge_fn = 		bfq_merge,
		.elevator_merged_fn =		bfq_merged_request,
		.elevator_merge_req_fn =	bfq_merged_requests,
		.elevator_allow_merge_fn =	bfq_allow_merge,
		.elevator_dispatch_fn =		bfq_dispatch_requests,
		.elevator_add_req_fn =		bfq_insert_request,
		.elevator_activate_req_fn =	bfq_activate_request,
		.elevator_deactivate_req_fn =	bfq_deactivate_request,
		.elevator_queue_empty_fn =	bfq_queue_empty,
		.elevator_completed_req_fn =	bfq_completed_request,
		.elevator_former_req_fn =	elv_rb_former_request,
		.elevator_latter_req_fn =	elv_rb_latter_request,
		.elevator_set_req_fn =		bfq_set_request,
		.elevator_put_req_fn =		bfq_put_request,
		.elevator_may_queue_fn =	bfq_may_queue,
		.elevator_init_fn =		bfq_init_queue,
		.elevator_exit_fn =		bfq_exit_queue,
		.trim =				bfq_free_io_context,
	},
	.elevator_attrs =	bfq_attrs,
	.elevator_name =	"bfq",
	.elevator_owner =	THIS_MODULE,
};

static int __init bfq_init(void)
{
	/*
	 * could be 0 on HZ < 1000 setups
	 */
	if (bfq_slice_idle == 0)
		bfq_slice_idle = 1;

	if (bfq_slab_setup())
		return -ENOMEM;

	elv_register(&iosched_bfq);

	return 0;
}

static void __exit bfq_exit(void)
{
	DECLARE_COMPLETION_ONSTACK(all_gone);
	elv_unregister(&iosched_bfq);
	ioc_gone = &all_gone;
	/* ioc_gone's update must be visible before reading ioc_count */
	smp_wmb();
	if (elv_ioc_count_read(ioc_count) != 0)
		wait_for_completion(ioc_gone);
	bfq_slab_kill();
}

module_init(bfq_init);
module_exit(bfq_exit);

MODULE_AUTHOR("Fabio Checconi, Paolo Valente");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Budget Fair Queueing IO scheduler");
