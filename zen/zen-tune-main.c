/**
 * main.c
 * zen-tune
 *
 * Copyright (C) 2008 Ryan Hope <rmh3093@gmail.com>
 *
 * Released under the GPL version 2 only.
 *
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

#include "sysfs.h"

static int zen_sysfs_init(void)
{
	int ret;

	ret = init_sysfs_interface();
	if (ret)
		goto out_error;

        return 0;

 out_error:
	return ret;
}

static void zen_sysfs_exit(void)
{
	cleanup_sysfs_interface();
}

module_init(zen_sysfs_init);
module_exit(zen_sysfs_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ryan Hope <rmh3093@gmail.com>");
